# MagicWand - Little Helpers for MagicDraw UML #

This project develops a family of plugins for the MagicDraw UML modeling tool that provide small but useful utilities, such as review annotations, prose descriptions of model elements, configurable quantitative information of individual model elements, and code annotations.

### Parts and Componente ###

Currently, MagicWand has four components

* The reporting templates and report editing tab,
* the quantitative attributes tab,
* the code annotations tab, and 
* the review comments tab.

Each can be used without considering the others, but since they have a very similar technological basis, they are deployed together.

### Deployment and Installation###

MagicWand consists of four plugins, a set of report templates, documentation, and samples. The installable part is a zipped folder that should be unzipped into the "plugins" directory inside the MD installation's bin directory. The current stable version (0.2) is available in the "Downloads" section of this repository. The current development version (0.3) is reflected in the SRC folder.

MagicWand comes without any kind of warranty. Surprise!



Good to know: [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)