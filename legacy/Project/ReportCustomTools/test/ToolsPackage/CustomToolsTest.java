/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package ToolsPackage;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Maggi
 */
public class CustomToolsTest {

    public CustomToolsTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of showElement method, of class CustomTools.
     */
    @Test
    public void testHideElement() {
        System.out.println("showElement");
        //String documentation = "sdfasdf<show></show>";
        String documentation =        
        "<doc hide=\"true\" > " +
            "<intro>" +
            "<h>introduction text header</h>" +
            "<p>paragraph 1 of introduction text" +
            "</p>" +
                "<p>paragraph 2 of introduction text" +
            "</p>" +
                "<p>paragraph 3 of introduction text" +
            "</p>" +
            "<ul>" +
                "list" +
                "<li> </li>" +
            "</ul>" +
            "</intro>" +
        "</doc>"; 
        CustomTools instance = new CustomTools();
        Boolean expResult = true;
        Boolean result = instance.hideElement(documentation);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of getItem method, of class CustomTools.
     */
    @Test
    public void testGetItem() {
        System.out.println("getItem");
        
        String documentation =
        "<doc hide=\"true\" > " +
            "<intro>" +
            "<h>introduction text header</h>" +
            "<p>paragraph 1 of introduction text</p>" +
            "<p>paragraph 2 of introduction text</p>" +
            "<p>paragraph 3 of introduction text</p>" +
            //"<ul>" +
                //"list" +
                //"<li>list item 1 </li>" +
                //"<li>list item 2 </li>" +
                //"<li>list item 3 </li>" +
                //"<li>list item 4 </li>" +
            //"</ul>" +
            "<p>paragraph after list</p>" +
            "</intro>" +
            "<body caption=\"body caption text\" />" +
            "<table caption=\"table caption text\" />" +
            "<diagram caption=\"diagram caption text\" />" +
        "</doc>";
         /*
        String documentation =
        "dfg<doc hide=\"true\" > </doc>";
          */
        String type = "intro";
        CustomTools instance = new CustomTools();
        String expResult = "introduction text";
        String result = instance.getItem(documentation, type, "");
        System.out.println("result:" + result + ":result");
        
        result = instance.getItem(documentation, "table", "");
        System.out.println("table:" + result + ":table");
        result = instance.getItem(documentation, "intro", "h");
        System.out.println("intro:" + result + ":intro");
        result = instance.getItem(documentation, "main", "h");
        System.out.println("main:" + result + ":main");
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
   assertEquals(expResult, result);
    }

    /*
        @Test
    public void testClean() {
        System.out.println("clean doc");
        String documentation = " khjkjkhkj" +
        "<doc hide=\"true\" > " +
            "<p>introduction text" +
            "<ul>" +
                "<li> </li>" +
            "</ul>" +
            "</p>" +
            "<h>introduction text header</h>" +
            "<text>main text</text>" +
            "<text>main text</text>" +
            "<text>main text</text>" +
            "<text>main text</text>" +
            "<body caption=\"sdfjhsdfdyshbu\" />" +
            "<table caption=\"sdfjhsdfdyshbu\" />" +
            "<diagram caption=\"sdfjhsdfdyshbu\" />" +
            "<text>afterword</text>" +
        "</doc>" + "asdfasdfasdf";
        CustomTools instance = new CustomTools();
        String expResult = "";
        String result = instance.cleanDoc(documentation);
        System.out.println("Result" + result);
        assertEquals(expResult, result);
        
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    } */

}