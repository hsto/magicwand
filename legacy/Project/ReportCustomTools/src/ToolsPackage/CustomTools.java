/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ToolsPackage;

import com.nomagic.magicreport.engine.Tool;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.StringReader;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.*;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

/**
 *
 * @author Maggi
 */
public class CustomTools extends Tool {

    public Boolean hideElement(String documentation) {
        
        PrintWriter out = null;
        try {
            out = new PrintWriter("reportlog.txt");
            out.println("start");
        } catch (FileNotFoundException ex) {
            Logger.getLogger(CustomTools.class.getName()).log(Level.SEVERE, null, ex);
        }
        out.println("documentation: " + documentation);
        
        String strdoc = cleanDoc(documentation);
        out.println("strdoc: " + strdoc);
        if (!strdoc.isEmpty()) {
            try {
                out.println("strdoc: " + strdoc);
                DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
                DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
                Document doc = dBuilder.parse(new InputSource(new StringReader(strdoc)));
                doc.getDocumentElement().normalize();

                NodeList nList = doc.getElementsByTagName("doc");

                if (nList.item(0) == null || nList.item(0).getNodeType() != Node.ELEMENT_NODE) {
                    out.close();
                    return false;
                } else {
                    Element e = (Element) nList.item(0);
                    out.println("hide: " + e.getAttribute("hide"));
                    if (e.getAttribute("hide").compareToIgnoreCase("true") == 0) {
                        out.close();
                        return true;
                    } else {
                        out.close();
                        return false;
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
                out.close();
                return false;
            }
        } else {
            out.close();
            return false;
        }
    }

    public String getItem(String documentation, String section, String type) {
        String strdoc = cleanDoc(documentation);
        if (!strdoc.isEmpty()) {
            try {
                DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
                DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
                Document doc = dBuilder.parse(new InputSource(new StringReader(strdoc)));
                doc.getDocumentElement().normalize();
                NodeList nList = doc.getElementsByTagName(section);
                if (nList != null && nList.getLength() > 0) {
                    if (section.equalsIgnoreCase("intro") ||
                            section.equalsIgnoreCase("main") ||
                            section.equalsIgnoreCase("afterword")) {
                        NodeList nList2 = nList.item(0).getChildNodes();
                        String s = "";
                        for (int i = 0; i < nList2.getLength(); i++) {
                            if (type.equalsIgnoreCase("h") && nList2.item(i).getNodeName().equalsIgnoreCase("h")) {
                                s = nList2.item(i).getTextContent();
                                // if (s != "")
                                /*if (s != null && !s.isEmpty())
                                    s = s + "\n";*/
                            }
                            if (type.equalsIgnoreCase("") && nList2.item(i).getNodeName().equalsIgnoreCase("p")) {
                                s = s + nList2.item(i).getTextContent();
                                // if (s != "")
                                if (s != null && !s.isEmpty())
                                    s = s + "\n";
                            }
                            if (type.equalsIgnoreCase("") && nList2.item(i).getNodeName().equalsIgnoreCase("ul")) {
                                NodeList nList3 = nList2.item(i).getChildNodes();
                                for (int i2 = 0; i2 < nList3.getLength(); i2++) {
                                    if (nList3.item(i2).getNodeName().equalsIgnoreCase("li") && !(nList3.item(i2).getTextContent().isEmpty())) {
                                        //int t = i2 + 1;
                                        // = s + "  " + t + ". " + nList3.item(i2).getTextContent() + "\n";
                                        s = s + nList3.item(i2).getTextContent();
                                        if (nList3.getLength() != i2 && nList3.item(i2).getTextContent() != "" )
                                          s = s + "\n";   
                                        /*if (nList3.item(i2).getTextContent() != "")
                                            s = s + "\n";*/
                                    }
                                }
                            }
                        }
                        return s;
                    } else if (section.equalsIgnoreCase("body") ||
                            section.equalsIgnoreCase("table") ||
                            section.equalsIgnoreCase("diagram")) {
                        Element e = (Element) nList.item(0);
                        return e.getAttribute("caption");
                    } else {
                        return "";
                    }

                } else {
                    return "";
                }
            } catch (Exception e) {
                e.printStackTrace();
                return "";
            }

        } else {
            return "";
        }
    }

    private String cleanDoc(String doc) {
        PrintWriter out = null;
        try {
            out = new PrintWriter("report_clean_log.txt");
            //out = new PrintWriter("C:/tmp/report_clean_log.txt");

        } catch (FileNotFoundException ex) {
            Logger.getLogger(CustomTools.class.getName()).log(Level.SEVERE, null, ex);
        }

        Matcher matcher = null;
        try {
            Pattern pattern = Pattern.compile("(?sm)(.*)(<doc)(.*)(/doc>)(.*)");
            matcher = pattern.matcher(doc);

            if (matcher.find()) {
                return matcher.group(2) + matcher.group(3) + matcher.group(4);
            } else {
                out.println("doc: " + doc);
                out.close();
                return "";
            }
        } catch (Exception e) {
            e.printStackTrace();
            out.println(e.getMessage());
            out.close();
            return "";
        }

    }
}
