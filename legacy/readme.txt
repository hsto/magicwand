Magnus Magnusson s104893
This delivery contains my thesis and related material.

"readmy.txt" (this document)
�MSc � Magnus Magnusson  (s104893).pdf�	The thesis as such (this document), as submitted to the IMM librarian for print
Installation
�ReportCustomTools.jar�
�ReportPluginPackage.rar�
�Templates.rar�
ReportPluginPackage
�plugin.xml�
�ReportPlugin.jar�
Templates
"Analysis Report.rtf"	
"appendixA.rtf"
"Business Processes.rtf"	
"Functions and Processes.rtf"	
"Information Models.rtf"
"macros.rtf"
"Model Overview.rtf"	
"System Structure Model.rtf"	Installation folder, see chapter Installation instructions of this document
Thesis	
�MSc � Magnus Magnusson  (s104893).docx� 	This document in Word format (docx) 
Project
ReportCustomTools
ReportPlugin	Source code for the custom tool and the plug-in
Demo project and report
�s3 analysis model sample [lms_2011] + annotations.mdxml�
�s3 analysis model sample [lms_2011] + annotations.rtf�
�s3 analysis model sample [lms_2011] + annotations.pdf�	Sample MagicDraw project with Documentation, generated report in rtf format and in pdf format with Table of Content
Material
�emmerichLiterateModelling.pdf�
�MagicDraw ReportWizard Template Creation Tutorial.pdf�
�MagicDraw ReportWizard UserGuide 17.0.03.pdf�
�MagicDraw UserManual.pdf�
�MagicDraw_OpenAPI_UserGuide_1701.pdf�
�no_magic_quick_reference_guide_uml.pdf�	Documents I refer to in this document
