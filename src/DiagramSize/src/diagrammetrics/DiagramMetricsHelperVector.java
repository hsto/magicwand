package diagrammetrics;

import java.awt.Point;
import java.util.ArrayList;
import java.util.List;

import com.nomagic.magicdraw.uml.symbols.paths.PathElement;
import diagrammetrics.model.CountElement;


public enum DiagramMetricsHelperVector {
	INSTANCE;

	public Point getVectorSum(List<PathElement> pathElements) {
		List<Point> vectors = new ArrayList<>();
		
		for (PathElement pathElement : pathElements) {
			int dx, dy;
			List<Point> points = pathElement.getAllBreakPoints();
			
			int indexA, indexB;
			if (pathElement.getHumanType().equals("Generalization")) {					
				indexA = 0;
				indexB = points.size() - 1;				
			} else {
				indexA = points.size() - 1;
				indexB = 0;
			}
			
//			if (/*pathElement.getHumanType().equals("Generalization") || */
//					(pathElement.getHumanType().equals("Association") && (pathElement.getSupplier().getHumanType().equals("Actor") || pathElement.getClient().getHumanType().equals("Actor"))) ) {
//				indexA = points.size() - 1;
//				indexB = 0;
//			} else {
//				indexA = 0;
//				indexB = points.size() - 1;
//			}
			
			dx = points.get(indexB).x - points.get(indexA).x;
			dy = (-points.get(indexB).y) - (-points.get(indexA).y);
			
			vectors.add( new Point(dx, dy) );	
		}
		
		return getTotalVectorSum(vectors);
		
//		int dx = 0, dy = 0;
//		for (Point point : vectors) {
//			dx += point.x;
//			dy += point.y;
//		}
//		
//		
//		return new Point(dx, dy);
	}
	
	public Point getTotalVectorSum(List<Point> vectors) {
		int dx = 0, dy = 0;
		for (Point point : vectors) {
			dx += point.x;
			dy += point.y;
		}		
		
		return new Point(dx, dy);		
	}
	
	public double getVectorSumDirection(Point vector) {		
		double dx = vector.x;
		double dy = vector.y;
		if (dx == 0 || dy == 0) return 0;
		
		double degree = Math.toDegrees(Math.atan(dy/dx));
		
		if (dx < 0 && dy < 0) {
			degree = 180 + degree;
		} else if (dx < 0) {			
			degree = 180 - Math.abs(degree);
		} else if (dy < 0) {
			degree = 360 - Math.abs(degree);
		}
		
		
		
		// Convert degree to compass unit
//    	if (degree >= 0 && degree <= 90) {
//    		degree = 90 - degree;
//    	} else if (degree >= 91 && degree <= 360) {
//    		degree = 360 -(degree - 90);
//    	}
		
		return degree;
	}
	
	public double getTotalVectorSumDirection(List<CountElement> countElements) {
		if (countElements.isEmpty()) return 0;
		List<Point> vectors = new ArrayList<Point>();
    	for (CountElement e : countElements) {
			vectors.add(e.count.vector);
		}
    	Point totalVector = DiagramMetricsHelperVector.INSTANCE.getTotalVectorSum(vectors);
    	double totalDegree = DiagramMetricsHelperVector.INSTANCE.getVectorSumDirection(totalVector);
    	return totalDegree;
	}
	
	public double concevertToPolarDegree(double degree) {
		double polar = degree;
    	if (degree >= 0 && degree <= 90) {
    		polar = 90 - degree;
    	} else if (degree >= 91 && degree <= 360) {
    		polar = 360 -(degree - 90);
    	}
    	return polar;
	}
	
	public double getVectorSumLength(Point vector) {
		
		double length = Math.sqrt(Math.pow(vector.x, 2) + Math.pow(vector.y, 2)); 
		
		return length;
	}
	
}
