package diagrammetrics.view;

// Imports for the GUI classes.
import diagrammetrics.model.DetailsListTable;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.RenderingHints;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.awt.geom.Line2D;

import javax.swing.JComponent;

/**
 * JKnob.java - A knob component. The knob can be rotated by dragging a spot on
 * the knob around in a circle. The knob will report its position in radians
 * when asked.
 *
 * @author Grant William Braught
 * @author Dickinson College
 * @version 12/4/2000
 */

public class JKnob extends JComponent implements MouseListener, MouseMotionListener, MouseWheelListener {

	private JPanelKnob knobPanel;
	
	private static final int radius = 32;
	private static final int spotRadius = 6;

	private double theta;
	private Color knobColor;
	private Color spotColor;

	private boolean pressedOnSpot;
	
	public State knobState;
	
	public static enum State {
		SUM, MEAN, MEDIAN, OFF;
	}
	
	/**
	 * No-Arg constructor that initializes the position of the knob to 0 radians
	 * (Up).
	 */	
	public JKnob(JPanelKnob mainPanel) {
		this(State.SUM, mainPanel);		
	}

	/**
	 * Constructor that initializes the position of the knob to the specified
	 * angle in radians.
	 *
	 * @param initAngle
	 *            the initial angle of the knob.
	 */
	public JKnob(State state, JPanelKnob mainPanel) {
		this(state, new Color(220,220,220), Color.black, mainPanel);
	}

	/**
	 * Constructor that initializes the position of the knob to the specified
	 * position and also allows the colors of the knob and spot to be specified.
	 *
	 * @param initAngle
	 *            the initial angle of the knob.
	 * @param initColor
	 *            the color of the knob.
	 * @param initSpotColor
	 *            the color of the spot.
	 */
	public JKnob(State state, Color initKnobColor, Color initSpotColor, JPanelKnob mainPanel) {

		knobState = state;
		theta = getStateTheta(state);		
		pressedOnSpot = false;
		knobColor = initKnobColor;
		spotColor = initSpotColor;
		this.knobPanel = mainPanel;

		this.addMouseListener(this);
		this.addMouseMotionListener(this);
		this.addMouseWheelListener(this);
	}

	/**
	 * Paint the JKnob on the graphics context given. The knob is a filled
	 * circle with a small filled circle offset within it to show the current
	 * angular position of the knob.
	 *
	 * @param g
	 *            The graphics context on which to paint the knob.
	 */
	final BasicStroke THIN_STROKE = new BasicStroke(1f, BasicStroke.CAP_ROUND, BasicStroke.JOIN_BEVEL);
	final Line2D tick1 = new Line2D.Double( (radius / 4), (radius - (radius / 2)) - 3,         (radius / 4) + 3, radius - (radius / 2) );
	
	final Line2D tick2 = new Line2D.Double( (radius / 8) - 2, (radius - (radius / 4) ) - 3,        (radius / 8) + 3, radius - (radius / 4) - 1);
	
	final Line2D tick3 = new Line2D.Double(0, radius, 5, radius);
	final Line2D tick4 = new Line2D.Double( (radius / 4), (radius + (radius / 2)) - 3, (radius / 4) - 3, radius + (radius / 2));
	//public void paint(Graphics g) {
	@Override
    public void paintComponent(Graphics g) {

		Graphics2D g2 = (Graphics2D) g;
		g2.setRenderingHints(new RenderingHints(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON));
		//g2.addRenderingHints(new RenderingHints(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON));

		// Draw the knob.
		g.setColor(knobColor);
		g2.fillOval(0, 0, 2 * radius, 2 * radius);
		// Draw border
		g.setColor(Color.black);
		g2.drawOval(0, 0, 2 * radius, 2 * radius);

		// Find the center of the spot.
		Point pt = getSpotCenter();
		int xc = (int) pt.getX();
		int yc = (int) pt.getY();

		// Draw the spot.
		g.setColor(spotColor);
		g.fillOval(xc - spotRadius, yc - spotRadius, 2 * spotRadius, 2 * spotRadius);
		
		
		g2.setStroke(THIN_STROKE);
        g2.draw(tick1);
        g2.draw(tick2);
        g2.draw(tick3);
        g2.draw(tick4);
        
        g.dispose();
        g2.dispose();
	}
	
	private double getStateTheta(State state) {
		double stateTheta = 0;
		if (state == State.SUM) {
			stateTheta = -0.8;			
		} else if (state == State.MEAN) {
			stateTheta = -1.25;
		} else if (state == State.MEDIAN) {
			stateTheta = -1.6;
		} else if (state == State.OFF) {
			stateTheta = -2.1;
		}
		return stateTheta;
	}
	
	public void setStateTheta(State state) {		
		this.theta = getStateTheta(state); 
	}
	
	public boolean isSumStateSelected() {
		return knobState == State.SUM;
	}
	
	public boolean isMeanStateSelected() {
		return knobState == State.MEAN;
	}
	
	public boolean isMedianStateSelected() {
		return knobState == State.MEDIAN;
	}
	
	public boolean isOffSelected() {
		return knobState == State.OFF;
	}
	

	/**
	 * Return the ideal size that the knob would like to be.
	 *
	 * @return the preferred size of the JKnob.
	 */
	public Dimension getPreferredSize() {
		return new Dimension(2 * radius, 2 * radius);
	}

	/**
	 * Return the minimum size that the knob would like to be. This is the same
	 * size as the preferred size so the knob will be of a fixed size.
	 *
	 * @return the minimum size of the JKnob.
	 */
	public Dimension getMinimumSize() {
		return new Dimension(2 * radius, 2 * radius);
	}

	/**
	 * Get the current anglular position of the knob.
	 *
	 * @return the current anglular position of the knob.
	 */
	public double getAngle() {
		return theta;
	}

	/**
	 * Calculate the x, y coordinates of the center of the spot.
	 *
	 * @return a Point containing the x,y position of the center of the spot.
	 */
	private Point getSpotCenter() {

		// Calculate the center point of the spot RELATIVE to the
		// center of the of the circle.

		int r = radius - spotRadius;

		int xcp = (int) (r * Math.sin(theta));
		int ycp = (int) (r * Math.cos(theta));

		// Adjust the center point of the spot so that it is offset
		// from the center of the circle. This is necessary becasue
		// 0,0 is not actually the center of the circle, it is the
		// upper left corner of the component!
		int xc = radius + xcp;
		int yc = radius - ycp;

		// Create a new Point to return since we can't
		// return 2 values!
		return new Point(xc, yc);
	}

	/**
	 * Determine if the mouse click was on the spot or not. If it was return
	 * true, otherwise return false.
	 *
	 * @return true if x,y is on the spot and false if not.
	 */
	private boolean isOnSpot(Point pt) {
		return (pt.distance(getSpotCenter()) < spotRadius);
	}

	// Methods from the MouseListener interface.

	/**
	 * Empy method because nothing happens on a click.
	 *
	 * @param e
	 *            reference to a MouseEvent object describing the mouse click.
	 */
	public void mouseClicked(MouseEvent e) {
	}

	/**
	 * Empty method because nothing happens when the mouse enters the Knob.
	 *
	 * @param e
	 *            reference to a MouseEvent object describing the mouse entry.
	 */
	public void mouseEntered(MouseEvent e) {
	}

	/**
	 * Empty method because nothing happens when the mouse exits the knob.
	 *
	 * @param e
	 *            reference to a MouseEvent object describing the mouse exit.
	 */
	public void mouseExited(MouseEvent e) {
	}

	/**
	 * When the mouse button is pressed, the dragging of the spot will be
	 * enabled if the button was pressed over the spot.
	 *
	 * @param e
	 *            reference to a MouseEvent object describing the mouse press.
	 */
	
	State oldState;
	public void mousePressed(MouseEvent e) {
		Point mouseLoc = e.getPoint();
		pressedOnSpot = isOnSpot(mouseLoc);
		oldState = knobState;
	}

	/**
	 * When the button is released, the dragging of the spot is disabled.
	 *
	 * @param e
	 *            reference to a MouseEvent object describing the mouse release.
	 */
	public void mouseReleased(MouseEvent e) {
		pressedOnSpot = false;
		if (oldState != knobState) {
			triggerState();
		}
	}

	// Methods from the MouseMotionListener interface.

	/**
	 * Empty method because nothing happens when the mouse is moved if it is not
	 * being dragged.
	 *
	 * @param e
	 *            reference to a MouseEvent object describing the mouse move.
	 */
	public void mouseMoved(MouseEvent e) {
	}

	/**
	 * Compute the new angle for the spot and repaint the knob. The new angle is
	 * computed based on the new mouse position.
	 *
	 * @param e
	 *            reference to a MouseEvent object describing the mouse drag.
	 */
	public void mouseDragged(MouseEvent e) {
		if (pressedOnSpot) {
			if ( (knobPanel.mainPanel.storedCountElements != null && knobPanel.mainPanel.storedCountElements.size() < 2)
					|| knobPanel.mainPanel.storedCountElements == null) return;
			
			int mx = e.getX();
			int my = e.getY();

			// Compute the x, y position of the mouse RELATIVE
			// to the center of the knob.
			int mxp = mx - radius;
			int myp = radius - my;

			// Compute the new angle of the knob from the
			// new x and y position of the mouse.
			// Math.atan2(...) computes the angle at which
			// x,y lies from the positive y axis with cw rotations
			// being positive and ccw being negative.
			//theta = Math.atan2(mxp, myp);
			
			double deltaTheta = Math.atan2(mxp, myp);
			if (deltaTheta >= getStateTheta(State.SUM) - 0.1 && deltaTheta <= 1.5) {
				setState(State.SUM);
			}			
			else if (deltaTheta < getStateTheta(State.SUM ) - 0.1 && deltaTheta > getStateTheta(State.MEAN ) - 0.2 ) {
				setState(State.MEAN);
			}			
			else if (deltaTheta < getStateTheta(State.MEAN ) - 0.2 && deltaTheta > getStateTheta(State.MEDIAN ) - 0.2 ) {
				setState(State.MEDIAN);
			}
			else {
				setState(State.OFF);
			}
		}
	}

	@Override
	public void mouseWheelMoved(MouseWheelEvent e) {
		if ( (knobPanel.mainPanel.storedCountElements != null && knobPanel.mainPanel.storedCountElements.size() < 2)
				|| knobPanel.mainPanel.storedCountElements == null) return;
		
		int rotatoions = e.getWheelRotation();
		if (rotatoions < 0) { // Up
			
			if (knobState == State.MEAN) {
				setState(State.SUM);
				triggerState();
			} else if (knobState == State.MEDIAN) {
				setState(State.MEAN);
				triggerState();
			} else if (knobState == State.OFF) {
				setState(State.MEDIAN);
				triggerState();
			}
			
		} else { // Down
			
			if (knobState == State.SUM) {
				setState(State.MEAN);
				triggerState();
			} else if (knobState == State.MEAN) {
				setState(State.MEDIAN);
				triggerState();
			} else if (knobState == State.MEDIAN) {
				setState(State.OFF);
				triggerState();
			}
		}
		
	}
	
	public void setState(State state) {
		theta = getStateTheta(state);
		knobState = state;
		
		repaint();
	}
	
//	public void triggerState(State state) {
//		if (state != knobState) {
//			setState(state);
//		}
//	}
	
	public void triggerState() {
		
		
		if (knobPanel.mainPanel.storedCountElements != null && !knobPanel.mainPanel.storedCountElements.isEmpty()) {
			knobPanel.mainPanel.setDiagramCount(knobPanel.mainPanel.storedCount, knobPanel.mainPanel.storedCountElements);
        }
		
		if (knobPanel.mainPanel.detailsListTable != null) {
			if (isOffSelected()) {
				knobPanel.mainPanel.detailsListTable.hide(DetailsListTable.COL_TOTAL);
			} else {
				knobPanel.mainPanel.detailsListTable.show(DetailsListTable.COL_TOTAL);
			}
			knobPanel.mainPanel.detailsListTable.sortColumns();
		}
	}
}