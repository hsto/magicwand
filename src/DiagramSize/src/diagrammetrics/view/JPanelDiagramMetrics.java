/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package diagrammetrics.view;

import diagrammetrics.model.DetailsListTable;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.GroupLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.event.TreeSelectionListener;

import com.nomagic.magicdraw.core.Application;
import com.nomagic.uml2.ext.magicdraw.classes.mdkernel.Element;

import diagrammetrics.DiagramMetricsHelper;
import diagrammetrics.DiagramMetricsHelperVector;
import diagrammetrics.controller.DiagramMetricsTreeSelectionDiagramListener;
import diagrammetrics.controller.DiagramMetricsTreeSelectionListener;
import diagrammetrics.controller.ActionListenerExport;
import diagrammetrics.controller.ShapeChangeListener;
import diagrammetrics.model.CountElement;
import eu.hansolo.custom.SteelCheckBox;
import eu.hansolo.steelseries.gauges.Radial;
import eu.hansolo.steelseries.tools.BackgroundColor;
import lib.SharedHelper;
import lib.SharedHierarchyListener;

/**
 *
 * @author Luai
 */
public class JPanelDiagramMetrics extends JPanel {

	private final JPanel innerPanel = new JPanel();
	private final GroupLayout layout = new GroupLayout(innerPanel);
	private final JPanel settingsPanel = new JPanel();
	private JPanel settingsPanelLeft = new JPanel();
    private JPanel settingsPanelCenter = new JPanel();    
    private final JPanel gaugePanel = new JPanel();
	private final JPanel detailsPanel = new JPanel();
	private JPanel scrollPane = new JPanel();
	
	private final JLabel labelElementName = new JLabel();
	
	private final JButton foldButtonSettings = new JButton();
	private final JButton foldButtonMeasurements = new JButton();
	private final JButton foldButtonDetails = new JButton();
	
	private JButton exportButton;
	private ActionListenerExport actionListenerExport = new ActionListenerExport(this);
	
	private SteelCheckBox detailsCheckBox = new SteelCheckBox();	
	private SteelCheckBox elementsCheckBox = new SteelCheckBox();
	private SteelCheckBox issuesCheckBox = new SteelCheckBox();
	private SteelCheckBox flowCheckBox = new SteelCheckBox();
	private SteelCheckBox librariesCheckBox = new SteelCheckBox();
	private SteelCheckBox showShapesCheckBox = new SteelCheckBox();
	
	private JTextField txtFieldScale = new JTextField();
	
	private JPanelKnob knobPanel = new JPanelKnob(this);
	
	public int storedCount;
	public List<CountElement> storedCountElements;
	private List<Element> storedElements;
	
	private JTable tableDetails;
	private JTable tableTotal;
	
	
	// http://www.java2s.com/Code/Jar/s/DownloadSteelSeries399jar.htm
	// http://www.java2s.com/Code/Jar/t/Downloadtrident71jar.htm
	//public final static Radial GAUGE = new Radial();
	private Radial gauge = new Radial();
	private AirCompass gaugeDirection = new AirCompass();
	
	private final static String ICON_RIGHT_FILE_NAME = "arrow-right.png";
    private final static String ICON_DOWN_FILE_NAME = "arrow-down.png";
	private final static String FILE_NAME = "link_DiagramSize.txt";
	private JLabel logoLabel = new JLabel();
	
	private ImageIcon imageIconRight;
	private ImageIcon imageIconDown;
	
    private DiagramMetricsTreeSelectionListener selectionListener = new DiagramMetricsTreeSelectionListener(this);
    private DiagramMetricsTreeSelectionDiagramListener selectionDiagramListener = new DiagramMetricsTreeSelectionDiagramListener(this);
    
    public JPanelDiagramMetrics() {
        initComponents();
        setDiagramCount(0, new ArrayList<CountElement>());
        
        this.addHierarchyListener(new SharedHierarchyListener(this, JPanelDiagramMetrics.this.getClass().getName(), false));
    }
    
    private void initComponents() {    	
    	logoLabel = SharedHelper.INSTANCE.initLogo();
        SharedHelper.INSTANCE.initLogoLink(logoLabel, getClass().getClassLoader().getResourceAsStream(FILE_NAME));
        imageIconRight = SharedHelper.INSTANCE.initIcon(ICON_RIGHT_FILE_NAME);
        imageIconDown = SharedHelper.INSTANCE.initIcon(ICON_DOWN_FILE_NAME);
        
    	initGauge();
		
    	JLabel labelTitel = new JLabel();
    	labelTitel.setFont(new Font("",Font.BOLD,16));
    	labelTitel.setText("Diagram Size Metrics");
    	labelTitel.setMinimumSize(new Dimension(40,25));
    	labelElementName.setFont(new Font("",Font.PLAIN,14));
        labelElementName.setText("Select element in containment tree");
        labelElementName.setMinimumSize(new Dimension(40,25));
    	
        Font font = new Font("",Font.BOLD,14);
        initFoldButton(foldButtonSettings, settingsPanel, "Settings", font);
        initFoldButton(foldButtonMeasurements, gaugePanel, "Measurements", font);
        initFoldButton(foldButtonDetails, scrollPane, "Details", font);
        
        Font fontState = new Font("Arial Italic", Font.PLAIN, 12);
        detailsCheckBox.setSelected(true);
        detailsCheckBox.setText("On");
        detailsCheckBox.setActionCommand("detailsCheckBox");
        detailsCheckBox.setBorder(BorderFactory.createEmptyBorder(0,0,0,20));
        detailsCheckBox.setFont(fontState);
        detailsCheckBox.addActionListener(new UpdateController(detailsCheckBox, "On", "Off"));
        elementsCheckBox.setSelected(true);
        elementsCheckBox.setText("Show");
        elementsCheckBox.setName(DetailsListTable.COL_ELEMENTS);
        elementsCheckBox.setBorder(BorderFactory.createEmptyBorder(0,0,0,20));
        elementsCheckBox.setFont(fontState);
        elementsCheckBox.addActionListener(new UpdateController(elementsCheckBox, "Show", "Hide"));
        issuesCheckBox.setSelected(true);
        issuesCheckBox.setText("Show");
        issuesCheckBox.setName(DetailsListTable.COL_ISSUES);
        issuesCheckBox.setBorder(BorderFactory.createEmptyBorder(0,0,0,20));
        issuesCheckBox.setFont(fontState);
        issuesCheckBox.addActionListener(new UpdateController(issuesCheckBox, "Show", "Hide"));
        flowCheckBox.setSelected(true);
        flowCheckBox.setText("Show");
        flowCheckBox.setName(DetailsListTable.COL_FLOW);
        flowCheckBox.setBorder(BorderFactory.createEmptyBorder(0,0,0,20));
        flowCheckBox.setFont(fontState);
        flowCheckBox.addActionListener(new UpdateController(flowCheckBox, "Show", "Hide"));
        showShapesCheckBox.setSelected(false);
        showShapesCheckBox.setText("Hide");
        showShapesCheckBox.setBorder(BorderFactory.createEmptyBorder(0,0,0,20));
        showShapesCheckBox.setFont(fontState);
        showShapesCheckBox.addActionListener(new ActionListener() {
        	private ShapeChangeListener listener;
			@Override
			public void actionPerformed(ActionEvent e) {
								
				if(showShapesCheckBox.isSelected()) showShapesCheckBox.setText("Show");
            	else showShapesCheckBox.setText("Hide");
            		
				if (showShapesCheckBox.isSelected()) {
					if (listener == null) listener = new ShapeChangeListener();
					Application.getInstance().getProject().addPropertyChangeListener(listener);
					Application.getInstance().getProject().getActiveDiagram().open();
				}
				else {
					listener.activeDiagram.getDiagramSurface().removePainter(listener.painter);
					listener.activeDiagram.getDiagramSurface().repaint();
					listener.activeDiagram = null;
					Application.getInstance().getProject().removePropertyChangeListener(listener);
				}
			}
		});
        librariesCheckBox.setSelected(false);
        librariesCheckBox.setText("Exclude");
        librariesCheckBox.setActionCommand("librariesCheckBox");
        librariesCheckBox.setBorder(BorderFactory.createEmptyBorder(0,0,0,20));
        librariesCheckBox.setFont(fontState);
        librariesCheckBox.addActionListener(new UpdateController(librariesCheckBox, "Include", "Exclude"));
        
        
        exportButton = new JButton("Export");
        exportButton.addActionListener(actionListenerExport);
        exportButton.setVisible(false);        
        
        JLabel labelDetails = new JLabel("Details  ");
        JLabel labelTotal = new JLabel("Total  ");
        JLabel labelElements = new JLabel("Elements  ");
        JLabel labelIssues = new JLabel("Issues  ");
        JLabel labelFlow = new JLabel("Flow  ");
        JLabel labelLibraries = new JLabel("Libraries  ");
        JLabel labelShowShapes = new JLabel("Shapes  ");
        JLabel labelScale = new JLabel("Needle scale  ");        
        Font boldFont = new Font("", Font.BOLD, 12);
        labelDetails.setFont(boldFont);
        labelTotal.setFont(boldFont);
        labelElements.setFont(boldFont);
        labelIssues.setFont(boldFont);
        labelFlow.setFont(boldFont);
        labelLibraries.setFont(boldFont);
        labelShowShapes.setFont(boldFont);
        labelScale.setFont(boldFont);
        labelDetails.setHorizontalAlignment(SwingConstants.TRAILING);
        labelDetails.setHorizontalAlignment(SwingConstants.TRAILING);
        labelTotal.setHorizontalAlignment(SwingConstants.TRAILING);
        labelElements.setHorizontalAlignment(SwingConstants.TRAILING);
        labelIssues.setHorizontalAlignment(SwingConstants.TRAILING);
        labelFlow.setHorizontalAlignment(SwingConstants.TRAILING);
        labelLibraries.setHorizontalAlignment(SwingConstants.TRAILING);
        labelShowShapes.setHorizontalAlignment(SwingConstants.TRAILING);
        labelScale.setHorizontalAlignment(SwingConstants.TRAILING);
        
        txtFieldScale.setText("10");
        txtFieldScale.setHorizontalAlignment(JTextField.RIGHT);
        txtFieldScale.setToolTipText("Scale direction pointer (2-10000). Press unter to update");
        txtFieldScale.addActionListener(new UpdateController(null, null, null));
        labelScale.setToolTipText("Scale direction pointer (2-10000). Press unter to update");
        labelShowShapes.setToolTipText("Highligt shapes in opened diagram");
        showShapesCheckBox.setToolTipText("Highligt shapes in opened diagram");
        
        JPanel inerSettingsPanel = new JPanel();
        GroupLayout mainPanelLayout = new GroupLayout(inerSettingsPanel);
        inerSettingsPanel.setLayout(mainPanelLayout);
        settingsPanelLeft.setLayout(new GridBagLayout());
        settingsPanelCenter.setLayout(new GridBagLayout());
//        GroupLayout settingsPanelRightLayout = new GroupLayout(settingsPanelRight);
//        settingsPanelRightLayout.setAutoCreateGaps(true);
//        settingsPanelRight.setLayout(settingsPanelRightLayout);
        
        inerSettingsPanel.setMaximumSize(new Dimension(425,120));
        inerSettingsPanel.setMinimumSize(new Dimension(425,120));
        inerSettingsPanel.setPreferredSize(new Dimension(425,120));
        JScrollPane mainPanelScrollPane = new JScrollPane (inerSettingsPanel);
        mainPanelScrollPane.setBorder(BorderFactory.createEmptyBorder());
        mainPanelScrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER);
        settingsPanel.setMinimumSize(new Dimension(40,120));
        settingsPanel.setMaximumSize(new Dimension(Short.MAX_VALUE, 120));
        settingsPanel.setLayout(new BorderLayout());
        settingsPanel.add(mainPanelScrollPane, BorderLayout.CENTER);
        
        settingsPanelLeft.setMaximumSize(new Dimension(130,75));
        settingsPanelLeft.setMinimumSize(new Dimension(130,75));
        settingsPanelLeft.setPreferredSize(new Dimension(130,75));
        settingsPanelCenter.setMaximumSize(new Dimension(155,100));
        settingsPanelCenter.setMinimumSize(new Dimension(155,100));
        settingsPanelCenter.setPreferredSize(new Dimension(155, 100));
        knobPanel.setMaximumSize(new Dimension(130,100));
        knobPanel.setMinimumSize(new Dimension(130,100));
        knobPanel.setPreferredSize(new Dimension(130, 100));
             
        settingsPanelLeft.add(labelElements, createGbcSettingsPanel(0, 0, false));
        settingsPanelLeft.add(elementsCheckBox, createGbcSettingsPanel(1, 0, false));
        settingsPanelLeft.add(labelIssues, createGbcSettingsPanel(0, 1, false));
        settingsPanelLeft.add(issuesCheckBox, createGbcSettingsPanel(1, 1, false));
        settingsPanelLeft.add(labelFlow, createGbcSettingsPanel(0, 2, false));
        settingsPanelLeft.add(flowCheckBox, createGbcSettingsPanel(1, 2, false));
//        settingsPanelLeft.add(labelShowShapes, createGbcSettingsPanel(0, 3, false));
//        settingsPanelLeft.add(showShapesCheckBox, createGbcSettingsPanel(1, 3, false));
        
//        settingsPanelCenter.add(labelTotal, createGbcSettingsPanel(0, 0));
//        settingsPanelCenter.add(totalCheckBox, createGbcSettingsPanel(1, 0));
        settingsPanelCenter.add(labelDetails, createGbcSettingsPanel(0, 0, false));
        settingsPanelCenter.add(detailsCheckBox, createGbcSettingsPanel(1, 0, false));
        settingsPanelCenter.add(labelShowShapes, createGbcSettingsPanel(0, 1, false));
        settingsPanelCenter.add(showShapesCheckBox, createGbcSettingsPanel(1, 1, false));
        settingsPanelCenter.add(labelLibraries, createGbcSettingsPanel(0, 2, false));
        settingsPanelCenter.add(librariesCheckBox, createGbcSettingsPanel(1, 2, false));
        settingsPanelCenter.add(labelScale, createGbcSettingsPanel(0, 3, false));
        settingsPanelCenter.add(txtFieldScale, createGbcSettingsPanel(1, 3, true));
        
        mainPanelLayout.setHorizontalGroup(mainPanelLayout.createSequentialGroup()
        		.addGap(10)
        		.addComponent(settingsPanelLeft)
        		.addComponent(settingsPanelCenter)
        		.addGap(5)
        		.addComponent(knobPanel)
        );
        
        mainPanelLayout.setVerticalGroup(mainPanelLayout.createParallelGroup()
        		.addComponent(settingsPanelLeft)
        		.addComponent(settingsPanelCenter)
        		.addComponent(knobPanel)
        );
        
        //gaugePanel.setLayout(new GridLayout(1,2));
        GroupLayout gaugeLayout = new GroupLayout(gaugePanel);
        gaugePanel.setLayout(gaugeLayout);
        gaugePanel.setMaximumSize(new Dimension(200,100));
        
        scrollPane.setLayout(new GridLayout(1,0));
        //detailsPanel.setLayout(new GridLayout(1,0));
//        detailsPanel.setLayout(new BorderLayout());
        detailsPanel.setLayout(new BoxLayout(detailsPanel, BoxLayout.Y_AXIS));
        JScrollPane pane = new JScrollPane(detailsPanel);
        pane.getVerticalScrollBar().setUnitIncrement(20);
        
    	innerPanel.setLayout(layout);
    	layout.setAutoCreateGaps(true);
        layout.setAutoCreateContainerGaps(true);
        
        layout.setHorizontalGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup()
                		.addGroup( layout.createSequentialGroup()
                				.addGroup(layout.createParallelGroup()
                						.addComponent(labelTitel)
                						.addComponent(labelElementName)                						
                				)                				
                				//.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, GroupLayout.PREFERRED_SIZE, Short.MAX_VALUE)
                				.addGap(4, 4, Short.MAX_VALUE)
                				.addComponent(logoLabel)
                		)
                		.addComponent(foldButtonSettings)
                		.addComponent(settingsPanel)
                		.addComponent(foldButtonMeasurements)
                		.addComponent(gaugePanel)
                		.addComponent(foldButtonDetails)
                		.addComponent(exportButton)
                		.addComponent(scrollPane)
                )     
        );
        
        layout.setVerticalGroup(layout.createSequentialGroup()
        		.addGroup(layout.createParallelGroup()
        				.addGroup(layout.createSequentialGroup()
        						.addComponent(labelTitel)
        						.addComponent(labelElementName)        						
        				)
        				.addComponent(logoLabel)
        		)
        		.addComponent(foldButtonSettings)
                .addComponent(settingsPanel)
        		.addComponent(foldButtonMeasurements)
        		.addComponent(gaugePanel)
        		.addComponent(foldButtonDetails)
        		.addComponent(exportButton)
        		.addComponent(scrollPane)
        );
        
        gaugeLayout.setHorizontalGroup(gaugeLayout.createSequentialGroup()
        		.addComponent(gauge)
        		.addGap(20)
        		.addComponent(gaugeDirection)
        );
        
        gaugeLayout.setVerticalGroup(gaugeLayout.createParallelGroup()
        		.addComponent(gauge)
        		.addComponent(gaugeDirection)
        );
        
        setLayout(new BorderLayout());
    	add(innerPanel);
    	scrollPane.add(exportButton);
    	scrollPane.add(pane);    	
    }
    
    private void initFoldButton(final JButton foldButton, final JPanel panel, final String name, Font font) {
    	panel.setVisible(false);
    	foldButton.setFont(font);
        SharedHelper.INSTANCE.setIconName(true, foldButton, name, imageIconRight, imageIconDown);
    	foldButton.setVisible(true);
    	foldButton.setMinimumSize(new Dimension(40,25));
    	foldButton.setMaximumSize(new Dimension(Short.MAX_VALUE, 30));
    	foldButton.setContentAreaFilled(false);
    	foldButton.setHorizontalAlignment(SwingConstants.LEFT);
    	foldButton.setBorder(BorderFactory.createEmptyBorder(4,1,4,4));
    	foldButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				if (panel.isVisible()) {
					panel.setVisible(false);
					if (name.equals("Details")) exportButton.setVisible(false);
                                        SharedHelper.INSTANCE.setIconName(true, foldButton, name, imageIconRight, imageIconDown);
				} else {
					panel.setVisible(true);
					if (name.equals("Details")) exportButton.setVisible(true);
                                        SharedHelper.INSTANCE.setIconName(false, foldButton, name, imageIconRight, imageIconDown);
					if (name.equals("Measurements") && storedCountElements != null && !storedCountElements.isEmpty()) {
						gaugeDirection.expaned = true;
						if (storedCount == 0 && storedCountElements.isEmpty()) {
							gaugeDirection.hidePlane = true;
						} else {
							gaugeDirection.hidePlane = false;
							Application.getInstance().getMainFrame().getBrowser().getActiveTree().getTreeModel().reload();
						}
					}
				}
			}
		});
    }
    
    private void initGauge() {
    	gauge.setTitle("Count");
		gauge.setUnitString("Elements");
//		gauge.setLogScale(true);
		gauge.setTrackVisible(true);
		gauge.setBackgroundColor(BackgroundColor.BLUE);
//		gauge.setMaxValue(Math.pow(10, 4));
//		gauge.setThreshold(4990);
//		gauge.setTrackStop(4990); // 100
//		gauge.setTrackSection(4625); // 70
//		gauge.setTrackStart(3260); // 20
		
		gauge.setMaxValue(100);
		gauge.setThreshold(100);
		gauge.setTrackStop(100); 
		gauge.setTrackSection(70);
		gauge.setTrackStart(30);
		
		gaugeDirection.setBackgroundColor(BackgroundColor.BLUE);
    }
    
    public void setDiagramCount (int count, List<CountElement> countElements) {
        // update knob
    	if (countElements.size() < 2) {
    		knobPanel.knob.setState(JKnob.State.SUM);
    		knobPanel.disableAggregation();
    	} else {
    		knobPanel.enableAggregation();
    		if (knobPanel.knob.isOffSelected()) {
    			gauge.setValue(0);
    			gaugeDirection.setValue(0);
    			gaugeDirection.setLength("0");
    			return;
    		}    		
    	}
    	
    	// get gauge value from knob
    	double i = (double) count;
    	if (knobPanel.knob.isMeanStateSelected()) {
    		if (count != 0 && countElements.size() != 0) i = count / countElements.size();
    	} else if (knobPanel.knob.isMedianStateSelected()) {
    		if (count != 0 && countElements.size() != 0) i = calcMedian(countElements);   		
    	}    	
        // set gauge
    	if (count == 0) gauge.setValue(count);
    	else gauge.setValue(i);
        
    	
    	// Get direction gauge values from knob
    	double degree = 0, length = 0;
    	if (knobPanel.knob.isSumStateSelected()) {
    		degree = DiagramMetricsHelperVector.INSTANCE.getTotalVectorSumDirection(countElements);
    		for (CountElement e : countElements) {
				length += e.count.strength;
			}
    	} else if (knobPanel.knob.isMedianStateSelected()) {
    		if (countElements.size() != 0) degree = calcMedianDir(countElements);
    		if (countElements.size() != 0) length = calcMedianDirLength(countElements);
    	} else { // Average
    		for (CountElement countElement : countElements) {
				degree += countElement.count.direction;
				length += countElement.count.strength;
			}
    		if (degree != 0 && countElements.size() != 0) degree = degree / countElements.size();
    		if (length != 0 && countElements.size() != 0) length = length / countElements.size();
    	}
    	
    	
    	// Update plane size
    	//int scale = 10; // 2 - 10000
    	int scale = getScaleValue();
    	int x = gaugeDirection.GAUGE_WIDTH;
    	if (length != 0) x = (int) (200 - (length / scale));
    	if (gaugeDirection.GAUGE_WIDTH < (gaugeDirection.GAUGE_WIDTH - x) ) x = 0;
    	gaugeDirection.offset = x;
    	gaugeDirection.planeX = gaugeDirection.planeY = x / 2;
	gaugeDirection.planeImage = gaugeDirection.create_AIRPLANE_Image(gaugeDirection.GAUGE_WIDTH - x, gaugeDirection.GAUGE_WIDTH - x);
	gaugeDirection.setValue(degree);
	gaugeDirection.setLength("" + ((int) length));
	
        
        // reset
	if (count == 0 && countElements.isEmpty()) gaugeDirection.hidePlane = true;
	else gaugeDirection.hidePlane = false;
    }
    
    private int getScaleValue() {
    	int scale = 10; // 2 - 10000
    	try {
    		scale = Integer.parseInt(txtFieldScale.getText());
    		
    		if (scale < 2 || scale > 10000) {
    			scale = 10;
    			txtFieldScale.setText("" + scale);
    		}
    	} catch(Exception e) {
    		txtFieldScale.setText("" + scale);
    	}
    	return scale;
    }
    
    public void addElements(List<Element> elements) {
    	if (elements.isEmpty()) return;
    	if (elements.size() > 1)
    		elements = DiagramMetricsHelper.INSTANCE.removeChildElements(elements);
    	storedElements = elements;
    	
    	if (elements.size() == 1) {
    		Element element = elements.get(0);
    		
    		String elementName = element.getHumanName().replaceFirst(element.getHumanType() + " ", "");
            labelElementName.setText(element.getHumanType() + " '" + elementName + "'");
    		
    		List<CountElement> countElements = DiagramMetricsHelper.INSTANCE.getDiagramsCountRecursive(element, librariesCheckBox.isSelected());
    		int count = 0;
    		for (CountElement e : countElements) {
				count += e.count.totalCount;
			}
    		//if (!countElements.isEmpty()) setDiagramCount(count, countElements);
    		setDiagramCount(count, countElements);
    		updateDetailsListTable(count, countElements);
    		storedCount = count;
    		storedCountElements = countElements;
    	} else {
    		labelElementName.setText(elements.size() + " elements selected");
    		
    		List<CountElement> countElements = new ArrayList<CountElement>();    		
    		int count = 0;
    		for (Element element : elements) {
    			List<CountElement> tempCountElements = DiagramMetricsHelper.INSTANCE.getDiagramsCountRecursive(element, librariesCheckBox.isSelected());
    			for (CountElement e : tempCountElements) {
    				count += e.count.totalCount;
    			}
    			countElements.addAll(tempCountElements);
//    			CountElement countElement = DiagramMetricsHelper.INSTANCE.getDiagramCount(element);
//				count += countElement.count.totalCount;
//				countElements.add(countElement);
			}
    		//if (!countElements.isEmpty()) setDiagramCount(count, countElements);
    		setDiagramCount(count, countElements);
    		updateDetailsListTable(count, countElements);
    		storedCount = count;
    		storedCountElements = countElements;
    	}
    }
    
    public DetailsListTable detailsListTable;
    private void updateDetailsListTable(int count, List<CountElement> countElements) {
    	detailsPanel.removeAll();
    	double countElementSize = countElements.size();
    	double totalAverage = (count != 0) ? count / countElementSize : 0;
    	int countLines = 0, countShapes = 0, countLabels = 0, countBends = 0, countCrossings = 0, countOverlaps = 0, countDirection = 0, countStrength = 0;
    	double averageLines = 0, averageShapes = 0, averageLabels = 0, averageBends = 0, averageCrossings = 0, averageOverlaps = 0, averageDirection = 0, averageStrength = 0;
    	
    	ArrayList<Object[]> data = new ArrayList<Object[]>();
    	for (CountElement e : countElements) {
    		if (detailsCheckBox.isSelected()) {
    			data.add(new Object[]{e.name, e.count.totalCount, e.count.lines + e.count.shapes + e.count.labels, e.count.bends + e.count.crossings + e.count.overlaps, e.count.direction + "\u00b0 / " +  e.count.strength});
    		} else {
    			data.add(new Object[]{e.name, e.count.totalCount, e.count.lines, e.count.shapes, e.count.labels, e.count.bends, e.count.crossings, e.count.overlaps, e.count.direction + "\u00b0", e.count.strength});    			
    		}
			countLines += e.count.lines;
			countShapes += e.count.shapes;
			countLabels += e.count.labels;
			countBends += e.count.bends;
			countCrossings += e.count.crossings;
			countOverlaps += e.count.overlaps;
			countDirection += e.count.direction;
			countStrength += e.count.strength;
		}
    	averageLines = (countLines != 0) ? countLines / countElementSize : 0;
    	averageShapes = (countShapes != 0) ? countShapes / countElementSize : 0;
    	averageLabels = (countLabels != 0) ? countLabels / countElementSize : 0;
    	averageBends = (countBends != 0) ? countBends / countElementSize : 0;
    	averageCrossings = (countCrossings != 0) ? countCrossings / countElementSize : 0;
    	averageOverlaps = (countOverlaps != 0) ? countOverlaps / countElementSize : 0;
    	averageDirection = (countDirection != 0) ? countDirection / countElementSize : 0;
    	averageStrength = (countStrength != 0) ? countStrength / countElementSize : 0;
    	
    	double totalDegree = DiagramMetricsHelperVector.INSTANCE.getTotalVectorSumDirection(countElements);
    	
    	ArrayList<Object[]> dataTotal = new ArrayList<Object[]>();
    	if (detailsCheckBox.isSelected()) {
    		dataTotal.add(new Object[] {"Total",count,countLines + countShapes + countLabels, countBends + countCrossings + countOverlaps, formatDouble(totalDegree) + "\u00b0 / " +  countStrength});
    		dataTotal.add(new Object[] {"Average",formatDouble(totalAverage), formatDouble(averageLines + averageShapes + averageLabels), formatDouble(averageBends + averageCrossings + averageOverlaps), formatDouble(averageDirection) + "\u00b0 / " + formatDouble(averageStrength)});
    	} else {
    		dataTotal.add(new Object[] {"Total",count,countLines,countShapes, countLabels, countBends, countCrossings, countOverlaps, formatDouble(totalDegree) + "\u00b0", countStrength});
    		dataTotal.add(new Object[] {"Average",formatDouble(totalAverage), formatDouble(averageLines), formatDouble(averageShapes), formatDouble(averageLabels), formatDouble(averageBends), formatDouble(averageCrossings), formatDouble(averageOverlaps), formatDouble(averageDirection) + "\u00b0", formatDouble(averageStrength)});    		
    	}
    	
    	detailsListTable = new DetailsListTable(tableDetails, tableTotal, detailsPanel, data, dataTotal, detailsCheckBox.isSelected(), countElements);
    	
    	detailsPanel.revalidate();
    }
    
    private double calcMedian(List<CountElement> countElements) {
    	
    	List<CountElement> list = new ArrayList<CountElement>(countElements);
		Collections.sort(list, new Comparator<CountElement>() {
			public int compare(CountElement arg0, CountElement arg1) {
				return arg0.count.totalCount - arg1.count.totalCount;
			}
		});
		
		if (list.size() == 1) return list.get(0).count.totalCount;
		
		if (list.size() % 2 == 0) { // Even
			int i = list.size() / 2;
			double sum = list.get(i - 1).count.totalCount + list.get(i).count.totalCount;
			return sum / 2;
		} else {
			int i = list.size() / 2;
			return list.get(i).count.totalCount;
		}
    	
    }
    
    private double calcMedianDir(List<CountElement> countElements) {
    	
    	List<CountElement> list = new ArrayList<CountElement>(countElements);
		Collections.sort(list, new Comparator<CountElement>() {
			public int compare(CountElement arg0, CountElement arg1) {
				return arg0.count.direction - arg1.count.direction;
			}
		});
		
		if (list.size() == 1) return list.get(0).count.direction;
		
		if (list.size() % 2 == 0) { // Even
			int i = list.size() / 2;
			double sum = list.get(i - 1).count.direction + list.get(i).count.direction;
			return sum / 2;
		} else {
			int i = list.size() / 2;
			return list.get(i).count.direction;
		}
    	
    }
    
    private double calcMedianDirLength(List<CountElement> countElements) {
    	
    	List<CountElement> list = new ArrayList<CountElement>(countElements);
		Collections.sort(list, new Comparator<CountElement>() {
			public int compare(CountElement arg0, CountElement arg1) {
				return arg0.count.strength - arg1.count.strength;
			}
		});
		
		if (list.size() == 1) return list.get(0).count.strength;
		
		if (list.size() % 2 == 0) { // Even
			int i = list.size() / 2;
			double sum = list.get(i - 1).count.strength + list.get(i).count.strength;
			return sum / 2;
		} else {
			int i = list.size() / 2;
			return list.get(i).count.strength;
		}
    	
    }
    
    private GridBagConstraints createGbcSettingsPanel(int col, int row, boolean insets) {
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.gridx = col;
        gbc.gridy = row;
        gbc.gridwidth = 1;
        gbc.gridheight = 1;

        gbc.anchor = (col == 0) ? GridBagConstraints.WEST : GridBagConstraints.EAST;
        gbc.fill = (col == 0) ? GridBagConstraints.BOTH : GridBagConstraints.HORIZONTAL;
        
        gbc.weightx = (col == 0) ? 0.1 : 1.0;
        gbc.weighty = 1.0;
        
        if (insets) gbc.insets = new Insets(0,0,0,35);
        return gbc;
    } 
    
//    class UnitActionListener implements ActionListener {
//
//		@Override
//		public void actionPerformed(ActionEvent arg0) {
//			setDiagramCount(totalCount, totalElementCountSize);
//			if (unitCheckBox.isSelected()) unitCheckBox.setText("Total");
//			else unitCheckBox.setText("Average");
//		}
//    	
//    }
    
    class UpdateController implements ActionListener {

    	SteelCheckBox checkBox;
    	String state1, state2;
    	
    	public UpdateController(SteelCheckBox checkBox, String state1, String state2) {
    		this.checkBox = checkBox;
			this.state1 = state1;
			this.state2 = state2;
		}
    	
        @Override
        public void actionPerformed(ActionEvent e) {
        	if (detailsListTable == null) return;
            if (checkBox != null) {
            	
            	if(checkBox.isSelected()) {
            		checkBox.setText(state1);
            		if (showGroups(checkBox)) {
            			detailsListTable.sortColumns();
            			return;
            		}
            	} else {
            		checkBox.setText(state2);
            		if (hideGroups(checkBox)) {
            			detailsListTable.sortColumns();
            			return;
            		}
            	}
            	
            }
            
            boolean isDetailsBoxSelected = checkBox != null && checkBox.getActionCommand() != null && checkBox.getActionCommand().equals("detailsCheckBox");
            
            if (storedCountElements != null && !storedCountElements.isEmpty()) {
            	if (isDetailsBoxSelected) updateDetailsListTable(storedCount, storedCountElements);
            	setDiagramCount(storedCount, storedCountElements);
            }
            
            if (isDetailsBoxSelected) {
            	if (updateAllGroups()) {
            		detailsListTable.sortColumns();
            	}
            } else if (checkBox != null && checkBox.getActionCommand() != null && checkBox.getActionCommand().equals("librariesCheckBox")) {
            	if (storedElements != null) {
            		addElements(storedElements);
            	}
            }
        }
        
        public boolean showGroups(SteelCheckBox checkBox) {
        	if (checkBox.getName() != null && !checkBox.getName().isEmpty()) {
        		if (detailsCheckBox.isSelected()) {
        			detailsListTable.show(checkBox.getName());        			
        		} else {
        			if (checkBox.getName().equals(DetailsListTable.COL_ELEMENTS)) {
        				detailsListTable.show(DetailsListTable.COL_LINES);
        				detailsListTable.show(DetailsListTable.COL_SHAPES);
        				detailsListTable.show(DetailsListTable.COL_LABELS);
        			} else if (checkBox.getName().equals(DetailsListTable.COL_ISSUES)) {
        				detailsListTable.show(DetailsListTable.COL_BENDS);
        				detailsListTable.show(DetailsListTable.COL_CROSSINGS);
        				detailsListTable.show(DetailsListTable.COL_OVERLAPS);
        			} else if (checkBox.getName().equals(DetailsListTable.COL_FLOW)) {
        				detailsListTable.show(DetailsListTable.COL_DIRECTION);
        				detailsListTable.show(DetailsListTable.COL_STRENGTH);
        			} else {
        				detailsListTable.show(checkBox.getName());
        			}
        		}
//        		detailsListTable.sortColumns();
        		return true;
        	}
        	return false;
        }
        
        public boolean hideGroups(SteelCheckBox checkBox) {
        	if (checkBox.getName() != null && !checkBox.getName().isEmpty()) {
        		if (detailsCheckBox.isSelected()) {
        			detailsListTable.hide(checkBox.getName());        			
        		} else {
        			if (checkBox.getName().equals(DetailsListTable.COL_ELEMENTS)) {
        				detailsListTable.hide(DetailsListTable.COL_LINES);
        				detailsListTable.hide(DetailsListTable.COL_SHAPES);
        				detailsListTable.hide(DetailsListTable.COL_LABELS);
        			} else if (checkBox.getName().equals(DetailsListTable.COL_ISSUES)) {
        				detailsListTable.hide(DetailsListTable.COL_BENDS);
        				detailsListTable.hide(DetailsListTable.COL_CROSSINGS);
        				detailsListTable.hide(DetailsListTable.COL_OVERLAPS);
        			} else if (checkBox.getName().equals(DetailsListTable.COL_FLOW)) {
        				detailsListTable.hide(DetailsListTable.COL_DIRECTION);
        				detailsListTable.hide(DetailsListTable.COL_STRENGTH);
        			} else {
        				detailsListTable.hide(checkBox.getName());
        			}
        		}
//        		detailsListTable.sortColumns();
        		return true;
        	}
        	return false;
        }
        
        public boolean updateAllGroups() {
        	boolean groupUpdated = false;
        	
//            if (!totalCheckBox.isSelected()) {
//            	if (hideGroups(totalCheckBox)) groupUpdated = true;            	
//            }
            if (knobPanel.knob.isOffSelected()) {
            	detailsListTable.hide(DetailsListTable.COL_TOTAL);
            	groupUpdated = true;            	
            }
            if (!elementsCheckBox.isSelected()) {
            	if (hideGroups(elementsCheckBox)) groupUpdated = true;
            }
            if (!issuesCheckBox.isSelected()) {
            	if (hideGroups(issuesCheckBox)) groupUpdated = true;
            }
            if (!flowCheckBox.isSelected()) {
            	if (hideGroups(flowCheckBox)) groupUpdated = true;
            }
            
            if (groupUpdated) {
            	detailsListTable.sortColumns();
            }
            return groupUpdated;
        }
        
    }
    
    public TreeSelectionListener getSelectionListener() {
        return selectionListener;
    }
    
    public TreeSelectionListener getSelectionDiagramListener() {
        return selectionDiagramListener;
    }
    
    public String formatDouble(double i) {
    	DecimalFormat df = new DecimalFormat("#0.00");
	return df.format(i);
    }
}
