package diagrammetrics.view;

import java.awt.Color;
import java.awt.Font;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

public class JPanelKnob extends JPanel {
	
	public JPanelDiagramMetrics mainPanel;
	
	public JKnob knob;
	private JButton btnSum = new JButton("Sum: \u03a3");
	private JButton btnMean = new JButton("Mean: \u03bc");
	private JButton btnMedian = new JButton("Median: m");
	private JButton btnOff = new JButton("Off");

	public JLabel labelKnob;
	
	public JPanelKnob(JPanelDiagramMetrics mainPanel) {
		this.mainPanel = mainPanel;
		this.knob = new JKnob(this);
		init();
		disableAggregation();
	}
	
	private void init() {
		initButton(btnSum, JKnob.State.SUM);
		initButton(btnMean, JKnob.State.MEAN);
		initButton(btnMedian, JKnob.State.MEDIAN);
		initButton(btnOff, JKnob.State.OFF);
		
		setLayout(null);
		add(btnSum);
		add(btnMean);
		add(btnMedian);
		add(btnOff);
		
		btnSum.setBounds   (15,0,57,10);
		btnMean.setBounds  (6,13,57,10);
		btnMedian.setBounds(2,26,57,10);
		btnOff.setBounds   (6,45,57,10);
		
		add(knob);
		knob.setBounds(60, 0, knob.getPreferredSize().width + 1, knob.getPreferredSize().height + 1);

		labelKnob = new JLabel("Aggregation");
		//labelKnob.setFont(new Font("", Font.BOLD, 11));
		labelKnob.setHorizontalAlignment(SwingConstants.CENTER);
		add(labelKnob);
		labelKnob.setBounds(60 - 10, knob.getPreferredSize().height , knob.getPreferredSize().width + 20, 18);
		
		
	}

	private void initButton(JButton btn, final JKnob.State state) {
		btn.setFont(new Font("Arial Italic", Font.PLAIN, 11));
		btn.setFocusPainted(false);
        btn.setMargin(new Insets(0, 0, 0, 0));
        btn.setContentAreaFilled(false);
        btn.setBorderPainted(false);
        btn.setOpaque(false);
        btn.setHorizontalAlignment(SwingConstants.RIGHT);
        btn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				if ( (mainPanel.storedCountElements != null  && mainPanel.storedCountElements.size() < 2)
						|| mainPanel.storedCountElements == null) return;
				
				if (knob.knobState != state) {
					knob.setState(state);
					knob.triggerState();
				}
			}
		});
	}
	
	public void enableAggregation () {
		labelKnob.setForeground(Color.black);
		btnMean.setForeground(Color.black);
		btnMedian.setForeground(Color.black);
		btnOff.setForeground(Color.black);
	}
	
	public void disableAggregation() {
		labelKnob.setForeground(Color.lightGray);
		btnMean.setForeground(Color.lightGray);
		btnMedian.setForeground(Color.lightGray);
		btnOff.setForeground(Color.lightGray);
	}
	
}
