package diagrammetrics;

import com.nomagic.magicdraw.ui.browser.WindowComponentContent;

import diagrammetrics.view.JPanelDiagramMetrics;

import java.awt.Component;

/**
 *
 * @author Luai
 */
public class DiagramMetricsWindowComponentContent implements WindowComponentContent {

    private final JPanelDiagramMetrics panel;
    
    public DiagramMetricsWindowComponentContent(JPanelDiagramMetrics panel) {
        this.panel = panel;
    }

    @Override
    public Component getWindowComponent() {
        return panel;
    }

    @Override
    public Component getDefaultFocusComponent() {
        return panel;
    }
    
}
