package diagrammetrics;

import java.awt.Point;
import java.awt.Rectangle;
import java.awt.geom.Line2D;
import java.awt.geom.Line2D.Double;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.nomagic.magicdraw.core.Application;
import com.nomagic.magicdraw.core.Project;
import com.nomagic.magicdraw.uml.InheritanceVisitor;
import com.nomagic.magicdraw.uml.symbols.DiagramPresentationElement;
import com.nomagic.magicdraw.uml.symbols.PresentationElement;
import com.nomagic.magicdraw.uml.symbols.paths.PathElement;
import com.nomagic.magicdraw.uml.symbols.shapes.AttributeView;
import com.nomagic.magicdraw.uml.symbols.shapes.BarView;
import com.nomagic.magicdraw.uml.symbols.shapes.CombinedFragmentHeaderView;
import com.nomagic.magicdraw.uml.symbols.shapes.CombinedFragmentView;
import com.nomagic.magicdraw.uml.symbols.shapes.ComponentView;
import com.nomagic.magicdraw.uml.symbols.shapes.DiagramFrameLabelView;
import com.nomagic.magicdraw.uml.symbols.shapes.DiagramFrameView;
import com.nomagic.magicdraw.uml.symbols.shapes.ElementPropertiesCompartmentView;
import com.nomagic.magicdraw.uml.symbols.shapes.EnumerationLiteralView;
import com.nomagic.magicdraw.uml.symbols.shapes.HeaderView.HeaderStereotypesView;
import com.nomagic.magicdraw.uml.symbols.shapes.InteractionOperandView;
import com.nomagic.magicdraw.uml.symbols.shapes.InteractionOperandsCompartmentView;
import com.nomagic.magicdraw.uml.symbols.shapes.LifeLineLineView;
import com.nomagic.magicdraw.uml.symbols.shapes.ObjectNodeView;
import com.nomagic.magicdraw.uml.symbols.shapes.OperationView;
import com.nomagic.magicdraw.uml.symbols.shapes.PartHeaderView;
import com.nomagic.magicdraw.uml.symbols.shapes.PartView;
import com.nomagic.magicdraw.uml.symbols.shapes.PseudoStateView;
import com.nomagic.magicdraw.uml.symbols.shapes.ReceptionView;
import com.nomagic.magicdraw.uml.symbols.shapes.RegionView;
import com.nomagic.magicdraw.uml.symbols.shapes.RegionsCompartmentView;
import com.nomagic.magicdraw.uml.symbols.shapes.SequenceLifelineView;
import com.nomagic.magicdraw.uml.symbols.shapes.ShapeElement;
import com.nomagic.magicdraw.uml.symbols.shapes.StateHeaderView;
import com.nomagic.magicdraw.uml.symbols.shapes.StateView;
import com.nomagic.magicdraw.uml.symbols.shapes.StructureCompartmentView;
import com.nomagic.magicdraw.uml.symbols.shapes.SwimlaneCellView;
import com.nomagic.magicdraw.uml.symbols.shapes.SwimlaneView;
import com.nomagic.magicdraw.uml.symbols.shapes.TextAreaView;
import com.nomagic.magicdraw.uml.symbols.shapes.TransitionToSelfView;
import com.nomagic.magicdraw.uml.symbols.shapes.TreeView;
import com.nomagic.magicdraw.uml.symbols.shapes.UseCaseView;
import com.nomagic.uml2.ext.jmi.helpers.StereotypesHelper;
import com.nomagic.uml2.ext.jmi.reflect.VisitorContext;
import com.nomagic.uml2.ext.magicdraw.classes.mdkernel.Diagram;
import com.nomagic.uml2.ext.magicdraw.classes.mdkernel.Element;
import com.nomagic.uml2.ext.magicdraw.mdprofiles.Stereotype;
import diagrammetrics.model.Count;
import diagrammetrics.model.CountElement;

public enum DiagramMetricsHelper {
	INSTANCE;
	
	private Project project = Application.getInstance().getProject();
	private DiagramVisitor diagramVisitor = new DiagramVisitor();
	private ChildVisitor childVisitor = new ChildVisitor();
	private String varLib1 = "Stereotype ModelLibrary";
	private String varLib2 = "Stereotype auxiliaryResource";
	
	
	public CountElement getDiagramCount(Element element) {
		Count count;
		count = countDiagram(element);
		
		return new CountElement(element, count);
	}
	
	public List<CountElement> getDiagramsCountRecursive(Element element, boolean includeLibraries) {
		
		ArrayList<Element> all = new ArrayList<Element>();
        all.add(element);
        Element current;
        diagramVisitor.countElements = new ArrayList<CountElement>();

        
        for (int i = 0; i < all.size(); i++) {
            current = all.get(i);
            try {
                current.accept(diagramVisitor);
            } catch (Exception e) {
                
            }
            if (i == 0 && current.getHumanType().equals("Model") && !includeLibraries) { // Root
            	for (Element e : current.getOwnedElement()) {
					List<Stereotype> stList = StereotypesHelper.getStereotypes(e);
					boolean skip = false;
					for (Stereotype st : stList) {
						if (st.getHumanName().equals(varLib1) || st.getHumanName().equals(varLib2))	skip = true;						
					}
					if (!skip) all.add(e);
				}
            } else {
            	all.addAll(current.getOwnedElement());            	
            }
        }
		
		return diagramVisitor.countElements;
	}
	
	public List<Element> removeChildElements(List<Element> elements) {
    	Map<Element,List<String>> idsTable = new HashMap<Element,List<String>>();
    	
    	// Init id lists
    	for (Element element : elements) {
    		childVisitor.ids = new ArrayList<String>();
    		
    		ArrayList<Element> all = new ArrayList<Element>();
            all.add(element);
            Element current;
            for (int i = 0; i < all.size(); i++) {
                current = all.get(i);
                try {
                    current.accept(childVisitor);
                } catch (Exception e) {
                    
                }
                all.addAll(current.getOwnedElement());                
            }
            idsTable.put(element,childVisitor.ids);
		}
    	
    	for (Iterator<Element> iterator = elements.iterator(); iterator.hasNext();) {
			Element element = (Element) iterator.next();
			for (Entry<Element, List<String>> entry : idsTable.entrySet()) {
    			if (entry.getKey() == element) continue;
    			
    			if (entry.getValue().contains(element.getID())) {
    				iterator.remove(); //elements.remove(element);
    				break;    				
    			}    			
    		}
		}
    	return elements;
    }
	
	
	class DiagramVisitor extends InheritanceVisitor {
		
		//public int count = 0;
		public List<CountElement> countElements = new ArrayList<CountElement>();
		
		
		@Override
		public void visitElement(Element arg0, VisitorContext arg1) {
			super.visitElement(arg0, arg1);
			if (arg0 instanceof Diagram) {
				Count count = countDiagram(arg0);
				if (count.totalCount != 0) {
					countElements.add(new CountElement(arg0, count));
				}
			}
		}
		
	}
	
    class ChildVisitor extends InheritanceVisitor {
    	
    	public List<String> ids = new ArrayList<String>();
    	
    	@Override
        public void visitElement(com.nomagic.uml2.ext.magicdraw.classes.mdkernel.Element element, VisitorContext context) {
            super.visitElement(element, context);
            ids.add(element.getID());
        }
    	
    	
    }
	
	private Count countDiagram(Element element) {
		if ( !(element instanceof Diagram) ) return new Count(0,0,0,0,0,0,0,0,0, new Point(0,0));
		DiagramPresentationElement diagram = project.getDiagram( (Diagram) element);
		diagram.ensureLoaded();		
    	int count = 0, lines = 0, shapes = 0, labels = 0, bends = 0, crossings = 0, overlaps = 0, direction = 0, strength = 0;
    	
    	List<Line2D.Double> lineSegments = new ArrayList<>();
    	List<PresentationElement> presentationElements = new ArrayList<>();
    	List<PathElement> pathElementsDirected = new ArrayList<>();
    	
    	
    	List<PresentationElement> all = new ArrayList<PresentationElement>();
    	all.addAll(diagram.getPresentationElements());
    	PresentationElement current;
    	
    	for (int i = 0; i < all.size(); i++) {
			current = all.get(i);
			
			if (current instanceof ShapeElement) {
				
				if (current.getBounds().isEmpty() && !(current instanceof TreeView)) continue;
				if (current instanceof DiagramFrameView) continue;									
                
//				if (!current.getName().isEmpty()) {
//					//ystem.err.println(current.getHumanType() + " :: " + current.getName()); // if (!Text Box) ++
//					labels++;
//				}
				
//				count++;
				shapes++;
				overlaps += addElementAndCheckOverlaps(current, presentationElements);
				
                // Use case components
                if (current instanceof ComponentView) {
                	for (PresentationElement p : current.getPresentationElements()) {
						if ((p instanceof ComponentView || p instanceof UseCaseView) && !all.contains(p)) {
							all.add(p);
						}
					}
                }
                
                // Activity diagram
                else if (current instanceof SwimlaneView) {
                	for (PresentationElement p : current.getPresentationElements()) {
						if (p instanceof SwimlaneCellView) {
							//  if (!all.contains(p)) all.add(p); // Area
							for (PresentationElement q : p.getPresentationElements()) {
								if (!all.contains(q)) {
									all.add(q);
								}
							}
						}
						else if (p instanceof ObjectNodeView && !all.contains(p)) {
							all.add(p);
						}
						else if (p instanceof BarView && !all.contains(p)) {
							all.add(p);
						}
						else if (p instanceof PathElement && !all.contains(p)) {
							all.add(p);
						}
					}
                }
                
                // State machine
                else if (current instanceof StateView) {
                	for (PresentationElement p : current.getPresentationElements()) {
						if (p instanceof StateHeaderView) {												
							for (PresentationElement q : p.getPresentationElements()) {
								if (q instanceof RegionsCompartmentView) {
									for (PresentationElement r : q.getPresentationElements()) {
										if(r instanceof RegionView) {
											for (PresentationElement s : r.getPresentationElements()) {
												if ((s instanceof StateView || s instanceof PathElement || s instanceof PseudoStateView) && !all.contains(s)) {
													all.add(s);
												}
											}
										}
									}
								}
							}
						} else if ((p instanceof TransitionToSelfView || p instanceof StateView) && !all.contains(p)) {
							all.add(p);
						}
					}
                }
                // Sequence diagram
                else if (current instanceof CombinedFragmentView) {
                	for (PresentationElement p : current.getPresentationElements()) {
						if (p instanceof CombinedFragmentHeaderView) {
							for (PresentationElement q : p.getPresentationElements()) {
								if (q instanceof ElementPropertiesCompartmentView || q instanceof InteractionOperandsCompartmentView) {
									for (PresentationElement r : q.getPresentationElements()) {
										if (r instanceof InteractionOperandView) {
											for (PresentationElement s : r.getPresentationElements()) {
												if (s instanceof CombinedFragmentView && !all.contains(s)) {
													all.add(s);
												}
											}
										}
									}
								}
							}
						}
					}
                }
                // Sequence diagram
                else if (current instanceof SequenceLifelineView) {
                	for (PresentationElement p : current.getPresentationElements()) {
                		if (p instanceof LifeLineLineView) {
                			for (PresentationElement q : p.getPresentationElements()) {
								if (!all.contains(q)) {
									all.add(q);
								}
							}
                		}
					}
                }
                
                else if(current instanceof PartView) {
                	for (PresentationElement p : current.getPresentationElements()) {
                		if (p instanceof PartHeaderView) {
                			for (PresentationElement q : p.getPresentationElements()) {
                				if (q instanceof StructureCompartmentView) {
                					for (PresentationElement r : q.getPresentationElements()) {
										if ((r instanceof PartView || r instanceof PathElement) && !all.contains(r)) {
											all.add(r);
										}
									}
                				}
							}
                		}
                	}
                }
                
                for (PresentationElement p : current.getPresentationElements()) {
					if (p instanceof PathElement && !all.contains(p)) {
						all.add(p);
					}
				}       
				
				
			} else if (current instanceof PathElement) {				
				
				PathElement pathElement = (PathElement) current;
				List<Point> points = pathElement.getAllBreakPoints();
				
//				if (!current.getName().isEmpty()) System.err.println(current.getName());
				
//				count += points.size() - 1;
				lines += points.size() - 1;
				if (points.size() > 2)
					bends += points.size() - 2;
				
				crossings += addPointsToLinesAndCheckCross(points, lineSegments);
				
				String type = pathElement.getHumanType();
				if (!(type.equals("Association") || type.equals("Anchor to Note")|| type.equals("Connector") || type.equals("Connection") || type.equals("Instance Specification") ||  type.equals("RMI") || type.equals("Ethernet") || type.equals("HTTP"))) {
////					count++;
//					lines++;
//					System.out.println(type);
					pathElementsDirected.add(pathElement);
				} else if (pathElement.getSupplier().getHumanType().equals("Actor") || pathElement.getClient().getHumanType().equals("Actor")) {
//					System.out.println(type);
					pathElementsDirected.add(pathElement);
				}
				
			}
						                       	
		}
//    	System.out.println();
    	Point vectorSum = DiagramMetricsHelperVector.INSTANCE.getVectorSum(pathElementsDirected);
    	direction = (int) DiagramMetricsHelperVector.INSTANCE.getVectorSumDirection(vectorSum);
    	strength = (int) DiagramMetricsHelperVector.INSTANCE.getVectorSumLength(vectorSum);
//    	System.err.println("Direction " + direction + " Strength " + strength);
    	
    	labels = countLabels(diagram.getPresentationElements());
    	
    	count = lines + shapes + labels + bends + crossings + overlaps /*+ direction + strength*/;
    	return new Count(count, lines, shapes, labels, bends, crossings, overlaps, direction, strength, vectorSum);
	}
	
	private int addElementAndCheckOverlaps(PresentationElement element, List<PresentationElement> elements) {
		int overlaps = 0;
		
		for (PresentationElement e : elements) {
			if (element.getBounds().intersects(e.getBounds())) overlaps++;
		}
		elements.add(element);
		
		return overlaps;
	}
	
	private int addPointsToLinesAndCheckCross(List<Point> points, List<Line2D.Double> lines) {
		List<Line2D.Double> tempLines = new ArrayList<>();		
		int cross = 0;
		
		for (int i = 0; i < points.size() - 1; i++) {
			tempLines.add( new Line2D.Double(points.get(i), points.get(i + 1)) );
		}
		
		for (Double line : tempLines) {
			for (Double l : lines) {
				if (line.intersectsLine(l)) cross++;
			}
		}		
		lines.addAll(tempLines);
		
		return cross;
	}
	
	private int countLabels(List<PresentationElement> list) {
		int count = 0;
		List<PresentationElement> all = new ArrayList<PresentationElement>();
    	all.addAll(list);
    	PresentationElement current;
    	
    	for (int i = 0; i < all.size(); i++) {
			current = all.get(i);
			
			if (current.getBounds().isEmpty() && !(current instanceof TreeView)) continue;
			if (current instanceof DiagramFrameView || current instanceof DiagramFrameLabelView) continue;
			
			if (current instanceof TextAreaView|| current instanceof AttributeView || current instanceof OperationView ||  current instanceof HeaderStereotypesView || current instanceof EnumerationLiteralView || current instanceof ReceptionView) {
            	Rectangle bounds = current.getBounds();
            	if (!bounds.isEmpty() && current.getName() != null && !current.getName().isEmpty()) {
//            		System.err.println(current.getName());
            		count++;
            	}
            }
        	all.addAll(current.getPresentationElements());
    	}
//    	System.out.println(count);
		return count;
	}

}
