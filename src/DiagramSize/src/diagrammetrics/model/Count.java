/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package diagrammetrics.model;

import java.awt.Point;

/**
 *
 * @author Luai
 */
public class Count {

    public int totalCount, lines, shapes, labels, bends, crossings, overlaps, direction, strength;
    public Point vector;

    public Count(int count, int lines, int shapes, int labels, int bends, int crossings, int overlaps, int direction, int strength, Point vector) {
        this.totalCount = count;
        this.lines = lines;
        this.shapes = shapes;
        this.labels = labels;
        this.bends = bends;
        this.crossings = crossings;
        this.overlaps = overlaps;
        this.direction = direction;
        this.strength = strength;
        this.vector = vector;
    }
}
