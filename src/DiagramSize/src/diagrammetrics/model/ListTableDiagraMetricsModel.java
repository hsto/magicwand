/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package diagrammetrics.model;

import java.util.ArrayList;
import java.util.List;

import javax.swing.table.AbstractTableModel;

/**
 *
 * @author Luai
 */
public class ListTableDiagraMetricsModel extends AbstractTableModel {
    
    private String[] header;
    private ArrayList<Object[]> data;
    private boolean group;
    List<CountElement> countElements;
    
    public ListTableDiagraMetricsModel(ArrayList<Object[]> data, String[] header, boolean group, List<CountElement> countElements) {
        this.data = data;
        this.header = header;
        this.group = group;
        this.countElements = countElements;
    }

    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
        data.get(rowIndex)[columnIndex] = aValue;
        fireTableCellUpdated(rowIndex, columnIndex);
        //fireTableDataChanged();
        
    }

    @Override
    public void fireTableCellUpdated(int row, int column) {
    
    }
    
    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return true;
    }

    @Override
    public int getRowCount() {
        return data.size();
    }

    @Override
    public int getColumnCount() {
        return header.length;
    }
    
    @Override
    public String getColumnName(int colum) {
        return header[colum];
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        if (countElements.isEmpty()) {
            return Object.class;
        }
        return getValueAt(0, columnIndex).getClass();
    }
    
    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
    	//if (rowIndex >= data.size()) return null;
        return data.get(rowIndex)[columnIndex];
    }

    public ArrayList<Object[]> getRows() {
        return data;
    }
    
    public void addRow() {
        if (group) data.add(new Object[] {"","","","",""});
        else data.add(new Object[] {"","","","","","","","","",""});
        fireTableRowsInserted(data.size()-2, data.size()-1);
    }
    
    public void addRowIndex(int index, Object[] object) {
        data.add(index, object);
        fireTableRowsInserted(index, index+1);
    }
    
    public void addRowIndexBefore(int index, Object[] object) {
        data.add(index, object);
        fireTableRowsInserted(index, index-1);
    }
    
    public void removeRow() {
        if (data.size() == 1) return; 
        data.remove(data.size()-1);
        fireTableRowsDeleted(data.size()-2, data.size()-1);
    }
    
    public String[] getHeader() {
        return header;
    }
    
}
