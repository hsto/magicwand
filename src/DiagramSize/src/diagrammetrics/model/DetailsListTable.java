package diagrammetrics.model;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.border.MatteBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.TableColumnModelEvent;
import javax.swing.event.TableColumnModelListener;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;

public class DetailsListTable {

	private JTable table;
	private JTable tableTotal;
	private JPanel panel;
	private boolean group = false;
	private Map<String, TableColumn> hiddenColumns = new HashMap<String, TableColumn>();
	private TableColumnModel colModel;
	private TableColumnModel colTotalModel;
	
	private static final String COL_DIAGRAM = "Diagram";
	public static final String COL_TOTAL = "Total";
	public static final String COL_ELEMENTS = "Elements";
	public static final String COL_ISSUES = "Issues";
	public static final String COL_FLOW = "Flow";
	public static final String COL_LINES = "Lines";
	public static final String COL_SHAPES = "Shapes";
	public static final String COL_LABELS = "Labels";
	public static final String COL_BENDS = "Bends";
	public static final String COL_CROSSINGS = "Crossings";
	public static final String COL_OVERLAPS = "Overlaps";
	public static final String COL_DIRECTION = "Direction";
	public static final String COL_STRENGTH = "Strength";

	public DetailsListTable(JTable table, JTable tableTotal, JPanel mainPanel, ArrayList<Object[]> data, ArrayList<Object[]> dataTotal, boolean group, List<CountElement> countElements) {
		this.table = table;
		this.tableTotal = tableTotal;
		this.panel = mainPanel;
		this.group = group;

		addReviewingListTable(data, dataTotal, countElements);
		colModel = this.table.getColumnModel();
		colTotalModel = this.tableTotal.getColumnModel();
	}

	private void addReviewingListTable(ArrayList<Object[]> dataInput, ArrayList<Object[]> dataInputTotal, List<CountElement> countElements) {		
		String[] header;
		if (group) {
			header = new String[]{COL_DIAGRAM, COL_TOTAL, COL_ELEMENTS, COL_ISSUES, COL_FLOW};
		} else {
			header = new String[]{COL_DIAGRAM, COL_TOTAL, COL_LINES, COL_SHAPES, COL_LABELS, COL_BENDS, COL_CROSSINGS, COL_OVERLAPS, COL_DIRECTION, COL_STRENGTH};						
		}
        
        ArrayList<Object[]> data = new ArrayList<Object[]>();
        if (dataInput != null) {
            data.addAll(dataInput);
        }

        final ListTableDiagraMetricsModel model = new ListTableDiagraMetricsModel(data, header, group, countElements);
        table = new JTable(model) {
        	
        	@Override
            public boolean isCellEditable(int row, int column) {
        		return false;
        	}
        	
            @Override
            public String getToolTipText(MouseEvent e) {
                String toolTip = null;
                java.awt.Point p = e.getPoint();
                if (p == null) return toolTip;
                int row = rowAtPoint(p);
                int col = columnAtPoint(p);
                
                if (row >= 0 && col >= 0 && /*col == 0 &&*/ this.getModel().getRowCount() > row) {
                	try {
                		toolTip = (String) getValueAt(row, col);
                	} catch (Exception ex) {
                		
                	} 
                }
                
                return toolTip;
            }

			@Override
			public Component prepareRenderer(TableCellRenderer renderer, int row, int col) {				
				Component c = super.prepareRenderer(renderer, row, col);
    			JComponent jc = (JComponent)c;
    			
    			if (group) {
    				if (col == 0) {
    					jc.setBorder(new MatteBorder(0, 0, 0, 1, Color.LIGHT_GRAY));
    					return jc;
    				} else if (col == 1) {
    					jc.setBorder(new MatteBorder(0, 1, 0, 1, Color.LIGHT_GRAY));
    					return jc;
    				} else if (col == 2) {
    					jc.setBorder(new MatteBorder(0, 1, 0, 1, Color.LIGHT_GRAY));
    					return jc;
    				} else if (col == 3) {
    					jc.setBorder(new MatteBorder(0, 1, 0, 1, Color.LIGHT_GRAY));
    					return jc;
    				} else if (col == 4) {
    					jc.setBorder(new MatteBorder(0, 1, 0, 0, Color.LIGHT_GRAY));
    					return jc;
    				}
    			} else {
    				if (col == 0) {
    					jc.setBorder(new MatteBorder(0, 0, 0, 1, Color.LIGHT_GRAY));
    					return jc;
    				} else if (col == 1) {
    					jc.setBorder(new MatteBorder(0, 1, 0, 1, Color.LIGHT_GRAY));
    					return jc;
    				} else if (col == 2) {
    					jc.setBorder(new MatteBorder(0, 1, 0, 0, Color.LIGHT_GRAY));
    					return jc;
    				} else if (col == 4) {
    					jc.setBorder(new MatteBorder(0, 0, 0, 1, Color.LIGHT_GRAY));
    					return jc;
    				} else if (col == 5) {
    					jc.setBorder(new MatteBorder(0, 1, 0, 0, Color.LIGHT_GRAY));
    					return jc;
    				} else if (col == 7) {
    					jc.setBorder(new MatteBorder(0, 0, 0, 1, Color.LIGHT_GRAY));
    					return jc;
    				} else if (col == 8) {
    					jc.setBorder(new MatteBorder(0, 1, 0, 0, Color.LIGHT_GRAY));
    					return jc;
    				}
    			}
				return super.prepareRenderer(renderer, row, col);
			}
            
            
        };

        table.getTableHeader().setReorderingAllowed(false);
        table.setAutoCreateRowSorter(true);
        table.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
        table.setFillsViewportHeight(true);

		
		final DefaultTableCellRenderer rightRenderer = new DefaultTableCellRenderer();
		rightRenderer.setHorizontalAlignment(SwingConstants.RIGHT);
		if (group)
			table.getColumn(COL_FLOW).setCellRenderer(rightRenderer);
		else
			table.getColumn(COL_DIRECTION).setCellRenderer(rightRenderer);		
		
		
		tableTotal = new JTable(dataInputTotal.size(), dataInputTotal.get(0).length) {
			@Override
			public TableCellRenderer getCellRenderer(int row, int column) {
				if (column > 0) return rightRenderer;
				return super.getCellRenderer(row, column);
			}
			
			@Override
            public String getToolTipText(MouseEvent e) {
                String toolTip = null;
                java.awt.Point p = e.getPoint();
                if (p == null) return toolTip;
                int row = rowAtPoint(p);
                int col = columnAtPoint(p);
                
                if (row >= 0 && col >= 0 && /*col == 0 &&*/ this.getModel().getRowCount() > row) {
                	try {
                		toolTip = (String) getValueAt(row, col);
                	} catch (Exception ex) {
                		
                	}
                }
                
                return toolTip;
            }
			
			@Override
			public Component prepareRenderer(TableCellRenderer renderer, int row, int col) {				
				Component c = super.prepareRenderer(renderer, row, col);
    			JComponent jc = (JComponent)c;
    			
    			if (group) {
    				if (col == 0) {
    					jc.setBorder(new MatteBorder(0, 0, 0, 1, Color.LIGHT_GRAY));
    					return jc;
    				} else if (col == 1) {
    					jc.setBorder(new MatteBorder(0, 1, 0, 1, Color.LIGHT_GRAY));
    					return jc;
    				} else if (col == 2) {
    					jc.setBorder(new MatteBorder(0, 1, 0, 1, Color.LIGHT_GRAY));
    					return jc;
    				} else if (col == 3) {
    					jc.setBorder(new MatteBorder(0, 1, 0, 1, Color.LIGHT_GRAY));
    					return jc;
    				} else if (col == 4) {
    					jc.setBorder(new MatteBorder(0, 1, 0, 0, Color.LIGHT_GRAY));
    					return jc;
    				}
    			} else {
    				if (col == 0) {
    					jc.setBorder(new MatteBorder(0, 0, 0, 1, Color.LIGHT_GRAY));
    					return jc;
    				} else if (col == 1) {
    					jc.setBorder(new MatteBorder(0, 1, 0, 1, Color.LIGHT_GRAY));
    					return jc;
    				} else if (col == 2) {
    					jc.setBorder(new MatteBorder(0, 1, 0, 0, Color.LIGHT_GRAY));
    					return jc;
    				} else if (col == 4) {
    					jc.setBorder(new MatteBorder(0, 0, 0, 1, Color.LIGHT_GRAY));
    					return jc;
    				} else if (col == 5) {
    					jc.setBorder(new MatteBorder(0, 1, 0, 0, Color.LIGHT_GRAY));
    					return jc;
    				} else if (col == 7) {
    					jc.setBorder(new MatteBorder(0, 0, 0, 1, Color.LIGHT_GRAY));
    					return jc;
    				} else if (col == 8) {
    					jc.setBorder(new MatteBorder(0, 1, 0, 0, Color.LIGHT_GRAY));
    					return jc;
    				}
    			}
				return super.prepareRenderer(renderer, row, col);
			}
		};
		
		for (int i = 0; i < dataInputTotal.size(); i++) {
			for (int j = 0; j < dataInputTotal.get(i).length; j++) {
				tableTotal.setValueAt(dataInputTotal.get(i)[j], i, j);
			}
			
		}
		
//		tableTotal.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
//	    tableTotal.setFillsViewportHeight(true);
	    
		// https://tips4java.wordpress.com/2008/11/10/table-column-adjuster/
		for (int column = 0; column < table.getColumnCount(); column++)
		{
		    TableColumn tableColumn = table.getColumnModel().getColumn(column);
		    TableColumn tableTotalColumn = tableTotal.getColumnModel().getColumn(column);
		    int preferredWidth = tableColumn.getMinWidth();
		    int maxWidth = tableColumn.getMaxWidth();
		 
		    for (int row = 0; row < table.getRowCount(); row++)
		    {
		        TableCellRenderer cellRenderer = table.getCellRenderer(row, column);
		        Component c = table.prepareRenderer(cellRenderer, row, column);
		        int width = c.getPreferredSize().width + table.getIntercellSpacing().width;
		        preferredWidth = Math.max(preferredWidth, width);
		 
		        //  We've exceeded the maximum width, no need to check other rows
		 
		        if (preferredWidth >= maxWidth)
		        {
		            preferredWidth = maxWidth;
		            break;
		        }
		    }
		 
		    tableColumn.setPreferredWidth( preferredWidth );
		    tableTotalColumn.setPreferredWidth( preferredWidth );
		}
		
		table.getColumnModel().addColumnModelListener(new TableColumnModelListener() {
			public void columnAdded(TableColumnModelEvent e) {}
			public void columnRemoved(TableColumnModelEvent e) {}
			public void columnMoved(TableColumnModelEvent e) {}
			public void columnSelectionChanged(ListSelectionEvent e) {}
			public void columnMarginChanged(ChangeEvent e) {
				synchColumnSizes();
			}
		});
		
////        getTable().getColumn(COL_DIAGRAM).setMaxWidth(200);
//        getTable().getColumn(COL_TOTAL).setMaxWidth(100);
//        getTable().getColumn(COL_TOTAL).setPreferredWidth(50);
////        getTable().getColumn(COL_TOTAL).setMinWidth(40);
//        
////        getTableTotal().getColumnModel().getColumn(0).setMaxWidth(200);
//        getTableTotal().getColumnModel().getColumn(1).setMaxWidth(100);
//        getTableTotal().getColumnModel().getColumn(1).setPreferredWidth(50);
////        getTableTotal().getColumnModel().getColumn(1).setMinWidth(40);
//        if (group) {
//        	getTable().getColumn(COL_ELEMENTS).setMaxWidth(100);
//        	getTable().getColumn(COL_ELEMENTS).setPreferredWidth(60);
////        	getTable().getColumn(COL_ELEMENTS).setMinWidth(40);
//        	getTable().getColumn(COL_ISSUES).setMaxWidth(100);
//        	getTable().getColumn(COL_ISSUES).setPreferredWidth(50);
////        	getTable().getColumn(COL_ISSUES).setMinWidth(40);
////        	getTable().getColumn(COL_FLOW).setMaxWidth(150);
//        	getTable().getColumn(COL_FLOW).setPreferredWidth(85);
////        	getTable().getColumn(COL_FLOW).setMinWidth(50);
//        	
//        	getTableTotal().getColumnModel().getColumn(2).setMaxWidth(100);
//        	getTableTotal().getColumnModel().getColumn(2).setPreferredWidth(50);
////        	getTableTotal().getColumnModel().getColumn(2).setMinWidth(40);
//        	getTableTotal().getColumnModel().getColumn(3).setMaxWidth(100);
//        	getTableTotal().getColumnModel().getColumn(3).setPreferredWidth(50);
////        	getTableTotal().getColumnModel().getColumn(3).setMinWidth(40);
////        	getTableTotal().getColumnModel().getColumn(4).setMaxWidth(150);
//        	getTableTotal().getColumnModel().getColumn(4).setPreferredWidth(85);
////        	getTableTotal().getColumnModel().getColumn(4).setMinWidth(50);
//        } else {
//        	getTable().getColumn(COL_LINES).setMaxWidth(100);
//        	getTable().getColumn(COL_LINES).setPreferredWidth(50);
//        	getTable().getColumn(COL_LINES).setMinWidth(40);
//        	getTable().getColumn(COL_SHAPES).setMaxWidth(100);
//        	getTable().getColumn(COL_SHAPES).setPreferredWidth(50);
//        	getTable().getColumn(COL_SHAPES).setMinWidth(40);
//        	getTable().getColumn(COL_LABELS).setMaxWidth(100);
//        	getTable().getColumn(COL_LABELS).setPreferredWidth(50);
//        	getTable().getColumn(COL_LABELS).setMinWidth(40);
//        	getTable().getColumn(COL_BENDS).setMaxWidth(100);
//        	getTable().getColumn(COL_BENDS).setPreferredWidth(50);
//        	getTable().getColumn(COL_BENDS).setMinWidth(40);
//        	getTable().getColumn(COL_CROSSINGS).setMaxWidth(100);
//        	getTable().getColumn(COL_CROSSINGS).setPreferredWidth(60);
//        	getTable().getColumn(COL_CROSSINGS).setMinWidth(40);
//        	getTable().getColumn(COL_OVERLAPS).setMaxWidth(100);
//        	getTable().getColumn(COL_OVERLAPS).setPreferredWidth(60);
//        	getTable().getColumn(COL_OVERLAPS).setMinWidth(40);
//        	getTable().getColumn(COL_DIRECTION).setMaxWidth(100);
//        	getTable().getColumn(COL_DIRECTION).setPreferredWidth(60);
//        	getTable().getColumn(COL_DIRECTION).setMinWidth(40);
//        	getTable().getColumn(COL_STRENGTH).setMaxWidth(100);
//        	getTable().getColumn(COL_STRENGTH).setPreferredWidth(60);
//        	getTable().getColumn(COL_STRENGTH).setMinWidth(40);
//        	
//        	getTable().getColumnModel().getColumn(2).setMaxWidth(100);
//        	getTable().getColumnModel().getColumn(2).setPreferredWidth(50);
//        	getTable().getColumnModel().getColumn(2).setMinWidth(40);
//        	getTable().getColumnModel().getColumn(3).setMaxWidth(100);
//        	getTable().getColumnModel().getColumn(3).setPreferredWidth(50);
//        	getTable().getColumnModel().getColumn(3).setMinWidth(40);
//        	getTable().getColumnModel().getColumn(4).setMaxWidth(100);
//        	getTable().getColumnModel().getColumn(4).setPreferredWidth(50);
//        	getTable().getColumnModel().getColumn(4).setMinWidth(40);
//        	getTable().getColumnModel().getColumn(5).setMaxWidth(100);
//        	getTable().getColumnModel().getColumn(5).setPreferredWidth(50);
//        	getTable().getColumnModel().getColumn(5).setMinWidth(40);
//        	getTable().getColumnModel().getColumn(6).setMaxWidth(100);
//        	getTable().getColumnModel().getColumn(6).setPreferredWidth(60);
//        	getTable().getColumnModel().getColumn(6).setMinWidth(40);
//        	getTable().getColumnModel().getColumn(7).setMaxWidth(100);
//        	getTable().getColumnModel().getColumn(7).setPreferredWidth(60);
//        	getTable().getColumnModel().getColumn(7).setMinWidth(40);
//        	getTable().getColumnModel().getColumn(8).setMaxWidth(100);
//        	getTable().getColumnModel().getColumn(8).setPreferredWidth(60);
//        	getTable().getColumnModel().getColumn(8).setMinWidth(40);
//        	getTable().getColumnModel().getColumn(9).setMaxWidth(100);
//        	getTable().getColumnModel().getColumn(9).setPreferredWidth(60);
//        	getTable().getColumnModel().getColumn(9).setMinWidth(40);
        
		
//		JPanel p = new JPanel(new GridBagLayout());
//		GridBagConstraints gbc = new GridBagConstraints();
//	    gbc.anchor = GridBagConstraints.WEST;
//	    p.add(new JScrollPane(table), gbc);
//	    gbc.gridy = 1;
//	    p.add(tableTotal, gbc);
		JPanel p = new JPanel(new BorderLayout());
		p.add(tableTotal, BorderLayout.NORTH);
		
		
        panel.add(new JScrollPane(table));
        panel.add(p);
//	    panel.add(p, BorderLayout.NORTH);
        table.setRowHeight(22);
        tableTotal.setRowHeight(22);
        table.setPreferredScrollableViewportSize(new Dimension(table.getPreferredScrollableViewportSize().width, dataInput.size() * table.getRowHeight()));
        //synchColumnSizes();
        panel.repaint();
	}

	private void synchColumnSizes() {
		TableColumnModel tcmTable = table.getColumnModel();
		TableColumnModel tcmTotalTable = tableTotal.getColumnModel();

		for (int i = 0; i < tcmTable.getColumnCount(); i++) {
			int width = tcmTable.getColumn(i).getWidth();
			tcmTotalTable.getColumn(i).setPreferredWidth(width);
		}
	}
	
	public void hide(String name) {
		int index = colModel.getColumnIndex(name);
		TableColumn col = colModel.getColumn(index);
		TableColumn colTotal = colTotalModel.getColumn(index);
		hiddenColumns.put(name, col);
		hiddenColumns.put(";" + name, colTotal);
//		hiddenColumns.put(":" + name, new Integer(index));
		colModel.removeColumn(col);
		colTotalModel.removeColumn(colTotal);
	}
	
	public void show(String name) {
		Object obj = hiddenColumns.remove(name);
		Object objTotal = hiddenColumns.remove(";" + name);
		if (obj == null || objTotal == null) return;
		colModel.addColumn((TableColumn) obj);
		colTotalModel.addColumn((TableColumn) objTotal);
//		obj = hiddenColumns.remove(":" + name);
//		if (obj == null) return;
//		
//		int column = ((Integer) obj).intValue();
//        int lastColumn = colModel.getColumnCount() - 1;
//        if (column < lastColumn) {
//            colModel.moveColumn(lastColumn, column);
//            colTotalModel.moveColumn(lastColumn, column);
//        }
		
//		// Sort columns
//		int lastColumn = colModel.getColumnCount() - 1;
//		if (name.equals(COL_TOTAL)) {
//			colModel.moveColumn(lastColumn, 1);
//			colTotalModel.moveColumn(lastColumn, 1);
//		}
//		
//		if (group) {
//			if (name.equals(COL_ELEMENTS)) {
//				if (hiddenColumns.get(COL_TOTAL) != null) { // Total hidden
//					colModel.moveColumn(lastColumn, 1);
//					colTotalModel.moveColumn(lastColumn, 1);
//				} else {
//					colModel.moveColumn(lastColumn, 2);
//					colTotalModel.moveColumn(lastColumn, 2);
//				}
//			} else if (name.equals(COL_ISSUES)) {
//				if (hiddenColumns.get(COL_FLOW) == null) { // Flow not hidden
//					
//					if (hiddenColumns.get(COL_TOTAL) == null) { // Total not hidden
//						if (hiddenColumns.get(COL_ELEMENTS) == null) { // Elements not hidden
//							colModel.moveColumn(lastColumn, 3);
//							colTotalModel.moveColumn(lastColumn, 3);
//						} else {
//							colModel.moveColumn(lastColumn, 2);
//							colTotalModel.moveColumn(lastColumn, 2);
//						}
//					} else { // Total hidden
//						if (hiddenColumns.get(COL_ELEMENTS) == null) { // Elements not hidden
//							colModel.moveColumn(lastColumn, 2);
//							colTotalModel.moveColumn(lastColumn, 2);
//						} else {
//							colModel.moveColumn(lastColumn, 1);
//							colTotalModel.moveColumn(lastColumn, 1);
//						}
//					}
//				}
//			}
//		} else {
//			
//			
//		}
	}
	
	public void sortColumns() {
		int index = 1;
		
		if (group) {
			if (hiddenColumns.get(COL_TOTAL) == null) {
				int i = colModel.getColumnIndex(COL_TOTAL);
				colModel.moveColumn(i, index);
				colTotalModel.moveColumn(i, index);
				index++;
			}
			
			if (hiddenColumns.get(COL_ELEMENTS) == null) {
				int i = colModel.getColumnIndex(COL_ELEMENTS);
				colModel.moveColumn(i, index);
				colTotalModel.moveColumn(i, index);
				index++;
			}
			
			if (hiddenColumns.get(COL_ISSUES) == null) {
				int i = colModel.getColumnIndex(COL_ISSUES);
				colModel.moveColumn(i, index);
				colTotalModel.moveColumn(i, index);
				index++;
			}
			
		} else {
			if (hiddenColumns.get(COL_TOTAL) == null) {
				int i = colModel.getColumnIndex(COL_TOTAL);
				colModel.moveColumn(i, index);
				colTotalModel.moveColumn(i, index);
				index++;
			}
			
			if (hiddenColumns.get(COL_LINES) == null) {
				int i = colModel.getColumnIndex(COL_LINES);
				colModel.moveColumn(i, index);
				colTotalModel.moveColumn(i, index);
				index++;
				
				i = colModel.getColumnIndex(COL_SHAPES);
				colModel.moveColumn(i, index);
				colTotalModel.moveColumn(i, index);
				index++;
				
				i = colModel.getColumnIndex(COL_LABELS);
				colModel.moveColumn(i, index);
				colTotalModel.moveColumn(i, index);
				index++;
			}
			
			if (hiddenColumns.get(COL_BENDS) == null) {
				int i = colModel.getColumnIndex(COL_BENDS);
				colModel.moveColumn(i, index);
				colTotalModel.moveColumn(i, index);
				index++;
				
				i = colModel.getColumnIndex(COL_CROSSINGS);
				colModel.moveColumn(i, index);
				colTotalModel.moveColumn(i, index);
				index++;
				
				i = colModel.getColumnIndex(COL_OVERLAPS);
				colModel.moveColumn(i, index);
				colTotalModel.moveColumn(i, index);
				index++;
			}
			
		}
		
	}
	
	public JTable getTable() {
        return table;
    }
	
	public JTable getTableTotal() {
        return tableTotal;
    }

}
