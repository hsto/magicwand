/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package diagrammetrics.model;

import com.nomagic.uml2.ext.magicdraw.classes.mdkernel.Element;

/**
 *
 * @author Luai
 */
public class CountElement {

    public String name;
    public Count count;

    public CountElement(Element element, Count count) {
        this.name = element.getHumanName().replaceFirst(element.getHumanType() + " ", "");
        this.count = count;
    }
}
