/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package diagrammetrics.controller;

import java.util.ArrayList;
import java.util.List;

import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;

import com.nomagic.magicdraw.core.Application;
import com.nomagic.magicdraw.ui.browser.Node;
import com.nomagic.magicdraw.ui.browser.Tree;
import com.nomagic.magicdraw.uml.BaseElement;
import com.nomagic.uml2.ext.magicdraw.classes.mdkernel.Element;

import diagrammetrics.view.JPanelDiagramMetrics;

/**
 *
 * @author Luai
 */
public class DiagramMetricsTreeSelectionDiagramListener implements TreeSelectionListener {

    private JPanelDiagramMetrics panel;
    private Tree tree;
    
    public DiagramMetricsTreeSelectionDiagramListener(JPanelDiagramMetrics panel) {
        this.panel = panel;
    }
    
    @Override
    public void valueChanged(TreeSelectionEvent e) {
    	if (!panel.isShowing()) return;
    	
    	tree = Application.getInstance().getMainFrame().getBrowser().getDiagramsTree();
    	Application.getInstance().getMainFrame().getBrowser().getContainmentTree().setSelectedNodes(null);
    	
    	List<Element> elements = new ArrayList<Element>();
        Node[] nodes = tree.getSelectedNodes();
        for (Node node : nodes) {
        	if (node != null) {
        		Object userObject = node.getUserObject();
        		if (userObject instanceof BaseElement && userObject instanceof Element) {
        			
        			Element element = (Element) userObject;
        			elements.add(element);
        			
        		}
        	}			
		}
        panel.addElements(elements);
        
        

    }
    
}
