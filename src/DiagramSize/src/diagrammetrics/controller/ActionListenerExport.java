package diagrammetrics.controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import javax.swing.JFileChooser;
import javax.swing.JTable;
import javax.swing.filechooser.FileFilter;

import diagrammetrics.view.JPanelDiagramMetrics;
import lib.ExportFileChooser;
import lib.ExportFileFilter;
import lib.SharedHelper;

public class ActionListenerExport implements ActionListener {

    private JPanelDiagramMetrics panel;

    public ActionListenerExport(JPanelDiagramMetrics panel) {
        this.panel = panel;
    }

    @Override
    public void actionPerformed(ActionEvent arg0) {
        if (panel.detailsListTable == null) {
            return;
        }

        JFileChooser jc = new ExportFileChooser(SharedHelper.ExportType.CSV);
        FileFilter ff = new ExportFileFilter(SharedHelper.ExportType.CSV);

        jc.addChoosableFileFilter(ff);
        jc.setFileFilter(ff);

        int res = jc.showSaveDialog(panel);

        if (res == JFileChooser.APPROVE_OPTION) {

            File file = jc.getSelectedFile();
            String path = file.getAbsolutePath();
            if (!path.endsWith(".csv")) {
                file = new File(path + ".csv");
            }

            try {
                BufferedWriter bw = new BufferedWriter(new FileWriter(file));
                String str = getExportTableString();
                bw.write(str);
                bw.close();
            } catch (IOException ex) {
            }
        }

    }

    private String getExportTableString() {
        JTable table = panel.detailsListTable.getTable();
        StringBuilder builder = new StringBuilder();
        String sep = ";";

        for (int i = 0; i < table.getModel().getRowCount(); i++) {
            for (int j = 0; j < table.getModel().getColumnCount(); j++) {
                String str = table.getModel().getValueAt(i, j).toString();
                if (j == table.getModel().getColumnCount() - 1) {
                    builder.append(str + "\n");
                } else {
                    builder.append(str + sep);
                }
            }
        }

        table = panel.detailsListTable.getTableTotal();
        for (int i = 0; i < table.getModel().getRowCount(); i++) {
            for (int j = 0; j < table.getModel().getColumnCount(); j++) {
                String str = table.getModel().getValueAt(i, j).toString();
                if (j == table.getModel().getColumnCount() - 1) {
                    builder.append(str + "\n");
                } else {
                    builder.append(str + sep);
                }
            }
        }

        return builder.toString();
    }

}
