package diagrammetrics.controller;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Rectangle;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.List;

import com.nomagic.magicdraw.core.Application;
import com.nomagic.magicdraw.core.Project;
import com.nomagic.magicdraw.ui.DiagramSurfacePainter;
import com.nomagic.magicdraw.uml.symbols.DiagramPresentationElement;
import com.nomagic.magicdraw.uml.symbols.PresentationElement;
import com.nomagic.magicdraw.uml.symbols.paths.PathElement;
import com.nomagic.magicdraw.uml.symbols.shapes.AttributeView;
import com.nomagic.magicdraw.uml.symbols.shapes.BarView;
import com.nomagic.magicdraw.uml.symbols.shapes.CombinedFragmentHeaderView;
import com.nomagic.magicdraw.uml.symbols.shapes.CombinedFragmentView;
import com.nomagic.magicdraw.uml.symbols.shapes.ComponentView;
import com.nomagic.magicdraw.uml.symbols.shapes.DiagramFrameLabelView;
import com.nomagic.magicdraw.uml.symbols.shapes.DiagramFrameView;
import com.nomagic.magicdraw.uml.symbols.shapes.ElementPropertiesCompartmentView;
import com.nomagic.magicdraw.uml.symbols.shapes.EnumerationLiteralView;
import com.nomagic.magicdraw.uml.symbols.shapes.InteractionOperandView;
import com.nomagic.magicdraw.uml.symbols.shapes.InteractionOperandsCompartmentView;
import com.nomagic.magicdraw.uml.symbols.shapes.LifeLineLineView;
import com.nomagic.magicdraw.uml.symbols.shapes.ObjectNodeView;
import com.nomagic.magicdraw.uml.symbols.shapes.OperationView;
import com.nomagic.magicdraw.uml.symbols.shapes.PartHeaderView;
import com.nomagic.magicdraw.uml.symbols.shapes.PartView;
import com.nomagic.magicdraw.uml.symbols.shapes.PseudoStateView;
import com.nomagic.magicdraw.uml.symbols.shapes.ReceptionView;
import com.nomagic.magicdraw.uml.symbols.shapes.RegionView;
import com.nomagic.magicdraw.uml.symbols.shapes.RegionsCompartmentView;
import com.nomagic.magicdraw.uml.symbols.shapes.SequenceLifelineView;
import com.nomagic.magicdraw.uml.symbols.shapes.ShapeElement;
import com.nomagic.magicdraw.uml.symbols.shapes.StateHeaderView;
import com.nomagic.magicdraw.uml.symbols.shapes.StateView;
import com.nomagic.magicdraw.uml.symbols.shapes.StructureCompartmentView;
import com.nomagic.magicdraw.uml.symbols.shapes.SwimlaneCellView;
import com.nomagic.magicdraw.uml.symbols.shapes.SwimlaneView;
import com.nomagic.magicdraw.uml.symbols.shapes.TextAreaView;
import com.nomagic.magicdraw.uml.symbols.shapes.TransitionToSelfView;
import com.nomagic.magicdraw.uml.symbols.shapes.TreeView;
import com.nomagic.magicdraw.uml.symbols.shapes.UseCaseView;
import com.nomagic.magicdraw.uml.symbols.shapes.HeaderView.HeaderStereotypesView;

public class ShapeChangeListener implements PropertyChangeListener{

	public DiagramPresentationElement activeDiagram;
	public CustomDiagramPainter painter;
	
	@Override
	public void propertyChange(PropertyChangeEvent evt) {
		
        if(evt.getPropertyName().equals(Project.DIAGRAM_WINDOW_ACTIVATED)) {
        	if (painter == null) painter = new CustomDiagramPainter();
        	if (activeDiagram != null && activeDiagram.getDiagramSurface() != null) {
        		activeDiagram.getDiagramSurface().removePainter(painter);
        		activeDiagram.getDiagramSurface().repaint();
        	}
            activeDiagram = Application.getInstance().getProject().getActiveDiagram();

            activeDiagram.getDiagramSurface().addPainter(painter);
        }
	}
	
	class CustomDiagramPainter extends DiagramSurfacePainter {
        @Override
		public void paint(Graphics g, DiagramPresentationElement diagram) {
        	//int count = 0;
        	g.setColor(Color.BLUE);                          
        	
        	List<PresentationElement> all = new ArrayList<PresentationElement>();
        	all.addAll(diagram.getPresentationElements());
        	PresentationElement current;
        	
        	for (int i = 0; i < all.size(); i++) {
				current = all.get(i);
				
//				if (current instanceof DiagramFrameView || current instanceof DiagramFrameLabelView) continue;
                
//                if (current instanceof TextAreaView|| current instanceof AttributeView || current instanceof OperationView ||  current instanceof HeaderStereotypesView || current instanceof EnumerationLiteralView || current instanceof ReceptionView) {
//                	Rectangle bounds = current.getBounds();
//                	if (!bounds.isEmpty() && current.getName() != null && !current.getName().isEmpty()) {
//                		bounds.grow(5,5);
//                		((Graphics2D)g).draw(bounds);
//                		//System.err.println(current.getName());
//                		count++;
//                	}
//                }
//                
//				all.addAll(current.getPresentationElements());
				
					if (current instanceof ShapeElement) {
						
						if (current.getBounds().isEmpty() && !(current instanceof TreeView)) continue;
						if (current instanceof DiagramFrameView) continue;									
						
						g.setColor(Color.BLUE);
						Rectangle bounds = current.getBounds();
                        bounds.grow(5,5);
                        ((Graphics2D)g).draw(bounds);
                        //count++;
                        
//                        System.err.println(current.getName());
						
//                        // Count actor twice because of name
//                        if (current instanceof ActorView) {
//                        	//if (!current.getName().isEmpty()) count++;
//                        }
                        
//                        if (current instanceof TreeView) {
//							all.add(current);
//						}
                        
                        // Use case components
                        if (current instanceof ComponentView) {
                        	for (PresentationElement p : current.getPresentationElements()) {
								if ((p instanceof ComponentView || p instanceof UseCaseView) && !all.contains(p)) all.add(p);
							}
                        }
                        
                        // Activity diagram
                        else if (current instanceof SwimlaneView) {
                        	for (PresentationElement p : current.getPresentationElements()) {
								if (p instanceof SwimlaneCellView) {
									//  if (!all.contains(p)) all.add(p); // Area
									for (PresentationElement q : p.getPresentationElements()) {
										if (!all.contains(q)) all.add(q);
									}
								}
								else if (p instanceof ObjectNodeView && !all.contains(p)) all.add(p);
								else if (p instanceof BarView && !all.contains(p)) all.add(p);
								else if (p instanceof PathElement && !all.contains(p)) all.add(p);
							}
                        }
                        
                        // State machine
                        else if (current instanceof StateView) {
                        	for (PresentationElement p : current.getPresentationElements()) {
								if (p instanceof StateHeaderView) {												
									for (PresentationElement q : p.getPresentationElements()) {
										if (q instanceof RegionsCompartmentView) {
											for (PresentationElement r : q.getPresentationElements()) {
												if(r instanceof RegionView) {
													for (PresentationElement s : r.getPresentationElements()) {
														if ((s instanceof StateView || s instanceof PathElement || s instanceof PseudoStateView) && !all.contains(s)) all.add(s);
													}
												}
											}
										}
									}
								} else if ((p instanceof TransitionToSelfView || p instanceof StateView) && !all.contains(p)) {
									all.add(p);
								}
							}
                        }
                        // Sequence diagram
                        else if (current instanceof CombinedFragmentView) {
                        	for (PresentationElement p : current.getPresentationElements()) {
								if (p instanceof CombinedFragmentHeaderView) {
									for (PresentationElement q : p.getPresentationElements()) {
										if (q instanceof ElementPropertiesCompartmentView || q instanceof InteractionOperandsCompartmentView) {
											for (PresentationElement r : q.getPresentationElements()) {
												if (r instanceof InteractionOperandView) {
													for (PresentationElement s : r.getPresentationElements()) {
														if (s instanceof CombinedFragmentView && !all.contains(s)) all.add(s);
													}
												}
											}
										}
									}
								}
							}
                        }
                        // Sequence diagram
                        else if (current instanceof SequenceLifelineView) {
                        	for (PresentationElement p : current.getPresentationElements()) {
                        		if (p instanceof LifeLineLineView) {
                        			for (PresentationElement q : p.getPresentationElements()) {
										if (!all.contains(q)) all.add(q);
									}
                        		}
							}
                        }
                        
                        else if(current instanceof PartView) {
                        	for (PresentationElement p : current.getPresentationElements()) {
                        		if (p instanceof PartHeaderView) {
                        			for (PresentationElement q : p.getPresentationElements()) {
                        				if (q instanceof StructureCompartmentView) {
                        					for (PresentationElement r : q.getPresentationElements()) {
												if ((r instanceof PartView || r instanceof PathElement) && !all.contains(r)) all.add(r);
											}
                        				}
									}
                        		}
                        	}
                        }
                        
                        for (PresentationElement p : current.getPresentationElements()) {
							if (p instanceof PathElement && !all.contains(p)) {
								all.add(p);
							}
						}       
						
						
					} else if (current instanceof PathElement) {
						
						g.setColor(Color.RED);
						
						PathElement pathElement = (PathElement) current;
						List<Point> points = pathElement.getAllBreakPoints();
						
						//count += points.size() - 1;
//						System.err.println(current.getName());
						for (Point p : points) {
							Rectangle bounds = new Rectangle(p.x - 2, p.y - 2, 4, 4);
							((Graphics2D)g).draw(bounds);
						}
						
						
//						String type = pathElement.getHumanType();
//						if (!(type.equals("Association") || type.equals("Anchor to Note")|| type.equals("Connector") || type.equals("Connection") || type.equals("RMI") || type.equals("Ethernet") || type.equals("HTTP"))) {
//							count++;
//						}
						
					}				                       	
			}
        	paintLabels(diagram.getPresentationElements(), g);
        	g.dispose();
        }
        
    	private void paintLabels(List<PresentationElement> list, Graphics g) {
    		List<PresentationElement> all = new ArrayList<PresentationElement>();
        	all.addAll(list);
        	PresentationElement current;
        	
        	for (int i = 0; i < all.size(); i++) {
    			current = all.get(i);
    			
    			if (current.getBounds().isEmpty() && !(current instanceof TreeView)) continue;
    			if (current instanceof DiagramFrameView || current instanceof DiagramFrameLabelView) continue;
    			
    			if (current instanceof TextAreaView|| current instanceof AttributeView || current instanceof OperationView ||  current instanceof HeaderStereotypesView || current instanceof EnumerationLiteralView || current instanceof ReceptionView) {
                	Rectangle bounds = current.getBounds();
                	if (!bounds.isEmpty() && current.getName() != null && !current.getName().isEmpty()) {
                		g.setColor(Color.LIGHT_GRAY);
                        bounds.grow(2,2);
                        ((Graphics2D)g).draw(bounds);
                	}
                }
            	all.addAll(current.getPresentationElements());
        	}
    	}
    }
}
