/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package diagrammetrics;

import java.util.Scanner;

import javax.swing.JTree;

import com.nomagic.magicdraw.core.Application;
import com.nomagic.magicdraw.core.Project;
import com.nomagic.magicdraw.core.project.ProjectEventListener;
import com.nomagic.magicdraw.ui.MainFrame;
import com.nomagic.magicdraw.ui.ProjectWindow;
import com.nomagic.magicdraw.ui.ProjectWindowsManager;
import com.nomagic.magicdraw.ui.WindowComponentInfo;
import com.nomagic.magicdraw.ui.WindowsManager;
import com.nomagic.magicdraw.ui.browser.Tree;
import com.nomagic.magicdraw.ui.browser.WindowComponentContent;

import diagrammetrics.view.JPanelDiagramMetrics;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import lib.SharedHelper;

/**
 *
 * @author Luai
 */
public class DiagramMetricsProjectEventListener implements ProjectEventListener {

    private Tree tree;
    private JTree jtree;
    private Tree treeDiagram;
    private JTree jtreeDiagram;
    public static String pluginName = "Diagram Size";    
    private final JPanelDiagramMetrics panel = new JPanelDiagramMetrics();
    public static String projectWindowId = "";
    
    WindowComponentInfo info = new WindowComponentInfo("DIAGRAMMETRICS", pluginName, null, WindowsManager.SIDE_EAST, WindowsManager.STATE_DOCKED, false);
    public boolean startup;
    public ProjectWindow window;
    
    public DiagramMetricsProjectEventListener() {
    }
    
    private static String FILE_NAME_STARTUP = System.getProperty("user.home") + "/.magicdraw/MagicWand/startup_DiagramSize.txt";

    @Override
    public void projectOpened(Project project) {
        tree = Application.getInstance().getMainFrame().getBrowser().getContainmentTree();
        jtree = tree.getTree();
        jtree.addTreeSelectionListener(panel.getSelectionListener());
        
        treeDiagram = Application.getInstance().getMainFrame().getBrowser().getDiagramsTree();
        jtreeDiagram = treeDiagram.getTree();
        jtreeDiagram.addTreeSelectionListener(panel.getSelectionDiagramListener());
        
        // protect documentation pane from direct editing
        Application.getInstance().getMainFrame().getBrowser().getDocPanel().getHTMLTextEditorComponent().setVisible(false);
        Application.getInstance().getMainFrame().getBrowser().getDocPanel().getHTMLEditorPane().setEnabled(false);
        Application.getInstance().getMainFrame().getBrowser().getDocPanel().getHTMLEditorPane().setVisible(false);
        Application.getInstance().getMainFrame().getBrowser().getDocPanel().setEnableHTMLCheckBox(false);
        Application.getInstance().getMainFrame().getBrowser().getDocPanel().setEnabledAll(false);
        

        MainFrame mainFrame = Application.getInstance().getMainFrame();
        ProjectWindowsManager projectWindowsManager = mainFrame.getProjectWindowsManager();
       
        WindowComponentContent content = new DiagramMetricsWindowComponentContent(panel);
        window = new ProjectWindow(info,content);
        
        startup = SharedHelper.INSTANCE.readStartup(FILE_NAME_STARTUP);
        if (startup) {
                projectWindowsManager.addWindow(window);
        	projectWindowsManager.activateWindow(window.getId());
        }
        projectWindowId = window.getId();
        
        
        
/*    	project.addPropertyChangeListener(new PropertyChangeListener() {
			
			@Override
			public void propertyChange(PropertyChangeEvent evt) {
                if(evt.getPropertyName().equals(Project.DIAGRAM_OPENED)) {
                    DiagramPresentationElement activeDiagram = Application.getInstance().getProject().getActiveDiagram();

                    activeDiagram.getDiagramSurface().addPainter(new DiagramSurfacePainter() {
                        @Override
						public void paint(Graphics g, DiagramPresentationElement diagram) {
                        	int count = 0;
                        	g.setColor(Color.BLUE);                          
                        	
                        	List<PresentationElement> all = new ArrayList<PresentationElement>();
                        	all.addAll(diagram.getPresentationElements());
                        	PresentationElement current;
                        	
                        	for (int i = 0; i < all.size(); i++) {
								current = all.get(i);
								
								if (current instanceof DiagramFrameView || current instanceof DiagramFrameLabelView) continue;
	                            
	                            if (current instanceof TextAreaView|| current instanceof AttributeView || current instanceof OperationView ||  current instanceof HeaderStereotypesView || current instanceof EnumerationLiteralView || current instanceof ReceptionView) {
	                            	Rectangle bounds = current.getBounds();
	                            	if (!bounds.isEmpty() && current.getName() != null && !current.getName().isEmpty()) {
	                            		bounds.grow(5,5);
	                            		((Graphics2D)g).draw(bounds);
	                            		//System.err.println(current.getName());
	                            		count++;
	                            	}
	                            }
	                            
								all.addAll(current.getPresentationElements());
								
//								if (current instanceof ShapeElement) {
//									
//									if (current.getBounds().isEmpty() && !(current instanceof TreeView)) continue;
//									if (current instanceof DiagramFrameView) continue;									
//									
//									g.setColor(Color.BLUE);
//									Rectangle bounds = current.getBounds();
//		                            bounds.grow(5,5);
//		                            ((Graphics2D)g).draw(bounds);
//		                            count++;
//		                            
////		                            System.err.println(current.getName());
//									
////		                            // Count actor twice because of name
////	                                if (current instanceof ActorView) {
////	                                	//if (!current.getName().isEmpty()) count++;
////	                                }
//	                                
////		                            if (current instanceof TreeView) {
////										all.add(current);
////									}
//		                            
//		                            // Use case components
//		                            if (current instanceof ComponentView) {
//	                                	for (PresentationElement p : current.getPresentationElements()) {
//											if ((p instanceof ComponentView || p instanceof UseCaseView) && !all.contains(p)) all.add(p);
//										}
//	                                }
//	                                
//	                                // Activity diagram
//	                                else if (current instanceof SwimlaneView) {
//	                                	for (PresentationElement p : current.getPresentationElements()) {
//											if (p instanceof SwimlaneCellView) {
//												//  if (!all.contains(p)) all.add(p); // Area
//												for (PresentationElement q : p.getPresentationElements()) {
//													if (!all.contains(q)) all.add(q);
//												}
//											}
//											else if (p instanceof ObjectNodeView && !all.contains(p)) all.add(p);
//											else if (p instanceof BarView && !all.contains(p)) all.add(p);
//											else if (p instanceof PathElement && !all.contains(p)) all.add(p);
//										}
//	                                }
//	                                
//	                                // State machine
//	                                else if (current instanceof StateView) {
//	                                	for (PresentationElement p : current.getPresentationElements()) {
//											if (p instanceof StateHeaderView) {												
//												for (PresentationElement q : p.getPresentationElements()) {
//													if (q instanceof RegionsCompartmentView) {
//														for (PresentationElement r : q.getPresentationElements()) {
//															if(r instanceof RegionView) {
//																for (PresentationElement s : r.getPresentationElements()) {
//																	if ((s instanceof StateView || s instanceof PathElement || s instanceof PseudoStateView) && !all.contains(s)) all.add(s);
//																}
//															}
//														}
//													}
//												}
//											} else if ((p instanceof TransitionToSelfView || p instanceof StateView) && !all.contains(p)) {
//												all.add(p);
//											}
//										}
//	                                }
//	                                // Sequence diagram
//	                                else if (current instanceof CombinedFragmentView) {
//	                                	for (PresentationElement p : current.getPresentationElements()) {
//											if (p instanceof CombinedFragmentHeaderView) {
//												for (PresentationElement q : p.getPresentationElements()) {
//													if (q instanceof ElementPropertiesCompartmentView || q instanceof InteractionOperandsCompartmentView) {
//														for (PresentationElement r : q.getPresentationElements()) {
//															if (r instanceof InteractionOperandView) {
//																for (PresentationElement s : r.getPresentationElements()) {
//																	if (s instanceof CombinedFragmentView && !all.contains(s)) all.add(s);
//																}
//															}
//														}
//													}
//												}
//											}
//										}
//	                                }
//	                                // Sequence diagram
//	                                else if (current instanceof SequenceLifelineView) {
//	                                	for (PresentationElement p : current.getPresentationElements()) {
//	                                		if (p instanceof LifeLineLineView) {
//	                                			for (PresentationElement q : p.getPresentationElements()) {
//													if (!all.contains(q)) all.add(q);
//												}
//	                                		}
//										}
//	                                }
//	                                
//	                                else if(current instanceof PartView) {
//	                                	for (PresentationElement p : current.getPresentationElements()) {
//	                                		if (p instanceof PartHeaderView) {
//	                                			for (PresentationElement q : p.getPresentationElements()) {
//	                                				if (q instanceof StructureCompartmentView) {
//	                                					for (PresentationElement r : q.getPresentationElements()) {
//															if ((r instanceof PartView || r instanceof PathElement) && !all.contains(r)) all.add(r);
//														}
//	                                				}
//												}
//	                                		}
//	                                	}
//	                                }
//	                                
//	                                for (PresentationElement p : current.getPresentationElements()) {
//										if (p instanceof PathElement && !all.contains(p)) all.add(p);
//									}       
//									
//									
//								} else if (current instanceof PathElement) {
//									
//									g.setColor(Color.RED);
//									
//									PathElement pathElement = (PathElement) current;
//									List<Point> points = pathElement.getAllBreakPoints();
//									
//									count += points.size() - 1;
////									System.err.println(current.getName());
//									for (Point p : points) {
//										Rectangle bounds = new Rectangle(p.x - 2, p.y - 2, 4, 4);
//										((Graphics2D)g).draw(bounds);
//									}
//									
//									
//									String type = pathElement.getHumanType();
//									if (!(type.equals("Association") || type.equals("Anchor to Note")|| type.equals("Connector") || type.equals("Connection") || type.equals("RMI") || type.equals("Ethernet") || type.equals("HTTP"))) {
//										count++;
//									}
//									
//								}
								
								
								                       	
							}
//                        	JPanelDiagramMetrics.GAUGE.setValueAnimated(count);
                        	int i = 0;
                        	//System.out.println("Count: " + count + " " + i);
                        }
                    });
                }				
			}
		});*/
    }

    @Override
    public void projectClosed(Project project) {
        
    }

    @Override
    public void projectSaved(Project project, boolean savedInServer) {
        
    }

    @Override
    public void projectActivated(Project project) {
        
    }

    @Override
    public void projectDeActivated(Project project) {
            
    }

    @Override
    public void projectReplaced(Project oldProject, Project newProject) {

    }

    @Override
    public void projectCreated(Project project) {
        
    }

    @Override
    public void projectPreClosed(Project project) {
        tree = Application.getInstance().getMainFrame().getBrowser().getContainmentTree();
        if (tree != null) {
	        jtree = tree.getTree();
	        if (panel != null && jtree != null) jtree.removeTreeSelectionListener(panel.getSelectionListener());
        }
        
        treeDiagram = Application.getInstance().getMainFrame().getBrowser().getDiagramsTree();
        if (treeDiagram != null) {
        	jtreeDiagram = treeDiagram.getTree();
        	if (panel != null && jtreeDiagram != null) jtreeDiagram.removeTreeSelectionListener(panel.getSelectionDiagramListener());
        }
        
        SharedHelper.INSTANCE.writeStartup(FILE_NAME_STARTUP, panel);
    }

    @Override
    public void projectPreClosedFinal(Project project) {
        
    }

    @Override
    public void projectPreSaved(Project project, boolean savedInServer) {
        
    }

    @Override
    public void projectPreActivated(Project project) {
        
    }

    @Override
    public void projectPreDeActivated(Project project) {
        
    }

    @Override
    public void projectPreReplaced(Project oldProject, Project newProject) {
        
    }

    @Override
    public void projectOpenedFromGUI(Project project) {
        
    }

    @Override
    public void projectActivatedFromGUI(Project project) {
        
    }
    
}
