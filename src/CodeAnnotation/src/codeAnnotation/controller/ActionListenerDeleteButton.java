/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package codeAnnotation.controller;

import codeAnnotation.CodeAnnotationHelper;
import com.nomagic.magicdraw.core.Application;
import com.nomagic.magicdraw.openapi.uml.ModelElementsManager;
import com.nomagic.magicdraw.openapi.uml.ReadOnlyElementException;
import com.nomagic.magicdraw.openapi.uml.SessionManager;
import com.nomagic.magicdraw.ui.browser.Node;
import com.nomagic.magicdraw.ui.browser.Tree;
import com.nomagic.magicdraw.uml.BaseElement;
import com.nomagic.uml2.ext.magicdraw.classes.mdkernel.Comment;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;

/**
 *
 * @author Luai
 */
public class ActionListenerDeleteButton implements ActionListener{
    
    private JButton jbDelete;
    
    public ActionListenerDeleteButton(JButton jbDelete) {
        this.jbDelete = jbDelete;
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
        Tree tree = Application.getInstance().getMainFrame().getBrowser().getContainmentTree();
        
        Node node = tree.getSelectedNode();
        if (node != null) {
           Object userObject = node.getUserObject();
           if (userObject instanceof BaseElement) {
               
               BaseElement baseElement = (BaseElement) userObject;
               Comment comment = CodeAnnotationHelper.INSTANCE.getComment(baseElement);
               SessionManager.getInstance().createSession("Delete Code Comment");

               try {
                   ModelElementsManager.getInstance().removeElement(comment);
                   jbDelete.setEnabled(false);
               } catch (ReadOnlyElementException ex) {
                   
               }
               SessionManager.getInstance().closeSession();
           }
        }
    }
    
}
