/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package codeAnnotation.controller;

import codeAnnotation.CodeAnnotationHelper;
import com.nomagic.magicdraw.core.Application;
import com.nomagic.magicdraw.openapi.uml.ModelElementsManager;
import com.nomagic.magicdraw.openapi.uml.ReadOnlyElementException;
import com.nomagic.magicdraw.openapi.uml.SessionManager;
import com.nomagic.magicdraw.ui.browser.Node;
import com.nomagic.magicdraw.ui.browser.Tree;
import com.nomagic.magicdraw.uml.BaseElement;
import com.nomagic.uml2.ext.jmi.helpers.ModelHelper;
import com.nomagic.uml2.ext.jmi.helpers.StereotypesHelper;
import com.nomagic.uml2.ext.magicdraw.classes.mdkernel.Comment;
import com.nomagic.uml2.ext.magicdraw.classes.mdkernel.Element;
import com.nomagic.uml2.ext.magicdraw.mdprofiles.Stereotype;
import com.nomagic.uml2.impl.ElementsFactory;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import org.fife.ui.rsyntaxtextarea.RSyntaxTextArea;

/**
 *
 * @author Luai
 */
public class ActionListenerSaveButton implements ActionListener {
    
    public final static String FIXED_STRING_NAME = "MagicWand Code Comment :: ";
    private RSyntaxTextArea txtArea;
    private JButton jbDelete;
    
    public ActionListenerSaveButton (JButton jbDelete) {
        this.jbDelete = jbDelete;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        Tree tree = Application.getInstance().getMainFrame().getBrowser().getContainmentTree();
        
        Node node = tree.getSelectedNode();        
        if (node != null) {
           Object userObject = node.getUserObject();
           if (userObject instanceof BaseElement) {
               
               BaseElement element = (BaseElement) userObject;
               if(element.getObjectParent() == null) return;
             
               //Comment commentExists = CodeAnnotationHelper.getComment(element);
               Comment commentExists = CodeAnnotationHelper.INSTANCE.getComment(element);
               
               ElementsFactory f = Application.getInstance().getProject().getElementsFactory();
               Comment comment = f.createCommentInstance();
               comment.setBody(FIXED_STRING_NAME + " " + element.getHumanName() + "\n\n" + txtArea.getText());
               
               ModelHelper.setElementComment((Element)element, comment);
               
               SessionManager.getInstance().createSession("Create code annotation");
               try {                  
                   if (commentExists != null) {
                       ModelElementsManager.getInstance().removeElement(commentExists);
                   }
                   ModelElementsManager.getInstance().addElement(comment, (Element) element.getObjectParent());
                   Stereotype stereotype = CodeAnnotationHelper.getCodeAnnotationStereotype();
                   StereotypesHelper.addStereotype(comment, stereotype);
                   jbDelete.setEnabled(true);
               } catch (ReadOnlyElementException ex) {
                   
               }
               SessionManager.getInstance().closeSession();
               
           }
        }
    }

    public void setEditor(RSyntaxTextArea txtArea) {
        this.txtArea = txtArea;
    }
    
}
