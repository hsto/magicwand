/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package codeAnnotation.controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JComboBox;
import org.fife.ui.rsyntaxtextarea.RSyntaxTextArea;
import org.fife.ui.rsyntaxtextarea.SyntaxConstants;

/**
 *
 * @author Luai
 */
public class ActionListenerSyntaxComboBox implements ActionListener {

    private RSyntaxTextArea txtArea;
    private String[] syntaxStrings;
    
    public ActionListenerSyntaxComboBox(String[] syntaxStrings) {
        this.syntaxStrings = syntaxStrings;
    }

    @Override
    public void actionPerformed(ActionEvent e) {        
         JComboBox cb = (JComboBox)e.getSource();
         String syntax = (String)cb.getSelectedItem();
         
         if (syntax.equals(syntaxStrings[0])) {
             txtArea.setSyntaxEditingStyle(SyntaxConstants.SYNTAX_STYLE_NONE);
         } else if (syntax.equals(syntaxStrings[1])) {
             txtArea.setSyntaxEditingStyle(SyntaxConstants.SYNTAX_STYLE_C);
         } else if (syntax.equals(syntaxStrings[2])) {
             txtArea.setSyntaxEditingStyle(SyntaxConstants.SYNTAX_STYLE_CPLUSPLUS);
         } else if (syntax.equals(syntaxStrings[3])) {
             txtArea.setSyntaxEditingStyle(SyntaxConstants.SYNTAX_STYLE_CSHARP);
         } else if (syntax.equals(syntaxStrings[4])) {
             txtArea.setSyntaxEditingStyle(SyntaxConstants.SYNTAX_STYLE_JAVA);
         } else if (syntax.equals(syntaxStrings[5])) {
             txtArea.setSyntaxEditingStyle(SyntaxConstants.SYNTAX_STYLE_JAVASCRIPT);
         } else if (syntax.equals(syntaxStrings[6])) {
             txtArea.setSyntaxEditingStyle(SyntaxConstants.SYNTAX_STYLE_MAKEFILE);
         } else if (syntax.equals(syntaxStrings[7])) {
             txtArea.setSyntaxEditingStyle(SyntaxConstants.SYNTAX_STYLE_PHP);
         } else if (syntax.equals(syntaxStrings[8])) {
             txtArea.setSyntaxEditingStyle(SyntaxConstants.SYNTAX_STYLE_PYTHON);
         } else if (syntax.equals(syntaxStrings[9])) {
             txtArea.setSyntaxEditingStyle(SyntaxConstants.SYNTAX_STYLE_RUBY);
         } else if (syntax.equals(syntaxStrings[10])) {
             txtArea.setSyntaxEditingStyle(SyntaxConstants.SYNTAX_STYLE_SCALA);
         } else if (syntax.equals(syntaxStrings[11])) {
             txtArea.setSyntaxEditingStyle(SyntaxConstants.SYNTAX_STYLE_SQL);
         } else if (syntax.equals(syntaxStrings[12])) {
             txtArea.setSyntaxEditingStyle(SyntaxConstants.SYNTAX_STYLE_UNIX_SHELL);
         } else if (syntax.equals(syntaxStrings[13])) {
             txtArea.setSyntaxEditingStyle(SyntaxConstants.SYNTAX_STYLE_WINDOWS_BATCH);
         } else if (syntax.equals(syntaxStrings[14])) {
             txtArea.setSyntaxEditingStyle(SyntaxConstants.SYNTAX_STYLE_XML);
         }        
         
    }

    public void setEditor(RSyntaxTextArea txtArea) {
        this.txtArea = txtArea;
    }
    
}
