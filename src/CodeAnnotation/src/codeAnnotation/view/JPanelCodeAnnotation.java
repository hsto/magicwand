package codeAnnotation.view;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.BorderFactory;
import javax.swing.GroupLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import org.fife.ui.rsyntaxtextarea.RSyntaxTextArea;
import org.fife.ui.rsyntaxtextarea.SyntaxConstants;
import org.fife.ui.rtextarea.RTextScrollPane;

import codeAnnotation.CodeAnnotationHelper;
import codeAnnotation.controller.CodeAnnotationTreeSelectionListener;
import codeAnnotation.controller.ActionListenerDeleteButton;
import codeAnnotation.controller.ActionListenerSaveButton;
import codeAnnotation.controller.ActionListenerSyntaxComboBox;

import com.nomagic.magicdraw.uml.BaseElement;
import com.nomagic.uml2.ext.magicdraw.classes.mdkernel.Comment;
import lib.SharedHelper;
import lib.SharedHierarchyListener;

/**
 *
 * @author Luai
 */
public class JPanelCodeAnnotation extends JPanel {

    private final JPanel innerPanel = new JPanel();
    private JPanel mainPanel = new JPanel();
    private JPanel settingsPanel = new JPanel();
    private final GroupLayout layout = new GroupLayout(innerPanel);

    private final JLabel labelElementName = new JLabel();

    private final JButton foldButtonSettings = new JButton();
    private final JButton foldButtonCode = new JButton();

    private final JButton jbSave = new JButton();
    private final JButton jbDelete = new JButton();

    private CodeAnnotationTreeSelectionListener selectionListener = new CodeAnnotationTreeSelectionListener(this);
    private ActionListenerSyntaxComboBox syntaxActionListener;
    private ActionListenerSaveButton actionListenerSaveButton;

    private JComboBox<String> syntaxList;
    private RSyntaxTextArea txtArea;
    private RTextScrollPane scrollPane;

    private final static String ICON_RIGHT_FILE_NAME = "arrow-right.png";
    private final static String ICON_DOWN_FILE_NAME = "arrow-down.png";
    private final static String FILE_NAME = "link_CodeAnnotation.txt";
    private JLabel logoLabel = new JLabel();

    private ImageIcon imageIconRight;
    private ImageIcon imageIconDown;

    public JPanelCodeAnnotation() {
        initComponents();
        this.addHierarchyListener(new SharedHierarchyListener(this, JPanelCodeAnnotation.this.getClass().getName(), false));
    }

    private void initComponents() {
        logoLabel = SharedHelper.INSTANCE.initLogo();
        SharedHelper.INSTANCE.initLogoLink(logoLabel, getClass().getClassLoader().getResourceAsStream(FILE_NAME));
        imageIconRight = SharedHelper.INSTANCE.initIcon(ICON_RIGHT_FILE_NAME);
        imageIconDown = SharedHelper.INSTANCE.initIcon(ICON_DOWN_FILE_NAME);

        JLabel labelTitel = new JLabel();
        labelTitel.setFont(new Font("", Font.BOLD, 16));
        labelTitel.setText("Code Annotations");
        labelTitel.setMinimumSize(new Dimension(40, 25));
        labelElementName.setFont(new Font("", Font.PLAIN, 14));
        labelElementName.setText("Select element in containment tree");
        labelElementName.setMinimumSize(new Dimension(40, 25));

        Font font = new Font("", Font.BOLD, 14);
        initFoldButton(foldButtonSettings, settingsPanel, "Settings", font);
        initFoldButton(foldButtonCode, mainPanel, "Code", font);

        String[] syntaxStrings = {"Plain", "C", "C++", "C#", "Java", "Javascript", "Makefile", "PHP", "Python", "Ruby", "Scala", "SQL", "Unix Shell", "Windows Batch", "XML"};
        syntaxList = new JComboBox<String>(syntaxStrings);
        syntaxList.setSelectedIndex(4);
        syntaxList.setMaximumSize(new Dimension(15, 15));
        syntaxActionListener = new ActionListenerSyntaxComboBox(syntaxStrings);
        syntaxList.addActionListener(syntaxActionListener);
        syntaxList.setEnabled(false);

        jbSave.setText("Confirm");
        jbSave.setToolTipText("Save code for this element");
        jbSave.setEnabled(false);
        actionListenerSaveButton = new ActionListenerSaveButton(jbDelete);
        jbSave.addActionListener(actionListenerSaveButton);
        jbDelete.setText("Clear");
        jbDelete.setToolTipText("Remove code for this element");
        jbDelete.setEnabled(false);
        jbDelete.addActionListener(new ActionListenerDeleteButton(jbDelete));

        innerPanel.setLayout(layout);
        layout.setAutoCreateGaps(true);
        layout.setAutoCreateContainerGaps(true);

        mainPanel.setLayout(new GridLayout(1, 0));
        jbSave.setVisible(false);
        jbDelete.setVisible(false);

        settingsPanel.setLayout(new GridLayout(1, 0));
        settingsPanel.setMinimumSize(new Dimension(150, 20));
        settingsPanel.setMaximumSize(new Dimension(150, 20));
        settingsPanel.add(syntaxList);

        layout.setHorizontalGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup()
                        .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup()
                                        .addComponent(labelTitel)
                                        .addComponent(labelElementName)
                                //.addComponent(syntaxList)
                                )
                                .addGap(4, 4, Short.MAX_VALUE)
                                .addComponent(logoLabel)
                        )
                        .addComponent(foldButtonSettings)
                        .addComponent(settingsPanel)
                        .addComponent(foldButtonCode)
                        .addComponent(mainPanel)
                        .addGroup(layout.createSequentialGroup()
                                .addComponent(jbSave)
                                .addComponent(jbDelete)
                        )
                )
        );

        layout.setVerticalGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup()
                        .addGroup(layout.createSequentialGroup()
                                .addComponent(labelTitel)
                                .addComponent(labelElementName)					
                        )
                        .addComponent(logoLabel)
                )
                .addComponent(foldButtonSettings)
                .addComponent(settingsPanel)
                .addComponent(foldButtonCode)
                .addComponent(mainPanel)
                .addGroup(layout.createParallelGroup()
                        .addComponent(jbSave)
                        .addComponent(jbDelete)
                )
        );

        setLayout(new BorderLayout());
        add(innerPanel);

    }

    private void initFoldButton(final JButton foldButton, final JPanel panel, final String name, Font font) {
        panel.setVisible(false);
        foldButton.setFont(font);
        SharedHelper.INSTANCE.setIconName(true, foldButton, name, imageIconRight, imageIconDown);
        foldButton.setVisible(true);
        foldButton.setMinimumSize(new Dimension(40, 25));
        foldButton.setMaximumSize(new Dimension(Short.MAX_VALUE, 30));
        foldButton.setContentAreaFilled(false);
        foldButton.setHorizontalAlignment(SwingConstants.LEFT);
        foldButton.setBorder(BorderFactory.createEmptyBorder(4, 1, 4, 4));
        foldButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                if (panel.isVisible()) {
                    panel.setVisible(false);
                    SharedHelper.INSTANCE.setIconName(true, foldButton, name, imageIconRight, imageIconDown);

                    if (name.equals("Code")) {
                        jbSave.setVisible(false);
                        jbDelete.setVisible(false);
                    }
                } else {
                    panel.setVisible(true);
                    SharedHelper.INSTANCE.setIconName(false, foldButton, name, imageIconRight, imageIconDown);
                    if (name.equals("Code")) {
                        jbSave.setVisible(true);
                        jbDelete.setVisible(true);
                    }
                }
            }
        });
    }

    public void setElement(BaseElement element) {
        updatePanel(element.getHumanType(), element.getHumanName());

        Comment comment = CodeAnnotationHelper.INSTANCE.getComment(element);
        if (comment != null) {
            txtArea.setText(CodeAnnotationHelper.getCodeFromComment(comment.getBody()));
            mainPanel.revalidate();
            jbDelete.setEnabled(true);
        } else {
            jbDelete.setEnabled(false);
        }
    }

    private void updatePanel(String type, String humanName) {
        String elementName = humanName.replaceFirst(type + " ", "");
        labelElementName.setText(type + " '" + elementName + "'");

        addCodeEditor();
        jbSave.setEnabled(true);
    }

    /**
     * Source : http://fifesoft.com/rsyntaxtextarea :
     * https://sourceforge.net/projects/rsyntaxtextarea
     */
    private void addCodeEditor() {
        if (scrollPane != null) {
            mainPanel.remove(scrollPane);
        }
        syntaxList.setEnabled(true);

        txtArea = new RSyntaxTextArea(10, 10);
        txtArea.setSyntaxEditingStyle(SyntaxConstants.SYNTAX_STYLE_JAVA);
        txtArea.setCodeFoldingEnabled(true);
        txtArea.setAntiAliasingEnabled(true);
        scrollPane = new RTextScrollPane(txtArea);
        scrollPane.setFoldIndicatorEnabled(true);
        mainPanel.add(scrollPane);
        mainPanel.revalidate();

        syntaxActionListener.setEditor(txtArea);
        actionListenerSaveButton.setEditor(txtArea);

    }
    
    public CodeAnnotationTreeSelectionListener getSelectionListener() {
        return selectionListener;
    }

}
