/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package codeAnnotation;

import codeAnnotation.controller.ActionListenerSaveButton;
import com.nomagic.magicdraw.core.Application;
import com.nomagic.magicdraw.core.Project;
import com.nomagic.magicdraw.openapi.uml.ReadOnlyElementException;
import com.nomagic.magicdraw.uml.BaseElement;
import com.nomagic.magicdraw.uml.InheritanceVisitor;
import com.nomagic.uml2.ext.jmi.helpers.StereotypesHelper;
import com.nomagic.uml2.ext.jmi.reflect.ClassTypes;
import com.nomagic.uml2.ext.jmi.reflect.VisitorContext;
import com.nomagic.uml2.ext.magicdraw.classes.mdkernel.Comment;
import com.nomagic.uml2.ext.magicdraw.classes.mdkernel.Element;
import com.nomagic.uml2.ext.magicdraw.mdprofiles.Stereotype;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Arrays;

/**
 *
 * @author Luai
 */
public enum CodeAnnotationHelper {
    INSTANCE;

    private final static String CODE_ANNOTAION_STEREOTYPE_NAME = "Code Annotation Stereotype";
    
    private static Project project;
    
    private TreeVisitor visitor = new TreeVisitor();
    
    public Comment getComment(BaseElement selectedBaseElement) {
        Comment comment = null;
        if (!(selectedBaseElement instanceof Element)) return null;
        Element element = (Element) selectedBaseElement;
        Element elementParent = (Element) selectedBaseElement.getObjectParent();         
        if (elementParent != null && elementParent.hasOwnedComment()) {
            for (Comment c : elementParent.getOwnedComment()) {                                                             
                if (checkComment(c, element)) {
                    return c;
                }
            }            
        }
        // If it's not foud locally it  serches from the tree
        visitor.selectedElement = element;
        comment = visitTree();

        return comment;
    }
    
    private boolean checkComment(Comment c, Element element) {
        if ( !(c.hasAnnotatedElement() && c.getAnnotatedElement().contains(element)) ) return false;
        
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new StringReader(c.getBody()));
            String id = reader.readLine();
            id = id.replace(ActionListenerSaveButton.FIXED_STRING_NAME, "");
            id = id.trim();
            if (element.getHumanName().trim().equals(id) && c.hasAnnotatedElement() && c.getAnnotatedElement().contains(element)) {
                return true;
            }
        } catch (IOException ex) {
            
        } finally {
        	try {
				if (reader != null) reader.close();
			} catch (IOException e) {
			}
        }
        return false;
    }
    
    private Comment visitTree() {
        visitor.foundComment = null;
        Element root = Application.getInstance().getMainFrame().getBrowser().getContainmentTree().getRootElement();
        ArrayList<Element> all = new ArrayList<Element>();
        all.add(root);
        Element current;
        
        // if current element has children, list will be increased.
        for (int i = 0; i < all.size(); i++) {
            current = all.get(i);
            try {
                // let's perform some action with this element in visitor.
                current.accept(visitor);
                if (visitor.foundComment != null) return visitor.foundComment; // Match found
            } catch (Exception e) {

            }
            // add all children into end of this list, so it emulates recursion.
            all.addAll(current.getOwnedElement());
        }
        return null;
    }
    
    class TreeVisitor extends InheritanceVisitor {
        
        public Element selectedElement;
        public Comment foundComment;
        
        @Override
        public void visitElement(Element element, VisitorContext context) {
            super.visitElement(element, context);
            if(ClassTypes.getShortName(element.getClassType()).equals("Comment")) {
                if (checkComment((Comment)element, selectedElement)) {
                    foundComment = (Comment) element;
                }
            }
        }
        
    }
    
    public static String getCodeFromComment(String str) {   
        String r = str.substring(str.indexOf("\n")+2);        
        return r;
    }
    
    public static void createCodeAnnotationStereotype(Project project) throws ReadOnlyElementException {
        if (StereotypesHelper.getStereotype(project, CODE_ANNOTAION_STEREOTYPE_NAME) == null) {        
            com.nomagic.uml2.ext.magicdraw.classes.mdkernel.Class metaClass = StereotypesHelper.getMetaClassByName(project, "Comment");
            StereotypesHelper.createStereotype(project.getModel(), CODE_ANNOTAION_STEREOTYPE_NAME, Arrays.asList(metaClass));            
        }
    }
    
    public static Stereotype getCodeAnnotationStereotype() {
        return StereotypesHelper.getStereotype(project, CODE_ANNOTAION_STEREOTYPE_NAME);
    }
    
    public static void setProject(Project project) {
        CodeAnnotationHelper.project = project;
    }

}
