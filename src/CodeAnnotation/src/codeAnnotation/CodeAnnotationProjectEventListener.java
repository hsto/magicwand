/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package codeAnnotation;


import codeAnnotation.view.JPanelCodeAnnotation;

import com.nomagic.magicdraw.core.Application;
import com.nomagic.magicdraw.core.Project;
import com.nomagic.magicdraw.core.project.ProjectEventListener;
import com.nomagic.magicdraw.openapi.uml.ReadOnlyElementException;
import com.nomagic.magicdraw.ui.MainFrame;
import com.nomagic.magicdraw.ui.ProjectWindow;
import com.nomagic.magicdraw.ui.ProjectWindowsManager;
import com.nomagic.magicdraw.ui.WindowComponentInfo;
import com.nomagic.magicdraw.ui.WindowsManager;
import com.nomagic.magicdraw.ui.browser.Tree;
import com.nomagic.magicdraw.ui.browser.WindowComponentContent;

import javax.swing.JTree;
import lib.SharedHelper;

/**
 *
 * @author Luai
 */
public class CodeAnnotationProjectEventListener implements ProjectEventListener {

    private Tree tree;
    private JTree jtree;
    public static String pluginName = "Code Annotations";    
    
    private final JPanelCodeAnnotation annotationPanel = new JPanelCodeAnnotation();
    public static String projectWindowId = "";
    
    WindowComponentInfo infoAnnotation = new WindowComponentInfo("CODE ANNOTATION", pluginName, null, WindowsManager.SIDE_EAST, WindowsManager.STATE_DOCKED, false);
    public ProjectWindow windowAnnotation;
    boolean startup = false;
            
    public CodeAnnotationProjectEventListener() {
        
    }
    
    private static String FILE_NAME_STARTUP = System.getProperty("user.home") + "/.magicdraw/MagicWand/startup_Code.txt";
    
    @Override
    public void projectOpened(Project project) {
        // get Documentation panel and 
        tree = Application.getInstance().getMainFrame().getBrowser().getContainmentTree();
        jtree = tree.getTree();
        jtree.addTreeSelectionListener(annotationPanel.getSelectionListener());
        
        // protect documentation pane from direct editing
        Application.getInstance().getMainFrame().getBrowser().getDocPanel().getHTMLTextEditorComponent().setVisible(false);
        Application.getInstance().getMainFrame().getBrowser().getDocPanel().getHTMLEditorPane().setEnabled(false);
        Application.getInstance().getMainFrame().getBrowser().getDocPanel().getHTMLEditorPane().setVisible(false);
        Application.getInstance().getMainFrame().getBrowser().getDocPanel().setEnableHTMLCheckBox(false);
        Application.getInstance().getMainFrame().getBrowser().getDocPanel().setEnabledAll(false);
        

        MainFrame mainFrame = Application.getInstance().getMainFrame();
        ProjectWindowsManager projectWindowsManager = mainFrame.getProjectWindowsManager();
       
        WindowComponentContent contentAnnotation = new CodeAnnotationWindowComponentContent(annotationPanel);
        windowAnnotation = new ProjectWindow(infoAnnotation,contentAnnotation);
        
        startup = SharedHelper.INSTANCE.readStartup(FILE_NAME_STARTUP);
        if (startup) {
                projectWindowsManager.addWindow(windowAnnotation);        
        	projectWindowsManager.activateWindow(windowAnnotation.getId());        	
        }
        
        projectWindowId = windowAnnotation.getId();
        		
        CodeAnnotationHelper.setProject(project);
        try {
            CodeAnnotationHelper.createCodeAnnotationStereotype(project);
        } catch (ReadOnlyElementException ex) {
            
        }
    }

    @Override
    public void projectClosed(Project project) {
       
    }

    @Override
    public void projectSaved(Project project, boolean savedInServer) {
        
    }

    @Override
    public void projectDeActivated(Project project) {
        
    }

    @Override
    public void projectCreated(Project project) {
        
    }

    @Override
    public void projectOpenedFromGUI(Project project) {
               
    }

    @Override
    public void projectActivatedFromGUI(Project project) {
    }

    @Override
    public void projectActivated(Project project) {

    }

    @Override
    public void projectReplaced(Project oldProject, Project newProject) {
        
    }

    @Override
    public void projectPreClosed(Project project) {
        tree = Application.getInstance().getMainFrame().getBrowser().getContainmentTree();
        if (tree != null) {
	        jtree = tree.getTree();
	        if (annotationPanel != null && jtree != null) {
                    jtree.removeTreeSelectionListener(annotationPanel.getSelectionListener());
                }
        }
        
        SharedHelper.INSTANCE.writeStartup(FILE_NAME_STARTUP, annotationPanel);
    }

    @Override
    public void projectPreClosedFinal(Project project) {
        
    }

    @Override
    public void projectPreSaved(Project project, boolean savedInServer) {
        
    }

    @Override
    public void projectPreActivated(Project project) {
        
    }

    @Override
    public void projectPreDeActivated(Project project) {

    }

    @Override
    public void projectPreReplaced(Project oldProject, Project newProject) {

    }
    
}
