/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package codeAnnotation;

import codeAnnotation.view.JPanelCodeAnnotation;
import com.nomagic.magicdraw.ui.browser.WindowComponentContent;
import java.awt.Component;

/**
 *
 * @author Luai
 */
public class CodeAnnotationWindowComponentContent implements WindowComponentContent{
    
    private final JPanelCodeAnnotation panel;
    
    public CodeAnnotationWindowComponentContent(JPanelCodeAnnotation panel) {
        this.panel = panel;
    }

    @Override
    public Component getWindowComponent() {
        return panel;
    }

    @Override
    public Component getDefaultFocusComponent() {
        return panel;
    }
    
}
