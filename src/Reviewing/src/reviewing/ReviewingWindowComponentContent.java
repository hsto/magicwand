package reviewing;

import com.nomagic.magicdraw.ui.browser.WindowComponentContent;
import java.awt.Component;
import reviewing.view.JPanelReviewing;

/**
 *
 * @author Luai
 */
public class ReviewingWindowComponentContent implements WindowComponentContent {

    private final JPanelReviewing panel;
    
    public ReviewingWindowComponentContent(JPanelReviewing panel) {
        this.panel = panel;
    }

    @Override
    public Component getWindowComponent() {
        return panel;
    }

    @Override
    public Component getDefaultFocusComponent() {
        return panel;
    }
    
}
