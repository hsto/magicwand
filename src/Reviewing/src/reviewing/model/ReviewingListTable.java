package reviewing.model;

import reviewing.model.ListTableReviewingModel;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.JCheckBox;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;
import reviewing.view.ButtonGoToEditor;
import reviewing.view.ButtonRenderer;

public class ReviewingListTable {

	private JTable table;
	private JPanel panel = new JPanel();
	private ArrayList<String> ids;

	public ReviewingListTable(JTable table, JPanel mainPanel, ArrayList<Object[]> data, ArrayList<String> ids) {
		this.table = table;
		this.panel = mainPanel;
		this.ids = ids;

		addReviewingListTable(data);
	}

	private void addReviewingListTable(ArrayList<Object[]> dataInput) {
		
		String[] header = new String[]{"", "Inspector", "Path"};
        
        ArrayList<Object[]> data = new ArrayList<Object[]>();
        if (dataInput != null) {
            data.addAll(dataInput);
        }
		
        final ButtonRenderer buttonrenderer = new ButtonRenderer("Go to");
        final ButtonGoToEditor buttonEditor = new ButtonGoToEditor(new JCheckBox());
        final ListTableReviewingModel model = new ListTableReviewingModel(data, header); 
        table = new JTable(model) {
        	
        	@Override
            public boolean isCellEditable(int row, int column) {
        		if(column == 1 || column == 2)
        			return false;        		
        		return true;
        	}
        	
        	@Override
            public TableCellRenderer getCellRenderer(int row, int column) {
                if (column == 0) {
                    return buttonrenderer;
                }                
                return super.getCellRenderer(row, column);
            }
            
            @Override
            public TableCellEditor getCellEditor(int row, int column) {
                if (column == 0) {
                	buttonEditor.setId(ids.get(row));
                    return buttonEditor;
                }
                return super.getCellEditor(row, column);
            }
        	
            @Override
            public String getToolTipText(MouseEvent e) {
                String toolTip = null;
                java.awt.Point p = e.getPoint();
                if (p == null) return toolTip;
                int row = rowAtPoint(p);
                int col = columnAtPoint(p);
                
                if (row >= 0 && col >= 0 && col == 2 && this.getModel().getRowCount() > row) {
                	try {
                		toolTip = (String) getValueAt(row, col);
                	} catch (Exception ex) {
                		
                	}
                }
                
                return toolTip;
            }
        };
        
        getTable().getColumn("").setMinWidth(55);
        getTable().getColumn("").setMaxWidth(55);
        getTable().getColumn("Inspector").setMinWidth(55);
        getTable().getColumn("Inspector").setMaxWidth(140);
        getTable().getColumn("Inspector").setPreferredWidth(60);
        getTable().getColumn("Path").setMinWidth(200);
        getTable().getColumn("Path").setPreferredWidth(250);
        
        getTable().setAutoCreateColumnsFromModel(true);
        
		getTable().setFillsViewportHeight(true);

        panel.add(new JScrollPane(getTable()));
        getTable().setRowHeight(22);
		
	}
	
	public JTable getTable() {
        return table;
    }
	

}
