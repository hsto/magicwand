/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package reviewing.model;

import reviewing.model.ListTableReviewingModel;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.DefaultCellEditor;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import reviewing.view.ButtonEditor;
import reviewing.view.ButtonRenderer;

/**
 *
 * @author Luai
 */
public class ReviewingTable {

    private JTable table;
    private JPanel mainPanel = new JPanel();
    
    public ReviewingTable(JTable table, JPanel mainPanel) {
        this.table = table;
        this.mainPanel = mainPanel;       
        
        addReviewingTable(null);
    }

    public ReviewingTable(JTable table, JPanel mainPanel, ArrayList<Object[]> data) {
        this.table = table;
        this.mainPanel = mainPanel;       
        
        addReviewingTable(data);
    }
    
    private void addReviewingTable(ArrayList<Object[]> dataInput) {
        String[] header = new String[]{"Inspector", "Severity", "Remark"};
                
        ArrayList<Object[]> data = new ArrayList<Object[]>();
        if (dataInput != null) {
            data.addAll(dataInput);
        } else {
            data.add(new Object[]{"","comment", ""});
        }        
        data.add(new Object[]{"", "", ""});
        
        
        final ButtonRenderer buttonrenderer = new ButtonRenderer("+");
        final ButtonEditor buttonEditor = new ButtonEditor(new JCheckBox());
        final ListTableReviewingModel model = new ListTableReviewingModel(data, header);        
        
        table = new JTable(model) {
            @Override
            public boolean isCellEditable(int row, int column) {
                // Add row
                if ((row == this.getModel().getRowCount()-1 && (column == 1 || column == 2)) || 
                        (row != this.getModel().getRowCount()-1 && column == 0) ) {
                    return false;
                }
                return true;
            }
            
            @Override
            public TableCellRenderer getCellRenderer(int row, int column) {
                // Add row
                if (this.getModel().getRowCount() - 1 == row && column == 0) {
                    return buttonrenderer;
                }
                
                return super.getCellRenderer(row, column);
            }
            
            @Override
            public TableCellEditor getCellEditor(int row, int column) {
                // Add row
                if (this.getModel().getRowCount() - 1 == row && column == 0) {
                    buttonEditor.setTable(this);
                    return buttonEditor;
                }
                return super.getCellEditor(row, column);
            }
            
            @Override
            public String getToolTipText(MouseEvent e) {
                String toolTip = null;
                java.awt.Point p = e.getPoint();
                if (p == null) return toolTip;
                int row = rowAtPoint(p);
                int col = columnAtPoint(p);
                
                if (row >= 0 && col >= 0 && col == 2 && this.getModel().getRowCount() > row) {
                	try {
                		toolTip = (String) getValueAt(row, col);
                	} catch (Exception ex) {
                		
                	}
                }
                
                return toolTip;
            }
        };
        
        getTable().getColumn("Inspector").setMinWidth(50);
        getTable().getColumn("Inspector").setMaxWidth(120);
        getTable().getColumn("Inspector").setPreferredWidth(100);
        getTable().getColumn("Severity").setMinWidth(50);
        getTable().getColumn("Severity").setMaxWidth(60);
        getTable().getColumn("Severity").setPreferredWidth(60);
        getTable().getColumn("Remark").setMinWidth(200);
        getTable().getColumn("Remark").setPreferredWidth(250);
        
        TableColumn column1 = getTable().getColumnModel().getColumn(1);
        String[] strings = {"comment", "minor", "major"};
        JComboBox<String> comboBox = new JComboBox<String>(strings);
        column1.setCellEditor(new DefaultCellEditor(comboBox));        
        
        getTable().setFillsViewportHeight(true);

        mainPanel.add(new JScrollPane(getTable()));
        getTable().setRowHeight(22);
        
    }

    public JTable getTable() {
        return table;
    }
    
}
