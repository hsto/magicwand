package reviewing;

import java.util.ArrayList;

import reviewing.view.JPanelReviewing;

import com.nomagic.magicdraw.core.Application;
import com.nomagic.magicdraw.ui.browser.Node;
import com.nomagic.magicdraw.uml.BaseElement;
import com.nomagic.magicdraw.uml.InheritanceVisitor;
import com.nomagic.uml2.ext.magicdraw.classes.mdkernel.Element;

public enum ReviewingSelectionHelper {
	INSTANCE;
	
	private TreeVisitor visitor = new TreeVisitor();
	public JPanelReviewing panel;
	
	public void selectNode(String id) {
		visitor.match_id = id; 
		visitor.foundElement = null;
		
		visitTree();
		
		if (visitor.foundElement == null) {
			javax.swing.JOptionPane.showMessageDialog(panel, "Element doesn't exist!");
			return;
		}
		
		Application.getInstance().getMainFrame().getBrowser().getContainmentTree().openNode(visitor.foundElement);
		
//		Node node = visitTree(id);
//		if (node == null) {
//			javax.swing.JOptionPane.showMessageDialog(panel, "Open some of the path in the containment tree and try again!");
//			return;
//		}
//		
//		Application.getInstance().getMainFrame().getBrowser().getContainmentTree().setSelectedNodes(new Node[] {node});
	}
	
	
	
	
//	private Node visitTree(String id) {
//		
//		Node root = Application.getInstance().getMainFrame().getBrowser().getContainmentTree().getRootNode();
//		
//		ArrayList<Node> all = new ArrayList<Node>();
//		all.add(root);
//		Node current;
//
//		for (int i = 0; i < all.size(); i++) {
//			current = all.get(i);
//			
//			if (current != null) {
//				Object userObject = current.getUserObject();
//				if (userObject instanceof BaseElement) {
//					BaseElement element = (BaseElement) userObject;
//					if (element.getID().equals(id))
//						return current;
//				}
//			}
//			
//			all.addAll(current.getChildren());
//		}
//		return null;
//	}
	
	private void visitTree() {
		Element root = Application.getInstance().getMainFrame().getBrowser().getContainmentTree().getRootElement();
		ArrayList<Element> all = new ArrayList<Element>();
		all.add(root);
		Element current;

		for (int i = 0; i < all.size(); i++) {
			current = all.get(i);
			try {
				current.accept(visitor);				
				if (visitor.foundElement != null) {
					return; // Match found
				}
			} catch (Exception e) {

			}
			all.addAll(current.getOwnedElement());
		}
	}
	
	class TreeVisitor extends InheritanceVisitor {		
		

		public String match_id;
		public BaseElement foundElement;
		
		@Override
		public void visitBaseElement(BaseElement element) throws Exception {
			super.visitBaseElement(element);
			
			if (element.getID().equals(match_id)) {
				foundElement = element;
			}
		}
		
	}

}
