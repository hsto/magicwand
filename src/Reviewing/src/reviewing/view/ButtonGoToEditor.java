/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package reviewing.view;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.DefaultCellEditor;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.JTextArea;

import reviewing.ReviewingSelectionHelper;

/**
 *
 * @author Luai
 */
public class ButtonGoToEditor extends DefaultCellEditor {
    
    protected JButton button;
    private boolean isPushed;
    private String id;

    public ButtonGoToEditor(JCheckBox jCheckBox) {
        super(jCheckBox);
        button = new JButton();
        button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                fireEditingStopped();
            }
        });
    }
    
    @Override
    public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {
        JPanel panel = new JPanel(new BorderLayout());
        button.setText("Go to");
        button.setContentAreaFilled(false);
        isPushed = true;
        panel.add(button, BorderLayout.WEST);
        
        JTextArea field = new JTextArea("");
        field.setEditable(false);

        panel.add(field, BorderLayout.CENTER);
        return panel;
    }

    @Override
    public Object getCellEditorValue() {
        if (isPushed) {
			ReviewingSelectionHelper.INSTANCE.selectNode(id);		        	
        }
        isPushed = false;        
       
        return "";
    }

    @Override
    public boolean stopCellEditing() {
        isPushed = false;
        return super.stopCellEditing();
    }

    @Override
    protected void fireEditingStopped() {
        super.fireEditingStopped();
    }
    
    public void setId(String id) {
    	this.id = id;
    }
}
