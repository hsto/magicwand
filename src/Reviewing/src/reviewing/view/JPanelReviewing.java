/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package reviewing.view;

import reviewing.model.ReviewingTable;
import reviewing.model.ReviewingListTable;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import javax.swing.BorderFactory;
import javax.swing.GroupLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import reviewing.ReviewingHelper;
import reviewing.ReviewingSelectionHelper;
import reviewing.controller.ReviewingTreeSelectionListener;
import reviewing.controller.ActionListenerDeleteButton;
import reviewing.controller.ActionListenerExport;
import reviewing.controller.ActionListenerImport;
import reviewing.controller.ActionListenerSaveButton;

import com.nomagic.magicdraw.uml.BaseElement;
import lib.SharedHelper;
import lib.SharedHierarchyListener;

/**
 *
 * @author Luai
 */
public class JPanelReviewing extends JPanel{
    
    private final JPanel innerPanel = new JPanel();
    private JPanel mainPanel = new JPanel();
    private JPanel reviewsPanel = new JPanel();
    private JPanel scrollPane = new JPanel();
    private final GroupLayout layout = new GroupLayout(innerPanel);
    
    //private final JLabel labelElementName = new JLabel();
    private final JLabel labelElementName = new JLabel();
    private JLabel nameLabel = new JLabel("Reviewer:");
    private JTextField nameField = new JTextField(System.getProperty("user.name"), 20);
    
    
    private final JButton jbSave = new JButton();
    private final JButton jbDelete = new JButton();
    private final JButton jbImport = new JButton();
    private final JButton jbExport = new JButton();
    private final JButton jbExportAll = new JButton();
    
    private final JButton foldbuttonSettings = new JButton("");
    private final JButton foldbuttonReview = new JButton("");
    private final JButton foldbuttonReviewsList = new JButton("");
    private final String foldbuttonSettingsName = "Settings";
    private final String foldbuttonReviewName = "Remarks for this element";
    private final String foldbuttonReviewsListName = "All remarks";
    
    private JTable table;
    private JTable tableReviewList;
    
    private final static String ICON_RIGHT_FILE_NAME = "arrow-right.png";
    private final static String ICON_DOWN_FILE_NAME = "arrow-down.png";
	private final static String FILE_NAME = "link_Reviewing.txt";
	private JLabel logoLabel = new JLabel();
    
	private ImageIcon imageIconRight;
	private ImageIcon imageIconDown;
	
    private ActionListenerSaveButton actionListenerSaveButton;
    
    private ReviewingTreeSelectionListener selectionListener = new ReviewingTreeSelectionListener(this);
    
    public JPanelReviewing() {
        initComponents();
        ReviewingSelectionHelper.INSTANCE.panel = this;
        updateReviewsList();
        this.addHierarchyListener(new SharedHierarchyListener(this, JPanelReviewing.this.getClass().getName(), false));
    }
    
    private void initComponents() {
    	logoLabel = SharedHelper.INSTANCE.initLogo();
        SharedHelper.INSTANCE.initLogoLink(logoLabel, getClass().getClassLoader().getResourceAsStream(FILE_NAME));
        imageIconRight = SharedHelper.INSTANCE.initIcon(ICON_RIGHT_FILE_NAME);
        imageIconDown = SharedHelper.INSTANCE.initIcon(ICON_DOWN_FILE_NAME);
        
        
        JLabel labelTitel = new JLabel();
    	labelTitel.setFont(new Font("",Font.BOLD,16));
    	labelTitel.setText("Inspection Comments");
    	labelTitel.setMinimumSize(new Dimension(40,25));
        labelElementName.setFont(new Font("",Font.PLAIN,14));
        labelElementName.setText("Select element in containment tree");
        labelElementName.setMinimumSize(new Dimension(40,25));        
        
        Font font = new Font("",Font.BOLD,14);
        initFoldButton(foldbuttonSettings, foldbuttonSettingsName, font);
        initFoldButton(foldbuttonReview, foldbuttonReviewName, font);
        initFoldButton(foldbuttonReviewsList, foldbuttonReviewsListName, font);
        
        nameField.setMaximumSize(new Dimension(150, 20));
        
        jbSave.setText("Confirm");
        jbSave.setToolTipText("Confirms reviews for this element");
        actionListenerSaveButton = new ActionListenerSaveButton(jbDelete);
        jbSave.addActionListener(actionListenerSaveButton);
        jbSave.setEnabled(false);
        jbDelete.setText("Clear");
        jbDelete.setToolTipText("Remove all reviews for this element");
        jbDelete.addActionListener(new ActionListenerDeleteButton(jbDelete, this));
        jbDelete.setEnabled(false);
        jbImport.setText("Import");
        jbImport.setToolTipText("Import a CSV file");
        jbImport.addActionListener(new ActionListenerImport(this));
        jbImport.setEnabled(true);
        jbExport.setText("Export table");
        jbExport.setToolTipText("Export reviews in current table to a CSV file");
        jbExport.addActionListener(new ActionListenerExport(this));
        jbExport.setEnabled(false);
        jbExportAll.setText("Export All");
        jbExportAll.setToolTipText("Export all reviews for all elements to a CSV file");
        jbExportAll.addActionListener(new ActionListenerExport(this));
        jbExportAll.setEnabled(false);
        nameLabel.setVisible(false);
		nameField.setVisible(false);
        mainPanel.setVisible(false);	
		jbSave.setVisible(false);
		jbDelete.setVisible(false);
		jbImport.setVisible(false);
		jbExport.setVisible(false);
		jbExportAll.setVisible(false);
        foldbuttonSettings.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
//				if (foldbuttonSettings.getText().equals("+ " + foldbuttonSettingsName)) foldbuttonSettings.setText("- " + foldbuttonSettingsName);
//				else foldbuttonSettings.setText("+ " + foldbuttonSettingsName);
				if (nameLabel.isVisible()) {
                                    SharedHelper.INSTANCE.setIconName(true, foldbuttonSettings, foldbuttonSettingsName, imageIconRight, imageIconDown);
                                } else {
                                    SharedHelper.INSTANCE.setIconName(false, foldbuttonSettings, foldbuttonSettingsName, imageIconRight, imageIconDown);
                                }
				nameLabel.setVisible(!nameLabel.isVisible());
				nameField.setVisible(!nameField.isVisible());
			}
		});
  
        foldbuttonReview.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				if (mainPanel.isVisible()) {
                                    SharedHelper.INSTANCE.setIconName(true, foldbuttonSettings, foldbuttonSettingsName, imageIconRight, imageIconDown);
                                } else {
                                    SharedHelper.INSTANCE.setIconName(false, foldbuttonSettings, foldbuttonSettingsName, imageIconRight, imageIconDown);
                                }
				mainPanel.setVisible(!mainPanel.isVisible());	
				jbSave.setVisible(!jbSave.isVisible());
				jbDelete.setVisible(!jbDelete.isVisible());
				jbImport.setVisible(!jbImport.isVisible());
				jbExport.setVisible(!jbExport.isVisible());
				jbExportAll.setVisible(!jbExportAll.isVisible());
			}
		});
        
        foldbuttonReviewsList.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
//            	if (reviewsPanel.getComponentCount() == 0) {
//            		scrollPane.setVisible(false);
//            		foldbuttonReviewsList.setText("+ " + foldbuttonReviewsListName);
//            		return;
//            	}
            	if (scrollPane.isVisible()) {
                    SharedHelper.INSTANCE.setIconName(true, foldbuttonSettings, foldbuttonSettingsName, imageIconRight, imageIconDown);
                } else {
                    SharedHelper.INSTANCE.setIconName(false, foldbuttonSettings, foldbuttonSettingsName, imageIconRight, imageIconDown);                    
                }
            	scrollPane.setVisible(!scrollPane.isVisible());
            }
        });        
      
        
        scrollPane.setVisible(false);
        scrollPane.setLayout(new GridLayout(1,0));
        innerPanel.setLayout(layout);
        layout.setAutoCreateGaps(true);
        layout.setAutoCreateContainerGaps(true);
        
        mainPanel.setLayout(new GridLayout(1,0));
        
//        scrollPane.setMaximumSize(new Dimension(Short.MAX_VALUE,600));
//        scrollPane.setPreferredSize(new Dimension(200,250));
        scrollPane.setMinimumSize(new Dimension(40,140));
        //mainPanel.setMaximumSize(new Dimension(Short.MAX_VALUE, 300));
        
        reviewsPanel.setLayout(new GridBagLayout());
        JScrollPane pane = new JScrollPane (reviewsPanel);
        pane.getVerticalScrollBar().setUnitIncrement(20);
        
        layout.setHorizontalGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup()
                		.addGroup(layout.createSequentialGroup()
                				.addGroup(layout.createParallelGroup()
                						.addComponent(labelTitel)
                						.addComponent(labelElementName)                						 
                				)
                				.addGap(4, 4, Short.MAX_VALUE)
                				.addComponent(logoLabel)
                		)
                		.addComponent(foldbuttonSettings)
                		.addGroup(layout.createSequentialGroup()
                				.addComponent(nameLabel)
                                .addComponent(nameField)
                        )
                        .addComponent(foldbuttonReview)
                        .addComponent(mainPanel)                    
                        .addGroup(layout.createSequentialGroup()
                                .addComponent(jbSave)
                                .addComponent(jbDelete)
                                .addComponent(jbImport)
                                .addComponent(jbExport)
                                .addComponent(jbExportAll)
                        )
                        .addComponent(foldbuttonReviewsList)
                        .addComponent(scrollPane)
                )     
        );
        
        layout.setVerticalGroup(layout.createSequentialGroup()
        		.addGroup(layout.createParallelGroup()
            			.addGroup(layout.createSequentialGroup()
            					.addComponent(labelTitel)
            					.addComponent(labelElementName)            					  					
            			)        			
                    	.addComponent(logoLabel)
                )
                .addComponent(foldbuttonSettings)
                .addGroup(layout.createParallelGroup()
            			.addComponent(nameLabel)
            			.addComponent(nameField)
            	)
            	.addComponent(foldbuttonReview)
                .addComponent(mainPanel)
                .addGroup(layout.createParallelGroup()
                        .addComponent(jbSave)
                        .addComponent(jbDelete)
                        .addComponent(jbImport)
                        .addComponent(jbExport)
                        .addComponent(jbExportAll)
                )
                .addComponent(foldbuttonReviewsList)
                .addComponent(scrollPane)
                
        );
        
        setLayout(new BorderLayout());
        add(innerPanel);
        
        scrollPane.add(pane);
    }
    
    private void initFoldButton(JButton foldButton, String name, Font font) {
    	foldButton.setFont(font);
        SharedHelper.INSTANCE.setIconName(true, foldButton, name, imageIconRight, imageIconDown);
    	foldButton.setVisible(true);
    	foldButton.setMinimumSize(new Dimension(40,25));
    	foldButton.setMaximumSize(new Dimension(Short.MAX_VALUE, 30));
    	foldButton.setContentAreaFilled(false);
    	foldButton.setHorizontalAlignment(SwingConstants.LEFT);
    	foldButton.setBorder(BorderFactory.createEmptyBorder(4,1,4,4));
    }

    public void setElement(BaseElement element) {
        mainPanel.removeAll();
        mainPanel.revalidate();
        
        updatePanel(element.getHumanType(), element.getHumanName());        
        jbSave.setEnabled(true);
        if (ReviewingHelper.getReviewComment() == null) {
            jbExport.setEnabled(false);
            jbExportAll.setEnabled(false);
        } else {
            jbExport.setEnabled(true);
            jbExportAll.setEnabled(true);
        }
        
        List<String[]> reviews = ReviewingHelper.getReviews(element.getID());
        if (reviews == null || reviews.isEmpty()) {
            jbDelete.setEnabled(false);
        } else {
            updateAndReadReviews(reviews);
            jbDelete.setEnabled(true);
        }
        
        
        updateReviewsList();
    }
    
    private ReviewingTable reviewingTable;
    private void updatePanel(String type, String humanName) {
        String elementName = humanName.replaceFirst(type + " ", "");
        labelElementName.setText(type + " '" + elementName + "'");
        
        reviewingTable = new ReviewingTable(table, mainPanel);
        actionListenerSaveButton.setTable(reviewingTable.getTable());
        actionListenerSaveButton.setNameField(nameField);
    }
    
    private void updateAndReadReviews(List<String[]> reviews) {
        mainPanel.removeAll();
        ArrayList<Object[]> data = new ArrayList<Object[]>();
        for (String[] review : reviews) {
            data.add(new Object[]{review[ReviewingHelper.INDEX_INSPECTOR], review[ReviewingHelper.INDEX_SEVERITY], review[ReviewingHelper.INDEX_REMARK]});
        }
        
        reviewingTable = new ReviewingTable(table, mainPanel, data);
        actionListenerSaveButton.setTable(reviewingTable.getTable());
        actionListenerSaveButton.setNameField(nameField);
        mainPanel.revalidate();
    }
    
    private ReviewingListTable reviewingListTable;
    private void updateReviewsList() {
    	reviewsPanel.removeAll();
    	List<String> reviews = ReviewingHelper.getReviews();
    	List<String> rows = new ArrayList<String>();
    	if (reviews == null || reviews.isEmpty()) return;
    	
    	for (String r : reviews) {
    		String[] str = r.split(";");
    		String reviewer = str[ReviewingHelper.INDEX_INSPECTOR];
    		String path = str[ReviewingHelper.INDEX_PATH];
    		String id = str[ReviewingHelper.ELEMENTID_INDEX];
    		String review = path + ";" + id + ";" + reviewer;
    		
    		if (!rows.contains(review)) rows.add(review);
		}
    	
    	
    	ArrayList<Object[]> data = new ArrayList<Object[]>();
    	ArrayList<String> ids = new ArrayList<String>();
    	for (int i = 0; i < rows.size(); i++) {
    		String review = rows.get(i);
    		String[] str = review.split(";");
    		String path = str[0];
    		String id = str[1];
    		String reviewer = str[2];
    		
    		data.add(new Object[]{"",reviewer,path});
    		ids.add(id);    		
		}
    	
    	reviewingListTable = new ReviewingListTable(tableReviewList, reviewsPanel, data, ids);
    	
    	reviewsPanel.revalidate();
    }
    
    
//    private void updateReviewsList() {
//    	reviewsPanel.removeAll();
//    	List<String> reviews = ReviewingHelper.getReviews();
//    	List<String> paths = new ArrayList<String>();
//    	if (reviews == null || reviews.isEmpty()) return;
//    	
//    	for (String r : reviews) {
//    		String[] str = r.split(";");
//    		String path = str[ReviewingHelper.INDEX_PATH];
//    		String id = str[ReviewingHelper.ELEMENTID_INDEX];
//    		String review = path + ";" + id;
//    		
//    		if (!paths.contains(review)) paths.add(review);
//		}
//    	
//    	
//    	for (int i = 0; i < paths.size(); i++) {
//    		String review = paths.get(i);
//    		String[] str = review.split(";");
//    		String path = str[0];
//    		String id = str[1];
//    		
//    		JButton button = new JButton("Go to");
//    		JLabel label = new JLabel(path);
//    		
//    		button.addActionListener(new ActionListener() {
//				
//				@Override
//				public void actionPerformed(ActionEvent e) {
//					ReviewingSelectionHelper.INSTANCE.selectNode(id);					
//				}
//			});
//    		
//			reviewsPanel.add(button, createGbc(0,i));
//			reviewsPanel.add(label, createGbc(1,i));
//		}
//    	
//    }
    
//    private GridBagConstraints createGbc(int col, int row) {
//        GridBagConstraints gbc = new GridBagConstraints();
//        gbc.gridx = col;
//        gbc.gridy = row;
//        gbc.anchor = GridBagConstraints.WEST;
//        gbc.weightx = (col == 0) ? 0.01 : 1.0;
//        return gbc;
//    }
    
    public ReviewingTreeSelectionListener getSelectionListener() {
        return selectionListener;
    }

    public void setTable(JTable table) {
        this.table = table;
    }
    
}
