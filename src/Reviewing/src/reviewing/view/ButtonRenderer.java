/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package reviewing.view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.border.LineBorder;
import javax.swing.table.TableCellRenderer;

/**
 *
 * @author Luai
 */
public class ButtonRenderer implements TableCellRenderer {
    
    private JButton button = new JButton();
    private String text;
    
    public ButtonRenderer(String text) {
        this.text = text;
    }

    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        JPanel panel = new JPanel(new BorderLayout());
        button.setText(text);
        button.setContentAreaFilled(false);
        
        panel.add(button, BorderLayout.WEST);
        
        JTextArea field = new JTextArea("");
        field.setEditable(false);
        if (isSelected) {
            field.setForeground(table.getSelectionForeground());
            field.setBackground(table.getSelectionBackground());
        } else {
            field.setForeground(table.getForeground());
            field.setBackground(table.getBackground());
        }
        if (hasFocus) {
            field.setBorder(new LineBorder(Color.gray));
        } else {
            field.setBorder(null);
        }

        panel.add(field, BorderLayout.CENTER);
        return panel;
    }
    
}
