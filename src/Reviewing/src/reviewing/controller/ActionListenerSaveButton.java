package reviewing.controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JTable;
import javax.swing.JTextField;

import reviewing.ReviewingHelper;

import com.nomagic.magicdraw.core.Application;
import com.nomagic.magicdraw.ui.browser.Node;
import com.nomagic.magicdraw.ui.browser.Tree;
import com.nomagic.magicdraw.uml.BaseElement;
import com.nomagic.magicdraw.uml.symbols.PresentationElement;
import com.nomagic.uml2.ext.magicdraw.classes.mdkernel.Element;

/**
 *
 * @author Luai
 */
public class ActionListenerSaveButton implements ActionListener {
    
    private JTable table;
    private JButton jbDelete;
    private JTextField nameField;

    public ActionListenerSaveButton(JButton jbDelete) {
        this.jbDelete = jbDelete;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
    	
//    	ReviewingSelectionHelper.INSTANCE.selectNode("_16_6_3ed01a0_1258927979515_990413_6388");
//    	if (true) return;

        Tree tree = Application.getInstance().getMainFrame().getBrowser().getContainmentTree();

        Node node = tree.getSelectedNode();
        if (node != null) {
            Object userObject = node.getUserObject();
            if (userObject instanceof BaseElement) {

                BaseElement element = (BaseElement) userObject;
                String artifactType = "model";
                String elementType = element.getHumanType();
                String elementName = element.getHumanName().replaceFirst(elementType + " ", "");
                String elementId = element.getID();
                String line = "";            
                String diagram = getDiagram(element.getID());

                
                StringBuilder builder = new StringBuilder();
                long id = ReviewingHelper.getAvailableId();

                if (table.getCellEditor() != null) {
                    table.getCellEditor().stopCellEditing();
                }

                String reviewer = nameField.getText();                
                reviewer = reviewer.replaceAll(";", "");
                reviewer = reviewer.replaceAll("\"", "");
                
                //builder.append(HEADER);               
                for (int row = 0; row < table.getModel().getRowCount() - 1; row++) {
                    String sep = ";";
                    String col0 = (String) table.getModel().getValueAt(row, 0);
                    if (!col0.isEmpty()) reviewer = col0;
                    String severity = (String) table.getModel().getValueAt(row, 1);
                    String remark = (String) table.getModel().getValueAt(row, 2);

//                    if(ReviewingHelper.alreadyExist(elementId, reviewer, severity, remark)) continue;
                    
                    String idString = "" + id;                                        
                    String path = ReviewingHelper.getPath(element);
                    
                    if (remark.contains("\"")) {
                        remark = remark.replaceAll("\"", "\"\"");
                    }
                    if (remark.contains(";") || remark.contains("\"\"")) {
                        remark = "\"" + remark + "\"";
                    }

                    if (/*(reviewer.equals("") || reviewer.equals(" ")) &&*/ (remark.equals("") || remark.equals(" "))) continue;                                      
                    
                    String str = idString + sep + artifactType + sep + elementType + sep + elementName + sep + elementId + sep;
                    builder.append(str);
                    str = line + sep + path + sep + diagram + sep;
                    builder.append(str);
                    str = remark + sep + severity + sep + reviewer + "\n";
                    builder.append(str);
                    id++;
                    reviewer = nameField.getText();
                }
                if (!builder.toString().equals("")) {
                    ReviewingHelper.saveReviews(builder.toString(), elementId);
                    jbDelete.setEnabled(true);
                }
            }
        }
    }
    
    private String getDiagram(String id) {
        List<PresentationElement> list = Application.getInstance().getProject().getActiveDiagram().getSelected();
        if (list != null && list.size() > 0) {
            for (PresentationElement e : list) {
                Element element = e.getActualElement();
                if (element.getID().equals(id))
                    return Application.getInstance().getProject().getActiveDiagram().getName();
            }
        }
        return "";
    }
    
    public void setTable(JTable table) {
        this.table = table;
    }

    public void setNameField(JTextField nameField) {
        this.nameField = nameField;
    }
       
}
