/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package reviewing.controller;

import com.nomagic.magicdraw.core.Application;
import com.nomagic.magicdraw.ui.browser.Node;
import com.nomagic.magicdraw.ui.browser.Tree;
import com.nomagic.magicdraw.uml.BaseElement;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import reviewing.ReviewingHelper;
import reviewing.view.JPanelReviewing;

/**
 *
 * @author Luai
 */
public class ActionListenerDeleteButton implements ActionListener {
    
    private JButton jbDelete;
    private JPanelReviewing jPanel;
        
    public ActionListenerDeleteButton(JButton jbDelete, JPanelReviewing jPanel) {
        this.jbDelete = jbDelete;
        this.jPanel = jPanel;
    }    
    
    @Override
    public void actionPerformed(ActionEvent e) {
        
        Tree tree = Application.getInstance().getMainFrame().getBrowser().getContainmentTree();
        
        Node node = tree.getSelectedNode();
        if (node != null) {
           Object userObject = node.getUserObject();
           if (userObject instanceof BaseElement) {
               
               BaseElement baseElement = (BaseElement) userObject;
               ReviewingHelper.deleteReviews(baseElement.getID());
               jPanel.setElement(baseElement);
               jbDelete.setEnabled(false);
           }
        }
        
    }
    
}
