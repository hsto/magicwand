package reviewing.controller;

import com.nomagic.magicdraw.core.Application;
import com.nomagic.magicdraw.ui.browser.Node;
import com.nomagic.magicdraw.ui.browser.Tree;
import com.nomagic.magicdraw.uml.BaseElement;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import reviewing.view.JPanelReviewing;

/**
 *
 * @author Luai
 */
public class ReviewingTreeSelectionListener implements TreeSelectionListener {

    private JPanelReviewing panel;
    private Tree tree;
    
    public ReviewingTreeSelectionListener(JPanelReviewing panel) {
        this.panel = panel;
    }

    @Override
    public void valueChanged(TreeSelectionEvent e) {
    	if (!panel.isShowing()) return;
    	
        tree = Application.getInstance().getMainFrame().getBrowser().getContainmentTree();
        
        Node node = tree.getSelectedNode();
        if (node != null) {
            Object userObject = node.getUserObject();
            if (userObject instanceof BaseElement) {

                BaseElement element = (BaseElement) userObject;
                panel.setElement(element);
            }
        }
    }
    
}
