/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package reviewing.controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.filechooser.FileFilter;

import reviewing.ReviewingHelper;
import reviewing.view.JPanelReviewing;

import com.nomagic.magicdraw.core.Application;
import com.nomagic.magicdraw.ui.browser.Node;
import com.nomagic.magicdraw.ui.browser.Tree;
import com.nomagic.magicdraw.uml.BaseElement;
import com.nomagic.uml2.ext.magicdraw.classes.mdkernel.Comment;
import lib.ExportFileChooser;
import lib.ExportFileFilter;
import lib.SharedHelper;

/**
 *
 * @author Luai
 */
public class ActionListenerExport implements ActionListener {

    JPanelReviewing jPanel;
    
    public ActionListenerExport (JPanelReviewing jPanel) {
        this.jPanel = jPanel;
    }   
    
    @Override
    public void actionPerformed(ActionEvent e) {
        JButton b = (JButton) e.getSource();
        JFileChooser jc = new ExportFileChooser(SharedHelper.ExportType.CSV);
        FileFilter ff = new ExportFileFilter(SharedHelper.ExportType.CSV);
        
        jc.addChoosableFileFilter(ff);
        jc.setFileFilter(ff);
        
        int res = jc.showSaveDialog(jPanel);
        
        if (res == JFileChooser.APPROVE_OPTION) {
            
            File file = jc.getSelectedFile();
            String path = file.getAbsolutePath();
            if (!path.endsWith(".csv")) {
                file = new File(path + ".csv");                
            }
            
            
            try {                
            	BufferedWriter bw = new BufferedWriter(new FileWriter(file));
                if (b.getText().equals("Export")) {
                    String reviews =  getElementReviews();
                    bw.write(reviews);
                    bw.close();
                } else {
                    Comment comment = ReviewingHelper.getReviewComment();
                    bw.write(comment.getBody());
                    bw.close();                    
                }
            } catch (IOException ex) {
            }
            
        }
    }
    
    private String getElementReviews() {
        Tree tree = Application.getInstance().getMainFrame().getBrowser().getContainmentTree();
        
        Node node = tree.getSelectedNode();
        if (node != null) {
            Object userObject = node.getUserObject();
            if (userObject instanceof BaseElement) {

                BaseElement element = (BaseElement) userObject;
                
                return ReviewingHelper.getElementReviews(element.getID());
            }
        }
        return "";
    }
    
}
