/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package reviewing.controller;

import com.nomagic.magicdraw.core.Application;
import com.nomagic.magicdraw.ui.browser.Node;
import com.nomagic.magicdraw.ui.browser.Tree;
import com.nomagic.magicdraw.uml.BaseElement;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JFileChooser;
import javax.swing.filechooser.FileNameExtensionFilter;
import reviewing.ReviewingHelper;
import reviewing.view.JPanelReviewing;

/**
 *
 * @author Luai
 */
public class ActionListenerImport implements ActionListener {

    private JPanelReviewing jPanel;
    
    public ActionListenerImport(JPanelReviewing jPanel) {
        this.jPanel = jPanel;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        JFileChooser jc = new JFileChooser();
        FileNameExtensionFilter ff = new FileNameExtensionFilter("CSV files (*.csv)", "csv");
        jc.setFileFilter(ff);
        
        int res = jc.showOpenDialog(jPanel);
        File file = jc.getSelectedFile();
        
        if (res == JFileChooser.APPROVE_OPTION && file.getName().endsWith(".csv")) {
            try {
                BufferedReader reader = new BufferedReader(new FileReader(file));
                List<String> reviews = ReviewingHelper.getReviews();
                List<String> newReviews = new ArrayList<String>();
                String line;
                while ((line = reader.readLine()) != null) {
                    if (line.equals(ReviewingHelper.HEADER) || line.equals(ReviewingHelper.FIXED_STRING_NAME) || line.equals("")) continue;
                    
                    if (reviews == null) {
                        newReviews.add(line);                                                
                    } else {
                        if (!reviews.contains(line)) {
                            newReviews.add(line);
                        }
                    }                    
                }
                if (!newReviews.isEmpty()) {
                    
                    ReviewingHelper.importReviews(newReviews);
                    updateTable();
                }
                if (reader != null) reader.close();
            } catch (FileNotFoundException ex) {
                
            } catch (IOException ex) {
                
            }
        }
    }
    
    private void updateTable() {
        Tree tree = Application.getInstance().getMainFrame().getBrowser().getContainmentTree();
        
        Node node = tree.getSelectedNode();
        if (node != null) {
            Object userObject = node.getUserObject();
            if (userObject instanceof BaseElement) {

                BaseElement element = (BaseElement) userObject;
                jPanel.setElement(element);
            }
        }
    }
    
}
