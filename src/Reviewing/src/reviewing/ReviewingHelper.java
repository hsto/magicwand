/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package reviewing;

import com.nomagic.magicdraw.core.Application;
import com.nomagic.magicdraw.core.Project;
import com.nomagic.magicdraw.openapi.uml.ModelElementsManager;
import com.nomagic.magicdraw.openapi.uml.ReadOnlyElementException;
import com.nomagic.magicdraw.openapi.uml.SessionManager;
import com.nomagic.magicdraw.uml.BaseElement;
import com.nomagic.uml2.ext.jmi.helpers.ModelHelper;
import com.nomagic.uml2.ext.jmi.helpers.StereotypesHelper;
import com.nomagic.uml2.ext.magicdraw.classes.mdkernel.Comment;
import com.nomagic.uml2.ext.magicdraw.classes.mdkernel.Element;
import com.nomagic.uml2.ext.magicdraw.mdprofiles.Stereotype;
import com.nomagic.uml2.impl.ElementsFactory;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author Luai
 */
public class ReviewingHelper {
    
    public static String HEADER = "ID;ArtifactType;ElementType;ElementName;ElementID;Line;Path;Diagram;Remark;Severity;Inspector";
    private final static String REVIEWING_STEREOTYPE_NAME = "Reviewing Stereotype";
    private static Project project;
    public final static String FIXED_STRING_NAME = "MagicWand Inspection Comment";
    
    public static int ELEMENTID_INDEX = 4;
    public static int INDEX_PATH = 6;
    public static int INDEX_REMARK = 8;
    public static int INDEX_SEVERITY = 9;
    public static int INDEX_INSPECTOR = 10;
    
    public static void saveReviews(String saveStr, String elementId) {        
        
        Stereotype stereotype = getReveiwingStereotype();
        if (stereotype == null){
            try {
                creatReviewingStereotype(project);
                stereotype = getReveiwingStereotype();
            } catch (ReadOnlyElementException ex) {

            }
        }
        
        if (stereotype == null) return;
        Comment comment = getReviewComment(stereotype.getOwner());
        if (comment == null) {
            ElementsFactory f = Application.getInstance().getProject().getElementsFactory();
            comment = f.createCommentInstance();            
            
            SessionManager.getInstance().createSession("Create comment");
            try {
                ModelElementsManager.getInstance().addElement(comment, stereotype.getOwner());
                StereotypesHelper.addStereotype(comment, stereotype);                
                ModelHelper.setElementComment(stereotype.getOwner(), comment);
                comment.setBody(HEADER + "\n" + FIXED_STRING_NAME + "\n" + saveStr);
            } catch (ReadOnlyElementException ex) {

            }
            SessionManager.getInstance().closeSession();
        } else {
            if (!elementId.equals("-1")) {
                deleteReviews(comment, elementId);
            }
            comment.setBody(comment.getBody() + saveStr);            
        }
    }
    
    public static Comment getReviewComment() {
        Comment comment = null;
        Stereotype stereotype = getReveiwingStereotype();
        if (stereotype != null){
            comment = getReviewComment(stereotype.getOwner());                
        }
        return comment;
    }
    
    private static Comment getReviewComment(Element element) {
        if (element != null && element.hasOwnedComment()) {
            for (Comment c : element.getOwnedComment()) {
                if (!c.getBody().contains("\n")) continue;
                String header = c.getBody().substring(0, c.getBody().indexOf("\n"));
                
                if (header.equals(HEADER) && c.hasAnnotatedElement() && c.getAnnotatedElement().contains(element)) {
                    return c;
                }
            
            }
        }        
        
        return null;
    }
    
    public static List<String[]> getReviews(String elementId) {
        Comment comment = getReviewComment();
        if (comment == null) return null;
        Scanner scanner = new Scanner(comment.getBody());
        List<String[]> reviews = new ArrayList<String[]>();
        
        while(scanner.hasNextLine()) {
            String nextLine = scanner.nextLine();
            if (nextLine.equals(HEADER) || nextLine.equals(FIXED_STRING_NAME)) continue;
            String[] decodedLine = decodeLine(nextLine);
            if (decodedLine[ELEMENTID_INDEX].equals(elementId)) {
                reviews.add(decodedLine);
            }            
        }
        if (scanner != null) scanner.close();
        return reviews;
    }
    
    public static List<String> getReviews() {
        Comment comment = getReviewComment();
        if (comment == null) return null;
        Scanner scanner = new Scanner(comment.getBody());
        List<String> reviews = new ArrayList<String>();
        
        while(scanner.hasNextLine()) {
            String nextLine = scanner.nextLine();
            if (nextLine.equals(HEADER) || nextLine.equals(FIXED_STRING_NAME)) continue;
            reviews.add(nextLine);   
        }
        if (scanner != null) scanner.close();
        return reviews;
    }
    
    // In csv form
    public static String getElementReviews(String elementId) {
        Comment comment = ReviewingHelper.getReviewComment();

        StringBuilder builder = new StringBuilder();
        Scanner scanner = new Scanner(comment.getBody());

        while (scanner.hasNextLine()) {
            String nextLine = scanner.nextLine();
            if (nextLine.equals(HEADER)) {
                builder.append(nextLine);
                builder.append("\n");
            } else if (nextLine.equals(FIXED_STRING_NAME)) {
                
            } else {
                String[] decodedLine = decodeLine(nextLine);
                if (decodedLine[ELEMENTID_INDEX].equals(elementId)) {
                    builder.append(nextLine);
                    builder.append("\n");
                }
            }
            
            
        }
        if (scanner != null) scanner.close();
        return builder.toString();
    }
    
    public static void deleteReviews(String elementId) {
        Comment comment = getReviewComment();
        deleteReviews(comment, elementId);
    }
    
    private static void deleteReviews(Comment comment, String elementId) {
        StringBuilder builder = new StringBuilder();
        Scanner scanner = new Scanner(comment.getBody());
        
        while(scanner.hasNextLine()) {
            String nextLine = scanner.nextLine();
            
            if (nextLine.equals(HEADER)) {
                builder.append(HEADER);
                builder.append("\n");
                builder.append(FIXED_STRING_NAME);
                builder.append("\n");
            } else if(nextLine.equals(FIXED_STRING_NAME)) {
                
            } else {
                String[] decodedLine = decodeLine(nextLine);
                if (!decodedLine[ELEMENTID_INDEX].equals(elementId)) {
                    builder.append(nextLine);
                    builder.append("\n");
                }
            }            
        }
        if (scanner != null) scanner.close();
        comment.setBody(builder.toString());
    } 
    
    public static long getAvailableId() {        
        Comment comment = getReviewComment();
        if (comment != null) {
            if (comment.getBody().trim().equals(HEADER)) return 1;
            String lastline = comment.getBody().substring(comment.getBody().trim().lastIndexOf("\n")).trim();
            if (!lastline.equals("") && !lastline.equals(HEADER)) {
                String[] strings = decodeLine(lastline);
                return Long.parseLong(strings[0]) + 1;
            }
        }

        return 1;
    }
    
    private static String[] decodeLine(String str) {
        str = str.replaceFirst(";\"", ";");
        str = replaceLast(str, "\";", ";");
        str = str.replaceAll("\"\"", "\"");
        
        String[] temp = str.split(";");
        String[] strings = new String[11];
        strings[0] = temp[0];
        strings[1] = temp[1];
        strings[2] = temp[2];
        strings[3] = temp[3];
        strings[4] = temp[4];
        strings[5] = temp[5];
        strings[6] = temp[6];
        strings[7] = temp[7];
        strings[8] = "";
        
        for (int i = 8; i < temp.length - 2; i++) {
            if (i == temp.length - 3) strings[8] += temp[i];
            else strings[8] += temp[i] + ";";
        }
        strings[9] = temp[temp.length-2];
        strings[10] = temp[temp.length-1]; 
        
        return strings;
    }
    
    public static void importReviews(List<String> reviews) {
        StringBuilder builder = new StringBuilder();
        long id = getAvailableId();
        
        for (String r : reviews) {
            String str;
            if (r.substring(0,1).equals(";")) {
                str = id + r + "\n";
            } else {
                str = id + r.substring(r.indexOf(";")) + "\n";
            }
            
            builder.append(str);
            id++;
        }
        
        Comment comment = getReviewComment();
        if (comment == null) {
            saveReviews("", "-1");
            comment = getReviewComment();
        }
        String body = comment.getBody();
        String newBody = body + builder.toString();
        comment.setBody(newBody);
    }
    
//    public static boolean alreadyExist(String elementId, String reviewer, String severity, String remark) {
//        List<String[]> reviews = getReviews(elementId);
//        if (reviews == null) return false;
//        
//        for (String[] s : reviews) {
//            if (s[INDEX_INSPECTOR].equals(reviewer) && s[INDEX_SEVERITY].equals(severity) && s[INDEX_REMARK].equals(remark))
//                return true;
//        }        
//        return false;
//    }
    
    public static String replaceLast(String str, String target, String replacement) {
        int lastIndex = str.lastIndexOf(target);
        
        if (lastIndex < 0) {
            return str;
        } else {
            return str.substring(0,lastIndex) + str.substring(lastIndex).replaceFirst(target, replacement);
        }
    }
    
    public static String getPath(BaseElement baseElement) {
        String str = "";
        List<String> pathNames = getPathNames(baseElement);
        Collections.reverse(pathNames);
        for (String s : pathNames) {
            str += s + "::";
        }
        str = replaceLast(str, "::", "");
        return str;
    }
    
    private static List<String> getPathNames(BaseElement baseElement) {
        List<String> names = new ArrayList<String>();
        
        if (baseElement.getObjectParent() == null) {
            names.add(baseElement.getHumanName().replaceFirst(baseElement + " ", ""));
            return names;
        } else {
            names.add(baseElement.getHumanName().replaceFirst(baseElement + " ", ""));
            names.addAll(getPathNames(baseElement.getObjectParent()));
        }
        
        return names;
    }

    public static void creatReviewingStereotype(Project project) throws ReadOnlyElementException {
        if (StereotypesHelper.getStereotype(project, REVIEWING_STEREOTYPE_NAME) == null) {        
            com.nomagic.uml2.ext.magicdraw.classes.mdkernel.Class metaClass = StereotypesHelper.getMetaClassByName(project, "Comment");
            StereotypesHelper.createStereotype(project.getModel(), REVIEWING_STEREOTYPE_NAME, Arrays.asList(metaClass));            
        }

    }
    
    public static Stereotype getReveiwingStereotype() {
        return StereotypesHelper.getStereotype(project, REVIEWING_STEREOTYPE_NAME);
    }
    
    public static void setProject(Project project) {
        ReviewingHelper.project = project;
                
    }    
    
}