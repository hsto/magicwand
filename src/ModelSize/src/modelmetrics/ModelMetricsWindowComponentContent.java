package modelmetrics;

import com.nomagic.magicdraw.ui.browser.WindowComponentContent;
import java.awt.Component;
import modelmetrics.view.JPanelModelMetrics;

/**
 *
 * @author Luai
 */
public class ModelMetricsWindowComponentContent implements WindowComponentContent {

    private final JPanelModelMetrics panel;
    
    public ModelMetricsWindowComponentContent(JPanelModelMetrics panel) {
        this.panel = panel;
    }

    @Override
    public Component getWindowComponent() {
        return panel;
    }

    @Override
    public Component getDefaultFocusComponent() {
        return panel;
    }
    
}
