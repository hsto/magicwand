package modelmetrics;

import java.util.Arrays;
import java.util.List;

public class Resources {

	public static List<String> abstractMetaClasses = Arrays
			.asList("ExecutableNode", "ActivityGroup", "ActivityNode",
					"FinalNode", "ControlNode", "Event", "ObjectNode", "Node",
					"Feature", "LiteralSpecification", "PackageableElement",
					"RedefinableElement", "Relationship", "StructuralFeature",
					"StructuredClassifier", "Type", "TypedElement",
					"ValueSpecification", "Vertex", "Behavior",
					"BehavioralFeature", "BehavioredClassifier", "Classifier",
					"DirectedRelationship", "Element",
					"EncapsulatedClassifier", "MultiplicityElement",
					"NamedElement", "Namespace", "ActivityEdge",
					"ExecutionSpecification", "InteractionFragment",
					"DeployedArtifact", "DeploymentTarget", "Model",
					"parameterablelement", "TemplateableElement",
					"Observation", "ConnectableElement", "MessageEvent",
					"Vertex", "ExecutableNode", "Action", "CallAction",
					"InvocationAction", "LinkAction", "Pin",
					"StructuralFeatureAction", "VariableAction",
					"WriteLinkAction", "WriteStructuralFeatureAction",
					"WriteVariableAction", "MessageEnd");

	public static List<String> majorMetaClasses = Arrays.asList("Action", "TimeConstraint",
			"InteractionConstraint", "Constraint", "ControlNode",
			"AnyReceiveEvent", "ChangeEvent", "MessageEvent", "CallEvent",
			"TimeEvent", "SignalEvent", "Event", "ObjectFlow", "ControlFlow",
			"ConsiderIgnoreFragment", "CombinedFragment", "EnumerationLiteral",
			"LiteralBoolean", "LiteralInteger", "LiteralUnlimitedNatural",
			"LiteralString", "ObjectNode", "AssociationClass", "Deployment",
			"Device", "ExceptionHandler", "ExecutionOccurrenceSpecification",
			"Expression", "Extend", "InterruptibleActivityRegion",
			"MessageEnd", "OccurrenceSpecification", "PackageMerge",
			"TimeInterval", "TimeObservation", "Artifact", "Continuation",
			"ExecutionEnvironment", "ExtensionPoint", "GeneralOrdering",
			"InterfaceRealization", "InteractionUse", "Reception",
			"Collaboration", "ProtocolStateMachine", "Interaction",
			"ElementImport", "TimeExpression", "Actor", "InteractionOperand",
			"StateMachine", "Lifeline", "PackageImport", "PrimitiveType",
			"Profile", "Signal", "StateInvariant", "Dependency",
			"ProtocolTransition", "Region", "Connector", "DataType", "Message",
			"Pseudostate", "Component", "Port", "ConnectorEnd", "UseCase",
			"Activity", "Enumeration", "State",
			"MessageOccurrenceSpecification", "Trigger", "Include",
			"Transition", "Package", "ActivityPartition", "Association",
			"Comment", "Operation", "Stereotype", "Parameter", "Class",
			"Generalization", "InstanceSpecification", "Property", "Slot",
			"InteractionFragment", "Flow", "Literal",
			// Diagrams
			"Class Diagram", "Use Case Diagram", "Sequence Diagram",
			"Activity Diagram",	"State Machine Diagram", "Component Diagram",
			"Object Diagram", "Package Diagram", "Deployment Diagram",
			"Communication Diagram", "Time Diagram", "Composite Structure Diagram"			
			);

	public static List<String> notIncluded = Arrays.asList("ElementValue", "Image",
			"Interface", "Diagram");

	// + ObjectFlow + ControlFlow + (InformationFlow) => Flow
	// + ConsiderIgnoreFragment + CombinedFragment => InteractionFragment ++
	/*
	 * EnumerationLiteral LiteralBoolean LiteralInteger LiteralUnlimitedNatural
	 * LiteralString LiteralNull LiteralReal => Literal
	 */
	public static List<String> roots = Arrays.asList("Action", "ObjectNode", "ControlNode",
			"ActivityNode", "Constraint", "Event", "InteractionFragment");

}
