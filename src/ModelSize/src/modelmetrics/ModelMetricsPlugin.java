/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelmetrics;

import java.awt.event.ActionEvent;

import com.nomagic.magicdraw.actions.ActionsConfiguratorsManager;
import com.nomagic.magicdraw.actions.MDAction;
import com.nomagic.magicdraw.core.Application;
import com.nomagic.magicdraw.ui.MainFrame;
import com.nomagic.magicdraw.ui.ProjectWindowsManager;
import lib.MainMenuConfigurator;

/**
 *
 * @author Luai
 */
public class ModelMetricsPlugin extends com.nomagic.magicdraw.plugins.Plugin {

    private ModelMetricsProjectEventListener projectEventListener;
    
    @Override
    public void init() {
        projectEventListener = new ModelMetricsProjectEventListener();
        Application.getInstance().addProjectEventListener(projectEventListener);
        
        ActionsConfiguratorsManager manager = ActionsConfiguratorsManager.getInstance();
        manager.addMainMenuConfigurator(new MainMenuConfigurator(new SimpleAction(null, ModelMetricsProjectEventListener.pluginName)));
    }

    @Override
    public boolean close() {
        return true;
    }

    @Override
    public boolean isSupported() {
        return true;
    }
    
    class SimpleAction extends MDAction {
    	
    	public SimpleAction(String id, String name) {
    		super(id, name, null, null);
    	}
    	
    	@Override
    	public void actionPerformed(ActionEvent e) {
            MainFrame mainFrame = Application.getInstance().getMainFrame();
            ProjectWindowsManager projectWindowsManager = mainFrame.getProjectWindowsManager();
            if (!projectEventListener.startup) {
                projectWindowsManager.addWindow(projectEventListener.window);
                projectEventListener.startup = true;
            }
            projectWindowsManager.activateWindow(ModelMetricsProjectEventListener.projectWindowId);
    	}
    	
    }
}
