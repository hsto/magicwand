/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelmetrics.view;

import java.awt.Graphics;
import java.awt.Insets;
import javax.swing.plaf.basic.BasicProgressBarUI;

/**
 *
 * @author Luai
 */
public class CustomProgressUI extends BasicProgressBarUI {

    @Override
    protected void paintString(Graphics g, int x, int y, int width, int height, int amountFull, Insets b) {
        int strLength = progressBar.getString().length();
        int factor = 10;
        if (strLength < 3) {
            factor = 10;
        } else if (strLength > 2 && strLength < 6) {
            factor = 8;
        } else if (strLength > 5) {
            factor = 7;
        }

        int i = (strLength == 1) ? amountFull * 2 + (factor * strLength) + 6 : amountFull * 2 + (factor * strLength);
        int j = (width * 2) - (amountFull * 2);
        if (j < (strLength * 12) + 10) {
            if (strLength < 3) {
                i -= 10 + (factor * strLength) + 12;
            } else if (strLength > 2 && strLength < 7) {
                i -= 10 + ((factor + 4) * strLength) + 12;
            } else if (strLength > 6) {
                i -= 10 + ((factor + 5) * strLength) + 12;
            }
        }
        super.paintString(g, x, y, i, height, amountFull, b);
    }
}
