package modelmetrics.view;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.swing.BorderFactory;
import javax.swing.GroupLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;

import modelmetrics.ModelMetricsHelper;
import modelmetrics.controller.ModelMetricsTreeSelectionListener;
import modelmetrics.Resources;
import modelmetrics.controller.ActionListenerExport;
import modelmetrics.controller.ActionListenerPrint;

import com.nomagic.magicdraw.uml.ClassTypes;
import com.nomagic.magicdraw.uml.DiagramType;
import com.nomagic.uml2.ext.magicdraw.classes.mdkernel.Element;

import eu.hansolo.custom.SteelCheckBox;
import lib.SharedHelper;
import lib.SharedHierarchyListener;
import model.ActionClass;
import model.Count;

/**
 *
 * @author Luai
 */
public class JPanelModelMetrics extends JPanel {
    
    private final JPanel innerPanel = new JPanel();
    private JPanel mainPanel = new JPanel();
    private JPanel settingsPanelLeft = new JPanel();
    private JPanel settingsPanelRight = new JPanel();
    private JPanel mainPanelContainer = new JPanel();
    private JPanel chartPanel = new JPanel();
    private JPanel gridPanel = new JPanel();
    private JPanel gridPanelContainer = new JPanel(new BorderLayout());
    private final GroupLayout layout = new GroupLayout(innerPanel);    
    
    
    private final JLabel labelElementName = new JLabel();
    
    private JLabel modelValueLabel = new JLabel();
    private JLabel diagramValueLabel = new JLabel();
    private JTextField txtFieldScale = new JTextField();
    
    private final JButton foldButtonSettings = new JButton();
	private final JButton foldButtonMeasurements = new JButton();
    
    private ModelMetricsTreeSelectionListener selectionListener = new ModelMetricsTreeSelectionListener(this);
    private ModelMetricsHelper helper = new ModelMetricsHelper(this);
    private List<Element> elements;
    
    public SteelCheckBox diagramsCheckBox = new SteelCheckBox();
    public SteelCheckBox elementsCheckBox = new SteelCheckBox();
    public SteelCheckBox scaleCheckBox = new SteelCheckBox();
    public SteelCheckBox sortCheckBox = new SteelCheckBox();
    private SteelCheckBox librariesCheckBox = new SteelCheckBox();
    public SteelCheckBox emptyCheckBox = new SteelCheckBox();
    public SteelCheckBox filterCheckBox = new SteelCheckBox();
    public SteelCheckBox groupCheckBox = new SteelCheckBox();
    public SteelCheckBox recursiveCheckBox = new SteelCheckBox();
   
    private JButton exportButton;
    private JButton printButton;
    private ActionListenerExport actionListenerExport = new ActionListenerExport(this);

    private final static String ICON_RIGHT_FILE_NAME = "arrow-right.png";
    private final static String ICON_DOWN_FILE_NAME = "arrow-down.png";
    private final static String FILE_NAME = "link_ModelSize.txt";
    private JLabel logoLabel = new JLabel();
	
    private ImageIcon imageIconRight;
    private ImageIcon imageIconDown;
	
    public JPanelModelMetrics() {
        initComponents();
        this.addHierarchyListener(new SharedHierarchyListener(this, JPanelModelMetrics.this.getClass().getName(), false));
    }
    
    private void initComponents() {
    	logoLabel = SharedHelper.INSTANCE.initLogo();
        SharedHelper.INSTANCE.initLogoLink(logoLabel, getClass().getClassLoader().getResourceAsStream(FILE_NAME));
        imageIconRight = SharedHelper.INSTANCE.initIcon(ICON_RIGHT_FILE_NAME);
        imageIconDown = SharedHelper.INSTANCE.initIcon(ICON_DOWN_FILE_NAME);
    	
    	JLabel labelTitel = new JLabel();
    	labelTitel.setFont(new Font("",Font.BOLD,16));
    	labelTitel.setText("Model Size Metrics");
    	labelTitel.setMinimumSize(new Dimension(40,25));
        labelElementName.setFont(new Font("",Font.PLAIN,14));
        labelElementName.setText("Select element in containment tree");
        labelElementName.setMinimumSize(new Dimension(40,25));
        
        Font font = new Font("",Font.BOLD,14);
        //initFoldButton(foldButtonSettings, mainPanel, "Settings", font);
        initFoldButton(foldButtonSettings, mainPanelContainer, "Settings", font);
        initFoldButton(foldButtonMeasurements, chartPanel, "Measurements", font);
        
        //textArea.setEditable(false);
        //textArea.setFont(new Font("Monospaced",Font.PLAIN,11));
        //JScrollPane pane = new JScrollPane (textArea);
        gridPanel.setLayout(new GridBagLayout());
        JScrollPane pane = new JScrollPane (gridPanelContainer);
        //JScrollPane pane = new JScrollPane (gridPanel);
        pane.getVerticalScrollBar().setUnitIncrement(20);
        
        JLabel diagramsLabel = new JLabel("Diagrams  ");
        JLabel elementsLabel = new JLabel ("Elements  ");
        JLabel sortLabel = new JLabel("Sort  ");
        JLabel groupLabel = new JLabel("Group  ");
        JLabel filterLabel = new JLabel("Secondary  ");        
        JLabel fitLabel = new JLabel("Scale  ");
        JLabel recursiveLabel = new JLabel("Recurse  ");        
        JLabel emptyLabel = new JLabel("Empty  ");
        JLabel labelLibraries = new JLabel("Libraries  ");
        
        Font boldFont = new Font("", Font.BOLD, 12);
        diagramsLabel.setFont(boldFont);
        elementsLabel.setFont(boldFont);
        sortLabel.setFont(boldFont);
        groupLabel.setFont(boldFont);
        filterLabel.setFont(boldFont);
        fitLabel.setFont(boldFont);
        recursiveLabel.setFont(boldFont);
        emptyLabel.setFont(boldFont);
        labelLibraries.setFont(boldFont);
        
        diagramsLabel.setHorizontalAlignment(SwingConstants.TRAILING);
        elementsLabel.setHorizontalAlignment(SwingConstants.TRAILING);
        sortLabel.setHorizontalAlignment(SwingConstants.TRAILING);
        fitLabel.setHorizontalAlignment(SwingConstants.TRAILING);
        emptyLabel.setHorizontalAlignment(SwingConstants.TRAILING);
        filterLabel.setHorizontalAlignment(SwingConstants.TRAILING);
        groupLabel.setHorizontalAlignment(SwingConstants.TRAILING);
        recursiveLabel.setHorizontalAlignment(SwingConstants.TRAILING);
        modelValueLabel.setHorizontalAlignment(SwingConstants.TRAILING);
        diagramValueLabel.setHorizontalAlignment(SwingConstants.TRAILING);
        labelLibraries.setHorizontalAlignment(SwingConstants.TRAILING);
        
        Font fontState = new Font("Arial Italic", Font.PLAIN, 12);
        diagramsCheckBox.setText("On");
        diagramsCheckBox.setFont(fontState);
        diagramsCheckBox.setSelected(true);
        diagramsCheckBox.setBorder(BorderFactory.createEmptyBorder(0,0,0,13));
        diagramsCheckBox.addActionListener(new UpdateController(diagramsCheckBox, "On", "Off"));
        elementsCheckBox.setText("On");
        elementsCheckBox.setFont(fontState);
        elementsCheckBox.setSelected(true);
        elementsCheckBox.setBorder(BorderFactory.createEmptyBorder(0,0,0,13));
        elementsCheckBox.addActionListener(new UpdateController(elementsCheckBox, "On", "Off"));
        scaleCheckBox.setText("All");
        scaleCheckBox.setFont(fontState);
        scaleCheckBox.setSelected(false);
        scaleCheckBox.setBorder(BorderFactory.createEmptyBorder(0,0,0,13));
        scaleCheckBox.addActionListener(new UpdateController(scaleCheckBox, "Fix", "All"));
        sortCheckBox.setText("Name");
        sortCheckBox.setFont(fontState);
        sortCheckBox.setSelected(true);
        sortCheckBox.setBorder(BorderFactory.createEmptyBorder(0,0,0,13));
        sortCheckBox.addActionListener(new UpdateController(sortCheckBox, "Name", "Count"));
        emptyCheckBox.setText("Show");
        emptyCheckBox.setFont(fontState);
        emptyCheckBox.setSelected(true);
        emptyCheckBox.setBorder(BorderFactory.createEmptyBorder(0,0,0,20));
        emptyCheckBox.addActionListener(new UpdateController(emptyCheckBox, "Show", "Hide  "));
        filterCheckBox.setText("Hide  ");
        filterCheckBox.setFont(fontState);
        filterCheckBox.setSelected(false);
        filterCheckBox.setBorder(BorderFactory.createEmptyBorder(0,0,0,20));
        filterCheckBox.addActionListener(new UpdateController(filterCheckBox, "Show", "Hide  "));
        groupCheckBox.setText("On");
        groupCheckBox.setFont(fontState);
        groupCheckBox.setSelected(true);
        groupCheckBox.setBorder(BorderFactory.createEmptyBorder(0,0,0,20));
        groupCheckBox.addActionListener(new UpdateController(groupCheckBox, "On", "Off"));
        recursiveCheckBox.setText("On");
        recursiveCheckBox.setFont(fontState);
        recursiveCheckBox.setSelected(true);
        recursiveCheckBox.setBorder(BorderFactory.createEmptyBorder(0,0,0,20));
        recursiveCheckBox.addActionListener(new UpdateController(recursiveCheckBox, "On", "Off"));
        librariesCheckBox.setSelected(false);
        librariesCheckBox.setText("Exclude");
        librariesCheckBox.setActionCommand("librariesCheckBox");
        librariesCheckBox.setBorder(BorderFactory.createEmptyBorder(0,0,0,20));
        librariesCheckBox.setFont(fontState);
        librariesCheckBox.addActionListener(new UpdateController(librariesCheckBox, "Include", "Exclude"));
        
        exportButton = new JButton("Export");
        exportButton.addActionListener(actionListenerExport);
        printButton = new JButton("Print");
        printButton.addActionListener(new ActionListenerPrint(gridPanel));
        printButton.setToolTipText("Print as PNG");
        
        exportButton.setVisible(false);
        printButton.setVisible(false);
        
        txtFieldScale.setText("150");
        txtFieldScale.setEnabled(false);
        txtFieldScale.setMinimumSize(new Dimension(40, 20));
        txtFieldScale.setMaximumSize(new Dimension(40, 20));
        txtFieldScale.setPreferredSize(new Dimension(40, 20));
        txtFieldScale.setHorizontalAlignment(JTextField.RIGHT);
        txtFieldScale.setToolTipText("Press unter to update");
        txtFieldScale.addActionListener(new UpdateController(null, null, null));
        
        innerPanel.setLayout(layout);
        layout.setAutoCreateGaps(true);
        layout.setAutoCreateContainerGaps(true);
        
        chartPanel.setLayout(new GridLayout(1,0));
        
        GroupLayout mainPanelLayout = new GroupLayout(mainPanel);
        mainPanel.setLayout(mainPanelLayout);        
//        mainPanelLayout.setAutoCreateGaps(true);
//        mainPanelLayout.setAutoCreateContainerGaps(true);
        settingsPanelLeft.setLayout(new GridBagLayout());
        settingsPanelRight.setLayout(new GridBagLayout());        
        
        mainPanel.setMaximumSize(new Dimension(370,147));
        mainPanel.setMinimumSize(new Dimension(370,147));
        mainPanel.setPreferredSize(new Dimension(370,147));
        JScrollPane mainPanelScrollPane = new JScrollPane (mainPanel);
        mainPanelScrollPane.setBorder(BorderFactory.createEmptyBorder());
        mainPanelScrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER);
        mainPanelContainer.setMinimumSize(new Dimension(40,147));
        mainPanelContainer.setMaximumSize(new Dimension(Short.MAX_VALUE, 147));
        mainPanelContainer.setLayout(new BorderLayout());
        mainPanelContainer.add(mainPanelScrollPane, BorderLayout.CENTER);
        
        settingsPanelLeft.setMaximumSize(new Dimension(200,128));
        settingsPanelLeft.setMinimumSize(new Dimension(200,128));
        settingsPanelLeft.setPreferredSize(new Dimension(200,128));
        settingsPanelRight.setMaximumSize(new Dimension(200,100));
        settingsPanelRight.setMinimumSize(new Dimension(200,100));
        settingsPanelRight.setPreferredSize(new Dimension(200,100));
        
        settingsPanelLeft.add(diagramsLabel, createGbcSettingsPanel(0, 0));
        settingsPanelLeft.add(diagramsCheckBox, createGbcSettingsPanel(1, 0));
        settingsPanelLeft.add(diagramValueLabel, createGbcSettingsPanel(2, 0));        
        settingsPanelLeft.add(elementsLabel, createGbcSettingsPanel(0, 1));
        settingsPanelLeft.add(elementsCheckBox,createGbcSettingsPanel(1, 1));
        settingsPanelLeft.add(modelValueLabel, createGbcSettingsPanel(2, 1));        
        settingsPanelLeft.add(fitLabel, createGbcSettingsPanel(0, 2));
        settingsPanelLeft.add(scaleCheckBox, createGbcSettingsPanel(1, 2));
        settingsPanelLeft.add(txtFieldScale, createGbcSettingsPanel(2, 2));
        settingsPanelLeft.add(sortLabel, createGbcSettingsPanel(0, 3));
        settingsPanelLeft.add(sortCheckBox, createGbcSettingsPanel(1, 3));
        settingsPanelLeft.add(labelLibraries, createGbcSettingsPanel(0, 4));
        settingsPanelLeft.add(librariesCheckBox, createGbcSettingsPanel(1, 4));
        
        settingsPanelRight.add(emptyLabel, createGbcSettingsPanel(0, 0));
        settingsPanelRight.add(emptyCheckBox, createGbcSettingsPanel(1, 0));
        settingsPanelRight.add(filterLabel, createGbcSettingsPanel(0, 1));
        settingsPanelRight.add(filterCheckBox, createGbcSettingsPanel(1, 1));
        settingsPanelRight.add(groupLabel, createGbcSettingsPanel(0, 2));
        settingsPanelRight.add(groupCheckBox, createGbcSettingsPanel(1, 2));
        settingsPanelRight.add(recursiveLabel, createGbcSettingsPanel(0, 3));
        settingsPanelRight.add(recursiveCheckBox, createGbcSettingsPanel(1, 3));
        
        mainPanelLayout.setHorizontalGroup(mainPanelLayout.createSequentialGroup()
        		.addGap(10)
        		.addComponent(settingsPanelLeft)
        		.addGap(20)
        		.addComponent(settingsPanelRight)
        );
        
        mainPanelLayout.setVerticalGroup(mainPanelLayout.createParallelGroup()
        		.addComponent(settingsPanelLeft)
        		.addComponent(settingsPanelRight)
        );
        
        layout.setHorizontalGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup()
                		.addGroup(layout.createSequentialGroup()
                				.addGroup(layout.createParallelGroup()
                						.addComponent(labelTitel)
                						.addComponent(labelElementName)
                				)
                				.addGap(4, 4, Short.MAX_VALUE)
                				.addComponent(logoLabel)
                		)
                		.addComponent(foldButtonSettings)
                        //.addComponent(mainPanel)
                		.addComponent(mainPanelContainer)
                        .addComponent(foldButtonMeasurements)
                        .addGroup(layout.createSequentialGroup()
                        		.addComponent(exportButton)
                        		.addComponent(printButton)
                        )
                        .addComponent(chartPanel)
                )     
        );
        
        layout.setVerticalGroup(layout.createSequentialGroup()
        		.addGroup(layout.createParallelGroup()
            			.addGroup(layout.createSequentialGroup()
            					.addComponent(labelTitel)
            					.addComponent(labelElementName)     					
            			)        			
                    	.addComponent(logoLabel)
                 )
                 .addComponent(foldButtonSettings)
                //.addComponent(mainPanel)
                 .addComponent(mainPanelContainer)
                .addComponent(foldButtonMeasurements)
                .addGroup(layout.createParallelGroup()
                		.addComponent(exportButton)
                		.addComponent(printButton)
		        )
                .addComponent(chartPanel)
        );
        
        setLayout(new BorderLayout());
        add(innerPanel);
        
        chartPanel.add(pane);
    }
    
    private void initFoldButton(final JButton foldButton, final JComponent panel, final String name, Font font) {
    	panel.setVisible(false);
    	foldButton.setFont(font);
        SharedHelper.INSTANCE.setIconName(true, foldButton, name, imageIconRight, imageIconDown);
    	foldButton.setVisible(true);
    	foldButton.setMinimumSize(new Dimension(40,25));
    	foldButton.setMaximumSize(new Dimension(Short.MAX_VALUE, 30));
    	foldButton.setContentAreaFilled(false);
    	foldButton.setHorizontalAlignment(SwingConstants.LEFT);
    	foldButton.setBorder(BorderFactory.createEmptyBorder(4,1,4,4));
    	foldButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				if (panel.isVisible()) {
					panel.setVisible(false);
                                        SharedHelper.INSTANCE.setIconName(true, foldButton, name, imageIconRight, imageIconDown);
					
					if (name.equals("Measurements")) {
						exportButton.setVisible(false);
						printButton.setVisible(false);
					}
				} else {
					panel.setVisible(true);
                                        SharedHelper.INSTANCE.setIconName(false, foldButton, name, imageIconRight, imageIconDown);
					if (name.equals("Measurements")) {
						exportButton.setVisible(true);
						printButton.setVisible(true);
					}
				}
			}
		});
    }
    
    private GridBagConstraints createGbcSettingsPanel(int col, int row) {
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.gridx = col;
        gbc.gridy = row;
        gbc.gridwidth = 1;
        gbc.gridheight = 1;

        gbc.anchor = (col == 0) ? GridBagConstraints.WEST : GridBagConstraints.EAST;
        gbc.fill = (col == 0) ? GridBagConstraints.BOTH : GridBagConstraints.HORIZONTAL;

        gbc.weightx = (col == 0) ? 0.1 : 1.0;
        gbc.weighty = 1.0;
        return gbc;
    } 
    
//    public void setElement(Element element) {
//        this.element = element;
//        Map<String, Integer> map = helper.getMetrics(element);
//        map = createDataSet(map);      
//        actionListenerExport.setMap(map);
//    }
    
    public void setElements(List<Element> elements) {
    	this.elements = elements;
    	if (elements.size() > 1)
    		this.elements = helper.removeChildElements(elements);
    	
    	Map<String, Integer> map = new HashMap<String, Integer>();
    	for (Element element : elements) {
    		
    		Map<String, Integer> tmp = helper.getMetrics(element, librariesCheckBox.isSelected());    		
    		for (Map.Entry<String, Integer> entry : tmp.entrySet()) {
    			
    			Integer count = map.get(entry.getKey());
    			count = (count == null) ? 0 : count;
    			int countTmp = entry.getValue();
    			map.put(entry.getKey(), count + countTmp);
    		}
		}
    	map = createDataSet(map);
    	actionListenerExport.setMap(map);
    	
    }
    
    public void updateMetrics() {        
    	Map<String, Integer> map = new HashMap<String, Integer>();
    	for (Element element : elements) {
    		map.putAll(helper.getMetrics(element, librariesCheckBox.isSelected()));    		
		}
        map = createDataSet(map);
        actionListenerExport.setMap(map);
    }
    
    private Map<String, Integer> createDataSet(Map<String, Integer> map) {
        gridPanel.removeAll();
        gridPanel.revalidate();
        gridPanel.repaint();
        gridPanelContainer.removeAll();
        gridPanelContainer.revalidate();
        diagramValueLabel.setText("");
        modelValueLabel.setText("");
        
        // aggregate
//        if (groupOnToggle.isSelected()) map = aggregate("Action", map);
//        if (groupOnToggle.isSelected()) map = aggregate("ObjectNode", map);
//        if (groupOnToggle.isSelected()) map = aggregate("ControlNode", map);
        if (groupCheckBox.isSelected()) map = aggregate(map);
        
        // Sort alphabetically
        map = new TreeMap<String, Integer>(map);        
        if (!sortCheckBox.isSelected()) {
            map = sortByComparator(map); // Sort by value
        }
        
        
        // Bar size
        Count count = getValue(map);
        int maxValue =  count.max;
        if (scaleCheckBox.isSelected()) {
        	//maxValue = count.model + count.diagram;
        	int value = maxValue;
        	try {
        		value = Integer.parseInt(txtFieldScale.getText());
        	} catch (NumberFormatException e) {
        		javax.swing.JOptionPane.showMessageDialog(null, "Invalid input! \n Input should be a number");
        	}        	
        	if (value < 1 || value > Short.MAX_VALUE) javax.swing.JOptionPane.showMessageDialog(null, "Invalid input! \n Input should be between 1 - " + Short.MAX_VALUE);
        	
        	maxValue = value;
        }
        
        int i = 0;
        for (Map.Entry<String, Integer> entry : map.entrySet()) {
            
            if (entry.getKey().equals("Diagram") && entry.getValue() != 0) {
                //diagramValueLabel.setText(entry.getValue().toString());
            } else {
            	if(Resources.notIncluded.contains(entry.getKey())) continue;
            	if(!filterCheckBox.isSelected() && !Resources.majorMetaClasses.contains(entry.getKey())) continue;
            	
            	JLabel label = new JLabel(entry.getKey(), JLabel.RIGHT);
            	label.setFont(label.getFont().deriveFont(11.0f));
                gridPanel.add(label,createGbc(0,i));
                
                JProgressBar bar = new JProgressBar(0, maxValue);
                bar.setFont(bar.getFont().deriveFont(11.0f));
                bar.setValue(entry.getValue());
                bar.setStringPainted(true);
                //bar.setString(""+bar.getValue());
                bar.setString("" + entry.getValue());
                bar.setBorderPainted(false);
                bar.setUI(new CustomProgressUI());
                if (entry.getValue() == 0) bar.setVisible(false);
                Dimension prefBarSize = bar.getPreferredSize();
                prefBarSize.height = 14;
                bar.setPreferredSize(prefBarSize);
                gridPanel.add(bar, createGbc(1,i));
                i++;
            }           
        }
        gridPanelContainer.add(gridPanel, BorderLayout.NORTH);
        if (elementsCheckBox.isSelected() && count.model != 0) modelValueLabel.setText("" + count.model);
        if (diagramsCheckBox.isSelected() && count.diagram != 0) diagramValueLabel.setText("" + count.diagram);
        return map;
    }
    
    private Count getValue(Map<String, Integer> map) {
        int modelCount = 0, diagramCount = 0, maxCount = 0;
        
        for (Map.Entry<String, Integer> entry : map.entrySet()) {
            if (entry.getKey().equals("Diagram") && !diagramsCheckBox.isSelected()) continue;
            if(Resources.notIncluded.contains(entry.getKey())) continue;
            if(!filterCheckBox.isSelected() && !Resources.majorMetaClasses.contains(entry.getKey())) continue;
            
            if (entry.getValue() > maxCount) maxCount = entry.getValue();
            
            List<String> diagrams = DiagramType.getAllDiagramTypes();
            if (diagrams.contains(entry.getKey()))
            	diagramCount += entry.getValue();
            else 
            	modelCount += entry.getValue();
        }
        
        return new Count(modelCount, diagramCount, maxCount);
    }
    
    private GridBagConstraints createGbc(int col, int row) {
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.gridx = col;
        gbc.gridy = row;
        gbc.gridwidth = 1;
        gbc.gridheight = 1;

        gbc.anchor = (col == 0) ? GridBagConstraints.WEST : GridBagConstraints.EAST;
        gbc.fill = (col == 0) ? GridBagConstraints.BOTH : GridBagConstraints.HORIZONTAL;

        gbc.weightx = (col == 0) ? 0.1 : 1.0;
//        gbc.weighty = 1.0;
        return gbc;
    }
    	
	private Map<String, Integer> aggregate(Map<String, Integer> map) {
		HashMap<String, Integer> newMap = new HashMap<String, Integer>();
		newMap.putAll(map);

		List<ActionClass> actionClasses = new ArrayList<ActionClass>();
		for (String s : Resources.roots) {
			actionClasses.add(new ActionClass(ClassTypes.getClassType(s), s));
		}

		for (Iterator<Map.Entry<String, Integer>> it = map.entrySet().iterator(); it.hasNext();) {
			Map.Entry<String, Integer> entry = it.next();
			Class currentClass = ClassTypes.getClassType(entry.getKey());
			if (currentClass == null) {
				continue;
			}
			List<Class> superClasses = ClassTypes.getSupertypes(currentClass);
			
			for (ActionClass act : actionClasses) {
				if (superClasses.contains(act.actionClass)) {
					int value = entry.getValue();
					Integer actionCount = newMap.get(act.type);
					if (actionCount == null) {
						actionCount = 0;
					}
					newMap.remove(entry.getKey());
					newMap.put(act.type, actionCount + value);
				}

			}
			
			if (ClassTypes.getShortName(currentClass).equals("ObjectFlow") || ClassTypes.getShortName(currentClass).equals("ControlFlow")) {
				int value = entry.getValue();
				Integer actionCount = newMap.get("Flow");
				if (actionCount == null) {
					actionCount = 0;
				}
				newMap.remove(entry.getKey());
				newMap.put("Flow", actionCount + value);
			 } else if(ClassTypes.getShortName(currentClass).equals("EnumerationLiteral") || 
					 ClassTypes.getShortName(currentClass).equals("LiteralBoolean") ||
					 ClassTypes.getShortName(currentClass).equals("LiteralInteger") ||
					 ClassTypes.getShortName(currentClass).equals("LiteralUnlimitedNatural") ||
					 ClassTypes.getShortName(currentClass).equals("LiteralString") ||
					 ClassTypes.getShortName(currentClass).equals("LiteralNull") ||
					 ClassTypes.getShortName(currentClass).equals("LiteralReal")) {
				int value = entry.getValue();
				Integer actionCount = newMap.get("Literal");
				if (actionCount == null) {
					actionCount = 0;
				}
				newMap.remove(entry.getKey());
				newMap.put("Literal", actionCount + value);				 
			 }

		}

		return newMap;
	}  
    
    // http://www.mkyong.com/java/how-to-sort-a-map-in-java/
    private Map<String, Integer> sortByComparator(Map<String, Integer> unsortMap) {
 
		// Convert Map to List
		List<Map.Entry<String, Integer>> list = 
			new LinkedList<Map.Entry<String, Integer>>(unsortMap.entrySet());
 
		// Sort list with comparator, to compare the Map values
		Collections.sort(list, new Comparator<Map.Entry<String, Integer>>() {
			public int compare(Map.Entry<String, Integer> o1,
                                           Map.Entry<String, Integer> o2) {
				return (o2.getValue()).compareTo(o1.getValue());
			}
		});
 
		// Convert sorted map back to a Map
		Map<String, Integer> sortedMap = new LinkedHashMap<String, Integer>();
		for (Iterator<Map.Entry<String, Integer>> it = list.iterator(); it.hasNext();) {
			Map.Entry<String, Integer> entry = it.next();
			sortedMap.put(entry.getKey(), entry.getValue());
		}
		return sortedMap;
	}
    
    public ModelMetricsTreeSelectionListener getSelectionListener() {
        return selectionListener;
    }
    
    class UpdateController implements ActionListener {

    	SteelCheckBox checkBox;
    	String state1, state2;
    	
    	public UpdateController(SteelCheckBox checkBox, String state1, String state2) {
    		this.checkBox = checkBox;
			this.state1 = state1;
			this.state2 = state2;
		}
    	
        @Override
        public void actionPerformed(ActionEvent e) {
            if(elements != null) {
            	updateMetrics();
            }
            
            if (checkBox == null) return;
            
            if(checkBox.isSelected()) {
            	checkBox.setText(state1);
            } else {
            	checkBox.setText(state2);
            }
            
            if (checkBox.getText().equals("Fix"))
            	txtFieldScale.setEnabled(true);
            else if (checkBox.getText().equals("All"))
            	txtFieldScale.setEnabled(false);
        }
        
    }
   
}