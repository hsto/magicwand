/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelmetrics.controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Map;

import javax.swing.JFileChooser;
import javax.swing.filechooser.FileFilter;
import lib.ExportFileChooser;
import lib.ExportFileFilter;
import lib.SharedHelper;

import modelmetrics.view.JPanelModelMetrics;

/**
 *
 * @author Luai
 */
public class ActionListenerExport implements ActionListener {
    
    private JPanelModelMetrics panel;
    private Map<String, Integer> map;

    public ActionListenerExport(JPanelModelMetrics panel) {
        this.panel = panel;
    }
    
    public void setMap(Map<String, Integer> map) {
        this.map = map;
    }    

    @Override
    public void actionPerformed(ActionEvent e) {
        if (map == null) return;
        
        JFileChooser jc = new ExportFileChooser(SharedHelper.ExportType.CSV);
        FileFilter ff = new ExportFileFilter(SharedHelper.ExportType.CSV);
        
        jc.addChoosableFileFilter(ff);
        jc.setFileFilter(ff);
        
        int res = jc.showSaveDialog(panel);
        
        if (res == JFileChooser.APPROVE_OPTION) {
            
            File file = jc.getSelectedFile();
            String path = file.getAbsolutePath();
            if (!path.endsWith(".csv")) {
                file = new File(path + ".csv");                
            }
            
            
            try {                
            	BufferedWriter bw = new BufferedWriter(new FileWriter(file));
                String str = getExportString();
                bw.write(str);
                bw.close();
            } catch (IOException ex) {
            }            
        }
        
    }
    
    private String getExportString() {
        StringBuilder builder = new StringBuilder();
        
        for (Map.Entry<String, Integer> entry : map.entrySet()) {
            if (entry.getKey().equals("Diagram") && entry.getValue() != 0) {
                continue;
            } else {
                builder.append(entry.getKey());
                builder.append(";");
                builder.append(entry.getValue());
                builder.append("\n");
            }
        }
        
        return builder.toString();
    }
    
    
}
