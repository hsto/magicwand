package modelmetrics.controller;

import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.filechooser.FileFilter;
import lib.ExportFileChooser;
import lib.ExportFileFilter;
import lib.SharedHelper;

public class ActionListenerPrint implements ActionListener {

    JPanel panel;

    public ActionListenerPrint(JPanel gridPanel) {
        this.panel = gridPanel;
    }

    @Override
    public void actionPerformed(ActionEvent e) {

        JFileChooser jc = new ExportFileChooser(SharedHelper.ExportType.PNG);
        FileFilter ff = new ExportFileFilter(SharedHelper.ExportType.PNG);
        jc.addChoosableFileFilter(ff);
        jc.setFileFilter(ff);

        int res = jc.showSaveDialog(panel);

        if (res == JFileChooser.APPROVE_OPTION) {

            File file = jc.getSelectedFile();
            String path = file.getAbsolutePath();
            if (!path.endsWith(".png")) {
                file = new File(path + ".png");
            }

            BufferedImage image = new BufferedImage(panel.getWidth(), panel.getHeight(), BufferedImage.TYPE_INT_RGB);
            Graphics2D g = image.createGraphics();
            panel.printAll(g);
            g.dispose();
            try {
                ImageIO.write(image, "png", file);
            } catch (IOException exp) {
            } finally {
                if (image != null) {
                    image.flush();
                }
            }

        }

    }

}
