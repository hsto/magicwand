/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelmetrics.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;

import modelmetrics.view.JPanelModelMetrics;

import com.nomagic.magicdraw.core.Application;
import com.nomagic.magicdraw.ui.browser.Node;
import com.nomagic.magicdraw.ui.browser.Tree;
import com.nomagic.magicdraw.uml.BaseElement;
import com.nomagic.uml2.ext.magicdraw.classes.mdkernel.Element;

/**
 *
 * @author Luai
 */
public class ModelMetricsTreeSelectionListener implements TreeSelectionListener{

    private JPanelModelMetrics panel;
    private Tree tree;
    
    public ModelMetricsTreeSelectionListener(JPanelModelMetrics panel) {
        this.panel = panel;
    }
    
    @Override
    public void valueChanged(TreeSelectionEvent e) {
    	if (!panel.isShowing()) return;
    	
        tree = Application.getInstance().getMainFrame().getBrowser().getContainmentTree();
        
        List<Element> elements = new ArrayList<Element>();
        Node[] nodes = tree.getSelectedNodes();
        for (Node node : nodes) {
        	if (node != null) {
        		Object userObject = node.getUserObject();
        		if (userObject instanceof BaseElement && userObject instanceof Element) {
        			
        			Element element = (Element) userObject;
        			elements.add(element);
        		}
        	}			
		}
        panel.setElements(elements);
        
        
//        Node node = tree.getSelectedNode();
//        if (node != null) {
//            Object userObject = node.getUserObject();
//            if (userObject instanceof BaseElement && userObject instanceof Element) {
//
//                Element element = (Element) userObject;
//                panel.setElement(element);
//            }
//        }
    }
    
}
