/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelmetrics;

import com.nomagic.magicdraw.uml.BaseElement;
import com.nomagic.magicdraw.uml.ClassTypes;
import com.nomagic.magicdraw.uml.DiagramType;
import com.nomagic.magicdraw.uml.InheritanceVisitor;
import com.nomagic.uml2.ext.jmi.helpers.StereotypesHelper;
import com.nomagic.uml2.ext.jmi.reflect.VisitorContext;
import com.nomagic.uml2.ext.magicdraw.classes.mdkernel.Diagram;
import com.nomagic.uml2.ext.magicdraw.classes.mdkernel.Element;
import com.nomagic.uml2.ext.magicdraw.mdprofiles.Stereotype;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import modelmetrics.view.JPanelModelMetrics;

/**
 *
 * @author Luai
 */
public class ModelMetricsHelper {
    
    private JPanelModelMetrics mainPanel;
    private StatisticsVisitor visitor = new StatisticsVisitor();
    private ChildVisitor childVisitor = new ChildVisitor();
    //private Integer countModelInstances = null;

    private String varLib1 = "Stereotype ModelLibrary";
    private String varLib2 = "Stereotype auxiliaryResource";
    
    public ModelMetricsHelper(JPanelModelMetrics mainPanel) {
        this.mainPanel = mainPanel;
    }
    
    public Map<String, Integer> getMetrics(Element element, boolean includeLibraries) {
//        countModelInstances = 0;
//        mainPanel.modelValueLabel.setText("");
        
        visitor.getMap().clear();
        visitor.initMap();
        
        if (mainPanel.elementsCheckBox.isSelected() || mainPanel.diagramsCheckBox.isSelected())
            visitChildren(element, mainPanel.recursiveCheckBox.isSelected(), includeLibraries);        
        
//        if (mainPanel.modelElementsCheckBox.isSelected()) {
//            if (countModelInstances != 0) mainPanel.modelValueLabel.setText(countModelInstances.toString());
//        }        
//        countModelInstances = null;
        
        return visitor.getMap();
    }
    
    private void visitChildren(Element element, boolean recursive, boolean includeLibraries) {
        
        ArrayList<Element> all = new ArrayList<Element>();
        all.add(element);
        Element current;

        if (!recursive && !(element instanceof Diagram)) {        	
        	if (element.getHumanType().equals("Model") && !includeLibraries) { // Root
            	for (Element e : element.getOwnedElement()) {
					List<Stereotype> stList = StereotypesHelper.getStereotypes(e);
					boolean skip = false;
					for (Stereotype st : stList) {
						if (st.getHumanName().equals(varLib1) || st.getHumanName().equals(varLib2))	skip = true;						
					}
					if (!skip) all.add(e);
				}
            } else {
            	all.addAll(element.getOwnedElement());
            }        	
        }
        
        // if current element has children, list will be increased.
        for (int i = 0; i < all.size(); i++) {
            current = all.get(i);
            try {
                // let's perform some action with this element in visitor.
                current.accept(visitor);
            } catch (Exception e) {
                
            }
            // add all children into end of this list, so it emulates recursion.
            if (recursive) {
            	if (!(current instanceof Diagram)) {
            		//all.addAll(current.getOwnedElement());
            		if (ClassTypes.getShortName(current.getClassType()).equals("Profile") || ClassTypes.getShortName(current.getClassType()).equals("Stereotype") )
            			continue;
            		
            		
            		if (current.getHumanType().equals("Model") && !includeLibraries) { // Root
                    	for (Element e : current.getOwnedElement()) {
                    		if (e.getHumanName().equals("Package UML Standard Profile")) continue;
        					List<Stereotype> stList = StereotypesHelper.getStereotypes(e);
        					boolean skip = false;
        					for (Stereotype st : stList) {
        						if (st.getHumanName().equals(varLib1) || st.getHumanName().equals(varLib2))	skip = true;						
        					}
        					if (!skip) all.add(e);
        				}
                    } else {
                    	for (Element e : current.getOwnedElement()) {
                    		if (e.getHumanName().equals("Package UML Standard Profile")) continue;
                    		all.add(e);						
                    	}                    	
                    }             		
            	}
            }
        }
    }
    
    public List<Element> removeChildElements(List<Element> elements) {
    	Map<Element,List<String>> idsTable = new HashMap<Element,List<String>>();
    	
    	// Init id lists
    	for (Element element : elements) {
    		childVisitor.ids = new ArrayList<String>();
    		
    		ArrayList<Element> all = new ArrayList<Element>();
            all.add(element);
            Element current;
            for (int i = 0; i < all.size(); i++) {
                current = all.get(i);
                try {
                    current.accept(childVisitor);
                } catch (Exception e) {
                    
                }
                all.addAll(current.getOwnedElement());                
            }
            idsTable.put(element,childVisitor.ids);
		}
    	
    	for (Iterator<Element> iterator = elements.iterator(); iterator.hasNext();) {
			Element element = (Element) iterator.next();
			for (Entry<Element, List<String>> entry : idsTable.entrySet()) {
    			if (entry.getKey() == element) continue;
    			
    			if (entry.getValue().contains(element.getID())) {
    				iterator.remove(); //elements.remove(element);
    				break;    				
    			}    			
    		}
		}
    	return elements;
    }
    
    
    /**
     * Visitor for counting model elements. Contains Map for counting results.
     * Must be overridden visit... method for every counted type of element. In
     * this example only ModelClass, Attribute and Operation will be counted.
     */
    class StatisticsVisitor extends InheritanceVisitor {
 
        /**
         * Map for counting results. Class type is a key, number is a value.
         */
        private HashMap<String, Integer> statsTable = new HashMap<String, Integer>();        
        
        public Map<String, Integer> getMap() {
            return statsTable;
        }
        
        public void initMap() {
            if (!mainPanel.emptyCheckBox.isSelected()) return;            
            
            if (mainPanel.elementsCheckBox.isSelected()) {
                Collection<String> list = ClassTypes.getAllClassNames();
                for (String s : list) {
                    //if (s.equals("Action") || s.equals("ObjectNode") || s.equals("ControlNode")) continue;
                	if (Resources.notIncluded.contains(s) || Resources.abstractMetaClasses.contains(s)) continue;
                	if(!mainPanel.filterCheckBox.isSelected() && !Resources.majorMetaClasses.contains(s)) continue;
                    statsTable.put(s, 0);                    
                }
            }
            
            if (mainPanel.diagramsCheckBox.isSelected()) {
                List<String> list = DiagramType.getAllDiagramTypes();
                for (String s : list) {
                    statsTable.put(s, 0);
                }
            }
            
        }
        
        @Override
        public void visitElement(com.nomagic.uml2.ext.magicdraw.classes.mdkernel.Element element, VisitorContext context) {
            super.visitElement(element, context);
            count(element);
        }

        /**
         * Increases value of this type count in results map. If there is no
         * such registered type, add it.
         *
         * @param element the element to count.
         */
        public void count(BaseElement element) {
            
            String classType = ClassTypes.getShortName(element.getClassType());
            
            if (mainPanel.elementsCheckBox.isSelected()) {               
                Integer count = statsTable.get(classType);
                if (count == null) {
                    count = 0;
                }
                if ( !(!mainPanel.diagramsCheckBox.isSelected() && classType.equals("Diagram")) ) {
                    statsTable.put(classType, count + 1);
                }
                
                //if (!classType.equals("Diagram")) countModelInstances++;
                
            } 
//            else if (classType.equals("Diagram")){
//                
//                Integer count = statsTable.get(classType);
//                if (count == null) {
//                    count = 0;
//                }
//                statsTable.put(classType, count + 1);
//            }            
            
            if (!mainPanel.diagramsCheckBox.isSelected()) return;
            
            if (element instanceof Diagram) {
                Diagram diagram = (Diagram) element;
                String diagramType = diagram.get_representation().getType();
                //String diagramType = DiagramType.getShortType(diagram.get_representation().getType());
                
                Integer i = statsTable.get(diagramType);
                if (i == null) {
                    i = 0;
                }
                statsTable.put(diagramType, i + 1);
            }
            
        }
        
        
    }
    
    class ChildVisitor extends InheritanceVisitor {
    	
    	public List<String> ids = new ArrayList<String>();
    	
    	@Override
        public void visitElement(com.nomagic.uml2.ext.magicdraw.classes.mdkernel.Element element, VisitorContext context) {
            super.visitElement(element, context);
            ids.add(element.getID());
        }
    	
    	
    }
    
    
}
