/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package configReader;

/**
 *
 * @author Luai
 */
public class Slot {
    
    private Multiplicity multiplicity;
    private String property;
    private String value;
    private String unit;
    private boolean unitCustom = false; // if the unit is customized by the user
    
    private String type;
    private String defaultValue;
    
    private String comment;
    
    public Slot() {
        this.multiplicity = null;
        this.property = "";
        this.value = "";
        this.unit = "";
        this.type = "";
        this.defaultValue = "";
        this.comment = "";
    }
    
    public static enum Multiplicity {
        NONE, OPTIONAL, AT_LEAST_ONE, EXACTLY_ONE, ANY_NUMBER_OF, PROPERTY_EDITABLE_FALSE;
    }
    
    public void setMultiplicity(Multiplicity multiplicity) {
        this.multiplicity = multiplicity;
    }
    
    public Multiplicity getMultiplicity () {
        return multiplicity;
    }
    
    public void setProperty(String property) {
        this.property = property;
    }
    
    public String getProperty() {
        return property;
    }
    
    public void setValue(String value) {
        this.value = value;
    }
    
    public String getValue() {
        return this.value;
    }
    
    public void setUnit(String unit) {
        this.unit = unit;
    }
    
    public String getUnit() {
        return unit;
    }
    
    public void setCommnet(String comment) {
        this.comment = comment;
    }
    
    public String getComment() {
        return comment;
    }
    
    public void setUnitCustom(boolean bool) {
        this.unitCustom = bool;
    }
    
    public boolean getUnitCustom() {
        return this.unitCustom;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDefaultValue() {
        return defaultValue;
    }

    public void setDefaultValue(String defaultValue) {
        this.defaultValue = defaultValue;
    }
    
}
