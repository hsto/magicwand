/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package configReader;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Luai
 */
public class Tag {

    private String nameType;
    private List<Slot> slots;
    private String comment;
    private List<String> subTags;
    private boolean isSubsetSymbol;
    
    private List<String> tokens = new ArrayList<String>();
    
    public Tag(String nameType) {
        this.nameType = nameType;
        this.slots = new ArrayList<Slot>();
        this.comment = "";
        this.subTags = new ArrayList<String>();
        this.isSubsetSymbol = false;
    }
    
    public void addToken(String token) {
        tokens.add(token);
    }
    
    public void removeLastToken() {
        tokens.remove(tokens.size()-1);
    }
    
    public String getNameType() {
        if (nameType.contains(ConfigReader.SUBSET_VAR2)) {
            return nameType.replace(ConfigReader.SUBSET_VAR2, "");
        } else {
            return nameType;            
        }        
    }
    
    // Used for _<: SubSet symbol
    public String getNameTypeUnfiltered() {
        return nameType;
    }
    
    public List<String> getTokens() {
        return tokens;
    }
    
    public void addSlot(Slot slot) {
        this.slots.add(slot);
    }
    public void setComment(String comment) {
        this.comment = comment;
    }
    
    public String getComment() {
        return comment;
    }
    
    public List<Slot> getSlots() {
        return slots;
    }
    
    public void setSlots(List<Slot> slots) {
        this.slots = slots;
    }

    public List<String> getSubTags() {
        return subTags;
    }

    public void setSubTags(List<String> subTags) {
        this.subTags = subTags;
    }

    public boolean isIsSubsetSymbol() {
        return isSubsetSymbol;
    }

    public void setIsSubsetSymbol(boolean isSubsetSymbol) {
        this.isSubsetSymbol = isSubsetSymbol;
    }
    
}
