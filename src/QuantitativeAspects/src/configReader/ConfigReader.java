/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package configReader;

import com.nomagic.uml2.ext.jmi.helpers.StereotypesHelper;

import configReader.Slot.Multiplicity;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Scanner;
import java.util.StringTokenizer;

import quantitative.QuantitativeHelper;

import com.nomagic.uml2.ext.magicdraw.classes.mdkernel.Class;

/**
 *
 * @author Luai
 */
public class ConfigReader {
    
    private static final String FILE_NAME = "config.txt";
    
    private static final String SUBSET_VAR1 = "and all its subclasses!";
    public static final String SUBSET_VAR2 = "_<:";
    
    private List<Tag> tags = new ArrayList<Tag>();
    
    public ConfigReader () {
        readFile(null);
    }
    
    public ConfigReader(File file) {
    	readFile(file);
    }
    
    private void readFile(File file) {    
      
        Scanner scanner = null;
        try {            
            if (file == null) scanner = new Scanner(getClass().getClassLoader().getResourceAsStream(FILE_NAME));
            else scanner = new Scanner(file);
            
            while(scanner.hasNextLine()) {
            	scanLine(scanner.nextLine());
            }
        } catch (Exception e) {
            //scanner = new Scanner(getClass().getResourceAsStream(FILE_NAME));            
        } finally {
        	if (scanner != null) scanner.close();
        }
        
        for (Tag tag : tags) {
            processTokens(tag);
            if (tag.getComment().contains(SUBSET_VAR1) || tag.getNameTypeUnfiltered().contains(SUBSET_VAR2)|| tag.isIsSubsetSymbol()) processSubTags(tag);
        }
        
    }

    private void scanLine(String nextLine) {
        String previous = "";
        String temp;
        boolean isSubSetSymbol = false;
        
        StringTokenizer st = new StringTokenizer(nextLine);
        while(st.hasMoreTokens()) {
            temp = st.nextToken();
            if (temp.equals(SUBSET_VAR2)) isSubSetSymbol = true;
            
            if(temp.equals("::=")) {
                if (!tags.isEmpty()) tags.get(tags.size()-1).removeLastToken();                
                Tag tag = new Tag(previous);                
                if (isSubSetSymbol) tag.setIsSubsetSymbol(true);
                tags.add(tag);
                isSubSetSymbol = false;
            } else {
                if (!tags.isEmpty()) {
                    tags.get(tags.size()-1).addToken(temp);
                }
            }
            
            previous = temp;
        }
        // New line
        if (!tags.isEmpty()) tags.get(tags.size()-1).addToken("\n");
        
    }

    private void processTokens(Tag tag) {
        List<List<String>> tagLines = generateLines(tag.getTokens());
        

        for (List<String> tagLine : tagLines) {
            
            Slot slot = new Slot();
            
            for(String s : tagLine) {
                if (s.equals("#")) {
                    int index = tagLine.indexOf(s);
                    String comment = makeComment(index+1, tagLine);
                    
                    if (index == 0)tag.setComment(comment);
                    else slot.setCommnet(comment);
                    break;
                }

                if (String.valueOf(s.charAt(0)).equals(":")) {
                    if (s.length() == 1) {
                        String unit = tagLine.get(tagLine.indexOf(s)+1);
                        slot.setUnit(unit);
                    } else {
                        String unit = tagLine.get(tagLine.indexOf(s));
                        unit =unit.replaceFirst(":", "");
                        slot.setUnit(unit);
                    }
                } 
                        
                if (String.valueOf(s.charAt(0)).equals("|")) {
                    if (s.length() == 1) {
                        String type = tagLine.get(tagLine.indexOf(s)+1);
                        slot.setType(type);
                    } else {
                        String type = tagLine.get(tagLine.indexOf(s));
                        type = type.replaceFirst("\\|", "");
                        slot.setType(type);
                    }
                }
                
                if (String.valueOf(s.charAt(0)).equals("=") && !slot.getType().isEmpty()) {
                    if (s.length() == 1) {
                        String defaultValue = tagLine.get(tagLine.indexOf(s)+1);
                        slot.setDefaultValue(defaultValue);
                    } else {
                        String defaultValue = tagLine.get(tagLine.indexOf(s));
                        defaultValue = defaultValue.replaceFirst("=", "");
                        slot.setDefaultValue(defaultValue);
                    }
                }
                
            }
            
            if (tagLine.get(0).equals("#")) continue;
            
            Multiplicity multi = findMultiplicity(tagLine.get(0));
            slot.setMultiplicity(multi);
            
            if (multi == Multiplicity.NONE) {
                slot.setProperty(tagLine.get(0));
            } else {
                slot.setProperty(tagLine.get(1));
            }
            
            if (!alreadyExists(tag, slot)) {               
                tag.addSlot(slot);
            }
        }
    }
    
    public void processTokensReset(Tag tag) {
        tag.getSlots().clear();
        processTokens(tag);
    }
    
    private List<List<String>> generateLines(List<String> tokens) {
        List<List<String>> lines = new ArrayList<List<String>>();
        List<String> tempList = new ArrayList<String>();
                
        for (String t : tokens) {
            if (t.equals("\n")) {
                if (!tempList.isEmpty()) lines.add(tempList);
                tempList = new ArrayList<String>();
            } else {
                tempList.add(t);
            }
            
        }
        
        return lines;
    }
    
    private String makeComment(int index, List<String> tagLine) {
        String r = "";
         
        for (int i = index; i < tagLine.size(); i++) {
            r += tagLine.get(i) + " ";
        }
        return r;
    } 

    private Multiplicity findMultiplicity(String s) {
        
        Multiplicity multi;
        
        if (s.equals("?")) {
            multi = Multiplicity.OPTIONAL;
        } else if (s.equals("!")) {
            multi = Multiplicity.EXACTLY_ONE;
        } else if (s.equals("*")) {
            multi = Multiplicity.ANY_NUMBER_OF;
        } else if (s.equals("+")) {
            multi = Multiplicity.AT_LEAST_ONE;
        } else {
            multi = Multiplicity.NONE;
        }
            
        return multi;
    }
    
    public String getMultiplicityChar(Multiplicity multi) {
        String r = "";
        
        if (multi == Multiplicity.OPTIONAL) {
            r = "?";
        } else if (multi == Multiplicity.EXACTLY_ONE) {
            r = "!";
        } else if (multi == Multiplicity.ANY_NUMBER_OF) {
            r = "*";            
        } else if (multi == Multiplicity.AT_LEAST_ONE) {
            r = "+";
        }
        
        return r;
    }
    
    public List<Tag> getTags() {
        return tags;
    }

    private boolean alreadyExists(Tag tag, Slot slot) {
        for (Slot s : tag.getSlots()) {
            if (s.getProperty().equals(slot.getProperty())) {
                return true;
            }
        }
        return false;
    }
    
    public boolean fromConfigFile(Tag tag, Slot slot) {
        List<List<String>> tagLines = generateLines(tag.getTokens());
        
        for (List<String> tagLine : tagLines) {
             for(String s : tagLine) {
                  if (s.equals("#")) break;
                  
                if (String.valueOf(s.charAt(0)).equals(":")) {
                    if (s.length() == 1) {
                        String unit = tagLine.get(tagLine.indexOf(s)+1);                        
                        if (slot.getUnit().equals(unit)) return true;                        
                    } else {
                        String unit = tagLine.get(tagLine.indexOf(s));
                        unit =unit.replaceFirst(":", "");
                        if (slot.getUnit().equals(unit)) return true;
                    }
                }                  
                
             }
        }        
        return false;
    }
    
    private void processSubTags(Tag tag) {
        Collection<Class> umlMetaClasses = StereotypesHelper.getUML2MetaClasses(QuantitativeHelper.project);
        
        for (Class c : umlMetaClasses) {
            if (c.getSuperClass() != null && !c.getSuperClass().isEmpty() &&
                    checkSubTagParent(c.getSuperClass(), tag.getNameType())) {
                tag.getSubTags().add(c.getName());
            }
        }
        
        
    }
    
    private boolean checkSubTagParent(Collection<Class> classes, String tagName) {
        Collection<Class> parents = new ArrayList<Class>();
        
        if (classes == null || classes.isEmpty()) return false;
        
        for (Class c : classes) {
            parents.addAll(c.getSuperClass());
            
            if (c.getName().equals(tagName)) return true;
        }    
        
        return checkSubTagParent(parents, tagName);
        
    }
    
//    private void processSubTags(Tag tag) {
//        Collection<Class> umlMetaClasses = StereotypesHelper.getUML2MetaClasses(QuantitativeHelper.project);
//        
//        for (Class c : umlMetaClasses) {
//            Collection<Class> temp = c.getSuperClass();
//            
//            while(temp != null) {
//                for (Class superClass : temp) {
//                    
//                    if (superClass.getName().equals(tag.getNameType())) {
//                        tag.getSubTags().add(c.getName());
//                        temp = null;
//                        
//                    } else {
//                        for (Class parentSuperClass : superClass.getSuperClass()) {                       
//                            temp = superClass.getSuperClass();
//                        }
//                    }
//                    
//                    
//                }
//            }
//            
//            
//        }
//        
//        com.nomagic.uml2.ext.magicdraw.classes.mdkernel.Class t = StereotypesHelper.getMetaClassByName(QuantitativeHelper.project, "CallBehaviorAction");
//        
//        Collection<Class> list = t.getSuperClass();
//        String str = "";
//        str += umlMetaClasses.size() + "\n";
//        for (Class s : list) {
//            str += "SuperClass " + s.getName() + " -- ";
//        }
//        str += t.getName() + "\n";
//        
//        
//        javax.swing.JOptionPane.showMessageDialog(null, str);
//    }
}


