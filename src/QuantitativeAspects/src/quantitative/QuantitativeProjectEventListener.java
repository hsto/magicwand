/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package quantitative;

import quantitative.view.JPanelQuantitativeAnnotation;

import com.nomagic.magicdraw.core.Application;
import com.nomagic.magicdraw.core.Project;
import com.nomagic.magicdraw.core.project.ProjectEventListener;
import com.nomagic.magicdraw.openapi.uml.ReadOnlyElementException;
import com.nomagic.magicdraw.ui.MainFrame;
import com.nomagic.magicdraw.ui.ProjectWindow;
import com.nomagic.magicdraw.ui.ProjectWindowsManager;
import com.nomagic.magicdraw.ui.WindowComponentInfo;
import com.nomagic.magicdraw.ui.WindowsManager;
import com.nomagic.magicdraw.ui.browser.Tree;
import com.nomagic.magicdraw.ui.browser.WindowComponentContent;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;

import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JTree;
import lib.SharedHelper;

/**
 *
 * @author rb328
 */
public class QuantitativeProjectEventListener implements ProjectEventListener {

    private Tree tree;
    private JTree jtree;
    public static String pluginName = "Quantitative Annotations";
    
    //private final JPanelQuantitativeAnnotation annotationPanel = new JPanelQuantitativeAnnotation();
    private JPanelQuantitativeAnnotation annotationPanel;
    public static String projectWindowId = "";
    
    WindowComponentInfo infoAnnotation = new WindowComponentInfo("QUANTITATIVE ANNOTATION", pluginName, null, WindowsManager.SIDE_EAST, WindowsManager.STATE_DOCKED, false);
    public boolean startup;
    public ProjectWindow window;
    
    public QuantitativeProjectEventListener()  {
        
    }

    //private static String FILE_NAME_STARTUP = "startup.txt";
    private static String FILE_NAME_STARTUP = System.getProperty("user.home") + "/.magicdraw/MagicWand/startup_Quantity.txt";
    
    @Override
    public void projectOpened(Project project) {
        // get Documentation panel and 
        //Application.getInstance().getGUILog().log(project.getHumanName() + "ReportProjectEventListener::projectOpenedFromGUI");
        QuantitativeHelper.INSTANCE.setProject(project);
        annotationPanel = new JPanelQuantitativeAnnotation();
        tree = Application.getInstance().getMainFrame().getBrowser().getContainmentTree();
        jtree = tree.getTree();
        jtree.addTreeSelectionListener(annotationPanel.getSelectionListener());
        
        // protect documentation pane from direct editing
        Application.getInstance().getMainFrame().getBrowser().getDocPanel().getHTMLTextEditorComponent().setVisible(false);
        Application.getInstance().getMainFrame().getBrowser().getDocPanel().getHTMLEditorPane().setEnabled(false);
        Application.getInstance().getMainFrame().getBrowser().getDocPanel().getHTMLEditorPane().setVisible(false);
        Application.getInstance().getMainFrame().getBrowser().getDocPanel().setEnableHTMLCheckBox(false);
        Application.getInstance().getMainFrame().getBrowser().getDocPanel().setEnabledAll(false);
        

        MainFrame mainFrame = Application.getInstance().getMainFrame();
        ProjectWindowsManager projectWindowsManager = mainFrame.getProjectWindowsManager();
       
        WindowComponentContent contentAnnotation = new QuantitativeWindowComponentContent(annotationPanel);
        window = new ProjectWindow(infoAnnotation,contentAnnotation);
        
        startup = SharedHelper.INSTANCE.readStartup(FILE_NAME_STARTUP);
        if (startup) {
                projectWindowsManager.addWindow(window);
        	projectWindowsManager.activateWindow(window.getId());
        }
        
        projectWindowId = window.getId();
        
        try {
            QuantitativeHelper.INSTANCE.createAnnotationStereotype(project);
        } catch (ReadOnlyElementException ex) {
            Logger.getLogger(QuantitativeProjectEventListener.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void projectClosed(Project project) {
        
    }

    @Override
    public void projectSaved(Project project, boolean savedInServer) {
        
    }

    @Override
    public void projectDeActivated(Project project) {
        
    }

    @Override
    public void projectCreated(Project project) {
        
    }

    @Override
    public void projectOpenedFromGUI(Project project) {
               
    }

    @Override
    public void projectActivatedFromGUI(Project project) {
        //Application.getInstance().getGUILog().log(project.getHumanName() + " projectActivatedFromGUI");
    }

    @Override
    public void projectActivated(Project project) {

    }

    @Override
    public void projectReplaced(Project oldProject, Project newProject) {
        
    }

    @Override
    public void projectPreClosed(Project project) {
        tree = Application.getInstance().getMainFrame().getBrowser().getContainmentTree();
        if (tree != null) {
        	jtree = tree.getTree();
        	if (annotationPanel != null && jtree != null) jtree.removeTreeSelectionListener(annotationPanel.getSelectionListener());
        }
        
        SharedHelper.INSTANCE.writeStartup(FILE_NAME_STARTUP, annotationPanel);
    }

    @Override
    public void projectPreClosedFinal(Project project) {
        
    }

    @Override
    public void projectPreSaved(Project project, boolean savedInServer) {
        
    }

    @Override
    public void projectPreActivated(Project project) {
        
    }

    @Override
    public void projectPreDeActivated(Project project) {

    }

    @Override
    public void projectPreReplaced(Project oldProject, Project newProject) {

    }

}
