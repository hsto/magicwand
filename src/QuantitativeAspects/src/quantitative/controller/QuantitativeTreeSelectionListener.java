/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package quantitative.controller;

import quantitative.view.JPanelQuantitativeAnnotation;
import com.nomagic.magicdraw.core.Application;
import com.nomagic.magicdraw.ui.browser.Node;
import com.nomagic.magicdraw.ui.browser.Tree;
import com.nomagic.magicdraw.uml.BaseElement;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;

/**
 *
 * @author Luai
 */
public class QuantitativeTreeSelectionListener implements TreeSelectionListener{

    private JPanelQuantitativeAnnotation annotationPanel;
    private Tree tree;
    
    public QuantitativeTreeSelectionListener(JPanelQuantitativeAnnotation annotationPanel) {
        this.annotationPanel = annotationPanel;
    }

    @Override
    public void valueChanged(TreeSelectionEvent e) {        
    	if (!annotationPanel.isShowing()) return;
    	
       tree = Application.getInstance().getMainFrame().getBrowser().getContainmentTree();
       
//       if (tree.getSelectedNodes().length > 1) {
//           annotationPanel.getInnerPanel().setVisible(false);
//           return;
//       }
//       
       Node node = tree.getSelectedNode();
       if (node != null) {
           Object userObject = node.getUserObject();
           if (userObject instanceof BaseElement) {
               
               BaseElement element = (BaseElement) userObject;               
               annotationPanel.setElement(element);
           }
       }
       
    }
    
}
