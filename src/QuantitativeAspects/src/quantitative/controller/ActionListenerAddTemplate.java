package quantitative.controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.JFileChooser;
import javax.swing.filechooser.FileNameExtensionFilter;

import quantitative.view.JPanelQuantitativeAnnotation;
import configReader.ConfigReader;

public class ActionListenerAddTemplate implements ActionListener {
	
	private JPanelQuantitativeAnnotation panel;

	public ActionListenerAddTemplate(JPanelQuantitativeAnnotation panel) {
		this.panel = panel;
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		
		JFileChooser jc = new JFileChooser();
        FileNameExtensionFilter ff = new FileNameExtensionFilter("Text files (*.txt)", "txt");
        jc.setFileFilter(ff);
        
        int res = jc.showOpenDialog(panel);
        File file = jc.getSelectedFile();
        
        if (res == JFileChooser.APPROVE_OPTION && file.getName().endsWith(".txt")) {
            ConfigReader newConfigReader = new ConfigReader(file);
            
            if (newConfigReader == null || newConfigReader.getTags().isEmpty()) {
            	JPanelQuantitativeAnnotation.newConfigReader = null;
            	javax.swing.JOptionPane.showMessageDialog(panel, "Invalid config file");
            } else {
            	JPanelQuantitativeAnnotation.newConfigReader = newConfigReader;
            	panel.txtTemplates.setText(file.getAbsolutePath());
            	panel.setElement();
            }
        }		
		
	}

}
