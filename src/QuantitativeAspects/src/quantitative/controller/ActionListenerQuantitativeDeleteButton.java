/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package quantitative.controller;

import quantitative.QuantitativeHelper;
import com.nomagic.magicdraw.core.Application;
import com.nomagic.magicdraw.openapi.uml.ModelElementsManager;
import com.nomagic.magicdraw.openapi.uml.ReadOnlyElementException;
import com.nomagic.magicdraw.openapi.uml.SessionManager;
import com.nomagic.magicdraw.ui.browser.Node;
import com.nomagic.magicdraw.ui.browser.Tree;
import com.nomagic.magicdraw.uml.BaseElement;
import com.nomagic.uml2.ext.magicdraw.classes.mdkernel.Comment;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JButton;

/**
 *
 * @author Luai
 */
public class ActionListenerQuantitativeDeleteButton implements ActionListener {

    private JButton jbDelete;
    
    
    public ActionListenerQuantitativeDeleteButton(JButton jbDelete) {
        this.jbDelete = jbDelete;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        
        Tree tree = Application.getInstance().getMainFrame().getBrowser().getContainmentTree();
        
        Node node = tree.getSelectedNode();
        if (node != null) {
           Object userObject = node.getUserObject();
           if (userObject instanceof BaseElement) {
               
               BaseElement baseElement = (BaseElement) userObject;
               Comment comment = QuantitativeHelper.INSTANCE.getComment(baseElement, true);
               if (comment == null) return;
               
               SessionManager.getInstance().createSession("Delete Comment");

               try {
                   ModelElementsManager.getInstance().removeElement(comment);
                   jbDelete.setEnabled(false);
               } catch (ReadOnlyElementException ex) {
               }
               SessionManager.getInstance().closeSession();
           }
        }
        
    }
    
}