/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package quantitative.controller;

import quantitative.QuantitativeHelper;
import com.nomagic.magicdraw.core.Application;
import com.nomagic.magicdraw.openapi.uml.ModelElementsManager;
import com.nomagic.magicdraw.openapi.uml.ReadOnlyElementException;
import com.nomagic.magicdraw.openapi.uml.SessionManager;
import com.nomagic.magicdraw.ui.browser.Node;
import com.nomagic.magicdraw.ui.browser.Tree;
import com.nomagic.magicdraw.uml.BaseElement;
import com.nomagic.uml2.ext.jmi.helpers.ModelHelper;
import com.nomagic.uml2.ext.jmi.helpers.StereotypesHelper;
import com.nomagic.uml2.ext.magicdraw.classes.mdkernel.Comment;
import com.nomagic.uml2.ext.magicdraw.classes.mdkernel.Element;
import com.nomagic.uml2.ext.magicdraw.mdprofiles.Stereotype;
import com.nomagic.uml2.impl.ElementsFactory;
import configReader.ConfigReader;
import configReader.Slot;
import configReader.Tag;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JTable;

/**
 *
 * @author Luai
 */
public class ActionListenerQuantitativeSaveButton implements ActionListener {

    //private JPanel panel;
    private JButton jbDelete;
    private Tag tag;
    private ConfigReader configReader;
    private JTable table;
    private boolean error = false;
    private final static String IN_SEP = "-";
    private final static String NEW_SEP = ", ";
    public final static String COMMENT_ID = "COMMENT_ID ";
    public final static String XML_ROOT = "Comment";
    private String commentName = "";
    //private String xmlTags;
    private StringBuilder xmlTags = new StringBuilder();
    private final static String FIXED_STRING_NAME = "MagicWand Quantitative Aspect Comment";
    
//    public static final String MANDAT_FIELD_AT_LEAST_ONE = ": At least one";
//    public static final String MANDAT_FIELD_EXATCLY_ONE = ": Exatcly one";
    
    public ActionListenerQuantitativeSaveButton(JButton jbDelete) {
        this.jbDelete = jbDelete;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        
        Tree tree = Application.getInstance().getMainFrame().getBrowser().getContainmentTree();
        
        Node node = tree.getSelectedNode();
        if (node != null) {
           Object userObject = node.getUserObject();
           if (userObject instanceof BaseElement) {
               
               BaseElement element = (BaseElement) userObject;
               
               if (table.getCellEditor() != null) {
                   table.getCellEditor().stopCellEditing();
               }
               
               String saveComment = getSaveComment();
               if (error) {
                   javax.swing.JOptionPane.showMessageDialog(null, saveComment);
                   error = false;
                   return;
               } else if (saveComment == null) {
                   return;
               }
                
//               // An comment already exists
//               if (AnnotationHelper.getComment(element) != null) {
//                   return;
//               }                
               Comment commentExists = QuantitativeHelper.INSTANCE.getComment(element, true);
               
               ElementsFactory f = Application.getInstance().getProject().getElementsFactory();
               Comment comment = f.createCommentInstance();
               comment.setBody(saveComment);
               //String xml = "<"+ XML_ROOT + ">\n" + xmlTags + "</" + XML_ROOT + ">";
               //comment.set_representationText(xml);
               
               ModelHelper.setElementComment((Element)element, comment);
               
               if (element.getObjectParent() == null) return;
               
               SessionManager.getInstance().createSession("Create comment");
               try {                  
                   if (commentExists != null) {
                       ModelElementsManager.getInstance().removeElement(commentExists);
                   }
                   ModelElementsManager.getInstance().addElement(comment, (Element) element.getObjectParent());
                   Stereotype stereotype = QuantitativeHelper.INSTANCE.getAnnotationStereotype();
                   StereotypesHelper.addStereotype(comment, stereotype);
                   jbDelete.setEnabled(true);
               } catch (ReadOnlyElementException ex) {
               }
               SessionManager.getInstance().closeSession();
               
           }
        }
        
    }
    
    private String getSaveComment() {
        StringBuilder str = new StringBuilder();
        str.append("<!-- ").append(FIXED_STRING_NAME).append(" -->\n");
        str.append("<").append(tag.getNameType()).append(" ").append(commentName).append("=\"[");
        xmlTags.append("<").append(XML_ROOT).append(">\n");
        
        if (table.getModel().getRowCount() == 0) return null;
        
        for (int row = 0; row < table.getModel().getRowCount()/*-1*/; row++) {
            String property = (String) table.getModel().getValueAt(row, 0);
            String value = (String) table.getModel().getValueAt(row, 1);
            String unit = (String) table.getModel().getValueAt(row, 2);
            String propertyXML = property;
            property = property.replaceAll("-", "");            
            property = property.replaceAll(" ", "");
            property = property.replaceAll("'", "\\\\'"); // Escaping
            property = property.substring(0, 1).toLowerCase() + property.substring(1);
            if (property.equals("")) continue;       
            
            value = value.replaceAll("-", "");
            value = value.replaceAll("'", "\\\\'");           
            
            updateUnitCustom(tag.getSlots().get(row));
            unit = unit.replaceAll("-", "");
            unit = unit.replaceAll("'", "\\\\'");
            Slot.Multiplicity multi = tag.getSlots().get(row).getMultiplicity();
            
            // Make a list if posible
            int index = row;
            StringBuilder list = new StringBuilder();
            while (index + 1 < table.getModel().getRowCount()) {
                
                //String s = ""+index;
                String nextProperty = (String) table.getModel().getValueAt(index + 1, 0);
                String nextValue = (String) table.getModel().getValueAt(index + 1, 1);
                String nextUnit = (String) table.getModel().getValueAt(index + 1, 2);
                
                nextValue = nextValue.replaceAll("-", "");
                nextValue = nextValue.replaceAll("'", "\\\\'");
                updateUnitCustom(tag.getSlots().get(index + 1));
                unit = unit.replaceAll("-", "");
                unit = unit.replaceAll("'", "\\\\'");
                
                if (propertyXML.equals(nextProperty)) {
                    list.append(getSaveRowListElement(property, propertyXML, nextValue, nextUnit, row, Slot.Multiplicity.NONE));
                    index++;
                } else {
                    break;
                }
            }
            if (!list.toString().equals("")) {
                if (value.equals("") && multi == Slot.Multiplicity.AT_LEAST_ONE) {
                    this.error = true;
                    return multi + ": " + property;
                }
                String firstRow = getSaveRowListElement(property, propertyXML, value, unit, row, multi);
                list = new StringBuilder(list.toString().trim());
                list = new StringBuilder(removeLastChar(list.toString()));
                str.append(property).append(IN_SEP + "[").append(firstRow).append(list).append("]" + NEW_SEP);
            } else {
                if ((multi == Slot.Multiplicity.EXACTLY_ONE || multi == Slot.Multiplicity.AT_LEAST_ONE)
                        && value.equals("")) {
                    this.error = true;
                    return multi + ": " + property;
                }                    
                
                str.append(getSaveRow(property, propertyXML, value, unit, row, multi));
                
            }
            row = index;
            
        }
        
        str = new StringBuilder(str.toString().trim());
        str = new StringBuilder(removeLastChar(str.toString()));
        str.append("]\">\n");
        xmlTags.append("</").append(XML_ROOT).append(">\n");
        str.append(xmlTags);
        str.append("</").append(tag.getNameType()).append(">");
        return str.toString();
    }
    
    private String getSaveRow(String property, String propertyXML, String value, String unit, int row, Slot.Multiplicity multi) {
        StringBuilder str = new StringBuilder();
        if (value.equals("")) {
            xmlTags.append("<").append(propertyXML).append(" unitValue=\"").append(unit).append("\" multi=\"").append(multi).append("\"").append("/>\n");
        } else if (value.equals("-")) {
            str.append(property).append(IN_SEP).append("'-'");
            xmlTags.append("<").append(propertyXML).append(" value=\"'-'\"");
            if (!unit.equals("") && Character.isUpperCase(property.charAt(0))) {
                str.append(IN_SEP).append("'").append(unit).append("'");
                xmlTags.append(" unitValue=\"").append(unit).append("\"");
            } else if (!unit.equals("")) {
                str.append(IN_SEP).append(unit);
                xmlTags.append(" unitValue=\"").append(unit).append("\"");
            }
            str.append(NEW_SEP);
            xmlTags.append(" multi=\"").append(multi).append("\"").append("/>\n");
        } else if (Character.isUpperCase(property.charAt(0))) {
            if (isInteger(value)) {
                if (unit.equals("")) {
                    str.append(property).append(IN_SEP).append(getInteger(value));
                    xmlTags.append("<").append(propertyXML).append(" value=\"").append(value).append("\"");
                } else {
                    str.append(property).append(IN_SEP).append(getInteger(value)).append(IN_SEP).append("'").append(unit).append("'");
                    xmlTags.append("<").append(propertyXML).append(" value=\"").append(value).append("\" unitValue=\"").append(unit).append("\"");
                }
                str.append(NEW_SEP);
                xmlTags.append(" multi=\"").append(multi).append("\"").append("/>\n");
            } else {
                if (unit.equals("")) {
                    str.append(property).append(IN_SEP).append("'").append(value).append("'");
                    xmlTags.append("<").append(propertyXML).append(" value=\"").append(value).append("\"");
                } else {
                    str.append(property).append(IN_SEP).append("'").append(value).append("'").append(IN_SEP).append("'").append(unit).append("'");
                    xmlTags.append("<").append(propertyXML).append(" value=\"").append(value).append("\" unitValue=\"").append(unit).append("\"");
                }
                str.append(NEW_SEP);
                xmlTags.append(" multi=\"").append(multi).append("\"").append("/>\n");
            }
        } else {
            str.append(property).append(IN_SEP).append(value);
            xmlTags.append("<").append(propertyXML).append(" value=\"").append(value).append("\"");
            if (!unit.equals("") && /*!unit.equals(tag.getSlots().get(row).getUnit()) &&*/ tag.getSlots().get(row).getUnitCustom()) {
                str.append(IN_SEP).append("'").append(unit).append("'");
                xmlTags.append(" unitValue=\"").append(unit).append("\" unitDef=\"true\"");
            } else if (!unit.equals("")) {
                str.append(IN_SEP).append(unit);
                xmlTags.append(" unitValue=\"").append(unit).append("\"");
            }
            str.append(NEW_SEP);
            xmlTags.append(" multi=\"").append(multi).append("\"").append("/>\n");
        }
        return str.toString();
    }
    
    private String getSaveRowListElement(String property, String propertyXML, String value, String unit, int row, Slot.Multiplicity first) {
        StringBuilder str = new StringBuilder();
        if (value.equals("")) {

        } else if (value.equals("-")) {
            str.append("'-'");
            xmlTags.append("<").append(propertyXML).append(" value=\"'-'\"");
            if (!unit.equals("") && Character.isUpperCase(property.charAt(0))) {
                str.append(IN_SEP).append("'").append(unit).append("'");
                xmlTags.append(" unitValue=\"").append(unit).append("\"");
            } else if (!unit.equals("")) {
                str.append(IN_SEP).append(unit);
                xmlTags.append(" unitValue=\"").append(unit).append("\"");
            }
            str.append(NEW_SEP);
            xmlTags.append(" multi=\"").append(first).append("\"").append("/>\n");
        } else if (Character.isUpperCase(property.charAt(0))) {
            if (isInteger(value)) {
                if (unit.equals("")) {
                    str.append(getInteger(value));
                    xmlTags.append("<").append(propertyXML).append(" value=\"").append(value).append("\"");
                } else {
                    str.append(getInteger(value)).append(IN_SEP).append("'").append(unit).append("'");
                    xmlTags.append("<").append(propertyXML).append(" value=\"").append(value).append("\" unitValue=\"").append(unit).append("\"");
                }
                str.append(NEW_SEP);
                xmlTags.append(" multi=\"").append(first).append("\"").append("/>\n");
            } else {
                if (unit.equals("")) {
                    str.append("'").append(value).append("'");
                    xmlTags.append("<").append(propertyXML).append(" value=\"").append(value).append("\"");
                } else {
                    str.append("'").append(value).append("'").append(IN_SEP).append("'").append(unit).append("'");
                    xmlTags.append("<").append(propertyXML).append(" value=\"").append(value).append("\" unitValue=\"").append(unit).append("\"");
                }
                str.append(NEW_SEP);
                xmlTags.append(" multi=\"").append(first).append("\"").append("/>\n");
            }
        } else {
            str.append(value);
            xmlTags.append("<").append(propertyXML).append(" value=\"").append(value).append("\"");
            if (!unit.equals("") && /*!unit.equals(tag.getSlots().get(row).getUnit()) &&*/ tag.getSlots().get(row).getUnitCustom()) {
                str.append(IN_SEP).append("'").append(unit).append("'");
                xmlTags.append(" unitValue=\"").append(unit).append("\" unitDef=\"true\"");
            } else if (!unit.equals("")) {
                str.append(IN_SEP).append(unit);
                xmlTags.append(" unitValue=\"").append(unit).append("\"");
            }
            str.append(NEW_SEP);
            xmlTags.append(" multi=\"").append(first).append("\"" + "/>\n");
        }
        return str.toString();
    }
    
    private boolean isInteger(String s) {
        try {
            Integer.parseInt(s);
        } catch (Exception e) {
            return false;
        }
        return true;
    }
    
    private int getInteger(String s) {
        return Integer.parseInt(s);
    }
    
    private String removeLastChar(String str) {
        if (str.length() > 0 && str.charAt(str.length() - 1) == ',') {
            str = str.substring(0, str.length() - 1);
        }
        return str;
    }
    
     private void updateUnitCustom(Slot slot) {
        slot.setUnitCustom(!configReader.fromConfigFile(tag, slot));
    }

    
    public void setTag(Tag tag) {
        this.tag = tag;
    }

    public void setConfigReader(ConfigReader configReader) {
        this.configReader = configReader;
    }
    
    public void setTable(JTable table) {
        this.table = table;
    }

    public void setCommentName(String humanName) {
        humanName = humanName.replaceAll("\\s+","");
        humanName = humanName.substring(0, 1).toLowerCase()+ humanName.substring(1);
        this.commentName = humanName;
    }

    
}
