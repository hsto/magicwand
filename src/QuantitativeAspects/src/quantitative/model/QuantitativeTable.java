/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package quantitative.model;

import configReader.ConfigReader;
import configReader.Slot;
import configReader.Tag;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import javax.swing.JCheckBox;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;
import quantitative.view.ButtonEditor;
import quantitative.view.ButtonRenderer;

/**
 *
 * @author Luai
 */
public class QuantitativeTable {

    private Tag tag;
    private ConfigReader configReader;
    private JTable table;
    private JPanel mainPanel = new JPanel();
    
    public QuantitativeTable(Tag tag, ConfigReader configReader, JTable table, JPanel mainPanel) {
        this.tag = tag;
        this.configReader = configReader;
        this.table = table;
        this.mainPanel = mainPanel;
        
        addAnnotationTable();
    }
    
       private void addAnnotationTable() {
        String[] header = new String[]{"Property", "Value", "Unit", "Type", "Default", "Multiplicity"};
        ArrayList<Object[]> data = new ArrayList<Object[]>();
        
        for (Slot s : tag.getSlots()) {
            String property = s.getProperty();
//            String info = "";
//           if (s.getMultiplicity() == Slot.Multiplicity.EXACTLY_ONE)
//                info = ActionListenerQuantitativeSaveButton.MANDAT_FIELD_EXATCLY_ONE;
            String value = s.getValue();            
            String unit = s.getUnit();
            String type = s.getType();
            String defaultValue = s.getDefaultValue();
            String multi = configReader.getMultiplicityChar(s.getMultiplicity());
            data.add(new Object[]{property,value,unit,type,defaultValue,multi});
        }
//        // For add new row
//        data.add(new Object[]{"","","","","",""});
        
        final ButtonRenderer buttonrenderer = new ButtonRenderer();
        final ButtonEditor buttonEditor = new ButtonEditor(new JCheckBox(),tag.getSlots());
        final ListTableQuantitativeModel model = new ListTableQuantitativeModel(data, header);
        
        table = new JTable(model) {
            @Override
            public boolean isCellEditable(int row, int column) {
                Slot slot = null;
                if (row < tag.getSlots().size()) {
                    slot = tag.getSlots().get(row);
                    if ((slot.getMultiplicity() == Slot.Multiplicity.EXACTLY_ONE && column == 0)
                            || (slot.getMultiplicity() == Slot.Multiplicity.PROPERTY_EDITABLE_FALSE && column == 0)) {
                        return false;
                    } else if (column == 3 || column == 4 | column == 5) {
                        return false;
                    } else if ((column == 0 && slot.getMultiplicity() != Slot.Multiplicity.ANY_NUMBER_OF) && (column == 0 && slot.getMultiplicity() != Slot.Multiplicity.AT_LEAST_ONE)) {
                        return false;
                    }
                    
                    else if (column == 2 && !slot.getUnit().isEmpty() && configReader.fromConfigFile(tag, slot)) {
                        return false;
                    }
                } 
//                // Add new Row
//                else if (row == this.getModel().getRowCount()-1 && (column == 1 || column == 2) ) {
//                    return false;
//                }
                return true;
            }
            
            @Override
            public TableCellRenderer getCellRenderer(int row, int column) {
                Slot slot = null;
                if (row < tag.getSlots().size()) {
                    slot = tag.getSlots().get(row);
                    if ((slot.getMultiplicity() == Slot.Multiplicity.ANY_NUMBER_OF
                            || slot.getMultiplicity() == Slot.Multiplicity.AT_LEAST_ONE) && column == 0) {
                        buttonrenderer.setSlot(slot);
                        return buttonrenderer;
                    }
                } 
//                // Add new Row
//                else if (this.getModel().getRowCount() - 1 == row && column == 0) {
//                    buttonrenderer.setSlot(null);
//                    return buttonrenderer;
//                } 
                
                return super.getCellRenderer(row, column);
                
            }
            
            @Override
             public TableCellEditor getCellEditor(int row, int column) {
                Slot slot = null;
                if (row < tag.getSlots().size()) {
                    slot = tag.getSlots().get(row);
                    if ((slot.getMultiplicity() == Slot.Multiplicity.ANY_NUMBER_OF
                            || slot.getMultiplicity() == Slot.Multiplicity.AT_LEAST_ONE) && column == 0) {
                        buttonEditor.setTable(this);
                        buttonEditor.setSlot(slot);
                        return buttonEditor;
                    }
                } 
//                // Add new row
//                else if (this.getModel().getRowCount() - 1 == row && column == 0) {
//                    buttonEditor.setTable(this);
//                    buttonEditor.setSlot(null);
//                    return buttonEditor;
//                }
                
                return super.getCellEditor(row, column);

            }
            
            @Override
            public String getToolTipText(MouseEvent e) {
                String toolTip = null;
                java.awt.Point p = e.getPoint();
                if (p == null) return toolTip;
                int row = rowAtPoint(p);
                int col = columnAtPoint(p);
                
                if (tag != null && col == 5 && row < tag.getSlots().size()) {
                	try {
	                    String tip = tag.getSlots().get(row).getMultiplicity().toString();
	                    tip = tip.replaceAll("_", " ");
	                    tip = tip.substring(0, 1) + tip.substring(1).toLowerCase();
	                    toolTip = tip;
                	} catch (Exception exp) {
                		
                	}
                }
                
                return toolTip;
            }
        };
        
        getTable().getColumn("Property").setMinWidth(100);
        getTable().getColumn("Property").setPreferredWidth(120);
        getTable().getColumn("Value").setPreferredWidth(80);
        getTable().getColumn("Unit").setPreferredWidth(40);
        getTable().getColumn("Type").setPreferredWidth(40);
        getTable().getColumn("Default").setPreferredWidth(40);
        getTable().getColumn("Multiplicity").setPreferredWidth(30);
        
        //table.setPreferredScrollableViewportSize(new Dimension(500, 200));
        getTable().setFillsViewportHeight(true);

        //table.setRowHeight(22);
        mainPanel.add(new JScrollPane(getTable()));
        getTable().setRowHeight(22);
    }

    public JTable getTable() {
        return table;
    }
    
}
