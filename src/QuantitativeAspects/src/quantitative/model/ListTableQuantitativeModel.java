/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package quantitative.model;

import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author Luai
 */
public class ListTableQuantitativeModel extends AbstractTableModel {

    @Override
    public void fireTableCellUpdated(int row, int column) {
    }

    private String[] header;
    private ArrayList<Object[]> data;
    
    public ListTableQuantitativeModel(ArrayList<Object[]> data, String[] header) {
        this.data = data;
        this.header = header;
    }

    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
        data.get(rowIndex)[columnIndex] = aValue;
        fireTableCellUpdated(rowIndex, columnIndex);
        //fireTableDataChanged();
        
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return true;
    }

    @Override
    public int getRowCount() {
        return data.size();
    }

    @Override
    public int getColumnCount() {
        return header.length;
    }
    
    @Override
    public String getColumnName(int colum) {
        return header[colum];
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        return data.get(rowIndex)[columnIndex];
    }

    public ArrayList<Object[]> getRows() {
        return data;
    }
    
    public void addRow() {
        data.add(new Object[] {"","","","","",""});
        fireTableRowsInserted(data.size()-2, data.size()-1);
    }
    
    public void addRowIndex(int index, Object[] object) {
        data.add(index, object);
        fireTableRowsInserted(index, index+1);
    }
    
    public void addRowIndexBefore(int index, Object[] object) {
        data.add(index, object);
        fireTableRowsInserted(index, index-1);
    }
    
    public void removeRow() {
        if (data.size() == 1) return; 
        data.remove(data.size()-1);
        fireTableRowsDeleted(data.size()-2, data.size()-1);
    }
    
    public String[] getHeader() {
        return header;
    }
}
