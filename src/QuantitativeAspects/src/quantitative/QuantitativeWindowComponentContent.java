/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package quantitative;

import java.awt.Component;

import quantitative.view.JPanelQuantitativeAnnotation;

import com.nomagic.magicdraw.ui.browser.WindowComponentContent;

/**
 *
 * @author Luai
 */
public class QuantitativeWindowComponentContent implements WindowComponentContent{
    
    private final JPanelQuantitativeAnnotation panel;
    
    public QuantitativeWindowComponentContent(JPanelQuantitativeAnnotation panel) {
        this.panel = panel;
    }

    @Override
    public Component getWindowComponent() {
        //Application.getInstance().getGUILog().log("enter Annotation getWindowComponent");
        return panel;
    }

    @Override
    public Component getDefaultFocusComponent() {
        //Application.getInstance().getGUILog().log("enter Annotation getDefaultFocusComponent");
        return panel;
    }
    
}
