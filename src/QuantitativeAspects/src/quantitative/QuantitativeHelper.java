/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package quantitative;

import com.nomagic.magicdraw.core.Application;
import quantitative.controller.ActionListenerQuantitativeSaveButton;
import com.nomagic.magicdraw.core.Project;
import com.nomagic.magicdraw.openapi.uml.ReadOnlyElementException;
import com.nomagic.magicdraw.uml.BaseElement;
import com.nomagic.magicdraw.uml.ClassTypes;
import com.nomagic.magicdraw.uml.InheritanceVisitor;
import com.nomagic.uml2.ext.jmi.helpers.StereotypesHelper;
import com.nomagic.uml2.ext.jmi.reflect.VisitorContext;
import com.nomagic.uml2.ext.magicdraw.classes.mdkernel.Comment;
import com.nomagic.uml2.ext.magicdraw.classes.mdkernel.Element;
import com.nomagic.uml2.ext.magicdraw.mdprofiles.Stereotype;
import configReader.ConfigReader;
import configReader.Slot;
import configReader.Tag;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import quantitative.view.JPanelQuantitativeAnnotation;

/**
 *
 * @author Luai
 */
public enum QuantitativeHelper {
    INSTANCE;

    private final static String ATTRIBUTE_VALUE = "value";
    private final static String ATTRIBUTE_UNIT_VALUE = "unitValue";
    private final static String ATTRIBUTE_UNIT_DEF = "unitDef";
    private final static String ATTRIBUTE_MULTIPLICITY = "multi";
    private final static String ANNOTAION_STEREOTYPE_NAME = "Quantitative Stereotype";
    
    public static Project project;
    
    private TreeVisitor visitor = new TreeVisitor();
    
    public Comment getComment(BaseElement selectedBaseElement, boolean visitTree) {
        Comment comment = null;
        if (!(selectedBaseElement instanceof Element)) return null;
        Element element = (Element) selectedBaseElement;
        Element elementParent = (Element) selectedBaseElement.getObjectParent();         
        if (elementParent != null && elementParent.hasOwnedComment()) {
            for (Comment c : elementParent.getOwnedComment()) {
                if (checkComment(c, element)) {
                    return c;
                }                
            }           
            
        }
        
        boolean existInConfig = false;
        String classType = ClassTypes.getShortName(element.getClassType());
        ConfigReader config = JPanelQuantitativeAnnotation.getEnabledConfigReader();
        for (Tag tag : config.getTags()) {
            if (classType.equalsIgnoreCase(tag.getNameType()) || tag.getSubTags().contains(classType)) {
                existInConfig = true;
            }
        }
        if (!existInConfig) return comment;        
        
        if (visitTree) {
            // If it's not foud locally it  serches from the tree
            visitor.selectedElement = element;
            comment = visitTree();
        }

        return comment;
    }
    
    private boolean checkComment(Comment c, Element element) {
        if ( !(c.hasAnnotatedElement() && c.getAnnotatedElement().contains(element)) ) return false;
        
        Document doc = null;
        String xmlTags = null;
        try {
            DocumentBuilder db = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            InputSource is = new InputSource();

            //is.setCharacterStream(new StringReader(c.get_representationText()));                    
            is.setCharacterStream(new StringReader(c.getBody()));
            doc = db.parse(is);

            if (doc.getDocumentElement() != null) {
                NodeList nodes = doc.getDocumentElement().getChildNodes();
                if (nodes.getLength() > 0) {
                    for (int i = 0; i < nodes.getLength(); i++) {
                        if (nodes.item(i).getNodeType() == Node.ELEMENT_NODE) {
                            xmlTags = nodes.item(i).getNodeName();
                            break;
                        }
                    }
                }
            }

            if (xmlTags != null && xmlTags.equals(ActionListenerQuantitativeSaveButton.XML_ROOT)
                    && c.hasAnnotatedElement() && c.getAnnotatedElement().contains(element)) {
                return true;
            }

//                    if (doc.getDocumentElement() != null) {
//                        root = doc.getDocumentElement().getNodeName();
//                    }
//                    if (root != null && root.equals(ActionListenerAnnotationSaveButton.XML_ROOT)
//                            && c.hasAnnotatedElement() && c.getAnnotatedElement().contains(element)) {
//                        return c;
//                    }
        } catch (Exception ex) {

        }
        return false;
    }
    
    private Comment visitTree() {
        visitor.foundComment = null;
        Element root = Application.getInstance().getMainFrame().getBrowser().getContainmentTree().getRootElement();
        ArrayList<Element> all = new ArrayList<Element>();
        all.add(root);
        Element current;

        // if current element has children, list will be increased.
        for (int i = 0; i < all.size(); i++) {
            current = all.get(i);
            try {
                // let's perform some action with this element in visitor.
                current.accept(visitor);
                if (visitor.foundComment != null) {
                    return visitor.foundComment; // Match found
                }
            } catch (Exception e) {

            }
            // add all children into end of this list, so it emulates recursion.
            all.addAll(current.getOwnedElement());
        }
        return null;
    }

    class TreeVisitor extends InheritanceVisitor {

        public Element selectedElement;
        public Comment foundComment;

        @Override
        public void visitElement(Element element, VisitorContext context) {
            super.visitElement(element, context);
            if (ClassTypes.getShortName(element.getClassType()).equals("Comment")) {
                if (checkComment((Comment) element, selectedElement)) {
                    foundComment = (Comment) element;
                }
            }
        }

    }
    
    public List<Slot> getCommentElements(Comment comment) {
        Document doc = null;
        try {
            DocumentBuilder db = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            InputSource is = new InputSource();
            //is.setCharacterStream(new StringReader(comment.get_representationText()));
            is.setCharacterStream(new StringReader(comment.getBody()));
            doc = db.parse(is);
        } catch (Exception ex) {

        }
        
        List<Slot> slots = new ArrayList<Slot>();
        int listCount = 0;
        String previous = "";
        NodeList nodes = null;
        if (doc != null && doc.getDocumentElement() != null) {
            nodes = doc.getElementsByTagName(ActionListenerQuantitativeSaveButton.XML_ROOT);
            Node node = nodes.item(0);
            nodes = node.getChildNodes();
        }
        if (nodes == null) return slots;
        
        for (int i = 0; i < nodes.getLength(); i++) {
            Node node = nodes.item(i);
            if (node.getNodeType() == Node.ELEMENT_NODE) {
                org.w3c.dom.Element element = (org.w3c.dom.Element) node;
                String property = element.getNodeName();
                String value = element.getAttribute(ATTRIBUTE_VALUE);
                String unit = element.getAttribute(ATTRIBUTE_UNIT_VALUE);
                String unitDef = element.getAttribute(ATTRIBUTE_UNIT_DEF);
                String multi = element.getAttribute(ATTRIBUTE_MULTIPLICITY);
                
                if (previous.equals(property)) {
                    listCount++;
                } else {
                    listCount = 0;
                }

                Slot s = new Slot();
                if (!property.equals("")) {
                    s.setProperty(property);
                }
                if (!value.equals("")) {
                    s.setValue(value);
                }
                if (!unit.equals("")) {
                    s.setUnit(unit);
                }
                if (!unitDef.equals("")) {
                    s.setUnitCustom(true);
                }
                if (multi.equals(Slot.Multiplicity.EXACTLY_ONE.toString())) {
                    s.setMultiplicity(Slot.Multiplicity.EXACTLY_ONE);
                }
                if (multi.equals(Slot.Multiplicity.ANY_NUMBER_OF.toString()) || multi.equals(Slot.Multiplicity.AT_LEAST_ONE.toString())) {
                    s.setMultiplicity(Slot.Multiplicity.valueOf(multi));
                    slots.add(slots.size() - listCount, s);
                    continue;
                }
                slots.add(s);
                previous = property;
            }
        }

        return slots;

    }

    public void createAnnotationStereotype(Project project) throws ReadOnlyElementException {
        if (StereotypesHelper.getStereotype(project, ANNOTAION_STEREOTYPE_NAME) == null) {        
            com.nomagic.uml2.ext.magicdraw.classes.mdkernel.Class metaClass = StereotypesHelper.getMetaClassByName(project, "Comment");
            StereotypesHelper.createStereotype(project.getModel(), ANNOTAION_STEREOTYPE_NAME, Arrays.asList(metaClass));            
        }

    }
    
    public Stereotype getAnnotationStereotype() {
        return StereotypesHelper.getStereotype(project, ANNOTAION_STEREOTYPE_NAME);
    }

    public void setProject(Project project) {
        QuantitativeHelper.project = project;
    }
    
    public List<Completeness> getSubCompletness(com.nomagic.magicdraw.ui.browser.Node node, ConfigReader configReader) {
        List<Completeness> compList = new ArrayList<Completeness>();
        
        Object userObject = node.getUserObject();
        if (userObject instanceof BaseElement) {
            BaseElement element = (BaseElement) userObject;
            Completeness compl = getCompletness(element, configReader);
            if (compl != null)
                compList.add(compl);
        }
        
        List<com.nomagic.magicdraw.ui.browser.Node> childNodes = node.getChildren();
        if (childNodes != null && childNodes.size() > 0) {
            for (com.nomagic.magicdraw.ui.browser.Node n : childNodes) {
                compList.addAll(getSubCompletness(n, configReader));
            }
        }
        
        return compList;
    }
    
    public Completeness getCompletness(BaseElement baseElement, ConfigReader configReader) {        

        Comment comment = getComment(baseElement, false);
        if (comment == null) {
            for (Tag tag : configReader.getTags()) {
                if (baseElement.getHumanType().equals(tag.getNameType())) {
                    return new Completeness(0, tag.getSlots().size());
                }
            }
            return null;
        }
        
        int i = 0;
        List<Slot> slots = getCommentElements(comment);
        for (Slot slot : slots) {
            if (!slot.getValue().isEmpty()) i++;
        }        
        
        Completeness completeness = new Completeness(i, slots.size());
        return completeness;
    }
    
    public class Completeness {

        public int complete;
        public int total;
        
        public Completeness(int comp, int tot) {
            complete = comp;
            total = tot;
        }
    
    }
}

//private static String spiltString(String string, int index) {
//        String[] str = string.split(" ");
//        return str[index];
//}