/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package quantitative.view;

import quantitative.model.QuantitativeTable;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import javax.swing.BorderFactory;
import javax.swing.GroupLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.event.TreeSelectionListener;

import quantitative.QuantitativeHelper;
import quantitative.QuantitativeHelper.Completeness;
import quantitative.controller.QuantitativeTreeSelectionListener;
import quantitative.controller.ActionListenerAddTemplate;
import quantitative.controller.ActionListenerQuantitativeDeleteButton;
import quantitative.controller.ActionListenerQuantitativeSaveButton;

import com.nomagic.magicdraw.core.Application;
import com.nomagic.magicdraw.ui.browser.Node;
import com.nomagic.magicdraw.ui.browser.Tree;
import com.nomagic.magicdraw.uml.BaseElement;
import com.nomagic.uml2.ext.magicdraw.classes.mdkernel.Comment;

import configReader.ConfigReader;
import configReader.Slot;
import configReader.Tag;
import lib.SharedHelper;
import lib.SharedHierarchyListener;

/**
 *
 * @author Luai
 */
public class JPanelQuantitativeAnnotation extends JPanel {
    
    private final JPanel innerPanel = new JPanel();
    private JPanel mainPanel = new JPanel();
    private JPanel settingsPanel = new JPanel();
    private final GroupLayout layout = new GroupLayout(innerPanel);
    
    private final JLabel labelElementName = new JLabel();
    private final JLabel completenessNodeLabelHeader = new JLabel("Current: ");
    private final JLabel completenessSubTreeLabelHeader = new JLabel("Subtree: ");
    private final JLabel completenessNodeLabel = new JLabel("-");
    private final JLabel completenessSubTreeLabel = new JLabel("-");
    
    private final JButton foldButtonSettings = new JButton();
	private final JButton foldButtonAnnotations = new JButton();
    
	public final JTextField txtTemplates = new JTextField("Empty");
	
	private final JButton jbTemplates = new JButton("...");
    private final JButton jbSave = new JButton();
    private final JButton jbDelete = new JButton(); 
    
    //private SteelCheckBox completenessCheckBox = new SteelCheckBox();
    
//    private final JSeparator jSeparatorHeader = new JSeparator();
//    private final JSeparator jSeparatorButton = new JSeparator();
    
    public static final ConfigReader configReader = null /*= new ConfigReader()*/;
    public static ConfigReader newConfigReader = null;
    private Tag tag;
    private JTable table;
    
    private ActionListenerQuantitativeSaveButton actionListenerAnnotationSaveButton;
    
    private BaseElement element;
    
    private final static String ICON_RIGHT_FILE_NAME = "arrow-right.png";
    private final static String ICON_DOWN_FILE_NAME = "arrow-down.png";
	private final static String FILE_NAME = "link_QuantitativeAspects.txt";
	private JLabel logoLabel = new JLabel();
    
	private ImageIcon imageIconRight;
	private ImageIcon imageIconDown;	
	
    private QuantitativeTreeSelectionListener selectionListener = new QuantitativeTreeSelectionListener(this);
    
    public JPanelQuantitativeAnnotation() {
        //configReader = new ConfigReader();
        tag = null;
        
        initComponents();
        this.addHierarchyListener(new SharedHierarchyListener(this, JPanelQuantitativeAnnotation.this.getClass().getName(), false));
    }
    
    private void initComponents() {
        logoLabel = SharedHelper.INSTANCE.initLogo();
        SharedHelper.INSTANCE.initLogoLink(logoLabel, getClass().getClassLoader().getResourceAsStream(FILE_NAME));
        imageIconRight = SharedHelper.INSTANCE.initIcon(ICON_RIGHT_FILE_NAME);
        imageIconDown = SharedHelper.INSTANCE.initIcon(ICON_DOWN_FILE_NAME);
        
        Font fontSmallB = new Font("",Font.BOLD,11);
        Font fontSmall = new Font("",Font.PLAIN,11);
        completenessNodeLabelHeader.setFont(fontSmallB);
        completenessNodeLabelHeader.setToolTipText("Completeness indicator");
        completenessSubTreeLabelHeader.setFont(fontSmallB);
        completenessSubTreeLabelHeader.setToolTipText("Completeness indicator");
        completenessNodeLabel.setFont(fontSmall);
        completenessNodeLabel.setToolTipText("Completeness indicator");
        completenessSubTreeLabel.setFont(fontSmall);
        completenessSubTreeLabel.setToolTipText("Completeness indicator");
        
        if (configReader != null && !configReader.getTags().isEmpty())
        	txtTemplates.setText("Default config file");
        
        JLabel labelTitel = new JLabel();
    	labelTitel.setFont(new Font("",Font.BOLD,16));
    	labelTitel.setText("Quantitative Annotations");
    	labelTitel.setMinimumSize(new Dimension(40,25));
        labelElementName.setFont(new Font("",Font.PLAIN,14));
        labelElementName.setText("Select element in containment tree");
        labelElementName.setMinimumSize(new Dimension(40,25));
        Font font = new Font("",Font.BOLD,14);
        initFoldButton(foldButtonSettings, settingsPanel, "Settings", font);
        initFoldButton(foldButtonAnnotations, mainPanel, "Annotations", font);	        
        
//        completenessCheckBox.setSelected(true);
//        completenessCheckBox.setText("Show");
//        completenessCheckBox.setBorder(BorderFactory.createEmptyBorder(0,0,0,20));
//        completenessCheckBox.setFont(new Font("Arial Italic", Font.PLAIN, 12));
//        completenessCheckBox.addActionListener(new ActionListener() {			
//			@Override
//			public void actionPerformed(ActionEvent e) {
//				if (completenessCheckBox.isSelected()) {
//					completenessCheckBox.setText("Show");
//					completenessNodeLabelHeader.setVisible(true);
//					completenessNodeLabel.setVisible(true);
//					completenessSubTreeLabelHeader.setVisible(true);
//					completenessSubTreeLabel.setVisible(true);
//				} else {
//					completenessCheckBox.setText("Hide");
//					completenessNodeLabelHeader.setVisible(false);
//					completenessNodeLabel.setVisible(false);
//					completenessSubTreeLabelHeader.setVisible(false);
//					completenessSubTreeLabel.setVisible(false);
//				}
//				
//			}
//		});
        
        JLabel labelTemplates = new JLabel("Templates");
        GroupLayout layoutSettings = new GroupLayout(settingsPanel);
        settingsPanel.setLayout(layoutSettings);
        settingsPanel.setMinimumSize(new Dimension(80,40));
        settingsPanel.setMaximumSize(new Dimension(Short.MAX_VALUE,40));
        txtTemplates.setEditable(false);
        jbTemplates.setMinimumSize(new Dimension(30,20));
        jbTemplates.setPreferredSize(new Dimension(30,20));
        jbTemplates.setMaximumSize(new Dimension(30,20));
        layoutSettings.setAutoCreateGaps(true);
        layoutSettings.setAutoCreateContainerGaps(true);
        layoutSettings.setHorizontalGroup(layoutSettings.createSequentialGroup()
                 .addComponent(labelTemplates)
                 .addComponent(txtTemplates)
                 .addComponent(jbTemplates)
        );
        
        layoutSettings.setVerticalGroup(layoutSettings.createSequentialGroup()
        		.addGroup(layoutSettings.createParallelGroup()
        				.addComponent(labelTemplates)
                        .addComponent(txtTemplates)
                        .addComponent(jbTemplates)
        		)
        );
        
        jbTemplates.setToolTipText("Add template");        
        jbTemplates.addActionListener(new ActionListenerAddTemplate(this));
        jbSave.setText("Confirm");
        jbSave.setToolTipText("Confirm annotation");
        actionListenerAnnotationSaveButton = new ActionListenerQuantitativeSaveButton(jbDelete);
        jbSave.addActionListener(actionListenerAnnotationSaveButton);
        jbSave.setEnabled(false);
        jbDelete.setText("Clear");
        jbDelete.setToolTipText("Remove the filled data in the table");
        jbDelete.addActionListener(new ActionListenerQuantitativeDeleteButton(jbDelete));
        jbDelete.setEnabled(false);
        
        innerPanel.setLayout(layout);
        layout.setAutoCreateGaps(true);
        layout.setAutoCreateContainerGaps(true);
        
        //mainPanel.setLayout(new BoxLayout(mainPanel, BoxLayout.Y_AXIS));
        mainPanel.setLayout(new GridLayout(1,0));
        
        layout.setHorizontalGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup()
                		.addGroup(layout.createSequentialGroup()
                				.addGroup(layout.createParallelGroup()
                				.addComponent(labelTitel)
                				.addComponent(labelElementName)
                				)
                			.addGap(4, 4, Short.MAX_VALUE)
                			.addComponent(logoLabel)
                        )
//                        .addComponent(jSeparatorHeader)                        
                        .addComponent(foldButtonSettings)
                        .addComponent(settingsPanel)
                        .addComponent(foldButtonAnnotations)
                        .addComponent(mainPanel)
//                        .addComponent(jSeparatorButton)                        
                        .addGroup(layout.createSequentialGroup()
                                .addComponent(jbSave)
                                .addComponent(jbDelete)
                                .addGroup(layout.createParallelGroup()
                                    .addComponent(completenessNodeLabelHeader)
                                    .addComponent(completenessSubTreeLabelHeader)
                                )
                                .addGroup(layout.createParallelGroup()
                                    .addComponent(completenessNodeLabel)
                                    .addComponent(completenessSubTreeLabel)
                                )
                        )                        
                )     
        );
        
        layout.setVerticalGroup(layout.createSequentialGroup()
        		.addGroup(layout.createParallelGroup()
        				.addGroup(layout.createSequentialGroup()
        					.addComponent(labelTitel)
        					.addComponent(labelElementName)
        			)
        			.addComponent(logoLabel)
        		)
//                .addComponent(jSeparatorHeader, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)                
                .addComponent(foldButtonSettings)
                .addComponent(settingsPanel)
        		.addComponent(foldButtonAnnotations)
        		.addComponent(mainPanel)
//                .addComponent(jSeparatorButton, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGroup(layout.createParallelGroup()
                        .addComponent(jbSave)
                        .addComponent(jbDelete)
                        .addGroup(layout.createSequentialGroup()
                            .addComponent(completenessNodeLabelHeader)
                            .addComponent(completenessSubTreeLabelHeader)
                        )
                        .addGroup(layout.createSequentialGroup()
                            .addComponent(completenessNodeLabel)
                            .addComponent(completenessSubTreeLabel)
                        )
                )
                
        );
        
        //viewport.add(innerPanel);
        setLayout(new BorderLayout());
        add(innerPanel);
       
    }
    
    private void initFoldButton(final JButton foldButton, final JPanel panel, final String name, Font font) {
    	panel.setVisible(false);
    	foldButton.setFont(font);
        SharedHelper.INSTANCE.setIconName(true, foldButton, name, imageIconRight, imageIconDown);
    	foldButton.setVisible(true);
    	foldButton.setMinimumSize(new Dimension(40,25));
    	foldButton.setMaximumSize(new Dimension(Short.MAX_VALUE, 30));
    	foldButton.setContentAreaFilled(false);
    	foldButton.setHorizontalAlignment(SwingConstants.LEFT);
    	foldButton.setBorder(BorderFactory.createEmptyBorder(4,1,4,4));
    	foldButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				if (panel.isVisible()) {
					panel.setVisible(false);
                                        SharedHelper.INSTANCE.setIconName(true, foldButton, name, imageIconRight, imageIconDown);					
					if (name.equals("Annotations")) {
						jbSave.setVisible(false);
						jbDelete.setVisible(false);
						completenessNodeLabelHeader.setVisible(false);
						completenessSubTreeLabelHeader.setVisible(false);
						completenessNodeLabel.setVisible(false);
						completenessSubTreeLabel.setVisible(false);
					}
				} else {
					panel.setVisible(true);
                                        SharedHelper.INSTANCE.setIconName(false, foldButton, name, imageIconRight, imageIconDown);
					if (name.equals("Annotations")) {
						jbSave.setVisible(true);
						jbDelete.setVisible(true);
						completenessNodeLabelHeader.setVisible(true);
						completenessSubTreeLabelHeader.setVisible(true);
						completenessNodeLabel.setVisible(true);
						completenessSubTreeLabel.setVisible(true);
					}
				}
			}
		});
    	if (name.equals("Annotations")) {
    		jbSave.setVisible(false);
			jbDelete.setVisible(false);
    		completenessNodeLabelHeader.setVisible(false);
			completenessSubTreeLabelHeader.setVisible(false);
			completenessNodeLabel.setVisible(false);
			completenessSubTreeLabel.setVisible(false);
    	}
    }

    private QuantitativeTable quantitativeTable;
    private void updatePanel(String type, String humanName) {
          String elementName = humanName.replaceFirst(type + " ", "");
          labelElementName.setText(type + " '" + elementName + "'");
          //addAnnotationTable();
          ConfigReader config = getEnabledConfigReader();
          quantitativeTable = new QuantitativeTable(tag, config, table, mainPanel);
          actionListenerAnnotationSaveButton.setTag(tag);
          actionListenerAnnotationSaveButton.setConfigReader(config);
          actionListenerAnnotationSaveButton.setTable(quantitativeTable.getTable());
          actionListenerAnnotationSaveButton.setCommentName(humanName);       
    }
    
    public void setTagName(String nameType, String humanName) {
        if (configReader == null && newConfigReader == null) return;
        
        ConfigReader config = getEnabledConfigReader();
        
        for (Tag t : config.getTags()) {
            
            if (!t.getSubTags().isEmpty()) {
                for (String s : t.getSubTags()) {
                    if (nameType.equals(s) || nameType.replaceAll(" ", "").equals(s)) {
                        tag = t;
                    }
                }
            }
            
            else if (t.getNameType().equals(nameType)) {
                tag = t;
            }

//            if (t.getComment().contains(ConfigReader.SUBSET_VAR1)) {
//            String str = "";
//            List<String> list = t.getSubTags();
//            Collections.sort(list);
//            for (String s : list) {
//                str += s + "\n";
//            }
//            javax.swing.JOptionPane.showMessageDialog(null, "Length " + t.getSubTags().size() + "\n" + str);
//            }
        }
        
        if (tag != null) {
            updatePanel(nameType, humanName);
            enableButtons();
        } else {
            tag = new Tag(nameType);
            updatePanel(nameType, humanName);
            enableButtons();
//            labelElementName.setText("No annotation for: "+ humanName);
//            disableButtons();
        }
    }
    
    public void setElement() {
    	if (this.element != null) setElement(element);
    }
    
    public void setElement(BaseElement element) {
    	this.element = element;
        if (tag == null) {
            setTagName(element.getHumanType(), element.getHumanName());
            //setTagName("UseCase", element.getHumanName());
        } else if (element.getHumanType().equals(tag.getNameType())) {
            clearMainPanel();
            setTagName(element.getHumanType(), element.getHumanName());  
        } else {
            clearMainPanel();
            tag = null;
            setTagName(element.getHumanType(), element.getHumanName());  
        }
        
        // Update the fields if a comment already exists
        if (tag != null) {
            Comment comment = QuantitativeHelper.INSTANCE.getComment(element, true);
            if (comment != null) {
                updateAndReadFromComment(comment, element.getHumanType(), element.getHumanName());
            }
            
            //updateCompleteness(element, node);
        }
        
        
    }
    
    private void clearMainPanel() {
        mainPanel.removeAll();
        mainPanel.revalidate();
        ConfigReader config = getEnabledConfigReader();
        config.processTokensReset(tag);
        //tag = null;
        table = null;
    }    

    
    private void updateAndReadFromComment(Comment comment, String humanType, String humanName) {
        mainPanel.removeAll();
        List<Slot> slots = QuantitativeHelper.INSTANCE.getCommentElements(comment);
        
        // Check new properties
        List<Slot> tempList = new ArrayList<Slot>();
        boolean exists = false;
        for (Slot s : tag.getSlots()) {
            String property = s.getProperty();
            for (Slot n : slots) {
                if (property.equals(n.getProperty())) {
                    exists = true;
                    break;
                }
            }
            if (!exists) tempList.add(s);
            exists = false;
        }
        
        for (Slot s : tempList) {
            slots.add(s);
        }
        
        tag.setSlots(slots);
        updatePanel(humanType, humanName);
        
        mainPanel.revalidate();
    }
    
    private void updateCompleteness(BaseElement baseElement, Node node) {
    	ConfigReader config = getEnabledConfigReader();
        Completeness complElement = QuantitativeHelper.INSTANCE.getCompletness(baseElement, config);
        List<Completeness> complSubTree = QuantitativeHelper.INSTANCE.getSubCompletness(node, config);
        
        if (complElement == null)
            completenessNodeLabel.setText("-");
        else            
            completenessNodeLabel.setText(complElement.complete + " of " + complElement.total + " completed");
        
        if (complSubTree == null || complSubTree.isEmpty()) {
            completenessSubTreeLabel.setText("-");
        }
        else {
            int completed = 0, total = 0;
            for (Completeness c : complSubTree) {
                completed += c.complete;
                total += c.total;
            }

            completenessSubTreeLabel.setText(completed + " of " + total + " completed");
        }
    }
    
    public static ConfigReader getEnabledConfigReader() {
    	ConfigReader config = (newConfigReader == null) ? configReader : newConfigReader;
    	return config;
    }
    
    public TreeSelectionListener getSelectionListener() {
        return selectionListener;
    }

    public JPanel getInnerPanel() {
        return innerPanel;
    }
    
    private void disableButtons() {
        jbSave.setEnabled(false);
        jbDelete.setEnabled(false);
    }
    
    private void enableButtons() {
        jbSave.setEnabled(true);
        //if (enableCheckDeleteButton()) jbDelete.setEnabled(true);
        jbDelete.setEnabled(true);
    }
    
    // Checks if there is a comment to delete
    private boolean enableCheckDeleteButton() {
        Tree tree = Application.getInstance().getMainFrame().getBrowser().getContainmentTree();
        
        Node node = tree.getSelectedNode();
        if (node != null) {
           Object userObject = node.getUserObject();
           if (userObject instanceof BaseElement) {
               
               BaseElement baseElement = (BaseElement) userObject;               
               if (QuantitativeHelper.INSTANCE.getComment(baseElement, false) != null)
                   return true;
           }
        }
        return false;
    }

}
