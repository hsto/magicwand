/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package quantitative.view;

import quantitative.model.ListTableQuantitativeModel;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import javax.swing.DefaultCellEditor;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.JTextArea;
import configReader.Slot;

/**
 *
 * @author Luai
 */
public class ButtonEditor extends DefaultCellEditor {

    protected JButton button;
    private boolean isPushed;
    private JTable table;
    private int row;
    private Slot slot;
    private List<Slot> slots;

    public ButtonEditor(JCheckBox checkBox, List<Slot> slots) {
        super(checkBox);
        this.slots = slots;
        button = new JButton();
        //button.setOpaque(true);
        button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                fireEditingStopped();
            }
        });
    }

    @Override
    public Component getTableCellEditorComponent(JTable table, Object value,
            boolean isSelected, int row, int column) {
        this.row = row;
        JPanel panel = new JPanel(new BorderLayout());
        button.setText("+");
        button.setContentAreaFilled(false);
        isPushed = true;
        panel.add(button, BorderLayout.WEST);
        
        JTextArea field = new JTextArea("");
        if (slot != null) {
//            String multi = "";
//            if (slot.getMultiplicity() == Slot.Multiplicity.AT_LEAST_ONE) 
//                multi = ActionListenerQuantitativeSaveButton.MANDAT_FIELD_AT_LEAST_ONE;

            field.setText(" " + slot.getProperty());
        }
//        if (isSelected) {
//            field.setForeground(table.getSelectionForeground());
//            field.setBackground(table.getSelectionBackground());
//        } else {
//            field.setForeground(table.getForeground());
//            field.setBackground(table.getBackground());
//        }

        panel.add(field, BorderLayout.CENTER);
        return panel;
    }

    @Override
    public Object getCellEditorValue() {
        if (isPushed) {
            if (slot != null) {
                Slot s = new Slot();
                s.setProperty(slot.getProperty());
                s.setUnit(slot.getUnit());
                s.setMultiplicity(Slot.Multiplicity.PROPERTY_EDITABLE_FALSE);
                slots.add(row + 1, s);
                ListTableQuantitativeModel m = (ListTableQuantitativeModel) table.getModel();
                m.addRowIndex(row + 1, new Object[]{s.getProperty(),"", s.getUnit(), s.getType(), s.getDefaultValue(), ""});
            } else {
                Slot s = new Slot();
                slots.add(s);
                ListTableQuantitativeModel m = (ListTableQuantitativeModel) table.getModel();
                m.addRowIndexBefore(row , new Object[]{"","","","","",""});
            }
        }
        isPushed = false;
        
        if (slot == null)
            return "";
        else 
            return slot.getProperty();
    }

    @Override
    public boolean stopCellEditing() {
        isPushed = false;
        return super.stopCellEditing();
    }

    @Override
    protected void fireEditingStopped() {
        super.fireEditingStopped();
    }

    public void setTable(JTable table) {
        this.table = table;
    }

    public void setSlot(Slot slot) {
        this.slot = slot;
    }
}
