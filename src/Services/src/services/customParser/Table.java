/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package services.customParser;

import services.customParser.BaseElement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Luai
 */
public class Table {
    
    private List<String> tableKeys;
    private List<List<BaseElement>> table;
    
    public Table() {
        this.tableKeys = new ArrayList<String>();
        this.table = new ArrayList<List<BaseElement>>();
    }

    public List<List<BaseElement>> getTable() {
        return table;
    }

    public List<String> getTableKeys() {
        return tableKeys;
    }
    
    public void addToTable(List<BaseElement> list) {
        table.add(list);
        updateKeys(list);
    }
    
    public void updateKeys(List<BaseElement> list) {
        for (BaseElement e : list) {            
            if (!tableKeys.contains(e.key)) {
                tableKeys.add(e.key);
            }
        }
    }
    
}
