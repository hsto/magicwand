/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package services.customParser;

/**
 *
 * @author Luai
 */
public class BaseElement {
    
    public String key;
    public String value;
    
    public BaseElement(String key, String value) {
        this.key = key;
        this.value = value;
    }
    
}
