/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package services.customParser;

import java.util.ArrayList;
import java.util.List;
import org.json.simple.JSONObject;

/**
 *
 * @author Luai
 */
public class Parse {
    
    public static List<BaseElement> parseBaseElement(JSONObject json) {
        List<BaseElement> list = new ArrayList<BaseElement>();
        String str = json.toString();
        
        str = str.replace("{", "");
        str = str.replace("}", "");

        String[] statements = str.split(",");
        for (String statement : statements) {
            String[] s = statement.split(":");
            list.add(new BaseElement(s[0].replace("\"", ""), s[1].replace("\"", "")));
        }
        
        return list;
    }
    
}
