/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package services.customParser;

import java.util.ArrayList;
import java.util.List;
import services.view.TableBuilder;

/**
 *
 * @author Luai
 */
public class CustomJSON {
    
    private List<BaseElement> attributtes;
    private List<Table> tables;
    
    public CustomJSON() {
        this.attributtes = new ArrayList<BaseElement>();
        this.tables = new ArrayList<Table>();
    }

    public List<BaseElement> getAttributtes() {
        return attributtes;
    }

    public List<Table> getTables() {
        return tables;
    }
    
    @Override
    public String toString() {
        
        if (tables.isEmpty()) {
            StringBuilder builder = new StringBuilder();
            
            for (BaseElement s : attributtes) {
                builder.append(s.key).append(": ").append(s.value).append("\n");
            }
            
            return builder.toString();
        }
        
        TableBuilder tableBuilder = new TableBuilder();
        
        for (Table table : tables) {
            
            String[] header = new String[table.getTableKeys().size()];
            header = table.getTableKeys().toArray(header);
            String[] seperator = new String[header.length];
            for (int i = 0; i < seperator.length; i++) {
                seperator[i] = "----------";                
            }
            tableBuilder.addRow(header);
            tableBuilder.addRow(seperator);
            
            for (List<BaseElement> t : table.getTable()) {
                List<String> tempRowList = new ArrayList<String>();
                for (String s : table.getTableKeys()) {
                    String value = "-";
                    for (BaseElement b : t) {
                        if (s.equals(b.key)) {
                            value = b.value;
                            break;
                        }
                    }
                    tempRowList.add(value);
                }
                String[] row = new String[tempRowList.size()]; 
                tableBuilder.addRow(tempRowList.toArray(row));
            }        
        }
        
        return tableBuilder.toString();
    }
    
}

//        Formatter formatter = new Formatter();
//        
//        for (Table table : tables) {
//            
//            for (String s : table.getTableKeys()) {
//                formatter.format("%20s", s);
//            }
//            formatter.format("%n");
//            
//            
//            for (List<BaseElement> t : table.getTable()) {
//                for (String s : table.getTableKeys()) {
//                    String value = "-";
//                    for (BaseElement b : t) {
//                        if (s.equals(b.key)) {
//                            value = b.value;
//                            break;
//                        }
//                    }
//                    formatter.format("%20s", value);
//                }
//                formatter.format("%n");
//            }        
//        }
//        
//        System.out.println(formatter.toString());
