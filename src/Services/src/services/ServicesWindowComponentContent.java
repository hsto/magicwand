/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package services;

import com.nomagic.magicdraw.ui.browser.WindowComponentContent;
import java.awt.Component;
import services.view.JPanelServices;

/**
 *
 * @author Luai
 */
public class ServicesWindowComponentContent implements WindowComponentContent {

    private final JPanelServices panel;
    
    public ServicesWindowComponentContent(JPanelServices panel) {
        this.panel = panel;
    }

    @Override
    public Component getWindowComponent() {
        return panel;
    }

    @Override
    public Component getDefaultFocusComponent() {
        return panel;
    }
    
}
