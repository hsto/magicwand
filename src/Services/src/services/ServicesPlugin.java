package services;

import java.awt.event.ActionEvent;


import com.nomagic.magicdraw.actions.ActionsConfiguratorsManager;
import com.nomagic.magicdraw.actions.MDAction;
import com.nomagic.magicdraw.core.Application;
import com.nomagic.magicdraw.ui.MainFrame;
import com.nomagic.magicdraw.ui.ProjectWindowsManager;
import lib.MainMenuConfigurator;

/**
 *
 * @author Luai
 */
public class ServicesPlugin extends com.nomagic.magicdraw.plugins.Plugin {
    private ServicesProjectEventListener projectEventListener;
    @Override
    public void init() {
        projectEventListener = new ServicesProjectEventListener();
        Application.getInstance().addProjectEventListener(projectEventListener);
        
        ActionsConfiguratorsManager manager = ActionsConfiguratorsManager.getInstance();
        manager.addMainMenuConfigurator(new MainMenuConfigurator(new SimpleAction(null, ServicesProjectEventListener.pluginName)));
    }

    @Override
    public boolean close() {
        return true;
    }

    @Override
    public boolean isSupported() {
        return true;
    }
    
    class SimpleAction extends MDAction {
    	
    	public SimpleAction(String id, String name) {
    		super(id, name, null, null);
    	}
    	
    	@Override
    	public void actionPerformed(ActionEvent e) {
            MainFrame mainFrame = Application.getInstance().getMainFrame();
            ProjectWindowsManager projectWindowsManager = mainFrame.getProjectWindowsManager();
            if (!projectEventListener.startup) {
                projectWindowsManager.addWindow(projectEventListener.window);
                projectEventListener.startup = true;
            }
            projectWindowsManager.activateWindow(ServicesProjectEventListener.projectWindowId);
    	}
    	
    }
    
}
