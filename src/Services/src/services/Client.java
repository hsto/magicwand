package services;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.FileEntity;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;

/**
 *
 * @author Luai
 */
public class Client {
    
    public static String ping(String url) {
        
        try {
            URL siteURL = new URL(url);
            HttpURLConnection connection = (HttpURLConnection) siteURL.openConnection();
            connection.setRequestMethod("GET");
            connection.connect();

            int code = connection.getResponseCode();
            if (code == 200) {
                return "Available host";
            }
            return "Response from host";
        } catch (Exception ex) {
            return "Unkhown host";
        }
        /*catch (MalformedURLException ex) {
            return "Unkhown host";
        } catch (IOException ex) {
            return "Unkhown host";
        } */
    }
    
    public static String postRequest(File file, String url, String id, String username, String password) {
        if (url.contains("{id}")) return "{\"message\":\"Enter id\"}";        
        
        try {
            HttpClient client = HttpClientBuilder.create().build();
            HttpPost post =  new HttpPost(url);
            
            if (username != null && password != null) {
                MultipartEntityBuilder multiBuilder = MultipartEntityBuilder.create();
//                multiBuilder.addBinaryBody(file.getName(), file);
                multiBuilder.addPart("file", new FileBody(file));
                multiBuilder.addTextBody("username", username);
                multiBuilder.addTextBody("password", password);
                HttpEntity multiEntity = multiBuilder.build();
                post.setEntity(multiEntity);
                
            } else {
                //FileEntity entity = new FileEntity(file, ContentType.create("text/plain","UTF-8"));
                FileEntity entity = new FileEntity(file);
                post.setEntity(entity);
            }
            
            HttpResponse response = client.execute(post);
            
            BufferedReader reader = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
            String str = "", line;
            while((line = reader.readLine()) != null) {
                str += line + "\n";
            }
            if (reader != null) reader.close();
            	
            return str;         
            
        } catch (IOException ex) {
            return "Error connecting";
        }
    }
    
    public static String postRequest(String url, String id, String username, String password, String a, String b, String c) {
        if (url.contains("{id}")) return "{\"message\":\"Enter id\"}";        
        
        try {
            HttpClient client = HttpClientBuilder.create().build();
            HttpPost post =  new HttpPost(url);
            
            if (username != null && password != null) {
                List<NameValuePair> param = new ArrayList<NameValuePair>();
                param.add(new BasicNameValuePair("username", username));
                param.add(new BasicNameValuePair("password", password));
                if (a != null && !a.isEmpty()) param.add(new BasicNameValuePair("A", a));
                if (b != null && !b.isEmpty()) param.add(new BasicNameValuePair("B", b));
                if (c != null && !c.isEmpty()) param.add(new BasicNameValuePair("C", c));
                post.setEntity(new UrlEncodedFormEntity(param));                
            } else {
                List<NameValuePair> param = new ArrayList<NameValuePair>();
                if (a != null && !a.isEmpty()) param.add(new BasicNameValuePair("A", a));
                if (b != null && !b.isEmpty()) param.add(new BasicNameValuePair("B", b));
                if (c != null && !c.isEmpty()) param.add(new BasicNameValuePair("C", c));
                post.setEntity(new UrlEncodedFormEntity(param));   
            }
            
            HttpResponse response = client.execute(post);
            
            BufferedReader reader = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
            String str = "", line;
            while((line = reader.readLine()) != null) {
                str += line + "\n";
            }
            if (reader != null) reader.close();
            return str;         
            
        } catch (IOException ex) {
            return "Error connecting";
        }
    }
    
    public static String getRequest(String url, String id) {
        if (url.contains("{id}")) return "{\"message\":\"Enter {id}\"}";
        
        try {
            HttpClient client = HttpClientBuilder.create().build();            
            HttpGet httpGet = new HttpGet(url);
            HttpResponse response = client.execute(httpGet);
            
            BufferedReader reader = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
            String str = "", line;
            while((line = reader.readLine()) != null) {
                str += line + "\n";
            }
            if (reader != null) reader.close();
            return str;
            
        } catch (IOException ex) {
            return "Error connecting";
        }
    }
    
    public static String putRequest(File file, String url, String id, String username, String password) {
        if (url.contains("{id}")) return "{\"message\":\"Enter id\"}";
        
        try {
            HttpClient client = HttpClientBuilder.create().build();
            HttpPut put =  new HttpPut(url);
            
            if (username != null && password != null) {
                MultipartEntityBuilder multiBuilder = MultipartEntityBuilder.create();
//                multiBuilder.addBinaryBody(file.getName(), file);
                multiBuilder.addPart("file", new FileBody(file));
                multiBuilder.addTextBody("username", username);
                multiBuilder.addTextBody("password", password);
                HttpEntity multiEntity = multiBuilder.build();
                put.setEntity(multiEntity);
//                List<NameValuePair> param = new ArrayList<NameValuePair>();
//                param.add(new BasicNameValuePair("username", username));
//                param.add(new BasicNameValuePair("password", password));
//                UrlEncodedFormEntity entity = new UrlEncodedFormEntity(param);
//                put.setEntity(entity);
                
            } else {                
                FileEntity entity = new FileEntity(file);
                put.setEntity(entity);
            }
            
            HttpResponse response = client.execute(put);
            
            BufferedReader reader = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
            String str = "", line;
            while((line = reader.readLine()) != null) {
                str += line + "\n";
            }
            if (reader != null) reader.close();
            return str;         
            
        } catch (IOException ex) {
            return "Error connecting";
        }
    }
    
    public static String putRequest(String url, String id, String username, String password, String a, String b, String c) {
        if (url.contains("{id}")) return "{\"message\":\"Enter id\"}";
        
        try {
            HttpClient client = HttpClientBuilder.create().build();
            HttpPut put =  new HttpPut(url);
            
           if (username != null && password != null) {
                List<NameValuePair> param = new ArrayList<NameValuePair>();
                param.add(new BasicNameValuePair("username", username));
                param.add(new BasicNameValuePair("password", password));
                if (a != null && !a.isEmpty()) param.add(new BasicNameValuePair("A", a));
                if (b != null && !b.isEmpty()) param.add(new BasicNameValuePair("B", b));
                if (c != null && !c.isEmpty()) param.add(new BasicNameValuePair("C", c));
                put.setEntity(new UrlEncodedFormEntity(param));                
            } else {
                List<NameValuePair> param = new ArrayList<NameValuePair>();
                if (a != null && !a.isEmpty()) param.add(new BasicNameValuePair("A", a));
                if (b != null && !b.isEmpty()) param.add(new BasicNameValuePair("B", b));
                if (c != null && !c.isEmpty()) param.add(new BasicNameValuePair("C", c));
                put.setEntity(new UrlEncodedFormEntity(param));   
            }
            
            HttpResponse response = client.execute(put);
            
            BufferedReader reader = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
            String str = "", line;
            while((line = reader.readLine()) != null) {
                str += line + "\n";
            }
            if (reader != null) reader.close();
            return str;         
            
        } catch (IOException ex) {
            return "Error connecting";
        }
    }
     
    public static String deleteRequest(String url, String id) {
        if (url.contains("{id}")) return "{\"message\":\"Enter id\"}";
        
        try {
            HttpClient client = HttpClientBuilder.create().build();
            HttpDelete delete =  new HttpDelete(url);
            HttpResponse response = client.execute(delete);
            
            BufferedReader reader = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
            String str = "", line;
            while((line = reader.readLine()) != null) {
                str += line + "\n";
            }
            if (reader != null) reader.close();
            return str;
            
        } catch (IOException ex) {
            return "Error connecting";
        }
    }
    
}
