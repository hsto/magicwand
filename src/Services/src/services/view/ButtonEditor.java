/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package services.view;

import services.model.ListTableServicesModel;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.DefaultCellEditor;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.JTextArea;

/**
 *
 * @author Luai
 */
public class ButtonEditor extends DefaultCellEditor {
    
    protected JButton button;
    private boolean isPushed;
    private JTable table;
    private int row;

    public ButtonEditor(JCheckBox jCheckBox) {
        super(jCheckBox);
        button = new JButton();
        button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                fireEditingStopped();
            }
        });
    }
    
    @Override
    public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {
        this.row = row;
        JPanel panel = new JPanel(new BorderLayout());
        button.setText("+");
        button.setContentAreaFilled(false);
        isPushed = true;
        panel.add(button, BorderLayout.WEST);
        
        JTextArea field = new JTextArea("");
        field.setEditable(false);

        panel.add(field, BorderLayout.CENTER);
        return panel;
    }

    @Override
    public Object getCellEditorValue() {
        if (isPushed) {
            ListTableServicesModel m = (ListTableServicesModel) table.getModel();
            m.addRowIndexBefore(row, new Object[]{"","","POST", "","","-"});

        }
        isPushed = false;        
       
        return "";
    }

    @Override
    public boolean stopCellEditing() {
        isPushed = false;
        return super.stopCellEditing();
    }

    @Override
    protected void fireEditingStopped() {
        super.fireEditingStopped();
    }

    public void setTable(JTable table) {
        this.table = table;
    }
    
}
