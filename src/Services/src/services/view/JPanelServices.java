package services.view;

import services.model.DropdownMenu;
import services.model.ServerTable;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.BorderFactory;
import javax.swing.GroupLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.SwingConstants;

import services.controller.ServicesTreeSelectionListener;
import services.controller.ActionListenerGoButton;
import services.controller.ActionListenerSelectElement;

import com.nomagic.magicdraw.uml.BaseElement;

import eu.hansolo.custom.SteelCheckBox;
import lib.SharedHelper;
import lib.SharedHierarchyListener;

/**
 *
 * @author Luai
 */
public class JPanelServices extends JPanel {    
    
    private final JPanel innerPanel = new JPanel();
    private JPanel mainPanel = new JPanel();
    private JPanel logPanel = new JPanel();
    private JPanel tablePanel = new JPanel();
    private final GroupLayout layout = new GroupLayout(innerPanel);
    private final GroupLayout layoutMain = new GroupLayout(mainPanel);
    
    private final JLabel labelElementName = new JLabel();
    
    private final JButton foldButtonSettings = new JButton();
    private final JButton foldButtonServices = new JButton();
    private final JButton foldButtonResults = new JButton();
    
    private final JButton goButton= new JButton();    

    
//    private final JSeparator jSeparatorHeader = new JSeparator();
//    private final JSeparator jSeparatorMiddle = new JSeparator();
//    private final JSeparator jSeparatorButton = new JSeparator();
    
    private JTable table;
    private JTextArea textArea = new JTextArea("****************************** Log start ******************************");
    
    private ActionListenerGoButton actionListenerGoButton;
    
    private JLabel labelA = new JLabel("A:");
    private JLabel labelB = new JLabel("B:");
    private JLabel labelC = new JLabel("C:");
//    public JTextField fieldA = new JTextField();
//    public JTextField fieldB = new JTextField();
//    public JTextField fieldC = new JTextField();
    public JLabel labelInfoA = new JLabel();
    public JLabel labelInfoB = new JLabel();
    public JLabel labelInfoC = new JLabel();
    
    public JButton buttonA = new JButton("Add");
    public JButton buttonAClear = new JButton("Clear");
    public JButton buttonB = new JButton("Add");
    public JButton buttonBClear = new JButton("Clear");
    public JButton buttonC = new JButton("Add");
    public JButton buttonCClear = new JButton("Clear");
    
    private final static String ICON_RIGHT_FILE_NAME = "arrow-right.png";
    private final static String ICON_DOWN_FILE_NAME = "arrow-down.png";
	private final static String FILE_NAME = "link_Services.txt";
	private JLabel logoLabel = new JLabel();
    
	private ImageIcon imageIconRight;
	private ImageIcon imageIconDown;
	
//    private TextFocusListener focusListenerA;
//    private TextFocusListener focusListenerB;
//    private TextFocusListener focusListenerC;
    
//    private JRadioButton wholeButton;
//    public JRadioButton partButton;
    
    public SteelCheckBox steelBox = new SteelCheckBox();
    
    private ServicesTreeSelectionListener selectionListener = new ServicesTreeSelectionListener(this);
    private BaseElement element;
    
    public JPanelServices () {
        initComponents();
        this.addHierarchyListener(new SharedHierarchyListener(this, JPanelServices.this.getClass().getName(), false));
    }
    
    private void initComponents() {
    	logoLabel = SharedHelper.INSTANCE.initLogo();
        SharedHelper.INSTANCE.initLogoLink(logoLabel, getClass().getClassLoader().getResourceAsStream(FILE_NAME));
        imageIconRight = SharedHelper.INSTANCE.initIcon(ICON_RIGHT_FILE_NAME);
        imageIconDown = SharedHelper.INSTANCE.initIcon(ICON_DOWN_FILE_NAME);
        
        Font font = new Font("",Font.BOLD,14);
        
        JLabel labelTitel = new JLabel();
    	labelTitel.setFont(new Font("",Font.BOLD,16));
    	labelTitel.setText("Remote Model Analysis Services");
    	labelTitel.setMinimumSize(new Dimension(40,25));
        labelElementName.setFont(new Font("",Font.PLAIN,14));
        labelElementName.setText("Select element in containment tree");
        labelElementName.setMinimumSize(new Dimension(40,25));
        
        initFoldButton(foldButtonSettings, mainPanel, "Settings", font);
        initFoldButton(foldButtonServices, tablePanel, "Services", font);
        initFoldButton(foldButtonResults, logPanel, "Results", font);
        
        textArea.setEditable(false);
        textArea.setFont(new Font("Monospaced",Font.PLAIN,11));
        JScrollPane pane = new JScrollPane (textArea);
        pane.getVerticalScrollBar().setUnitIncrement(20);
        pane.setMinimumSize(new Dimension(150, 150));
        pane.setMaximumSize(new Dimension(150, 150));
        pane.setPreferredSize(new Dimension(150, 150));
        
        labelA.setToolTipText("Select element from tree");
        labelB.setToolTipText("Select element from tree");
        labelC.setToolTipText("Select element from tree");
        
        
        goButton.setText("Go");
        actionListenerGoButton = new ActionListenerGoButton(this);
        actionListenerGoButton.setTextArea(textArea);
        goButton.addActionListener(actionListenerGoButton);
        
        innerPanel.setLayout(layout);
        layout.setAutoCreateGaps(true);
        layout.setAutoCreateContainerGaps(true);
        
        mainPanel.setLayout(layoutMain);
        layoutMain.setAutoCreateGaps(true);
        layoutMain.setAutoCreateContainerGaps(true);
        
        //mainPanel.setLayout(new GridLayout(1,0));
        logPanel.setLayout(new GridLayout(1,0));
        tablePanel.setLayout(new GridLayout(1,0));
        
        layout.setHorizontalGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup()
                		.addGroup( layout.createSequentialGroup()
                				.addGroup(layout.createParallelGroup()
                						.addComponent(labelTitel)
                						.addComponent(labelElementName)                						
                				)                				
                				//.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, GroupLayout.PREFERRED_SIZE, Short.MAX_VALUE)
                				.addGap(4, 4, Short.MAX_VALUE)
                				.addComponent(logoLabel)
                		)
//                        .addComponent(jSeparatorHeader)
                		.addComponent(foldButtonSettings)
                        .addComponent(mainPanel)
//                        .addComponent(jSeparatorMiddle)
                        .addComponent(foldButtonServices)
                        .addComponent(tablePanel)
                        .addComponent(foldButtonResults)
                        .addComponent(logPanel)
//                        .addComponent(jSeparatorButton)                        
                )     
        );
        
        layout.setVerticalGroup(layout.createSequentialGroup()
        		.addGroup(layout.createParallelGroup()
        				.addGroup(layout.createSequentialGroup()
        						.addComponent(labelTitel)
        						.addComponent(labelElementName)        						
        				)
        				.addComponent(logoLabel)
        		)
//                .addComponent(jSeparatorHeader, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addComponent(foldButtonSettings)
        		.addComponent(mainPanel)
//                .addComponent(jSeparatorMiddle, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addComponent(foldButtonServices)
        		.addComponent(tablePanel)
                .addComponent(foldButtonResults)
                .addComponent(logPanel)
//                .addComponent(jSeparatorButton, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                
        );
        
        steelBox.setSelected(true);
        steelBox.setText("All");
        steelBox.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				if (steelBox.isSelected()) {
					disablePartSelection();
					steelBox.setText("All");
				} else {
					enablePartSelection();
					steelBox.setText("Parts");
				}
			}
		});
        
//        wholeButton = new JRadioButton("Whole model");
//        partButton = new JRadioButton("Parts of model");
//        wholeButton.setSelected(true);
//        wholeButton.setActionCommand("Whole");
//        partButton.setActionCommand("Part");
//        wholeButton.addActionListener( new ActionListener() {
//            @Override
//            public void actionPerformed(ActionEvent e) {
//                disablePartSelection();
//            }
//        });
//        partButton.addActionListener(new ActionListener() {
//            @Override
//            public void actionPerformed(ActionEvent e) {
//                enablePartSelection();
//            }
//        });
//        ButtonGroup group = new ButtonGroup();
//        group.add(wholeButton);
//        group.add(partButton);
        
//        fieldA.setMinimumSize(new Dimension(30,20));
//        fieldA.setMaximumSize(new Dimension(150,20));
//        fieldA.setPreferredSize(new Dimension(150,20));
//        fieldB.setMinimumSize(new Dimension(30,20));
//        fieldB.setMaximumSize(new Dimension(150,20));
//        fieldB.setPreferredSize(new Dimension(150,20));
//        fieldC.setMinimumSize(new Dimension(30,20));
//        fieldC.setMaximumSize(new Dimension(150,20));
//        fieldC.setPreferredSize(new Dimension(150,20));
        
//        focusListenerA = new TextFocusListener(fieldA, labelInfoA);
//        focusListenerB = new TextFocusListener(fieldB, labelInfoB);
//        focusListenerC = new TextFocusListener(fieldC, labelInfoC);
//        fieldA.addFocusListener(focusListenerA);
//        fieldB.addFocusListener(focusListenerB);
//        fieldC.addFocusListener(focusListenerC);
        
        
        buttonA.addActionListener(new ActionListenerSelectElement(labelInfoA));
        buttonAClear.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				labelInfoA.setText("");
				labelInfoA.setName("");
			}
		});
        
        buttonB.addActionListener(new ActionListenerSelectElement(labelInfoB));
		buttonBClear.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				labelInfoB.setText("");
				labelInfoB.setName("");
			}
		});

		buttonC.addActionListener(new ActionListenerSelectElement(labelInfoC));
		buttonCClear.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				labelInfoC.setText("");
				labelInfoC.setName("");
			}
		});
        
        
        disablePartSelection();
        
        addTable();
        addDropDown();
        steelBox.setBorder(BorderFactory.createEmptyBorder(0,0,0,20));
        layoutMain.setHorizontalGroup(layoutMain.createSequentialGroup()
                .addGroup(layoutMain.createParallelGroup()
                		.addGroup(layoutMain.createSequentialGroup()
//                				.addGroup(layoutMain.createParallelGroup()
//                						.addComponent(wholeButton)                        
//                						.addComponent(partButton)
//                				)
                				.addComponent(steelBox)
                				.addGap(80,120,Short.MAX_VALUE)
                				.addGroup(layoutMain.createSequentialGroup()
                						.addComponent(dropdownMenu.getComboBox())
                						.addComponent(goButton)
                				)
                		)
                        .addGroup(layoutMain.createSequentialGroup()
                        	.addComponent(buttonA)
                        	.addComponent(buttonAClear)
                            .addComponent(labelA)
//                            .addComponent(fieldA)
                            .addComponent(labelInfoA)
                        )
                        .addGroup(layoutMain.createSequentialGroup()
                        	.addComponent(buttonB)
                            .addComponent(buttonBClear)
                            .addComponent(labelB)
//                            .addComponent(fieldB)
                            .addComponent(labelInfoB)
                        )
                        .addGroup(layoutMain.createSequentialGroup()
                        	.addComponent(buttonC)
                            .addComponent(buttonCClear)
                            .addComponent(labelC)
//                            .addComponent(fieldC)
                            .addComponent(labelInfoC)
                        )
                        
                )
        );
        
        layoutMain.setVerticalGroup(layoutMain.createSequentialGroup()
        		.addGroup(layoutMain.createParallelGroup()
//        				.addGroup(layoutMain.createSequentialGroup()
//        						.addComponent(wholeButton)
//        						.addComponent(partButton)
//        				)
        				.addComponent(steelBox)
        				.addGroup(layoutMain.createParallelGroup()
        						.addComponent(dropdownMenu.getComboBox())
        						.addComponent(goButton)
        				)        				
    	        )
                .addGroup(layoutMain.createParallelGroup()
                	.addComponent(buttonA)
                    .addComponent(buttonAClear)
                    .addComponent(labelA)
//                    .addComponent(fieldA)
                    .addComponent(labelInfoA)
                )
                .addGroup(layoutMain.createParallelGroup()
                	.addComponent(buttonB)
                    .addComponent(buttonBClear)
                    .addComponent(labelB)
//                    .addComponent(fieldB)
                    .addComponent(labelInfoB)
                )
                .addGroup(layoutMain.createParallelGroup()
                	.addComponent(buttonC)
                    .addComponent(buttonCClear)
                    .addComponent(labelC)
//                    .addComponent(fieldC)
                    .addComponent(labelInfoC)
                )
        );
        
        logPanel.add(pane);
        
        setLayout(new BorderLayout());
        add(innerPanel);
    }
    
    private void initFoldButton(final JButton foldButton, final JPanel panel, final String name, Font font) {
    	panel.setVisible(false);
    	foldButton.setFont(font);
        SharedHelper.INSTANCE.setIconName(true, foldButton, name, imageIconRight, imageIconDown);
    	foldButton.setVisible(true);
    	foldButton.setMinimumSize(new Dimension(40,25));
    	foldButton.setMaximumSize(new Dimension(Short.MAX_VALUE, 30));
    	foldButton.setContentAreaFilled(false);
    	foldButton.setHorizontalAlignment(SwingConstants.LEFT);
    	foldButton.setBorder(BorderFactory.createEmptyBorder(4,1,4,4));
    	foldButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				if (panel.isVisible()) {
					panel.setVisible(false);
                                        SharedHelper.INSTANCE.setIconName(true, foldButton, name, imageIconRight, imageIconDown);
				} else {
					panel.setVisible(true);
                                        SharedHelper.INSTANCE.setIconName(false, foldButton, name, imageIconRight, imageIconDown);
				}
			}
		});
    }
    
    private ServerTable serverTable;
    private void addTable() {
        serverTable = new ServerTable(table, tablePanel);
        actionListenerGoButton.setTable(serverTable.getTable());
        table = serverTable.getTable();
    }    
    
    private DropdownMenu dropdownMenu;
    private void addDropDown() {        
        dropdownMenu = new DropdownMenu(table, mainPanel);
        serverTable.setDropdownMenu(dropdownMenu);
        actionListenerGoButton.setDropdownMenu(dropdownMenu);
    }
    
    public void setElement(BaseElement e) {
        element = e;
        
        labelInfoA.setText("");
        labelInfoA.setName("");
        labelInfoB.setText("");
        labelInfoB.setName("");
        labelInfoC.setText("");
        labelInfoC.setName("");
        
//        focusListenerA.fieldValue = e.getID();
//        focusListenerB.fieldValue = e.getID();
//        focusListenerC.fieldValue = e.getID();
//        
//        String labelName = e.getHumanType() + " '" + e.getHumanName().replaceFirst(e.getHumanType() + " ", "") + "'";
//        focusListenerA.labelInfoValue = labelName;
//        focusListenerB.labelInfoValue = labelName;
//        focusListenerC.labelInfoValue = labelName;        
    }
    
    private void disablePartSelection() {
//        fieldA.setEnabled(false);
//        fieldB.setEnabled(false);
//        fieldC.setEnabled(false);
    	buttonA.setEnabled(false);
    	buttonAClear.setEnabled(false);
    	buttonB.setEnabled(false);
    	buttonBClear.setEnabled(false);
    	buttonC.setEnabled(false);
    	buttonCClear.setEnabled(false);
        labelA.setForeground(Color.LIGHT_GRAY);
        labelB.setForeground(Color.LIGHT_GRAY);
        labelC.setForeground(Color.LIGHT_GRAY);        
        labelInfoA.setForeground(Color.LIGHT_GRAY);
        labelInfoB.setForeground(Color.LIGHT_GRAY);
        labelInfoC.setForeground(Color.LIGHT_GRAY);
    }
    
    private void enablePartSelection() {
//        fieldA.setEnabled(true);
//        fieldB.setEnabled(true);
//        fieldC.setEnabled(true);
    	buttonA.setEnabled(true);
    	buttonAClear.setEnabled(true);
    	buttonB.setEnabled(true);
    	buttonBClear.setEnabled(true);
    	buttonC.setEnabled(true);
    	buttonCClear.setEnabled(true);
        labelA.setForeground(Color.BLACK);
        labelB.setForeground(Color.BLACK);
        labelC.setForeground(Color.BLACK);        
        labelInfoA.setForeground(Color.BLACK);
        labelInfoB.setForeground(Color.BLACK);
        labelInfoC.setForeground(Color.BLACK);
    }

    public ServicesTreeSelectionListener getSelectionListener() {
        return selectionListener;
    }
   
    
}
