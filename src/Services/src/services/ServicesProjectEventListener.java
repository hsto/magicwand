/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package services;


import com.nomagic.magicdraw.core.Application;
import com.nomagic.magicdraw.core.Project;
import com.nomagic.magicdraw.core.project.ProjectEventListener;
import com.nomagic.magicdraw.ui.MainFrame;
import com.nomagic.magicdraw.ui.ProjectWindow;
import com.nomagic.magicdraw.ui.ProjectWindowsManager;
import com.nomagic.magicdraw.ui.WindowComponentInfo;
import com.nomagic.magicdraw.ui.WindowsManager;
import com.nomagic.magicdraw.ui.browser.Tree;
import com.nomagic.magicdraw.ui.browser.WindowComponentContent;

import javax.swing.JTree;
import lib.SharedHelper;

import services.view.JPanelServices;

/**
 *
 * @author Luai
 */
public class ServicesProjectEventListener implements ProjectEventListener {
    
    private Tree tree;
    private JTree jtree;    
    public static String pluginName = "Services Annotaions";
    private final JPanelServices panel = new JPanelServices();
    public static String projectWindowId = "";
    
    WindowComponentInfo info = new WindowComponentInfo("SERVICES", pluginName, null, WindowsManager.SIDE_EAST, WindowsManager.STATE_DOCKED, false);

    public boolean startup;
    public ProjectWindow window;
    
    public ServicesProjectEventListener() {
    }

    private static String FILE_NAME_STARTUP = System.getProperty("user.home") + "/.magicdraw/MagicWand/startup_Services.txt";
    
    @Override
    public void projectOpened(Project project) {
        tree = Application.getInstance().getMainFrame().getBrowser().getContainmentTree();
        jtree = tree.getTree();
        jtree.addTreeSelectionListener(panel.getSelectionListener());
        
        // protect documentation pane from direct editing
        Application.getInstance().getMainFrame().getBrowser().getDocPanel().getHTMLTextEditorComponent().setVisible(false);
        Application.getInstance().getMainFrame().getBrowser().getDocPanel().getHTMLEditorPane().setEnabled(false);
        Application.getInstance().getMainFrame().getBrowser().getDocPanel().getHTMLEditorPane().setVisible(false);
        Application.getInstance().getMainFrame().getBrowser().getDocPanel().setEnableHTMLCheckBox(false);
        Application.getInstance().getMainFrame().getBrowser().getDocPanel().setEnabledAll(false);
        

        MainFrame mainFrame = Application.getInstance().getMainFrame();
        ProjectWindowsManager projectWindowsManager = mainFrame.getProjectWindowsManager();
       
        WindowComponentContent content = new ServicesWindowComponentContent(panel);
        window = new ProjectWindow(info,content);
        
        startup = SharedHelper.INSTANCE.readStartup(FILE_NAME_STARTUP);
        if (startup) {
                projectWindowsManager.addWindow(window);
        	projectWindowsManager.activateWindow(window.getId());
        }
        
        projectWindowId = window.getId();
    }

    @Override
    public void projectClosed(Project project) {
        
    }

    @Override
    public void projectSaved(Project project, boolean savedInServer) {
        
    }

    @Override
    public void projectActivated(Project project) {
        
    }

    @Override
    public void projectDeActivated(Project project) {
        
    }

    @Override
    public void projectReplaced(Project oldProject, Project newProject) {
        
    }

    @Override
    public void projectCreated(Project project) {
        
    }

    @Override
    public void projectPreClosed(Project project) {
        tree = Application.getInstance().getMainFrame().getBrowser().getContainmentTree();
        if (tree != null) {
	        jtree = tree.getTree();
	        if (panel != null && jtree != null) {
                    jtree.removeTreeSelectionListener(panel.getSelectionListener());
                }
        }
        
        SharedHelper.INSTANCE.writeStartup(FILE_NAME_STARTUP, panel);
    }

    @Override
    public void projectPreClosedFinal(Project project) {
        
    }

    @Override
    public void projectPreSaved(Project project, boolean savedInServer) {
        
    }

    @Override
    public void projectPreActivated(Project project) {
        
    }

    @Override
    public void projectPreDeActivated(Project project) {
        
    }

    @Override
    public void projectPreReplaced(Project oldProject, Project newProject) {
        
    }

    @Override
    public void projectOpenedFromGUI(Project project) {
                
    }

    @Override
    public void projectActivatedFromGUI(Project project) {
        
    }
    
}
