/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package services.model;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JComboBox;
import javax.swing.JPanel;
import javax.swing.JTable;

/**
 *
 * @author Luai
 */
public class DropdownMenu {
    
    private JTable table;
    private JPanel mainPanel;
    private JComboBox<String> comboBox = new JComboBox<String>();
    private int selectedIndex;
    
    public DropdownMenu(JTable table, JPanel mainPanel) {
        this.table = table;
        this.mainPanel = mainPanel;
        
        addNew();
        
    }
    
    private void addNew() {
        for (int i = 0; i < this.table.getModel().getRowCount() - 1; i++) {
            comboBox.addItem((String)this.table.getValueAt(i, 0));
        }
        
        comboBox.setMinimumSize(new Dimension(50,26));
        comboBox.setMaximumSize(new Dimension(150,26));
        comboBox.setPreferredSize(new Dimension(150,26));
        
        comboBox.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                 JComboBox item = (JComboBox)e.getSource();
                 selectedIndex = item.getSelectedIndex();                
            }
        });
    }
    
    public void upadate() {
        mainPanel.remove(comboBox);        
        comboBox = new JComboBox<String>();
        addNew();
        
        mainPanel.add(comboBox);
        mainPanel.revalidate();
    }

    public JComboBox<String> getComboBox() {
        return comboBox;
    }

    public void setComboBox(JComboBox<String> comboBox) {
        this.comboBox = comboBox;
    }

    public JTable getTable() {
        return table;
    }

    public void setTable(JTable table) {
        this.table = table;
    }

    public int getSelectedIndex() {
        return selectedIndex;
    }
}
