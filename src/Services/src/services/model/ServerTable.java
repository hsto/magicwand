/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package services.model;

import java.awt.event.MouseEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import javax.swing.DefaultCellEditor;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;

import services.Client;
import services.view.ButtonEditor;
import services.view.ButtonRenderer;

/**
 *
 * @author Luai
 */
public class ServerTable {

    private JTable table;
    private JPanel tablePanel = new JPanel();
    
    private static final String FILE_NAME = "configTable.txt";
    public final static String COL_NAME = "Name";
    public final static String COL_URI = "URI";
    public final static String COL_METHOD = "Method";
    public final static String COL_USERNAME = "Username";
    public final static String COL_PASSWORD = "Password";
    public final static String COL_STATUS = "Status";
    public final static int COL_INDEX_NAME = 0;
    public final static int COL_INDEX_URI = 1;
    public final static int COL_INDEX_METHOD = 2;
    public final static int COL_INDEX_USERNAME = 3;
    public final static int COL_INDEX_PASSWORD = 4;
    public final static int COL_INDEX_STATUS = 5;
    
    
    private DropdownMenu dropdownMenu;
    
    private Thread statustThread;
    
    public ServerTable(JTable table, JPanel tablePanel) {
        this.table = table;
        this.tablePanel = tablePanel;
        
        addTable();
    }
    
    private void addTable() {
        String[] header = new String[]{COL_NAME, COL_URI, COL_METHOD, COL_USERNAME, COL_PASSWORD, COL_STATUS};
        ArrayList<Object[]> data = new ArrayList<Object[]>();
        List<String[]> list = readConfig();
        for (String[] s : list) {
            data.add(new Object[]{s[COL_INDEX_NAME],s[COL_INDEX_URI],s[COL_INDEX_METHOD],"","","-"});
        }        
//        data.add(new Object[]{"Upload model","http://hypersonic.compute.dtu.dk/models","POST","","","-"});
//        data.add(new Object[]{"Detect clones","http://hypersonic.compute.dtu.dk/model/{id}/clones","GET","","","-"});
//        data.add(new Object[]{"Replace model","http://hypersonic.compute.dtu.dk/model/{id}","PUT","","",""});
//        data.add(new Object[]{"Delete model","http://hypersonic.compute.dtu.dk/model/{id}","DELETE","","",""});
        data.add(new Object[]{"","","","","",""});
        
        final ButtonRenderer buttonrenderer = new ButtonRenderer();
        final ButtonEditor buttonEditor = new ButtonEditor(new JCheckBox());
        final ListTableServicesModel model = new ListTableServicesModel(data, header);
        
        // Ping
        if (data.size() > 1) {
            for (int i = 0; i < data.size()-1; i++) {
                String url = (String) model.getValueAt(i, COL_INDEX_URI);
                model.setValueAt(Client.ping(url), i, COL_INDEX_STATUS);
            }
        }
        
        setTable(new JTable(model) {
            @Override
            public boolean isCellEditable(int row, int column) {
                // Add row
                if ((row == this.getModel().getRowCount()-1 && column != 0) || (column == COL_INDEX_STATUS)) {
                    return false;
                }
                return true;
            }
            
            @Override
            public TableCellRenderer getCellRenderer(int row, int column) {
                // Add row
                if (this.getModel().getRowCount() - 1 == row && column == 0) {
                    return buttonrenderer;
                }
                
                return super.getCellRenderer(row, column);
            }
            
            @Override
            public TableCellEditor getCellEditor(int row, int column) {
                // Add row
                if (this.getModel().getRowCount() - 1 == row && column == 0) {
                    buttonEditor.setTable(this);
                    return buttonEditor;
                }
                
                return super.getCellEditor(row, column);
            }
            
            @Override
            public String getToolTipText(MouseEvent e) {
                String toolTip = null;
                java.awt.Point p = e.getPoint();
                if (p == null) return toolTip;
                int row = rowAtPoint(p);
                int col = columnAtPoint(p);
                
                if (row >= 0 && col >= 0 && this.getModel().getRowCount() > row) {
                	try {
                		toolTip = (String) getValueAt(row, col);
                	} catch (Exception ex) {
                		
                	}
                }
                
                return toolTip;
            }
        });
        
        table.addPropertyChangeListener(new PropertyChangeListener() {

            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                
                // Update dropdown
                if (dropdownMenu != null && evt.getPropertyName().equals("tableCellEditor")) {
                    dropdownMenu.setTable(table);
                    dropdownMenu.upadate();
                }
                
                // Ping
                if (evt.toString().contains("editingColumn=" + COL_INDEX_URI)) {
                    
                    String row = null;
                    String[] strings = evt.toString().split(",");
                    for (String s : strings) {
                        if (s.contains("editingRow=")) {
                            row = s.replace("editingRow=", "");
                            break;
                        }
                    }
                    if (row == null) return;
                    
                    final String url = (String) table.getModel().getValueAt(Integer.parseInt(row), COL_INDEX_URI);
                    final String finalRow = row;
                   
                    if (statustThread != null) {
                        statustThread.interrupt();
                        statustThread = null;
                    }
                    
                    statustThread = new Thread(new Runnable() {
                        @Override
                        public void run() {
                            String status = Client.ping(url);
                            table.getModel().setValueAt(status, Integer.parseInt(finalRow), COL_INDEX_STATUS);
                        }
                    });
                    statustThread.start();
                    
                    //String status = Client.ping(url);
                    //table.getModel().setValueAt(status, Integer.parseInt(row), COL_INDEX_STATUS);
                }
            }
            
        });
        
        getTable().getColumn(COL_NAME).setMinWidth(50);
        getTable().getColumn(COL_NAME).setMaxWidth(120);
        getTable().getColumn(COL_NAME).setPreferredWidth(100);
        getTable().getColumn(COL_URI).setMinWidth(150);
        getTable().getColumn(COL_URI).setPreferredWidth(200);
        getTable().getColumn(COL_METHOD).setMinWidth(50);
        getTable().getColumn(COL_METHOD).setMaxWidth(60);
        getTable().getColumn(COL_METHOD).setPreferredWidth(60);
        getTable().getColumn(COL_USERNAME).setMinWidth(50);
        getTable().getColumn(COL_USERNAME).setMaxWidth(70);
        getTable().getColumn(COL_USERNAME).setPreferredWidth(70);
        getTable().getColumn(COL_PASSWORD).setMinWidth(50);
        getTable().getColumn(COL_PASSWORD).setMaxWidth(70);
        getTable().getColumn(COL_PASSWORD).setPreferredWidth(70);
        getTable().getColumn(COL_STATUS).setMinWidth(50);
        getTable().getColumn(COL_STATUS).setMaxWidth(120);
        getTable().getColumn(COL_STATUS).setPreferredWidth(80);
        
        TableColumn columnMethod = getTable().getColumn(COL_METHOD);
        String[] strings = {"POST","GET","PUT","DELETE"};
        JComboBox<String> combobox = new JComboBox<String>(strings);
        columnMethod.setCellEditor(new DefaultCellEditor(combobox));
        
//        TableColumn columnPassword = getTable().getColumn(COL_PASSWORD);
//        JPasswordField password = new JPasswordField();
//        columnPassword.setCellEditor(new DefaultCellEditor(password));
        
        getTable().setFillsViewportHeight(true);
        tablePanel.add(new JScrollPane(getTable()));
        getTable().setRowHeight(22);
    }

    private List<String[]> readConfig() {
        List<String[]> list = new ArrayList<String[]>();
        Scanner scanner = null;
        try {            
            scanner = new Scanner(getClass().getClassLoader().getResourceAsStream(FILE_NAME));
        } catch (Exception e) {
            //scanner = new Scanner(getClass().getResourceAsStream(FILE_NAME));            
        }
        
        if (scanner == null) return list; 
        
        while(scanner.hasNextLine()) {
            list.add(scanner.nextLine().split(";"));
        }
        scanner.close();
        return list;
    }
    
    public JTable getTable() {
        return table;
    }

    public void setTable(JTable table) {
        this.table = table;
    }

    public DropdownMenu getDropdownMenu() {
        return dropdownMenu;
    }

    public void setDropdownMenu(DropdownMenu dropdownMenu) {
        this.dropdownMenu = dropdownMenu;
    }
    
    
}
