/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package services.controller;

import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import javax.swing.JLabel;
import javax.swing.JTextField;

/**
 * Not used anymore !!!!!!!
 * @author Luai
 */
public class TextFocusListener implements FocusListener {

    private JTextField field;
    private JLabel labelInfo;
    public String fieldValue = "";
    public String labelInfoValue = "";
    
    public TextFocusListener(JTextField field, JLabel labelInfo) {
        this.field = field;
        this.labelInfo = labelInfo;
    }

    @Override
    public void focusGained(FocusEvent e) {
        if (field.getText().isEmpty()) {
            field.setText(fieldValue);
            labelInfo.setText(labelInfoValue);
        }
        
    }

    @Override
    public void focusLost(FocusEvent e) {
        if (field.getText().isEmpty()) {            
            labelInfo.setText("");
        }
    }
    
}
