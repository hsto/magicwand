/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package services.controller;

import com.nomagic.magicdraw.core.Application;
import com.nomagic.magicdraw.ui.browser.Node;
import com.nomagic.magicdraw.ui.browser.Tree;
import com.nomagic.magicdraw.uml.BaseElement;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import services.view.JPanelServices;

/**
 *
 * @author Luai
 */
public class ServicesTreeSelectionListener implements TreeSelectionListener {
    
    private JPanelServices panel;
    private Tree tree;
    
    public ServicesTreeSelectionListener(JPanelServices panel) {
        this.panel = panel;
    }

    @Override
    public void valueChanged(TreeSelectionEvent e) {
    	if (!panel.isShowing()) return;
    	
        tree = Application.getInstance().getMainFrame().getBrowser().getContainmentTree();
        
        Node node = tree.getSelectedNode();
        if (node != null) {
            Object userObject = node.getUserObject();
            if (userObject instanceof BaseElement) {

                BaseElement element = (BaseElement) userObject;
                panel.setElement(element);
            }
        }
    }
    
}
