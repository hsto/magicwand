package services.controller;

import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.swing.JLabel;

import com.nomagic.magicdraw.core.Application;
import com.nomagic.magicdraw.core.Project;
import com.nomagic.magicdraw.ui.dialogs.MDDialogParentProvider;
import com.nomagic.magicdraw.ui.dialogs.SelectElementInfo;
import com.nomagic.magicdraw.ui.dialogs.SelectElementTypes;
import com.nomagic.magicdraw.ui.dialogs.selection.ElementSelectionDlg;
import com.nomagic.magicdraw.ui.dialogs.selection.ElementSelectionDlgFactory;
import com.nomagic.magicdraw.uml.BaseElement;
import com.nomagic.magicdraw.uml.ClassTypes;
import com.nomagic.uml2.ext.jmi.helpers.ModelHelper;
import com.nomagic.uml2.ext.magicdraw.classes.mdkernel.Element;
import com.nomagic.uml2.ext.magicdraw.classes.mdkernel.Property;

public class ActionListenerSelectElement implements ActionListener {
	
	JLabel label;
	
	public ActionListenerSelectElement(JLabel label) {
		this.label = label;
	}

	@Override
	public void actionPerformed(ActionEvent e) {

		ElementSelectionDlg elementSelectionDlg = createElementSelectionDialog();

		elementSelectionDlg.show();

		if (elementSelectionDlg.isOkClicked()) {
			BaseElement element = elementSelectionDlg.getSelectedElement();
			
			String labelName = element.getHumanType() + " '" + element.getHumanName().replaceFirst(element.getHumanType() + " ", "") + "'";
			this.label.setText(labelName);
			this.label.setName(element.getID());
		}

	}

	private ElementSelectionDlg createElementSelectionDialog() {
		// Only properties and and their subtypes are offered to select.
		List<Class> types = ClassTypes.getSubtypes(BaseElement.class);
		SelectElementTypes selectElementTypes = new SelectElementTypes(types,
				types, null, types);

		// Available properties are filtered so that only the ones which start
		// with 'p' are selected.
		final Collection<Property> candidates = getSelectionCandidates();

		// Gets elements which are initially selected in the dialog.
		List<Property> initialSelection = getInitialSelection(candidates);		

		Frame dialogParent = MDDialogParentProvider.getProvider()
				.getDialogParent();
		ElementSelectionDlg selectionDlg = ElementSelectionDlgFactory.create(
				dialogParent, "Select", null);

		SelectElementInfo selectElementInfo = new SelectElementInfo(true,
				false, null, true);
		ElementSelectionDlgFactory.initSingle(selectionDlg, selectElementTypes,
				selectElementInfo, initialSelection);
		
		return selectionDlg;
	}

	/**
	 * Gets elements which are initially selected in the dialog. We select only
	 * those properties which have Class set as their types.
	 *
	 * @param candidates
	 *            candidate elements from which to select.
	 * @return a list of initially selected elements.
	 */
	private List<Property> getInitialSelection(Collection<Property> candidates) {
		List<Property> initialSelection = new ArrayList<Property>();

		for (Property property : candidates) {
			if (property.getType() instanceof com.nomagic.uml2.ext.magicdraw.classes.mdkernel.Class) {
				initialSelection.add(property);
			}
		}

		return initialSelection;
	}

	/**
	 * Gets candidates which should be offered in the selection dialog.
	 *
	 * @param start
	 *            start of the name by which to filter elements.
	 * @return candidates which are offered to select in the selection dialog.
	 */
	private static Collection<Property> getSelectionCandidates() {
		Project project = Application.getInstance().getProject();
		Collection<? extends Element> candidates = ModelHelper
				.getElementsOfType(project.getModel(),
						new Class[] { Property.class }, false, true);

		final ArrayList<Property> properties = new ArrayList<Property>();
		for (Element element : candidates) {
			Property property = (Property) element;
		}
		return properties;
	}

}
