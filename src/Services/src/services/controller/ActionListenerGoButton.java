package services.controller;

import services.customParser.Table;
import services.customParser.BaseElement;
import services.customParser.CustomJSON;
import services.customParser.Parse;
import com.nomagic.magicdraw.core.Application;
import com.nomagic.magicdraw.utils.ExtendedFile;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import javax.swing.JTable;
import javax.swing.JTextArea;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import org.json.simple.parser.ContainerFactory;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import services.Client;
import services.model.DropdownMenu;
import services.view.JPanelServices;
import services.model.ServerTable;

/**
 *
 * @author Luai
 */
public class ActionListenerGoButton implements ActionListener {

    private JTable table;
    private DropdownMenu dropdownMenu;
    private JTextArea textArea;
    private String id = "";
    
    private String a = "";
    private String b = "";
    private String c = "";
    private String params;
    
    private JPanelServices jPanelServices;

    public ActionListenerGoButton(JPanelServices aThis) {
        this.jPanelServices = aThis;
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
        
        updateParams();
        
        if (table.getCellEditor() != null) {
            table.getCellEditor().stopCellEditing();
        }
        
//        File file = null;
//        try {
//            file = new File(getClass().getClassLoader().getResource("LMS_2011_with_MR_Annotations.mdxml").toURI());
//        } catch (URISyntaxException ex) {
//            Logger.getLogger(ActionListenerGoButton.class.getName()).log(Level.SEVERE, null, ex);
//        }
        
        ExtendedFile file = Application.getInstance().getProject().getFile();
        
        if (!file.getExtension().equals("mdxml")) {
            javax.swing.JOptionPane.showMessageDialog(null, "The project extension have to be .mdzml");
            return;
        }
        
        String req = (String) table.getModel().getValueAt(dropdownMenu.getSelectedIndex(), ServerTable.COL_INDEX_METHOD);
        String url = (String) table.getModel().getValueAt(dropdownMenu.getSelectedIndex(), ServerTable.COL_INDEX_URI);
        String usernameColumn = (String) table.getModel().getValueAt(dropdownMenu.getSelectedIndex(), ServerTable.COL_INDEX_USERNAME);
        String passwordColumn = (String) table.getModel().getValueAt(dropdownMenu.getSelectedIndex(), ServerTable.COL_INDEX_PASSWORD);
        
        String username = null, password = null;
        if (!usernameColumn.isEmpty() && !passwordColumn.isEmpty()) {
            username = usernameColumn;
            password = passwordColumn;           
        }
        
        if (!id.isEmpty()) url = url.replace("{id}", id);
        
        String responce = "";
        if (req.equals("GET")) {            
            if (username != null & password != null) {
                String param;
                if (url.contains("?")) param = "&username=" + username + "&password=" + password;
                else param = "?username=" + username + "&password=" + password;                
                url += param;
            }
            if (!params.isEmpty()) {
                String param;
                if (url.contains("?")) param = "&" + params;
                else param = "?" + params;            
                url += param;
            }
            responce = Client.getRequest(url, id);
        } else if (req.equals("POST")) {
            if (params.isEmpty()) responce = Client.postRequest(file, url, id, username, password);
            else responce = Client.postRequest(url, id, username, password, a, b, c);
        } else if (req.equals("PUT")) {
            if (params.isEmpty()) responce = Client.putRequest(file, url, id, username, password);
            else responce = Client.putRequest(url, id, username, password, a, b, c);
        } else if (req.equals("DELETE")) {
            if (username != null & password != null) {
                String param;
                if (url.contains("?")) param = "&username=" + username + "&password=" + password;
                else param = "?username=" + username + "&password=" + password;                
                url += param;
            }
            if (!params.isEmpty()) {
                String param;
                if (url.contains("?")) param = "&" + params;
                else param = "?" + params;            
                url += param;
            }
            responce = Client.deleteRequest(url, id);
        }

        if(username != null && password != null && (req.equals("POST") || req.equals("PUT"))) {
            url += " Username:" + username + " Password:" + password;
        }
        if(!params.isEmpty() && (req.equals("POST") || req.equals("PUT"))) {
            url += " " + a + " " + b + " " + c;
        }
        
        updateTextArea(responce, url, req);
        //textArea.setText(str);
    }

    private void updateTextArea(String str, String url, String method) {
        
        JSONObject json = (JSONObject) JSONValue.parse(str);
        String cachedId = "";
        if (json != null) {
            Object objId = json.get("id");
            //Object objMessage = json.get("message");
            if (objId != null) {
                String idValue = objId.toString();
                cachedId = "";
                if (idValue != null && !idValue.isEmpty()) {
                    id = idValue;
                    cachedId = "Cached id = " + id + "\n";
                }
            }/* else if (objMessage != null) {
                str = objMessage.toString();
            }  */         
        } 
        
        str = readString(str);
        
        textArea.setText(textArea.getText() + "\n" + method + " request: " + url + "\n" + cachedId + str + "***********************************************************************\n");
    }
    
//    // Debug
//    private void updateInfoToTextArea(String str, String url) {
//        textArea.setText("Request " + url + "\n" + str  + "\n***************************\n" + textArea.getText());
//    }
    
    private String readString(String s) {
        
        JSONParser parser = new JSONParser();
        ContainerFactory containerFactory = new ContainerFactory() {
            @Override
            public List creatArrayContainer() {
                return new LinkedList();
            }

            @Override
            public Map createObjectContainer() {
                return new LinkedHashMap();
            }

        };
    
        CustomJSON customJson = new CustomJSON();
        try {
            Map json = (Map) parser.parse(s, containerFactory);
            Iterator iter = json.entrySet().iterator();
            while (iter.hasNext()) {
                Map.Entry entry = (Map.Entry) iter.next();
                //System.out.println(entry.getKey() + "=>" + entry.getValue());                
                
                try {
                    Object obj = parser.parse(JSONValue.toJSONString(entry.getValue()));
                    JSONArray jsonArray = (JSONArray) obj;
                    Iterator inerIter = jsonArray.iterator();
                    Table jsonTable = new Table();
                    while(inerIter.hasNext()) {                        
                        JSONObject jsonObj = (JSONObject) inerIter.next();
                        jsonTable.addToTable(Parse.parseBaseElement(jsonObj));
                    }
                    customJson.getTables().add(jsonTable);
                } catch (ParseException pe) {
                    
                } catch (ClassCastException e) {
                    // Make object not a list
                    customJson.getAttributtes().add(new BaseElement(entry.getKey().toString(), entry.getValue().toString()));
                }
                
            }
//            System.out.println(JSONValue.toJSONString(json.get("clones")));
            
        } catch (ParseException pe) {
            textArea.setText(textArea.getText() + "\nError connecting or parsing JSON.");
        }
                
        return customJson.toString();
    }
    
    private void updateParams() {
        a = "";
        b = "";
        c = "";
        params = "";
         //if (jPanelServices.partButton.isSelected()) {
        if (!jPanelServices.steelBox.isSelected()) {
        	 a = (jPanelServices.labelInfoA.getText().isEmpty()) ? "" : "A=" + jPanelServices.labelInfoA.getName();
             b = (jPanelServices.labelInfoB.getText().isEmpty()) ? "" : "B=" + jPanelServices.labelInfoB.getName();
             c = (jPanelServices.labelInfoC.getText().isEmpty()) ? "" : "C=" + jPanelServices.labelInfoC.getName();
//            a = (jPanelServices.fieldA.getText().isEmpty()) ? "" : "A=" + jPanelServices.fieldA.getText();
//            b = (jPanelServices.fieldB.getText().isEmpty()) ? "" : "B=" + jPanelServices.fieldB.getText();
//            c = (jPanelServices.fieldC.getText().isEmpty()) ? "" : "C=" + jPanelServices.fieldC.getText();
            
            if (!a.isEmpty()) {
                if (b.isEmpty() && c.isEmpty())  params += a;                
                else params += a + "&";
            }
            
            if (!b.isEmpty()) {
                if (!c.isEmpty()) params += b + "&";
                else  params += b;
            }
            
            if (!c.isEmpty()) params += c;            
        }
    }
    
    public void setTable(JTable table) {
        this.table = table;
    }

    public void setDropdownMenu(DropdownMenu dropdownMenu) {
        this.dropdownMenu = dropdownMenu;
    }

    public void setTextArea(JTextArea textArea) {
        this.textArea = textArea;
    }

}



        //project.getPrimaryProject().getProjectDescriptor();
        //Application.getInstance().getProjectsManager()
        //Application.getInstance().getProject().getFile();
        //Application.getInstance().getProject().getLocationURI();
        //Application.getInstance().getProject().getFile().createNewFile();