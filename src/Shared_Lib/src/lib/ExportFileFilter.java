/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lib;

import java.io.File;
import javax.swing.filechooser.FileFilter;
import lib.SharedHelper.ExportType;

/**
 *
 * @author Luai
 */
public class ExportFileFilter extends FileFilter {

    private SharedHelper.ExportType type;
    
    public ExportFileFilter(ExportType type) {
        this.type = type;
    }
    
    @Override
    public boolean accept(File f) {
        if (f.isDirectory()) {
            return true;
        } else {
            return f.getName().toLowerCase().endsWith("." + type.toString().toLowerCase());
        }
    }

    @Override
    public String getDescription() {
        return type.toString().toUpperCase() + " file";
    }
}
