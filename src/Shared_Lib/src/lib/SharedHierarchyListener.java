/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lib;

import com.jidesoft.docking.FrameContainer;
import com.nomagic.magicdraw.core.Application;
import com.nomagic.magicdraw.ui.browser.Node;
import java.awt.event.HierarchyEvent;
import java.awt.event.HierarchyListener;
import javax.swing.JPanel;

/**
 *
 * @author Luai
 */
public class SharedHierarchyListener implements HierarchyListener {

    private boolean reload;
    private boolean isHidden = false;
    private JPanel panel;
    private String panelClassName;

    public SharedHierarchyListener(JPanel panel, String panelClassName, boolean reload) {
        this.reload = reload;
        this.panel = panel;
        this.panelClassName = panelClassName;
    }

    @Override
    public void hierarchyChanged(HierarchyEvent e) {
        if (((HierarchyEvent.SHOWING_CHANGED & e.getChangeFlags()) != 0 && e.getChanged().getClass() != FrameContainer.class)
                || ((HierarchyEvent.PARENT_CHANGED & e.getChangeFlags()) != 0 && e.getChanged().getClass() == FrameContainer.class)) {

            if (e.getChanged().getClass().getName().equals(panelClassName)) {
                return;
            }

            if (isHidden && !panel.isVisible()) {
                panel.setVisible(true);
                isHidden = false;
                triggerTreeListener();

            } else if (!panel.isShowing() && panel.isVisible()) {
                panel.setVisible(false);
                isHidden = true;
            }
        }
    }

    public void triggerTreeListener() {
        if (Application.getInstance().getMainFrame().getBrowser().getActiveTree() != null) {
            
            if (reload) {
                Application.getInstance().getMainFrame().getBrowser().getActiveTree().getTreeModel().reload();
            } else {
                Node[] nodes = Application.getInstance().getMainFrame().getBrowser().getActiveTree().getSelectedNodes();
                Application.getInstance().getMainFrame().getBrowser().getActiveTree().setSelectedNodes(null);
                Application.getInstance().getMainFrame().getBrowser().getActiveTree().setSelectedNodes(nodes);
            }
        }
    }

}
