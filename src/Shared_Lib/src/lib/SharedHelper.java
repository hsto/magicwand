/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lib;

import java.awt.Desktop;
import java.awt.Image;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.net.URL;
import java.util.Scanner;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 *
 * @author Luai
 */
public enum SharedHelper {
    INSTANCE;

    private final static String LOGO_FILE_NAME = "logo.png";
    
    public static enum ExportType {
        CSV, PNG;
    }
    
    public boolean readStartup(String fileNameStartup) {
        boolean start = false;

        Scanner scanner = null;
        try {
            //scanner = new Scanner(getClass().getClassLoader().getResourceAsStream(FILE_NAME_STARTUP));
            scanner = new Scanner(new File(fileNameStartup));
            while (scanner.hasNextLine()) {
                if (scanner.nextLine().equals("true")) {
                    start = true;
                    break;
                }
            }
        } catch (Exception e) {
            start = false;
        } finally {
            if (scanner != null) {
                scanner.close();
            }
        }

        return start;
    }

    public void writeStartup(String fileNameStartup, JPanel panel) {
        File file = new File(fileNameStartup);
        if (!file.exists()) {
            file.getParentFile().mkdir();
        }
        PrintWriter writer;
        try {
            writer = new PrintWriter(fileNameStartup);
            writer.println(panel.isDisplayable());
            writer.close();
        } catch (FileNotFoundException e) {
        }
    }

    public JLabel initLogo() {

        BufferedImage logo = null;
        Image imageLogo = null;        
        try {
            logo = ImageIO.read(getClass().getClassLoader().getResourceAsStream(LOGO_FILE_NAME));
            imageLogo = logo.getScaledInstance(64, 64, Image.SCALE_SMOOTH);
            return new JLabel(new ImageIcon(imageLogo));            
        } catch (IOException e) {
            return null;
        } finally {
            if (logo != null) {
                logo.flush();
            }
            if (imageLogo != null) {
                imageLogo.flush();
            }
        }
    }
    
    public void initLogoLink(JLabel logoLabel, InputStream linkFileInputStream) {
        String str = "";
        Scanner scanner = null;
        try {
            scanner = new Scanner(linkFileInputStream);
            while (scanner.hasNextLine()) {
                str = scanner.nextLine();
                if (!str.isEmpty()) {
                    break;
                }
            }
        } catch (Exception e) {
        } finally {
            if (scanner != null) {
                scanner.close();
            }
        }

        if (str.isEmpty()) {
            return;
        }

        final String link = str;
        logoLabel.setToolTipText(link);
        logoLabel.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent evt) {
                try {
                    Desktop.getDesktop().browse(new URL(link).toURI());
                } catch (Exception e) {
                }
            }
        });
    }

     public ImageIcon initIcon(String fileName) {
        BufferedImage icon = null;
        try {
            icon = ImageIO.read(getClass().getClassLoader().getResourceAsStream(fileName));
            return new ImageIcon(icon);
        } catch (IOException e) {
            return null;
        } finally {
            if (icon != null) {
                icon.flush();
            }
        }
    }

    public void setIconName(boolean collapsed, JButton foldButton, String name, ImageIcon imageIconRight, ImageIcon imageIconDown) {
        if (collapsed) {
            if (imageIconRight != null) {
                foldButton.setIcon(imageIconRight);
                foldButton.setText(name);
            } else {
                foldButton.setText("+ " + name);
            }
        } else {
            if (imageIconDown != null) {
                foldButton.setIcon(imageIconDown);
                foldButton.setText(name);
            } else {
                foldButton.setText("- " + name);
            }
        }
    }

}
