package lib;



import com.nomagic.actions.AMConfigurator;
import com.nomagic.actions.ActionsCategory;
import com.nomagic.actions.ActionsManager;
import com.nomagic.actions.NMAction;
import com.nomagic.magicdraw.actions.MDActionsCategory;

public class MainMenuConfigurator implements AMConfigurator{

	private String mainMenuName = "MagicWand";
	private NMAction action;	
	
	public MainMenuConfigurator(NMAction action) {
		this.action = action;
	}
	
	@Override
	public int getPriority() {
		return AMConfigurator.MEDIUM_PRIORITY;
	}

	@Override
	public void configure(ActionsManager manager) {
		ActionsCategory category = (ActionsCategory) manager.getActionFor(mainMenuName);
		
		if (category == null) {
			category = new MDActionsCategory(mainMenuName, mainMenuName);
			category.setNested(true);
			manager.addCategory(category);
		}
		category.addAction(action);
		
		
	}

}
