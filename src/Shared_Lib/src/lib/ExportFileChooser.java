/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lib;

import java.io.File;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import lib.SharedHelper.ExportType;

/**
 *
 * @author Luai
 */
public class ExportFileChooser extends JFileChooser {

    private ExportType type;
    
    public ExportFileChooser(ExportType type) {
        this.type = type;
    }
    
    @Override
    public void approveSelection() {
        File file = new File(getSelectedFile() + "." + type.toString().toLowerCase());
        if (getSelectedFile().exists() || file.exists()) {
            int val = JOptionPane.showConfirmDialog(this, "File already exists, overwrite?", "Warning!", JOptionPane.YES_NO_OPTION);
            if (val == JOptionPane.NO_OPTION || val == JOptionPane.CLOSED_OPTION) {
                return;
            }
        }
        super.approveSelection();
    }
}
