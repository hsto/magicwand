/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package reportEntry.model;

import java.util.Vector;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author rb328
 */
public class ListTableModel extends AbstractTableModel {

    @Override
    public void fireTableCellUpdated(int row, int column) {
    }

    private Vector<String> rows;
    
    public ListTableModel(Vector<String> iRows) {
        rows = iRows;
    }

    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
        rows.set(rowIndex, (String) aValue);
        fireTableDataChanged();
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return true;
    }

    public int getRowCount() {
        return rows.size();
    }

    public int getColumnCount() {
        return 1;
    }

    public Object getValueAt(int rowIndex, int columnIndex) {
        return rows.get(rowIndex);
    }

    public Vector<String> getRows() {
        return rows;
    }
    
    public void addRow() {
        rows.add("");
        fireTableRowsInserted(rows.size()-2, rows.size()-1);
    }
    
    public int removeRow() {
        if (rows.size() == 1) {
            return 0;
        } 
        rows.remove(rows.size()-1);
        fireTableRowsDeleted(rows.size()-2, rows.size()-1);
        return 1;
    }
}
