/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package reportEntry.view;

import reportEntry.model.ListTableCellEditor;
import reportEntry.controller.TableModelListenerTextBoxes;
import reportEntry.controller.DocumentListenerTextBoxes;
import reportEntry.controller.ActionListenerAddTextAreaButton;
import reportEntry.controller.ActionListenerJButtonHide;
import reportEntry.model.ListTableModel;
import reportEntry.TextHelper;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Vector;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import reportXMLpackage.Doc;
import reportXMLpackage.ObjectFactory;
import reportXMLpackage.Ultype;

/**
 *
 * @author rb328
 */
public class JPanelTextSection extends javax.swing.JPanel{
    
    public JPanelMainEdit mainPanel;
    
    GroupLayout layout = new GroupLayout(this);
    
    private final JButton jbFoldingButton = new JButton();
    private final JLabel labelHeader = new JLabel();
    private final JLabel labelText = new JLabel();
    private final JTextField tfHeader = new JTextField();
    private final JButton addTextAreaButton = new JButton("Add text");
    private final JButton addListAreaButton = new JButton("Add list");
    private final JButton removeTextAreaButton = new JButton("Remove text");
    private final JPanel JPanelTextContainer = new JPanel();
    private final List<JComponent> textBoxesList = new ArrayList<JComponent>();
    private final DocumentListenerTextBoxes documentlistenerTextBoxes; 
    private final TableModelListenerTextBoxes tableModellistenerTextBoxes;
    private final String sectionType;
   
    public JPanelTextSection(DocumentListenerTextBoxes listener, 
            TableModelListenerTextBoxes tableModellistenerTextBoxes,
            String sectionType, JPanelMainEdit mainPanel) {
        documentlistenerTextBoxes = listener;
        this.tableModellistenerTextBoxes = tableModellistenerTextBoxes;
        this.sectionType = sectionType;
        this.mainPanel = mainPanel;
        initComponents();
    }

    public enum eSectionType {
      intro, main, afterword
    }
    
    public void fillSection(String documentation, Doc doc){
        inactivateListeners();
        
        tfHeader.setText(TextHelper.getItem(documentation, sectionType, "h"));
        
        boolean empty = true;
        eSectionType e;
        e = eSectionType.valueOf(sectionType);
        ListIterator<Object> listIterator = null;
        switch (e) {
            case intro:
                if ( doc.getIntro() != null){
                    listIterator = doc.getIntro().getTextgroup().listIterator();
                    empty = false;
                }
                break;
            case main:
                if ( doc.getMain() != null){
                    listIterator = doc.getMain().getTextgroup().listIterator();
                    empty = false;
                }
                break;
            case afterword:
                if ( doc.getAfterword() != null){
                    listIterator = doc.getAfterword().getTextgroup().listIterator();
                    empty = false;
                }
                break;

        }
 
        if (listIterator != null ){
        while ( !empty & listIterator.hasNext()) {
            Object object = listIterator.next();

            if (object.getClass().equals(String.class)) {
                addJTextArea((String) object);
            }

            if (object.getClass().equals(Ultype.class)) {
                Ultype ui = (Ultype) object;
                Vector<String> rowData = new Vector<String>();
                Iterator<String> iterator = ui.getLi().iterator();
                ListTableModel model = new ListTableModel(rowData);
                while (iterator.hasNext()) {
                    rowData.add(iterator.next().toString());
                    //Application.getInstance().getGUILog().log("rowdata add: " + rowData.lastElement().toString());
                }
                JTable list = new JTable(model);
                addTable(list);
            }
        }
        }
        
        activateListeners();
        
    }
    
    private void inactivateListeners() {
        tfHeader.getDocument().removeDocumentListener(documentlistenerTextBoxes);
        
        for (JComponent c : textBoxesList) {
            if (c.getClass().equals(JTextArea.class)) {
                JTextArea t = (JTextArea) c;
                t.getDocument().removeDocumentListener(documentlistenerTextBoxes);
            }
            if (c.getClass().equals(JTable.class)) {
                JTable t = (JTable) c;
                t.getModel().removeTableModelListener(tableModellistenerTextBoxes);
            }
        }
    }
    
    private void activateListeners() {
        tfHeader.getDocument().addDocumentListener(documentlistenerTextBoxes);
        
        for (JComponent c : textBoxesList) {
            if (c.getClass().equals(JTextArea.class)) {
                JTextArea t = (JTextArea) c;
                t.getDocument().addDocumentListener(documentlistenerTextBoxes);
            }
            if (c.getClass().equals(JTable.class)) {
                JTable t = (JTable) c;
                t.getModel().addTableModelListener(tableModellistenerTextBoxes);
            }
        }
    }
    
    public List<Object> getSectionsText(ObjectFactory of) {

        List<Object> l = new ArrayList<Object>();
        for (JComponent c : textBoxesList) {
            if (c.getClass().equals(JTextArea.class)) {
                JTextArea t = (JTextArea) c;
                if (!t.getText().equals(""))
                   l.add(t.getText());
            }
            if (c.getClass().equals(JTable.class)) {
                JTable t = (JTable) c;

                ListTableModel model = (ListTableModel) t.getModel();

                if (t.getCellEditor() != null) {
                    t.getCellEditor().stopCellEditing();
                }

                model.fireTableDataChanged();

                ArrayList<String> p = new ArrayList<String>();

                for (String s : model.getRows()) {
                    if (!s.isEmpty() | !s.trim().isEmpty()) {
                        p.add(s);
                    }
                }
 
                Ultype ul = of.createUltype();
                ul.getLi().addAll(p);
                if ( ! ul.getLi().isEmpty())
                    l.add(ul);
            }

        }
        return l;
    }

    public void updateSection(Doc doc, ObjectFactory of){

        eSectionType e;
        e = eSectionType.valueOf(sectionType);
        
       switch (e) {
            case intro:
                Doc.Intro intro = of.createDocIntro();
                intro.setH(tfHeader.getText());
                intro.getTextgroup().clear();
                intro.getTextgroup().addAll(getSectionsText(of));
                if (!intro.getH().isEmpty() |
                        intro.getTextgroup().size() > 0)
                    doc.setIntro(intro);
                break;
            case main:
                Doc.Main main = of.createDocMain();
                main.setH(tfHeader.getText());
                main.getTextgroup().clear();
                main.getTextgroup().addAll(getSectionsText(of));
                if (!main.getH().isEmpty() |
                        main.getTextgroup().size() > 0)
                    doc.setMain(main);
                break;
            case afterword:
                Doc.Afterword afterword = of.createDocAfterword();
                afterword.setH(tfHeader.getText());
                afterword.getTextgroup().clear();
                afterword.getTextgroup().addAll(getSectionsText(of));
                if (!afterword.getH().isEmpty() |
                        afterword.getTextgroup().size() > 0)
                    doc.setAfterword(afterword);
                break;
       } 
    }
    public JButton getJbFoldingButton() {
        return jbFoldingButton;
    }
 
    private void initComponents() {
        //jbFoldingButton.setBorder(BorderFactory.createLineBorder(Color.BLACK, 1));
        //jbFoldingButton.setBorder(BorderFactory.createMatteBorder(0,0,1,0,Color.BLACK));
        jbFoldingButton.setMinimumSize(new Dimension(40,25));
        jbFoldingButton.setMaximumSize(new Dimension(Short.MAX_VALUE, 30));
        jbFoldingButton.setContentAreaFilled(false);
        jbFoldingButton.setHorizontalAlignment(SwingConstants.LEFT);
        jbFoldingButton.setBorder(BorderFactory.createEmptyBorder(4,1,4,4));
        jbFoldingButton.addActionListener(new ActionListenerJButtonHide(this));
        
        labelHeader.setFont(new Font("", Font.BOLD, 12));
        labelText.setFont(new Font("", Font.BOLD, 12));
        
        tfHeader.setMaximumSize(new Dimension(Short.MAX_VALUE, 15));
        //tfHeader.setMinimumSize(new Dimension(10,15));
        tfHeader.setSelectionColor(Color.gray);
        tfHeader.getDocument().addDocumentListener(documentlistenerTextBoxes);
        //tfIntroHeader.setFont(new Font(TextAttribute.WEIGHT_BOLD));
        
        //addJTextArea("");
        
        JPanelTextContainer.setLayout(new BoxLayout(JPanelTextContainer, BoxLayout.Y_AXIS));
        JPanelTextContainer.setMaximumSize(new Dimension(Short.MAX_VALUE, Short.MAX_VALUE));
        JPanelTextContainer.setMinimumSize(new Dimension(30, 30));
        JPanelTextContainer.setVisible(false);
        
        addTextAreaButton.addActionListener(new ActionListenerAddTextAreaButton(this));
        addTextAreaButton.setName("addtext");
        addListAreaButton.addActionListener(new ActionListenerAddTextAreaButton(this));
        addListAreaButton.setName("addlist");
        removeTextAreaButton.addActionListener(new ActionListenerAddTextAreaButton(this));
        removeTextAreaButton.setName("removeText");
        
        this.setLayout(layout);

        layout.setAutoCreateGaps(true);
        layout.setAutoCreateContainerGaps(true);

        layout.setHorizontalGroup(
                layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                        .addComponent(labelHeader)
                        .addComponent(tfHeader)
                        .addComponent(labelText)
                        .addComponent(JPanelTextContainer)
                        .addGroup(layout.createSequentialGroup()
                            .addComponent(addTextAreaButton)
                            .addComponent(removeTextAreaButton)
                            .addComponent(addListAreaButton))
                )
        );
        layout.setVerticalGroup(
                layout.createSequentialGroup()
                        .addComponent(labelHeader)
                        .addComponent(tfHeader)
                        .addComponent(labelText)
                        .addComponent(JPanelTextContainer)
                        .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                            .addComponent(addTextAreaButton)
                            .addComponent(removeTextAreaButton)
                            .addComponent(addListAreaButton)) 
        ); 
                
        this.setVisible(false);
    }

    public void addJTextArea( String s ){
        JTextArea taIntroText = new JTextArea(s);
        taIntroText.setPreferredSize(new Dimension(10, 40));
        taIntroText.setMinimumSize(new Dimension(20, 20));
        taIntroText.setMaximumSize(new Dimension(Short.MAX_VALUE, Short.MAX_VALUE));
        taIntroText.setLineWrap(true);
        taIntroText.setWrapStyleWord(true);  
        taIntroText.getDocument().addDocumentListener(documentlistenerTextBoxes);
        JPanelTextContainer.add(taIntroText);
        textBoxesList.add(taIntroText);
        JPanelTextContainer.setVisible(true);
    }

    public void addTable(JTable t) {
        final JTable table = t;
        Dimension size = new Dimension(15, 15);

        //table.setMinimumSize(new Dimension(370, 15));
        //table.setMaximumSize(new Dimension(370, Short.MAX_VALUE));

        table.setCellEditor(new ListTableCellEditor() {});

        final JPanel listPanel = new JPanel();
        listPanel.setLayout(new BoxLayout(listPanel, BoxLayout.X_AXIS));
        //listPanel.setLayout(new BoxLayout(listPanel, BoxLayout.LINE_AXIS));
        listPanel.setMaximumSize(new Dimension(Short.MAX_VALUE, Short.MAX_VALUE));
        listPanel.setMinimumSize(new Dimension(20, 20));
        
        //listPanel.add(new Box.Filler(size, size, size));
        //table.setAlignmentY(Component.BOTTOM_ALIGNMENT);
        //Box box = Box.
        
        listPanel.add(table);
        //listPanel.setAlignmentX(Component.BOTTOM_ALIGNMENT);
        JButton addButton = new JButton("+");
        addButton.setAlignmentX(Component.TOP_ALIGNMENT);
        addButton.setMaximumSize(size);
        addButton.setMinimumSize(size);
        addButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                ListTableModel m = (ListTableModel) table.getModel();
                m.addRow();
            }
        });
        
        JButton removeButton = new JButton("-");
        removeButton.setAlignmentX(Component.BOTTOM_ALIGNMENT);
        removeButton.setMaximumSize(size);
        removeButton.setMinimumSize(size);
        removeButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                ListTableModel m = (ListTableModel) table.getModel();
                if (m.removeRow() == 0) {
                    Component filler = null;
                    for (int i = 0; i < JPanelTextContainer.getComponents().length; i++) {
                        if (JPanelTextContainer.getComponents()[i].equals(listPanel)) {
                            
                            
                            JPanelTextContainer.remove(listPanel);
                            textBoxesList.remove(table);
                            m.removeTableModelListener(tableModellistenerTextBoxes);
                            
                            if (i > 0 && JPanelTextContainer.getComponents()[i-1] instanceof Box.Filler) 
                                filler = JPanelTextContainer.getComponents()[i-1];
                            if (filler != null) {
                                JPanelTextContainer.remove(filler);
                                filler = null;
                            }                            
                            JPanelTextContainer.revalidate();                            
                            
                        }
                    }

                    if (JPanelTextContainer.getComponents().length == 0) {
                        JPanelTextContainer.setVisible(false);
                    }
                    
                }                    
            }
        });
        
        table.getModel().addTableModelListener(tableModellistenerTextBoxes);
        listPanel.add(addButton);
        listPanel.add(removeButton);
        //listPanel.add(new Box.Filler(size, size, size));
        JPanelTextContainer.add(listPanel);
        textBoxesList.add(table);
        JPanelTextContainer.setVisible(true);
        //JPanelTextContainer.revalidate();
        //JPanelTextContainer.repaint();
    }

    public void clearFields() {
        tfHeader.setText("");
        for (JComponent a : textBoxesList) {
           JPanelTextContainer.remove(a);
        }
        textBoxesList.clear();
        JPanelTextContainer.removeAll();
        JPanelTextContainer.invalidate();
        JPanelTextContainer.setVisible(false);
    }
    public JLabel getLabelHeader() {
        return labelHeader;
    }

    public JLabel getLabelText() {
        return labelText;
    }
    
    public JTextField getTfHeader() {
        return tfHeader;
    }
 
    public JButton getAddTextAreaButton() {
        return addTextAreaButton;
    }

    public JButton getAddListAreaButton() {
        return addListAreaButton;
    }
    
    public JPanel getJPanelTextContainer() {
        return JPanelTextContainer;
    }
    
    public String getSectionType() {
        return sectionType;
    }
    
    public List<JComponent> getTextBoxesList() {
        return textBoxesList;
    }
}
