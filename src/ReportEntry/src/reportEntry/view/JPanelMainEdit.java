/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package reportEntry.view;

import reportEntry.controller.TableModelListenerTextBoxes;
import reportEntry.controller.DocumentListenerTextBoxes;
import reportEntry.controller.ActionListenerHideElementCheckBox;
import reportEntry.controller.ActionListenerSaveButton;
import reportEntry.controller.ReportTreeSelectionListener;
import reportEntry.TextHelper;
import com.nomagic.magicdraw.uml.BaseElement;
import eu.hansolo.custom.SteelCheckBox;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import javax.swing.BorderFactory;
import javax.swing.GroupLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import lib.SharedHelper;
import lib.SharedHierarchyListener;
import reportEntry.controller.ReportTreeSelectionListener.Completeness;
import reportXMLpackage.Doc;
import reportXMLpackage.ObjectFactory;

/**
 * jScrollPane1
 *
 * @author rb328
 */
//public class JPanelMainEdit extends javax.swing.JPanel{
public class JPanelMainEdit extends javax.swing.JScrollPane {

    public JPanel innerPanel = new JPanel();
    private final GroupLayout layout = new GroupLayout(innerPanel);
    //private JCheckBox cbHideElement = new JCheckBox();
    public SteelCheckBox cbHideElement = new SteelCheckBox();
    private JLabel labelElementName = new JLabel();
    private final JLabel completenessNodeLabelHeader = new JLabel("Current: ");
    private final JLabel completenessSubTreeLabelHeader = new JLabel("Subtree: ");
    private final JLabel completenessNodeLabel = new JLabel("-");
    private final JLabel completenessSubTreeLabel = new JLabel("-");
    //private final JButton jbIntroduction = new JButton();
    //private final JButton jbMaintext = new JButton();
    //private final JButton jbAfterword = new JButton();
    private final JButton jbSave = new JButton();
    
//    private final JSeparator jSeparatorIntro = new JSeparator();
//    private final JSeparator jSeparatorMain = new JSeparator();
//    private final JSeparator jSeparatorAfterword = new JSeparator();
    
    private final DocumentListenerTextBoxes documentlistenerTextBoxes = new DocumentListenerTextBoxes(this);
    private final TableModelListenerTextBoxes tableModellistenerTextBoxes = new TableModelListenerTextBoxes(this);
    
    private final JButton foldButtonSettings = new JButton();
    
    private final JPanelTextSection introSection = new JPanelTextSection(documentlistenerTextBoxes, tableModellistenerTextBoxes, "intro", this);
    private final JPanelTextSection mainSection = new JPanelTextSection(documentlistenerTextBoxes, tableModellistenerTextBoxes, "main", this);
    private final JPanelTextSection afterwordSection = new JPanelTextSection(documentlistenerTextBoxes, tableModellistenerTextBoxes, "afterword", this);
    private final List<JPanelTextSection> sectionsList = new ArrayList<JPanelTextSection>();
    
    private final ActionListenerSaveButton actionlistenerSaveButton = new ActionListenerSaveButton(this);
    private final ActionListenerHideElementCheckBox actionlistenerHideElementCheckBox = new ActionListenerHideElementCheckBox(this);
    
    private final ReportTreeSelectionListener selectionListener = new ReportTreeSelectionListener(this);

    private final static String ICON_RIGHT_FILE_NAME = "arrow-right.png";
    private final static String ICON_DOWN_FILE_NAME = "arrow-down.png";
    private final static String FILE_NAME = "link_CodeAnnotation.txt";
    private JLabel logoLabel = new JLabel();
    
    public ImageIcon imageIconRight;
    public ImageIcon imageIconDown;
    
    public boolean skipSave = false;
    
    public JPanelMainEdit() {
        this.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        this.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
        initComponents();
        this.addHierarchyListener(new SharedHierarchyListener(innerPanel, JPanelMainEdit.this.getClass().getName(), false));
    }
    
    public ReportTreeSelectionListener getSelectionListener() {
        return selectionListener;
    }

    public JButton getJbSave() {
        return jbSave;
    }

    public JPanel getInnerPanel() {
        return innerPanel;
    }
    public JPanelTextSection getIntroSection() {
        return introSection;
    }

    public JPanelTextSection getMainSection() {
        return mainSection;
    }

    private void initComponents() {
       logoLabel = SharedHelper.INSTANCE.initLogo();
        SharedHelper.INSTANCE.initLogoLink(logoLabel, getClass().getClassLoader().getResourceAsStream(FILE_NAME));
        imageIconRight = SharedHelper.INSTANCE.initIcon(ICON_RIGHT_FILE_NAME);
        imageIconDown = SharedHelper.INSTANCE.initIcon(ICON_DOWN_FILE_NAME);
        
        getSectionsList().add(introSection);
        getSectionsList().add(mainSection);
        getSectionsList().add(afterwordSection);
        
        this.getVerticalScrollBar().setUnitIncrement(20);
        
        JLabel labelTitel = new JLabel();
    	labelTitel.setFont(new Font("",Font.BOLD,16));
    	labelTitel.setText("Prose Annotations for Reporting");
    	labelTitel.setMinimumSize(new Dimension(40,25));
        labelElementName.setFont(new Font("",Font.PLAIN,14));
        labelElementName.setText("Select element in containment tree");
        labelElementName.setMinimumSize(new Dimension(40,25));
        
        Font font = new Font("",Font.BOLD,14);
        
        final String foldButtonSetteingsName = "Settings";
        
        final JLabel labelHideElement = new JLabel("Exclude element from report ");   
        labelHideElement.setVisible(false);
        cbHideElement.setVisible(false);
    	foldButtonSettings.setFont(font);
        SharedHelper.INSTANCE.setIconName(true, foldButtonSettings, foldButtonSetteingsName, imageIconRight, imageIconDown);
    	foldButtonSettings.setVisible(true);
    	foldButtonSettings.setMinimumSize(new Dimension(40,25));
    	foldButtonSettings.setMaximumSize(new Dimension(Short.MAX_VALUE, 30));
    	foldButtonSettings.setContentAreaFilled(false);
    	foldButtonSettings.setHorizontalAlignment(SwingConstants.LEFT);
    	foldButtonSettings.setBorder(BorderFactory.createEmptyBorder(4,1,4,4));
    	foldButtonSettings.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				if (cbHideElement.isVisible()) {
                                        labelHideElement.setVisible(false);
                                        cbHideElement.setVisible(false);
                                        SharedHelper.INSTANCE.setIconName(true, foldButtonSettings, foldButtonSetteingsName, imageIconRight, imageIconDown);
				} else {
                                        labelHideElement.setVisible(true);
                                        cbHideElement.setVisible(true);
                                        SharedHelper.INSTANCE.setIconName(false, foldButtonSettings, foldButtonSetteingsName, imageIconRight, imageIconDown);
				}
			}
		});
        
        cbHideElement.setSelected(false);
        cbHideElement.setToolTipText("Check here if you want the element to be omitted from the generated report.");
        //cbHideElement.setText("Hide element");
//        cbHideElement.setText("Exclude element from report");
        cbHideElement.setText("No");
        cbHideElement.setName("labelElementName");
        cbHideElement.addActionListener(actionlistenerHideElementCheckBox);
        cbHideElement.setBorder(BorderFactory.createEmptyBorder(0, 10, 0, 10));

        SharedHelper.INSTANCE.setIconName(true, introSection.getJbFoldingButton(), "Introduction", imageIconRight, imageIconDown);
        introSection.getJbFoldingButton().setFont(font);
        introSection.getJbFoldingButton().setToolTipText("Click to fold or unfold the introduction text");
        introSection.getJbFoldingButton().setVisible(true);
 
        Font fontSmallB = new Font("",Font.BOLD,11);
        Font fontSmall = new Font("",Font.PLAIN,11);
        completenessNodeLabelHeader.setFont(fontSmallB);
        completenessNodeLabelHeader.setToolTipText("Completeness indicator");
        completenessSubTreeLabelHeader.setFont(fontSmallB);
        completenessSubTreeLabelHeader.setToolTipText("Completeness indicator");
        completenessNodeLabel.setFont(fontSmall);
        completenessNodeLabel.setToolTipText("Completeness indicator");
        completenessSubTreeLabel.setFont(fontSmall);
        completenessSubTreeLabel.setToolTipText("Completeness indicator");
        
       
//        URL imageURL = this.getClass().getResource("images/plus_small.png");
//        if (imageURL != null) {
//            ImageIcon icon = new ImageIcon(imageURL);
//            introSection.getJbFoldingButton().setIcon(icon);
//            mainSection.getJbFoldingButton().setIcon(icon);
//            afterwordSection.getJbFoldingButton().setIcon(icon);
//        }
        /*
         Image img = icon.getImage() ;  
         Image newimg = img.getScaledInstance( NEW_WIDTH, NEW_HEIGHT,  java.awt.Image.SCALE_SMOOTH ) ;  
         icon = new ImageIcon( newimg );
         */

        
        SharedHelper.INSTANCE.setIconName(true,  mainSection.getJbFoldingButton(), "Body", imageIconRight, imageIconDown);
        mainSection.getJbFoldingButton().setFont(font);
        mainSection.getJbFoldingButton().setToolTipText("Folds or unfolds the main text");
        mainSection.getJbFoldingButton().setVisible(true);
 
        SharedHelper.INSTANCE.setIconName(true, afterwordSection.getJbFoldingButton(), "Closure", imageIconRight, imageIconDown);
        afterwordSection.getJbFoldingButton().setFont(font);
        afterwordSection.getJbFoldingButton().setToolTipText("Folds or unfolds the afterword text");
        afterwordSection.getJbFoldingButton().setVisible(true);

        innerPanel.setLayout(layout);

        introSection.setToolTipText("introSection");
        introSection.getLabelHeader().setText("Introduction header");
        introSection.getLabelText().setText("Introduction text");
        mainSection.setToolTipText("mainSection");
        mainSection.getLabelHeader().setText("Main text header");
        mainSection.getLabelText().setText("Main text");
        afterwordSection.setToolTipText("afterwordSection");
        afterwordSection.getLabelHeader().setText("Afterword header");
        afterwordSection.getLabelText().setText("Afterword text");
        
        jbSave.setText("Save");
        jbSave.setToolTipText("Save changes to project");
        jbSave.addActionListener(actionlistenerSaveButton);
        
        layout.setAutoCreateGaps(true);
        layout.setAutoCreateContainerGaps(true);

        layout.setHorizontalGroup(
                layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup()
                                        .addComponent(labelTitel)
                                        .addComponent(labelElementName)                                        
                                )
                                .addGap(4, 4, Short.MAX_VALUE)
                                .addComponent(logoLabel)
                        )
                        .addComponent(foldButtonSettings)
                        .addGroup(layout.createSequentialGroup()
                            .addComponent(labelHideElement)
                            .addComponent(cbHideElement)
                        )
                        .addComponent(introSection.getJbFoldingButton())
                        .addComponent(introSection)
//                        .addComponent(jSeparatorIntro)
                        .addComponent(mainSection.getJbFoldingButton())
                        .addComponent(mainSection)
//                        .addComponent(jSeparatorMain)
                        .addComponent(afterwordSection.getJbFoldingButton())                    
                        .addComponent(afterwordSection)
//                        .addComponent(jSeparatorAfterword)
                        .addGroup(layout.createSequentialGroup()
                            .addComponent(jbSave)
                            .addGroup(layout.createParallelGroup()
                                .addComponent(completenessNodeLabelHeader)
                                .addComponent(completenessSubTreeLabelHeader)
                            )
                            .addGroup(layout.createParallelGroup()
                                .addComponent(completenessNodeLabel)
                                .addComponent(completenessSubTreeLabel)
                            )
                        )
                )
        );
        
        layout.setVerticalGroup(
                layout.createSequentialGroup()
                //.addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                .addGroup(layout.createParallelGroup()
        			.addGroup(layout.createSequentialGroup()
                                                .addComponent(labelTitel)
        					.addComponent(labelElementName)       
        			)        			
                	.addComponent(logoLabel)
                )
                .addComponent(foldButtonSettings)
                .addGroup(layout.createParallelGroup()
                    .addComponent(labelHideElement)
                    .addComponent(cbHideElement)
                )
                .addComponent(introSection.getJbFoldingButton())
                .addComponent(introSection)
//                .addComponent(jSeparatorIntro, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addComponent(mainSection.getJbFoldingButton())
                .addComponent(mainSection)
//                .addComponent(jSeparatorMain, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addComponent(afterwordSection.getJbFoldingButton())
                .addComponent(afterwordSection)
//                .addComponent(jSeparatorAfterword, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGroup(layout.createParallelGroup()
                    .addComponent(jbSave)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(completenessNodeLabelHeader)
                        .addComponent(completenessSubTreeLabelHeader)
                    )
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(completenessNodeLabel)
                        .addComponent(completenessSubTreeLabel)
                    )
                )
        );
        
        viewport.add(innerPanel);
    }
    
    public void fillSections(String documentation, Doc doc, BaseElement element, Completeness current, List<Completeness> sub) {

        clearFields();
        
        cbHideElement.setSelected(TextHelper.hideElement(documentation));

        labelElementName.setText("Documentation for " + element.getHumanName());

        if (doc != null) {
            for (JPanelTextSection s : getSectionsList()) {
                s.fillSection(documentation, doc);
            }
        }
        
        updateCompleteness(current, sub);
    }
    
    public void updateSections(Doc doc, ObjectFactory of) {
        
        String hide = cbHideElement.isSelected() ? "true" : "false";
        doc.setHide(hide);
        
        for (JPanelTextSection s : getSectionsList()) {
            s.updateSection(doc, of);
        }
    }
    
    private void updateCompleteness(Completeness current, List<Completeness> sub) {
        if (current == null)
            completenessNodeLabel.setText("-");
        else            
            completenessNodeLabel.setText(current.complete + " of " + current.total + " completed");
        
        if (sub == null || sub.isEmpty()) {
            completenessSubTreeLabel.setText("-");
        }
        else {
            int completed = 0, total = 0;
            for (Completeness c : sub) {
                completed += c.complete;
                total += c.total;
            }

            completenessSubTreeLabel.setText(completed + " of " + total + " completed");
        }
    }
    
    public void clearFields(){
        labelElementName.setText("");
        cbHideElement.setSelected(false);
        introSection.clearFields();
        mainSection.clearFields();
        afterwordSection.clearFields();
        jbSave.setEnabled(false);
    }

    public JPanelTextSection getAfterwordSection() {
        return afterwordSection;
    }

    public JCheckBox getCbHideElement() {
        return cbHideElement;
    }

//    public void setCbHideElement(JCheckBox cbHideElement) {
//        this.cbHideElement = cbHideElement;
//    }

    public JLabel getLabelElementName() {
        return labelElementName;
    }

    public void setLabelElementName(JLabel labelElementName) {
        this.labelElementName = labelElementName;
    }

    public List<JPanelTextSection> getSectionsList() {
        return sectionsList;
    }

}
