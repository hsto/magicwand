/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package reportEntry.controller;

import reportEntry.view.JPanelTextSection;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import lib.SharedHelper;

/**
 *
 * @author rb328
 */
//public class actionListenerJButtonSections implements ActionListener {
public class ActionListenerJButtonHide implements ActionListener {

    JPanelTextSection panel = null;
    //ImageIcon icon_plus = null;
    //ImageIcon icon_minus = null;
    
    public ActionListenerJButtonHide() {
    }

    public ActionListenerJButtonHide(JPanelTextSection panel) {
        this.panel = panel;
//        URL imageURL = this.getClass().getResource("images/plus_small.png");
//        if (imageURL != null) {
//            icon_plus = new ImageIcon(imageURL);
//        }
//        imageURL = this.getClass().getResource("images/minus_small.png");
//        if (imageURL != null) {
//            icon_minus = new ImageIcon(imageURL);
//        }
//        
//        if( panel.isVisible() )
//           panel.getJbFoldingButton().setIcon(icon_minus);
//        else
//           panel.getJbFoldingButton().setIcon(icon_plus);
        
        setText();
    }


    @Override
    public void actionPerformed(ActionEvent e) {
        //glayout.setHonorsVisibility(!glayout.getHonorsVisibility());
        panel.setVisible(!panel.isVisible());
        
//        if( panel.isVisible() )
//           panel.getJbFoldingButton().setIcon(icon_minus);
//        else
//           panel.getJbFoldingButton().setIcon(icon_plus);
        
        setText();
        
    }
    
    private void setText() {
        if( panel.isVisible()) {
            //panel.getJbFoldingButton().setText("- Show/hide text for " + this.panel.getSectionType() +  " section");
            if (panel.getSectionType().equals("intro")) {
                SharedHelper.INSTANCE.setIconName(false, panel.getJbFoldingButton(), "Introduction", panel.mainPanel.imageIconRight, panel.mainPanel.imageIconDown);
            } else if (panel.getSectionType().equals("main")) {
                SharedHelper.INSTANCE.setIconName(false, panel.getJbFoldingButton(), "Body", panel.mainPanel.imageIconRight, panel.mainPanel.imageIconDown);
            } else if (panel.getSectionType().equals("afterword")) {
                SharedHelper.INSTANCE.setIconName(false, panel.getJbFoldingButton(), "Closure", panel.mainPanel.imageIconRight, panel.mainPanel.imageIconDown);
            }
        }
        else {
           //panel.getJbFoldingButton().setText("+ Show/hide text for " + this.panel.getSectionType() +  " section");
            if (panel.getSectionType().equals("intro")) {
                SharedHelper.INSTANCE.setIconName(true, panel.getJbFoldingButton(), "Introduction", panel.mainPanel.imageIconRight, panel.mainPanel.imageIconDown);
            } else if (panel.getSectionType().equals("main")) {
               SharedHelper.INSTANCE.setIconName(true, panel.getJbFoldingButton(), "Body", panel.mainPanel.imageIconRight, panel.mainPanel.imageIconDown);
            } else if (panel.getSectionType().equals("afterword")) {
                SharedHelper.INSTANCE.setIconName(true, panel.getJbFoldingButton(), "Closure", panel.mainPanel.imageIconRight, panel.mainPanel.imageIconDown);
            }
        }
    }
}
