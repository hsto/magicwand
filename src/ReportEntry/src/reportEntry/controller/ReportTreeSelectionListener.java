/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package reportEntry.controller;

import reportEntry.view.JPanelMainEdit;
import static reportEntry.TextHelper.cleanDoc;
import com.nomagic.magicdraw.core.Application;
import com.nomagic.magicdraw.ui.browser.Node;
import com.nomagic.magicdraw.ui.browser.Tree;
import com.nomagic.magicdraw.uml.BaseElement;
import com.nomagic.uml2.ext.jmi.helpers.ModelHelper;
import com.nomagic.uml2.ext.magicdraw.classes.mdkernel.Element;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import javax.swing.JOptionPane;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import reportEntry.TextHelper;
import reportEntry.view.JPanelTextSection;
import reportEntry.view.JPanelTextSection.eSectionType;
import reportXMLpackage.Doc;
import reportXMLpackage.Ultype;

/**
 *
 * @author rb328
 */
public class ReportTreeSelectionListener implements TreeSelectionListener {

    private Tree tree;
    //private JPanelEditDocumentation panel;
    private JPanelMainEdit mainEditPanel;
    //private GroupLayout mainEditPanelLayout;
    //private BoxLayout mainEditPanelLayout;
    //private documentListenerTextBoxes documentlistenerTextBoxes;

    public ReportTreeSelectionListener(JPanelMainEdit mainEditPanel) {
        //this.panel = panel;
        this.mainEditPanel = mainEditPanel;
        //mainEditPanelLayout = (GroupLayout) this.mainEditPanel.getLayout();
        //mainEditPanelLayout = (BoxLayout) this.mainEditPanel.getLayout();
        //this.documentlistenerTextBoxes = documentlistenerTextBoxes;
    }

    @Override
    public void valueChanged(TreeSelectionEvent e) {
        if (!mainEditPanel.innerPanel.isShowing()) return;
        if (mainEditPanel.skipSave) {
            mainEditPanel.skipSave = false;
            return;
        }
        
        // if safe button is enabled, ask if want to save before leaving
        tree = Application.getInstance().getMainFrame().getBrowser().getContainmentTree();
        /* -- taka út á meðan sendi 
        Application.getInstance().getGUILog().log("valueChanged "
                + " oldpath " + e.getOldLeadSelectionPath() + " newpath " + e.getNewLeadSelectionPath()); */

        if (tree == null) {
            //javax.swing.JOptionPane.showMessageDialog(null, "ReportTreeSelectonListener::valueChanged::containment tree is null ");
        } else {
            if (e.getOldLeadSelectionPath() != null & mainEditPanel.getJbSave().isEnabled()) {
                int option = JOptionPane.showConfirmDialog(null, "You are changing from an element without saving your changes."
                        + "\nIf you like to save your changes choose Yes, if you like to discard your changes choose No,"
                        + "\nif you want to return to element choose Cancel.",
                        "Unsaved documentation", JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.INFORMATION_MESSAGE);
                if (option == JOptionPane.YES_OPTION) {
                    // TODO
                    // save then populate from new node
                    //populatePanel();
                    Node node = tree.getSelectedNode();
                    if (node == null) {
                        //javax.swing.JOptionPane.showMessageDialog(null, "ReportTreeSelectonListener::valueChanged::node is null - JOptionPane.YES_OPTION");
                    } else {
                        Object userObject = node.getUserObject();
                        if (userObject instanceof BaseElement) {

                            BaseElement element = (BaseElement) userObject;
                            TextHelper t = new TextHelper();
                            t.updateDocumentation(mainEditPanel, element);
                            populatePanel2();
                        }
                    }
                }
                if (option == JOptionPane.NO_OPTION) {
                    //populatePanel();
                    //Application.getInstance().getGUILog().log("NO_OPTION");
                    populatePanel2();
                }
                if (option == JOptionPane.CANCEL_OPTION) {
                    //Application.getInstance().getGUILog().log("CANCEL_OPTION");
                    tree.getTree().removeTreeSelectionListener(this);
                    tree.getTree().setSelectionPath(e.getOldLeadSelectionPath());
                    //Application.getInstance().getGUILog().log("ReportTreeSelectonListener:: changed back to"
                            //+ " oldpath " + e.getOldLeadSelectionPath() + " from newpath " + e.getNewLeadSelectionPath());
                    tree.getTree().addTreeSelectionListener(this);
                }

            } else {
                //populatePanel();
                //Application.getInstance().getGUILog().log("valueChanged default");
                populatePanel2();
            }

        }
    }

    private void populatePanel2() {

        if (tree.getSelectedNodes().length > 1) { // gera þetta fyrir mainpanel
            mainEditPanel.getInnerPanel().setVisible(false);
            //panel.getLabelElementName().setToolTipText("Only documentation for one element can be edited at same time.");
            //panel.getLabelElementName().setText("More than one element in containment tree selected.");
        } else {

            Node node = tree.getSelectedNode();

            if (node == null) {
                //javax.swing.JOptionPane.showMessageDialog(null, "ReportTreeSelectonListener::valueChanged::node is null - have to change what to do");
            } else {
                if (!mainEditPanel.getInnerPanel().isVisible()) {
                    mainEditPanel.getInnerPanel().setVisible(true);
                }

                Object userObject = node.getUserObject();
                if (userObject instanceof BaseElement) {
                    
                    //Application.getInstance().getGUILog().log("populatePanel2 normal");

                    BaseElement element = (BaseElement) userObject;
                    if ( !(element instanceof Element) ) return;
                    String documentation = ModelHelper.getComment((Element) element);
                    // taka burt listeners fyrir mainpanel 
                    //mainEditPanel.getTfIntroHeader().getDocument().removeDocumentListener(documentlistenerTextBoxes);
                    //panel.getTaIntroText().getDocument().removeDocumentListener(documentlistenerTextBoxes);
                    //mainEditPanel.clearFields();

                    if (TextHelper.checkAlreadyExists(documentation)) {
                        //Application.getInstance().getGUILog().log("populatePanel2 has documentation");
                        String strdoc = cleanDoc(documentation);

                        JAXBContext jaxbContext;
                        Doc doc = null;

                        if (!strdoc.isEmpty()) {
                            try {
                                jaxbContext = JAXBContext.newInstance(Doc.class);
                                InputStream stream = new ByteArrayInputStream(strdoc.getBytes(StandardCharsets.UTF_8));
                                Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
                                doc = (Doc) unmarshaller.unmarshal(stream);

                            } catch (JAXBException ex) {
                                Application.getInstance().getGUILog().log("ReportTreeSelectionListener:populatePanel JAXBException: " + ex.getMessage());

                            } catch (Exception e) {
                                Application.getInstance().getGUILog().log("ReportTreeSelectionListener:populatePanel Exception: " + e.getMessage());
                            }
                        }
                        //panel.setFieldsVisible();
                        mainEditPanel.fillSections(documentation, doc, element, getCompletness(doc), getSubCompletness(node));

                    } else {
                        //Application.getInstance().getGUILog().log("populatePanel2 no documentation");
                        mainEditPanel.fillSections("", null, element, getCompletness(null), getSubCompletness(node));
                        //panel.setFieldsInvisible();
                        //panel.getLabelElementName().setToolTipText("If you like to create documentation for this element, right click on it in the containment tree and select "
                        //        + "'Insert Documentation XML'");
                        //panel.getLabelElementName().setText("Documentation for " + element.getHumanName() + " does not exist.");

                    }
                }
            }
        }
    }
    
    
    private List<Completeness> getSubCompletness(Node node) {
        List<Completeness> compList = new ArrayList<Completeness>();
        Doc doc = null;        
        
        Object userObject = node.getUserObject();
        if (userObject instanceof BaseElement) {
            BaseElement element = (BaseElement) userObject;
            
            // Get doc for current element                        
            doc = getDoc(element);            
            
            Completeness compl = getCompletness(doc);
            if (compl != null)
                compList.add(compl);
        }
        
        List<Node> childNodes = node.getChildren();
        if (childNodes != null && childNodes.size() > 0) {
            for (Node n : childNodes) {
                compList.addAll(getSubCompletness(n));
            }
        }
        
        return compList;
    }
    
    private Completeness getCompletness(Doc doc) {
        int complete = 0, total = 0;
        
        //if (doc == null) return new Completeness(0, 0);
        if (doc == null) return new Completeness(0, 9);
        
        for (JPanelTextSection textSec : mainEditPanel.getSectionsList()) {
            eSectionType e;
            e = JPanelTextSection.eSectionType.valueOf(textSec.getSectionType());
            
            ListIterator<Object> listIterator = null;
            switch (e) {
                case intro:
                    if (doc.getIntro() != null) {
                        if (doc.getIntro().getH() != null && !doc.getIntro().getH().equals("") && !doc.getIntro().getH().equals(" "))
                            complete++;
                        total++;
                        listIterator = doc.getIntro().getTextgroup().listIterator();
                    }
                    break;
                case main:
                    if (doc.getMain() != null) {
                        if (doc.getMain().getH() != null && !doc.getMain().getH().equals("") && !doc.getMain().getH().equals(" "))
                            complete++;
                        total++;
                        listIterator = doc.getMain().getTextgroup().listIterator();
                    }
                    break;
                case afterword:
                    if (doc.getAfterword() != null) {
                        if (doc.getAfterword().getH() != null && !doc.getAfterword().getH().equals("") && !doc.getAfterword().getH().equals(" "))
                            complete++;
                        total++;
                        listIterator = doc.getAfterword().getTextgroup().listIterator();
                    }
                    break;
            }
            
            if (listIterator != null) {
                while (listIterator.hasNext()) {
                    Object object = listIterator.next();
                    
                    if (object.getClass().equals(String.class)) {
                        String str = (String) object;
                        if (!str.equals("") && !str.equals(" ")) {
                            complete++;
                        }
                        total++;
                    }

                    if (object.getClass().equals(Ultype.class)) {
                        Ultype ui = (Ultype) object;
                        Iterator<String> iterator = ui.getLi().iterator();
                        boolean enter = false;
                        while (iterator.hasNext()) {
                            String str = iterator.next();
                            if (!str.equals("") && !str.equals(" ")) {
                                complete++;                                
                            }
                            total++;
                            enter = true;
                        }
                        if (!enter) total++;
                    }                    
                }
            }          
            
            listIterator = null;
        }
        
        //if (complete > 0 && complete < 9 && total < 9) total = 9;
        
        return new Completeness(complete, total);
    }
    
    private Doc getDoc(BaseElement baseElement) {
        Doc doc = null;
        String documentation = ModelHelper.getComment((Element) baseElement);
        if (TextHelper.checkAlreadyExists(documentation)) {
            String strdoc = cleanDoc(documentation);

            JAXBContext jaxbContext;
            if (!strdoc.isEmpty()) {
                try {
                    jaxbContext = JAXBContext.newInstance(Doc.class);
                    InputStream stream = new ByteArrayInputStream(strdoc.getBytes(StandardCharsets.UTF_8));
                    Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
                    doc = (Doc) unmarshaller.unmarshal(stream);
                } catch (JAXBException ex) {
                }
            }
        }
        return doc;
    }
    
    public static class Completeness {

        public int complete;
        public int total;
        
        public Completeness(int comp, int tot) {
            complete = comp;
            total = tot;
        }
    
    }
}
