/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package reportEntry.controller;

import reportEntry.view.JPanelMainEdit;
import reportEntry.TextHelper;
import com.nomagic.magicdraw.core.Application;
import com.nomagic.magicdraw.ui.browser.Browser;
import com.nomagic.magicdraw.ui.browser.Node;
import com.nomagic.magicdraw.ui.browser.Tree;
import com.nomagic.magicdraw.uml.BaseElement;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 *
 * @author rb328
 */
public class ActionListenerSaveButton implements ActionListener {

    private final JPanelMainEdit panel;

    public ActionListenerSaveButton(JPanelMainEdit panel) {
        this.panel = panel;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        //Application.getInstance().getGUILog().log("enter actionPerformed of actionListenerSaveButton");
        if (panel.getJbSave().isEnabled()) {
            //javax.swing.JOptionPane.showMessageDialog(null, "save action performed: ");
            Browser browser = Application.getInstance().getMainFrame().getBrowser();

            Tree tree = browser.getContainmentTree();
            if (tree == null) {
                
                //javax.swing.JOptionPane.showMessageDialog(null, "actionListenerSaveButton::actionPerformed::containment tree is null " + e.getSource().toString());
            } else {
                Node node = tree.getSelectedNode();

                if (node == null) {
                    //javax.swing.JOptionPane.showMessageDialog(null, "actionListenerSaveButton::actionPerformed::node is null");
                } else {

                    Object userObject = node.getUserObject();
                    if (userObject instanceof BaseElement) {
                        BaseElement element = (BaseElement) userObject;
                        TextHelper t = new TextHelper();
                        t.updateDocumentation(panel, element);
                        
                        //ProjectsManager pm;
                        //pm.getActiveProject().
                    }
                }
            }
        }
    }

}
