/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package reportEntry.controller;

import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import reportEntry.view.JPanelMainEdit;

/**
 *
 * @author rb328
 */
public class DocumentListenerTextBoxes implements DocumentListener {

    private final JPanelMainEdit panel;

    public DocumentListenerTextBoxes(JPanelMainEdit panel) {
        this.panel = panel;
    }

    public void insertUpdate(DocumentEvent e) { 
        if (!panel.getJbSave().isEnabled()) {
            panel.getJbSave().setEnabled(true);
        }
    }

    public void removeUpdate(DocumentEvent e) {
        if (!panel.getJbSave().isEnabled()) {
            panel.getJbSave().setEnabled(true);
        }
    }

    public void changedUpdate(DocumentEvent e) {
        if (!panel.getJbSave().isEnabled()) {
            panel.getJbSave().setEnabled(true);
        }
    }

}
