/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package reportEntry.controller;

import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import reportEntry.view.JPanelMainEdit;

/**
 *
 * @author rb328
 */
public class TableModelListenerTextBoxes implements TableModelListener{
    
    private final JPanelMainEdit panel;

    public TableModelListenerTextBoxes(JPanelMainEdit panel) {
        this.panel = panel;
    }

    public void tableChanged(TableModelEvent e) {
                if (!panel.getJbSave().isEnabled()) {
            panel.getJbSave().setEnabled(true);
        }
    }
    
}
