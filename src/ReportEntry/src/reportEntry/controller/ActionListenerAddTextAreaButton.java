/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package reportEntry.controller;

import reportEntry.model.ListTableModel;
import reportEntry.view.JPanelTextSection;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Vector;
import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JTable;
import javax.swing.JTextArea;

public class ActionListenerAddTextAreaButton implements ActionListener {

    //private JPanel panel;
    private JPanelTextSection panel;

    public ActionListenerAddTextAreaButton(JPanelTextSection panel) {
        this.panel = panel;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        JButton b = (JButton) e.getSource();
        Dimension size = new Dimension(0, 5);
        
        if(!panel.getJPanelTextContainer().isVisible() && !b.getName().equals("removeText")) {
            panel.getJPanelTextContainer().setVisible(true);
        }
        
        if (b.getName().equals("addtext")) {
            panel.getJPanelTextContainer().add(new Box.Filler(size, size, size));
            panel.addJTextArea("");
        }
        
        else if (b.getName().equals("removeText")) {
            JTextArea area = null;
            Component filler = null;
            
            for (int i = 0; i < panel.getJPanelTextContainer().getComponents().length; i++) {
                if (panel.getJPanelTextContainer().getComponents()[i] instanceof JTextArea) {  
                    area = (JTextArea) panel.getJPanelTextContainer().getComponents()[i];
                    if (i > 0 && panel.getJPanelTextContainer().getComponents()[i-1] instanceof Box.Filler)
                        filler = panel.getJPanelTextContainer().getComponents()[i-1];                        
                }
            }
            if (area != null) {
                panel.getJPanelTextContainer().remove(area);
                panel.getTextBoxesList().remove(area);
               
                if (filler != null) {
                    panel.getJPanelTextContainer().remove(filler);
                    filler = null;
                }
                panel.getJPanelTextContainer().revalidate();
                
                if (panel.getJPanelTextContainer().getComponents().length == 0) {
                    panel.getJPanelTextContainer().setVisible(false);
                }
            }
            
        }

        else if (b.getName().equals("addlist")) {
            JTable tList = new JTable();
            panel.getJPanelTextContainer().add(new Box.Filler(size, size, size));
            
            Vector<String> rowData = new Vector<String>();
            
            final ListTableModel model = new ListTableModel(rowData);
            
            rowData.add("");
            
            tList.setModel(model);
            
            panel.addTable(tList);
            
        }

        panel.revalidate();
        panel.repaint();
    }

}
