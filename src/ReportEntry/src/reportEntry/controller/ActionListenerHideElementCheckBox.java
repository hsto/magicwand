/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package reportEntry.controller;

import reportEntry.view.JPanelMainEdit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.event.ChangeEvent;

/**
 *
 * @author rb328
 */
public class ActionListenerHideElementCheckBox implements ActionListener {

    private final JPanelMainEdit panel;

    public ActionListenerHideElementCheckBox(JPanelMainEdit panel) {
        this.panel = panel;
    }

    public void stateChanged(ChangeEvent e) {
        //Application.getInstance().getGUILog().log("actionListenerHideElementCheckBox::stateChanged");
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        
        if (panel.cbHideElement.isSelected()) {
            panel.cbHideElement.setText("Yes");
        } else {
            panel.cbHideElement.setText("No");
        }
        
        
        //Application.getInstance().getGUILog().log("actionListenerHideElementCheckBox::actionPerformed " + e.getActionCommand());
        if (!panel.getJbSave().isEnabled()) {
            panel.getJbSave().setEnabled(true);
        }
    }

}
