/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package reportEntry;

import com.nomagic.magicdraw.core.Application;
import com.nomagic.magicdraw.core.GUILog;
import com.nomagic.magicdraw.ui.browser.actions.DefaultBrowserAction;
import com.nomagic.magicdraw.uml.InheritanceVisitor;
import com.nomagic.uml2.ext.magicdraw.auxiliaryconstructs.mdmodels.Model;
import com.nomagic.uml2.ext.magicdraw.classes.mdkernel.Element;
import com.nomagic.uml2.ext.magicdraw.classes.mdkernel.Package;
import java.awt.event.ActionEvent;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

/**
 *
 * @author rb328
 */
public class BrowserActionConsistencyChecking extends DefaultBrowserAction {

    private InheritanceVisitor visitor = new InheritanceVisitor();
    private Boolean bErrorsFound; // = false;
    private Integer iErrorsFound; // = 0;       

    public BrowserActionConsistencyChecking() {
        super("CHECK_PROJECT_ACTION", "Check Model", null, null);
    }

    @Override
    public void actionPerformed(ActionEvent evt) {

        /* testing
         Project p = new Project();
         //p.getm
         MainFrame mf = new MainFrame;
         mf.

         */
        bErrorsFound = false;
        iErrorsFound = 0;
        Model model = Application.getInstance().getProject().getModel();
        GUILog logger = Application.getInstance().getGUILog();

        // lists of all possible names for each package
        List<String> alPackage1Names = Arrays.asList("1 System Parts", "System Parts", "1 System Structure", "System Structure");
        List<String> alPackage2Names = Arrays.asList("2 Information Elements", "Information Elements", "2 Information Model", "Information Model");
        List<String> alPackage3Names = Arrays.asList("3 Product Processes", "Product Processes");
        List<String> alPackage4Names = Arrays.asList("4 Business Processes", "Business Processes");

        // Application.getInstance().getProject().g
        String name = model.getHumanName();
            //String type = model.getHumanType();
        //String message = "";
        //message = message + "Name: " + name + ", type: " + type + "\n";
        if (!checkIsOwnedBy(model, "Model Overview", "Diagram")) {
            logger.log("Required top level Diagram 'Model Overview' not found in model " + name
                    + ". Please correct that and create a ‘Content Diagram’ with the name 'Model Overview' in model " + name + ".");
            foundError();
        }

            // check elements that are on a level directly under ther model, and if packages check if correct
        // check for package 1
        PackageMessage package1 = checkPackageExists(model, alPackage1Names);
        if (package1.count == 0) {
            logger.log("Required top level package not found in " + name
                    + ". Please correct that and create a package with one of the following names: "
                    + alPackage1Names.toString());
            foundError();
        } else if (package1.count > 1) {
            logger.log("Too many top level packages found in " + name
                    + " that match possible 1st package. Please correct that and include only one package with one of the following names: "
                    + alPackage1Names.toString());
            foundError();
        } else {
            if (!checkIsOwnedBy(package1.p, "Actors", "Package")) {
                logger.log("Required Package ‘Actors’ was not found in " + package1.p.getHumanName()
                        + ". Please correct that and create a Package with the name ‘Actors’ in " + package1.p.getHumanName() + ".");
                foundError();
            }
            if (!checkIsOwnedBy(package1.p, "Neighbors", "Package")) {
                logger.log("Required Package ‘Neighbors’ was not found in '" + package1.p.getHumanName()
                        + "'. Please correct that and create a Package with the name ‘Neighbors’ in " + package1.p.getHumanName() + ".");
                foundError();
            }
            if (!checkIsOwnedBy(package1.p, "", "Component")) {
                logger.log("No Components were found in " + package1.p.getHumanName()
                        + ". Please correct that and create one or more Component with the name '" + name + "' and/or other names in " + package1.p.getHumanName() + ".");
                foundError();
            }
            if (!(checkIsOwnedBy(package1.p, "System Decomposition", "Diagram") | checkIsOwnedBy(package1.p, "System Structure.*", "Diagram"))) {
                logger.log("Diagram ‘System Decomposition' or 'System Structure*' was not found in " + package1.p.getHumanName()
                        + ". Please correct that and create a ‘Composite Structure Diagram’ with the name ‘System Decomposition' or 'System Structure*' in " + package1.p.getHumanName() + ".");
                foundError();
            }
            if (!checkIsOwnedBy(package1.p, "Domain Architecture", "Diagram")) {
                logger.log("Diagram ‘Domain Architecture' was not found in " + package1.p.getHumanName()
                        + ". Please correct that and create a ‘Use Case Diagram’ with the name ‘Domain Architecture' in " + package1.p.getHumanName() + ".");
                foundError();
            }
            if (!checkIsOwnedBy(package1.p, "System Context", "Diagram")) {
                logger.log("Diagram ‘System Context' was not found in " + package1.p.getHumanName()
                        + ". Please correct that and create a ‘Use Case Diagram’ with the name ‘System Context' in " + package1.p.getHumanName() + ".");
                foundError();
            }

        }
        // check for package 2
        PackageMessage package2 = checkPackageExists(model, alPackage2Names);
        if (package2.count == 0) {
            logger.log("Required top level package not found in " + name
                    + ". Please correct that and create a package with one of the following names: "
                    + alPackage1Names.toString());
            foundError();
        } else if (package2.count > 1) {
            logger.log("Too many top level packages found in " + name
                    + " that match possible 1st package. Please correct that and include only one package with one of the following names: "
                    + alPackage1Names.toString());
            foundError();
        }

        // check for package 3
        PackageMessage package3 = checkPackageExists(model, alPackage3Names);
        if (package3.count == 0) {
            logger.log("Required top level package not found in " + name
                    + ". Please correct that and create a package with one of the following names: "
                    + alPackage1Names.toString());
            foundError();
        } else if (package3.count > 1) {
            logger.log("Too many top level packages found in " + name
                    + " that match possible 1st package. Please correct that and include only one package with one of the following names: "
                    + alPackage1Names.toString());
            foundError();
        }

        // check for package 4
        PackageMessage package4 = checkPackageExists(model, alPackage4Names);
        if (package4.count == 0) {
            logger.log("Required top level package not found in " + name
                    + ". Please correct that and create a package with one of the following names: "
                    + alPackage1Names.toString());
            foundError();
        } else if (package4.count > 1) {
            logger.log("Too many top level packages found in " + name
                    + " that match possible 1st package. Please correct that and include only one package with one of the following names: "
                    + alPackage1Names.toString());
            foundError();
        }

            //visitor.visitElement(model, null);
        //javax.swing.JOptionPane.showMessageDialog(null, "Model name: " + name + ", type: " + type);
        //javax.swing.JOptionPane.showMessageDialog(null, message);
        // ModelHelper.g
        if (!bErrorsFound) {
            logger.log("There were no errors found in " + name);
        } else {
            logger.log("\nThere were " + iErrorsFound + " errors found in " + name);
        }
    }//void

    private PackageMessage checkPackageExists(Model model, List list) {
        String message = "";
        PackageMessage pm = new PackageMessage();
        int counter = 0;
        //ReportHelper.
        Iterator itr = model.getOwnedElement().iterator();
        while (itr.hasNext()) {
            Element element = (Element) itr.next();
            if (element.getHumanType().equals("Package")) {
                Package p = (Package) element;
                ListIterator itrList = list.listIterator();
                while (itrList.hasNext()) {
                    String name = itrList.next().toString();
                    if (p.getName().equals(name)) {
                        message = p.getName();
                        pm.p = p;
                        counter++;
                    }
                }
            }
        }
        if (counter == 1) {
            pm.name = message;
        } else {
            pm.name = "";
        }
        pm.count = counter;
        return pm;
    }

        // if string name is empty, returns true if some element with required type is found,
    // else if element is found with matching name and type
    private boolean checkIsOwnedBy(Element el, String name, String type) {
        Iterator itr = el.getOwnedElement().iterator();
        while (itr.hasNext()) {
            Element element = (Element) itr.next();
            if (name.equals("") & element.getHumanType().equals(type)) {
                return true;
            } //else if( element.getHumanName().equals(name) & element.getHumanType().equals(type))
            else if (element.getHumanName().matches(element.getHumanType() + " " + name) & element.getHumanType().equals(type)) {
                return true;
            }
        }
        return false;
    }

    private void foundError() {
        bErrorsFound = true;
        iErrorsFound++;
    }

    class CheckingVisitor extends InheritanceVisitor {

    }

    class PackageMessage {

        String name;
        Integer count;
        Package p;
    }
}//class

