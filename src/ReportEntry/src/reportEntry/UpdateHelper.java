/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package reportEntry;

import com.nomagic.magicdraw.core.Application;
import com.nomagic.magicdraw.uml.BaseElement;
import com.nomagic.magicdraw.uml.InheritanceVisitor;
import com.nomagic.uml2.ext.jmi.helpers.ModelHelper;
import com.nomagic.uml2.ext.magicdraw.classes.mdkernel.Element;
import java.util.ArrayList;

/**
 *
 * @author Luai
 */
public class UpdateHelper {
    
    UpdateVisitor visitor = new UpdateVisitor();
    
    public void updateFixedString() {
        Element root = Application.getInstance().getMainFrame().getBrowser().getContainmentTree().getRootElement();
        
        ArrayList<Element> all = new ArrayList<Element>();
        all.add(root);
        Element current;
        
        for (int i = 0; i < all.size(); i++) {
            current = all.get(i);
            try {
                current.accept(visitor);
            } catch (Exception e) {
                
            }
            all.addAll(current.getOwnedElement());
        }
        
    }
    
    class UpdateVisitor extends InheritanceVisitor {
     
        TextHelper helper = new TextHelper();
        
        @Override
        public void visitBaseElement(BaseElement element) {
            try {
                super.visitBaseElement(element);
            } catch (Exception ex) {
                
            }
            
            String documentation = ModelHelper.getComment((Element) element);
            if (TextHelper.checkAlreadyExists(documentation)) {
                ModelHelper.setComment( (Element) element, TextHelper.FIXED_STRING_NAME + documentation);
            }
        }
    }
    
}
