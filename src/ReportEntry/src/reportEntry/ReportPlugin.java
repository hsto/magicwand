/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package reportEntry;

import com.nomagic.actions.AMConfigurator;
import com.nomagic.actions.ActionsCategory;
import com.nomagic.actions.ActionsManager;
import com.nomagic.magicdraw.actions.ActionsConfiguratorsManager;
import com.nomagic.magicdraw.actions.BrowserContextAMConfigurator;
import com.nomagic.magicdraw.actions.MDAction;
import com.nomagic.magicdraw.actions.MDActionsCategory;
import com.nomagic.magicdraw.core.Application;
import com.nomagic.magicdraw.ui.MainFrame;
import com.nomagic.magicdraw.ui.ProjectWindowsManager;
import com.nomagic.magicdraw.ui.browser.Tree;
import java.awt.event.ActionEvent;
import lib.MainMenuConfigurator;

/**
 *
 * @author Maggi
 */
public class ReportPlugin extends com.nomagic.magicdraw.plugins.Plugin {

    //private browseractionInsertDocumentation insertDocumentation;
    private BrowserActionClearDocumentation clearDocumentation;
    //private browseractionSetHideTrue setHideTrue;
    //private browseractionSetHideFalse setHideFalse;
    private BrowserActionConsistencyChecking consistencyChecking;
    private ReportProjectEventListener projectEventListener;
    
    @Override
    public void init() {
        //insertDocumentation = new browseractionInsertDocumentation();
        clearDocumentation = new BrowserActionClearDocumentation();
        //setHideTrue = new browseractionSetHideTrue();
        //setHideFalse = new browseractionSetHideFalse();
        consistencyChecking = new BrowserActionConsistencyChecking();
        // add action into containment tree context
        //tree = Application.getInstance().getMainFrame().getBrowser().getContainmentTree();
        //tree = Application.getInstance().getMainFrame().getBrowser().getContainmentTree();
    
        projectEventListener = new ReportProjectEventListener();
        Application.getInstance().addProjectEventListener(projectEventListener);

        ActionsConfiguratorsManager manager = ActionsConfiguratorsManager.getInstance();
        manager.addMainMenuConfigurator(new MainMenuConfigurator(new SimpleAction(null, ReportProjectEventListener.pluginName)));
        
        
        ActionsConfiguratorsManager.getInstance().addContainmentBrowserContextConfigurator(new BrowserContextAMConfigurator() {

            @Override
            public void configure(ActionsManager manager, Tree browser) {
                MDActionsCategory category = new MDActionsCategory("", "");
                //category.addAction(insertDocumentation);
                category.addAction(clearDocumentation);
                //category.addAction(setHideTrue);
                //category.addAction(setHideFalse);
                category.addAction(consistencyChecking);
                manager.addCategory(category);
            }

            @Override
            public int getPriority() {
                return AMConfigurator.MEDIUM_PRIORITY;
            }
        });

        ActionsConfiguratorsManager.getInstance().addMainToolbarConfigurator(new AMConfigurator() {

            @Override
            public void configure(ActionsManager actionsManager) {
                final ActionsCategory category = new ActionsCategory("REPORT_TOOLBAR", "Report Toolbar");
                actionsManager.addCategory(category);
                category.addAction(consistencyChecking);
                //category.addAction(new MDAction("EDIT_DOCUMENTATION_ACTION", "Edit documentation", null, null));
            }

            @Override
            public int getPriority() {
                return AMConfigurator.MEDIUM_PRIORITY;
            }
        });
    }

    @Override
    public boolean close() {
        return true;
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public boolean isSupported() {
        return true;
        //throw new UnsupportedOperationException("Not supported yet.");
    }
    
    class SimpleAction extends MDAction {
    	
    	public SimpleAction(String id, String name) {
    		super(id, name, null, null);
    	}
    	
    	@Override
    	public void actionPerformed(ActionEvent e) {
            MainFrame mainFrame = Application.getInstance().getMainFrame();
            ProjectWindowsManager projectWindowsManager = mainFrame.getProjectWindowsManager();
            if (!projectEventListener.startup) {
                projectWindowsManager.addWindow(projectEventListener.window);
                projectEventListener.startup = true;
            }
            projectWindowsManager.activateWindow(ReportProjectEventListener.projectWindowId);
    	}
    	
    }
}
