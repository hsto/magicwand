/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package reportEntry;

import reportEntry.view.JPanelMainEdit;
import com.nomagic.magicdraw.core.Application;
import com.nomagic.magicdraw.core.Project;
import com.nomagic.magicdraw.core.project.ProjectEventListener;
import com.nomagic.magicdraw.ui.MainFrame;
import com.nomagic.magicdraw.ui.ProjectWindow;
import com.nomagic.magicdraw.ui.ProjectWindowsManager;
import com.nomagic.magicdraw.ui.WindowComponentInfo;
import com.nomagic.magicdraw.ui.WindowsManager;
import com.nomagic.magicdraw.ui.browser.Tree;
import com.nomagic.magicdraw.ui.browser.WindowComponentContent;
import java.util.Scanner;
import javax.swing.JTree;
import lib.SharedHelper;

/**
 *
 * @author rb328
 */
public class ReportProjectEventListener implements ProjectEventListener {

    private Tree tree;
    private JTree jtree;
    private final JPanelMainEdit mainEditPanel = new JPanelMainEdit();
    private static String FILE_NAME = "updateFixedString.txt";
    public static String pluginName = "Prose Annotations";
    public static String projectWindowId = "";
    
    WindowComponentInfo info = new WindowComponentInfo("REPORT ENTRY", pluginName, null, WindowsManager.SIDE_EAST, WindowsManager.STATE_DOCKED, false);
    
    public boolean startup;
    public ProjectWindow window;
    
    ReportProjectEventListener()  {
        
    }

    //private static String FILE_NAME_STARTUP = "startup.txt";
    private static String FILE_NAME_STARTUP = System.getProperty("user.home") + "/.magicdraw/MagicWand/startup_Prose.txt";
    
    @Override
    public void projectOpened(Project project) {
        // get Documentation panel and 
        //Application.getInstance().getGUILog().log(project.getHumanName() + "ReportProjectEventListener::projectOpenedFromGUI");
        tree = Application.getInstance().getMainFrame().getBrowser().getContainmentTree();
        jtree = tree.getTree();
        jtree.addTreeSelectionListener(mainEditPanel.getSelectionListener());
        
        // protect documentation pane from direct editing
        Application.getInstance().getMainFrame().getBrowser().getDocPanel().getHTMLTextEditorComponent().setVisible(false);
        Application.getInstance().getMainFrame().getBrowser().getDocPanel().getHTMLEditorPane().setEnabled(false);
        Application.getInstance().getMainFrame().getBrowser().getDocPanel().getHTMLEditorPane().setVisible(false);
        Application.getInstance().getMainFrame().getBrowser().getDocPanel().setEnableHTMLCheckBox(false);
        Application.getInstance().getMainFrame().getBrowser().getDocPanel().setEnabledAll(false);



        MainFrame mainFrame = Application.getInstance().getMainFrame();
        ProjectWindowsManager projectWindowsManager = mainFrame.getProjectWindowsManager();
        
        
        WindowComponentContent content2 = new ReportEntryWindowComponentContent(tree, mainEditPanel);
        window = new ProjectWindow(info, content2);
        
        startup = SharedHelper.INSTANCE.readStartup(FILE_NAME_STARTUP);
        if (startup) {
            projectWindowsManager.addWindow(window);
            projectWindowsManager.activateWindow(window.getId());
        }
        
        projectWindowId = window.getId();
        
        updateFixedString();
    }

    @Override
    public void projectClosed(Project project) {
        
    }

    @Override
    public void projectSaved(Project project, boolean savedInServer) {
        
    }

    @Override
    public void projectDeActivated(Project project) {
        
    }

    @Override
    public void projectCreated(Project project) {
        
    }

    @Override
    public void projectOpenedFromGUI(Project project) {
        
    }
    
    private void updateFixedString() {
        
        boolean update = false;
        
        Scanner scanner = null;
        try {
            scanner = new Scanner(getClass().getClassLoader().getResourceAsStream(FILE_NAME));
        } catch(Exception e) {
            update = false;
        }
        
         while(scanner.hasNextLine()) {
             if (scanner.nextLine().equals("true")) {
                 update = true;
                 break;
             }
         }
         
         if (update) {
             UpdateHelper helper = new UpdateHelper();
             helper.updateFixedString();
         }
        
    }

    @Override
    public void projectActivatedFromGUI(Project project) {
        //Application.getInstance().getGUILog().log(project.getHumanName() + " projectActivatedFromGUI");
    }

    @Override
    public void projectActivated(Project project) {

    }

    @Override
    public void projectReplaced(Project oldProject, Project newProject) {
        
    }

    @Override
    public void projectPreClosed(Project project) {
        tree = Application.getInstance().getMainFrame().getBrowser().getContainmentTree();
		if (tree != null) {
			jtree = tree.getTree();
			if (mainEditPanel != null && jtree != null) {
                            jtree.removeTreeSelectionListener(mainEditPanel.getSelectionListener());
                        }
		}
        
        SharedHelper.INSTANCE.writeStartup(FILE_NAME_STARTUP, mainEditPanel.innerPanel);
    }

    @Override
    public void projectPreClosedFinal(Project project) {
        
    }

    @Override
    public void projectPreSaved(Project project, boolean savedInServer) {
        
    }

    @Override
    public void projectPreActivated(Project project) {
        
    }

    @Override
    public void projectPreDeActivated(Project project) {

    }

    @Override
    public void projectPreReplaced(Project oldProject, Project newProject) {

    }

}
