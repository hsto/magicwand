/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package reportEntry;

import com.nomagic.magicdraw.core.Application;
import org.xml.sax.ErrorHandler;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

/**
 *
 * @author rb328
 */
public class XmlValidationErrorHandler implements ErrorHandler {

    public void warning(SAXParseException exception) throws SAXException {
        Application.getInstance().getGUILog().log("SAXParseException WARNING " + exception.getMessage());
    }

    public void error(SAXParseException exception) throws SAXException {
        Application.getInstance().getGUILog().log("SAXParseException ERROR " + exception.getMessage());
        throw exception;
    }

    public void fatalError(SAXParseException exception) throws SAXException {
        Application.getInstance().getGUILog().log("SAXParseException FATAL ERROR " + exception.getMessage());
    }    
}
