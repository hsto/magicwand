/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package reportEntry;

import reportEntry.view.JPanelMainEdit;
import reportEntry.controller.ActionListenerSaveButton;
import com.nomagic.magicdraw.core.Application;
import com.nomagic.magicdraw.uml.BaseElement;
import com.nomagic.uml2.ext.jmi.helpers.ModelHelper;
import com.nomagic.uml2.ext.magicdraw.classes.mdkernel.Element;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.io.StringWriter;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.ListIterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.util.JAXBSource;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import reportXMLpackage.Doc;
import reportXMLpackage.Ultype;
import reportXMLpackage.ObjectFactory;

/**
 *
 * @author rb328
 */
public class TextHelper {

    public final static String FIXED_STRING_NAME = "<!-- MagicWand Prose Comment -->\n";
    
    // returns true if there exists a part in the string "doc" that matches "<doc /doc>", with anything between opening and closing
    public static boolean checkAlreadyExists(String doc) {
        Matcher matcher = null;
        try {
            Pattern pattern = Pattern.compile("(?sm)(.*)(<doc)(.*)(/doc>)(.*)");
            matcher = pattern.matcher(doc);

            if (matcher.find()) {
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            javax.swing.JOptionPane.showMessageDialog(null, "An error occurred checking if XML exist:\n"
                    + e.getMessage());
            return false;
        }
    }

    public static Boolean hideElement(String documentation) {

        String strdoc = cleanDoc(documentation);
        if (!strdoc.isEmpty()) {
            try {
                DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
                DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
                Document doc = dBuilder.parse(new InputSource(new StringReader(strdoc)));
                doc.getDocumentElement().normalize();

                NodeList nList = doc.getElementsByTagName("doc");

                if (nList.item(0) == null || nList.item(0).getNodeType() != org.w3c.dom.Node.ELEMENT_NODE) {
                    return false;
                } else {
                    org.w3c.dom.Element e = (org.w3c.dom.Element) nList.item(0);
                    if (e.getAttribute("hide").compareToIgnoreCase("true") == 0) {
                        return true;
                    } else {
                        return false;
                    }
                }
            } catch (Exception e) {
                return false;
            }
        } else {
            return false;
        }
    }

    public static String getItem(String documentation, String section, String type) {
        String strdoc = cleanDoc(documentation);
        if (!strdoc.isEmpty()) {
            try {
                DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
                DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
                Document doc = dBuilder.parse(new InputSource(new StringReader(strdoc)));
                doc.getDocumentElement().normalize();
                NodeList nList = doc.getElementsByTagName(section);
                if (nList != null && nList.getLength() > 0) {
                    if (section.equalsIgnoreCase("intro")
                            || section.equalsIgnoreCase("main")
                            || section.equalsIgnoreCase("afterword")) {
                        NodeList nList2 = nList.item(0).getChildNodes();
                        String s = "";
                        for (int i = 0; i < nList2.getLength(); i++) {
                            if (type.equalsIgnoreCase("h") && nList2.item(i).getNodeName().equalsIgnoreCase("h")) {
                                s = nList2.item(i).getTextContent();
                            }
                            if (type.equalsIgnoreCase("") && nList2.item(i).getNodeName().equalsIgnoreCase("p")) {
                                s = s + nList2.item(i).getTextContent();
                            }
                            if (type.equalsIgnoreCase("") && nList2.item(i).getNodeName().equalsIgnoreCase("ul")) {
                                NodeList nList3 = nList2.item(i).getChildNodes();
                                for (int i2 = 0; i2 < nList3.getLength(); i2++) {
                                    if (nList3.item(i2).getNodeName().equalsIgnoreCase("li") && !(nList3.item(i2).getTextContent().isEmpty())) {
                                        //int t = i2 + 1;
                                        // = s + "  " + t + ". " + nList3.item(i2).getTextContent() + "\n";
                                        s = s + nList3.item(i2).getTextContent();
                                        if (nList3.getLength() != i2 && nList3.item(i2).getTextContent() != "") {
                                            s = s + "\n";
                                        }
                                    }
                                }
                            }
                        }
                        return s;
                    } else if (section.equalsIgnoreCase("body")
                            || section.equalsIgnoreCase("table")
                            || section.equalsIgnoreCase("diagram")) {
                        org.w3c.dom.Element e = (org.w3c.dom.Element) nList.item(0);
                        return e.getAttribute("caption");
                    } else {
                        return "";
                    }
                } else {
                    return "";
                }
            } catch (Exception e) {
                return "";
            }

        } else {
            return "";
        }
    }

    public static String getItemNew(String documentation, String section, String type) {
        String strdoc = cleanDoc(documentation);

        JAXBContext jaxbContext;
        Doc doc;

        /*
         Ultype ul = objFactory.createUltype();
         ul.getLi().addAll(p);
        
         i.getTextgroup().add(ul);
         doc.setIntro(i);*/
        if (!strdoc.isEmpty()) {
            try {
                jaxbContext = JAXBContext.newInstance(Doc.class);
                InputStream stream = new ByteArrayInputStream(strdoc.getBytes(StandardCharsets.UTF_8));
                Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
                doc = (Doc) unmarshaller.unmarshal(stream);

                if (section.equalsIgnoreCase("intro")
                        || section.equalsIgnoreCase("main")
                        || section.equalsIgnoreCase("afterword")) {

                    Application.getInstance().getGUILog().log("intro header" + doc.getIntro().getH());
                    ListIterator<Object> listIterator = doc.getIntro().getTextgroup().listIterator();
                    while (listIterator.hasNext()) {
                        //Application.getInstance().getGUILog().log("textgroup: "+listIterator.next().getClass());
                        if (listIterator.next().getClass().equals(String.class)) {
                            Application.getInstance().getGUILog().log("object in textgroup is string: ");
                        }
                        if (listIterator.next().getClass().equals(Ultype.class)) {
                            Application.getInstance().getGUILog().log("object in textgroup is Ultype: ");
                        }
                    }
                    String s = "";
                    return s;
                } else if (section.equalsIgnoreCase("body")
                        || section.equalsIgnoreCase("table")
                        || section.equalsIgnoreCase("diagram")) {
                    // org.w3c.dom.Element e = (org.w3c.dom.Element) nList.item(0);
                    // return e.getAttribute("caption");
                } else {
                    return "";
                }

            } catch (JAXBException ex) {
                Logger.getLogger(TextHelper.class.getName()).log(Level.SEVERE, null, ex);
                return "";
            } catch (Exception e) {
                return "";
            }

        } else {
            return "";
        }
        return null;
    }

    // return the part of the string "doc" that is enclosed by "<doc /doc>", including the "<doc /doc>"
    public static String cleanDoc(String doc) {
        Matcher matcher = null;
        try {
            Pattern pattern = Pattern.compile("(?sm)(.*)(<doc)(.*)(/doc>)(.*)");
            matcher = pattern.matcher(doc);

            if (matcher.find()) {
                return matcher.group(2) + matcher.group(3) + matcher.group(4);
            } else {
                return "";
            }
        } catch (Exception e) {
            javax.swing.JOptionPane.showMessageDialog(null, "An error occurred removing XML:\n"
                    + e.getMessage());
            return "";
        }
    }

    // return the part of the string "doc" that is outside the part enclosed by "<doc /doc>"
    /**
     *
     * @param doc
     * @return
     */
    public static String removeDoc(String doc) {
        Matcher matcher = null;
        try {
            Pattern pattern = Pattern.compile("(?sm)(.*)(<doc)(.*)(/doc>)(.*)");
            matcher = pattern.matcher(doc);

            if (matcher.find()) {
                return matcher.group(1) + matcher.group(5);
            } else {
                return "";
            }
        } catch (Exception e) {
            javax.swing.JOptionPane.showMessageDialog(null, "An error occurred removing XML:\n"
                    + e.getMessage());
            return "";
        }
    }

    // replace the part of the string "doc" that is enclosed by "<doc /doc>" with the string "docxml"
    private static String changeDoc(String doc, String docxml) {
        Matcher matcher = null;
        try {
            Pattern pattern = Pattern.compile("(?sm)(.*)(<doc)(.*)(/doc>)(.*)");
            matcher = pattern.matcher(doc);

            if (matcher.find()) {
                return matcher.group(1) + docxml + matcher.group(5);
            } else {
                return doc;
            }
        } catch (Exception e) {
            javax.swing.JOptionPane.showMessageDialog(null, "An error occurred changing XML:\n"
                    + e.getMessage());
            return doc;
        }
    }

    public static String changeHide(String documentation, String Value) {
        // strip documentation of anything outside of "<doc /doc>"
        String strdoc = TextHelper.cleanDoc(documentation);
        if (!strdoc.isEmpty()) {
            try {
                // parse string into XML document
                DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
                DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
                Document doc = dBuilder.parse(new InputSource(new StringReader(strdoc)));
                doc.getDocumentElement().normalize();

                NodeList nList = doc.getElementsByTagName("doc");

                if (nList.item(0) == null || nList.item(0).getNodeType() != org.w3c.dom.Node.ELEMENT_NODE) {
                    return documentation;
                } //if
                else {
                    org.w3c.dom.Element e = (org.w3c.dom.Element) nList.item(0);
                    if (!e.hasAttribute("hide")) {
                        javax.swing.JOptionPane.showMessageDialog(null, "An error occurred setting hide to true:\n"
                                + "XML structure has no attribute \"hide\"");
                        return documentation;
                    } else if (e.getAttribute("hide").compareToIgnoreCase(Value) != 0) {
                        e.setAttribute("hide", Value);
                        Document newXmlDocument = DocumentBuilderFactory.newInstance().newDocumentBuilder().newDocument();
                        for (int i = 0; i < nList.getLength(); i++) {
                            org.w3c.dom.Node node = nList.item(i);
                            org.w3c.dom.Node copyNode = newXmlDocument.importNode(node, true);
                            newXmlDocument.appendChild(copyNode);
                        }
                        TransformerFactory tf = TransformerFactory.newInstance();
                        Transformer transformer = tf.newTransformer();
                        transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
                        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
                        StringWriter writer = new StringWriter();
                        transformer.transform(new DOMSource(newXmlDocument), new StreamResult(writer));
                        //String output = writer.getBuffer().toString().replaceAll("\n|\r", "");
                        String output = writer.getBuffer().toString();
                        return TextHelper.changeDoc(documentation, output);
                    }//if
                    else {
                        return documentation;
                    }//else
                }//else
            } catch (Exception e) {
                javax.swing.JOptionPane.showMessageDialog(null, "An error occurred setting hide to true:\n"
                        + e.getMessage());
                return documentation;
            }// catch
        } else {
            return documentation;
        }//else
    }//void

    public static String changeItem(String documentation, String section, String type, String newText) {
        String strdoc = TextHelper.cleanDoc(documentation);
        if (!strdoc.isEmpty()) {
            try {
                // build an XML document from the stripped documentation
                DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
                DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
                Document doc = dBuilder.parse(new InputSource(new StringReader(strdoc)));
                doc.getDocumentElement().normalize();
                NodeList nList = doc.getElementsByTagName(section);
                if (nList != null && nList.getLength() > 0) {
                    if (section.equalsIgnoreCase("intro")
                            || section.equalsIgnoreCase("main")
                            || section.equalsIgnoreCase("afterword")) {

                        NodeList nList2 = nList.item(0).getChildNodes();
                        for (int i = 0; i < nList2.getLength(); i++) {
                            // update if found the right node
                            if ((type.equalsIgnoreCase("h") && nList2.item(i).getNodeName().equalsIgnoreCase("h"))
                                    | (type.equalsIgnoreCase("p") && nList2.item(i).getNodeName().equalsIgnoreCase("p"))) {
                                nList2.item(i).setTextContent(newText);
                            }
                        }
                        TransformerFactory tf = TransformerFactory.newInstance();
                        Transformer transformer = tf.newTransformer();
                        transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
                        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
                        StringWriter writer = new StringWriter();
                        transformer.transform(new DOMSource(doc), new StreamResult(writer));
                        String output = writer.getBuffer().toString();
                        //javax.swing.JOptionPane.showMessageDialog(null, "output string: " + output);
                        return TextHelper.changeDoc(documentation, output);
                    }
                } else {
                    javax.swing.JOptionPane.showMessageDialog(null, "nList is null or empty");
                    return documentation;
                }
            } catch (Exception e) {
                javax.swing.JOptionPane.showMessageDialog(null, "exception " + e.getMessage());
                return documentation;
            }
        } else {
            javax.swing.JOptionPane.showMessageDialog(null, "stripped doc is empty");
            return documentation;
        }

        return documentation;
    }

    public static String getXML(String hide) {
        String docXML
                = "<doc hide=\"" + hide + "\" >\n"
                + "  <intro>\n"
                + "    <h></h>\n"
                + "    <p></p>\n"
                + "    <ul>\n"
                + "      <li></li>\n"
                + "    </ul>\n"
                + "  </intro>\n"
                + "  <main>\n"
                + "    <h></h>\n"
                + "    <p></p>\n"
                + "    <ul\n>"
                + "      <li></li>\n"
                + "    </ul>\n"
                + "  </main>\n"
                + "  <afterword>\n"
                + "    <h></h>\n"
                + "    <p></p>\n"
                + "    <ul>\n"
                + "      <li></li>\n"
                + "    </ul>\n"
                + "  </afterword>\n"
                + "  <body caption=\"\" />\n"
                + "  <table caption=\"\" />\n"
                + "  <diagram caption=\"\" />\n"
                + "</doc>";
        return docXML;
    }

    public static boolean validateXML(String text) {

        return false;
    }

    public void updateDocumentation(JPanelMainEdit panel, BaseElement element) {
        
        String documentation = ModelHelper.getComment((Element) element);
        //Application.getInstance().getGUILog().log("update for element: " + element.getHumanName());
        // Build XML using JAXB
        ObjectFactory objFactory = new ObjectFactory();
        Doc doc = objFactory.createDoc();
        
        /***********************************************************/
        panel.updateSections(doc, objFactory);
        /***********************************************************/
        
        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(Doc.class);
            Marshaller marshaller = jaxbContext.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            marshaller.setProperty(Marshaller.JAXB_FRAGMENT, true);
            StringWriter writer = new StringWriter();

            marshaller.marshal(doc, writer);
            String output = writer.toString();
            //Application.getInstance().getGUILog().log("update output: " + output);

            // call validator on output
            JAXBSource source = new JAXBSource(jaxbContext, doc);
            SchemaFactory sf = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
            URL url = getClass().getResource("/reportEntry/documentationXmlSchema.xsd");
            Schema schema = sf.newSchema(url);

            Validator validator = schema.newValidator();
            validator.setErrorHandler(new XmlValidationErrorHandler());
            validator.validate(source, null);
            
            //SessionManager sessionManager = SessionManager.getInstance();
            //sessionManager.createSession("insert documentation");
            
            //Application.getInstance().getGUILog().log("output " +  output + " comment "+ModelHelper.getComment((Element) element) + " docu " + documentation);
            if ( checkAlreadyExists(ModelHelper.getComment((Element) element))){
            ModelHelper.setComment((Element) element, FIXED_STRING_NAME + changeDoc(documentation, output));
            }
            else{
                ModelHelper.setComment((Element) element, FIXED_STRING_NAME + documentation + output);
            }
            //sessionManager.closeSession();

            //Application.getInstance().getGUILog().log("output from JAXB: " +  output);
        } catch (JAXBException ex) {
            Logger.getLogger(ActionListenerSaveButton.class.getName()).log(Level.SEVERE, null, ex);
            Application.getInstance().getGUILog().log("JAXBException " + ex.getMessage());
        } catch (SAXException ex) {
            Logger.getLogger(ActionListenerSaveButton.class.getName()).log(Level.SEVERE, null, ex);
            Application.getInstance().getGUILog().log("SAXException " + ex.getMessage());
        } catch (IOException ex) {
            Logger.getLogger(ActionListenerSaveButton.class.getName()).log(Level.SEVERE, null, ex);
            Application.getInstance().getGUILog().log("IOException " + ex.getMessage());
        }
        
        // TODO
        // doesn't work, have to ask in forum
        //Application.getInstance().getMainFrame().getBrowser().getDocPanel().repaint();
        /*Application.getInstance().getGUILog().log("x: "+panel.gettList().getBounds().x + " " +
                "y: "+panel.gettList().getBounds().y + " " +
                "heigth: "+panel.gettList().getBounds().height + " " +
                "width: "+panel.gettList().getBounds().width );
        //panel.getbAddLine().setBounds(x, y, width, height);
        panel.getbAddLine().setBounds(panel.gettList().getBounds().x+411+10, panel.gettList().getBounds().y+48, 20, 20);*/
        panel.getJbSave().setEnabled(false);

    }

//    public String getDocumentation(JPanelEditDocumentation panel, BaseElement element) {
//        String documentation = ModelHelper.getComment((Element) element);
//
//        // Build XML using JAXB
//        ObjectFactory objFactory = new ObjectFactory();
//        Doc doc = objFactory.createDoc();
//
//        String hide = (panel.getCbHideElement().isSelected()) ? "true" : "false";
//        doc.setHide(hide);
//
//        Doc.Intro i = objFactory.createDocIntro();
//        i.setH(panel.getTfIntroHeader().getText());
//        i.getTextgroup().add(panel.getTaIntroText().getText());
//
//        ArrayList<String> p = new ArrayList<String>();
//        p.add("aaa");
//        p.add("bbb");
//        p.add("ccc");
//
//        //i.getTextgroup().add(p);
//        Ultype ul = objFactory.createUltype();
//        ul.getLi().addAll(p);
//
//        i.getTextgroup().add(ul);
//        doc.setIntro(i);
//
//        try {
//            JAXBContext jaxbContext = JAXBContext.newInstance(Doc.class);
//            Marshaller marshaller = jaxbContext.createMarshaller();
//            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
//            marshaller.setProperty(Marshaller.JAXB_FRAGMENT, true);
//            StringWriter writer = new StringWriter();
//
//            marshaller.marshal(doc, writer);
//            String output = writer.toString();
//
//            // call validator on output
//            JAXBSource source = new JAXBSource(jaxbContext, doc);
//            SchemaFactory sf = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
//            URL url = getClass().getResource("/ReportPluginPackage/documentationXmlSchema.xsd");
//            Schema schema = sf.newSchema(url);
//
//            Validator validator = schema.newValidator();
//            validator.setErrorHandler(new XmlValidationErrorHandler());
//            validator.validate(source, null);
//
//            ModelHelper.setComment((Element) element, changeDoc(documentation, output));
//
//            //Application.getInstance().getGUILog().log("output from JAXB: " +  output);
//        } catch (JAXBException ex) {
//            Logger.getLogger(ActionListenerSaveButton.class.getName()).log(Level.SEVERE, null, ex);
//            Application.getInstance().getGUILog().log("JAXBException " + ex.getMessage());
//        } catch (SAXException ex) {
//            Logger.getLogger(ActionListenerSaveButton.class.getName()).log(Level.SEVERE, null, ex);
//            Application.getInstance().getGUILog().log("SAXException " + ex.getMessage());
//        } catch (IOException ex) {
//            Logger.getLogger(ActionListenerSaveButton.class.getName()).log(Level.SEVERE, null, ex);
//            Application.getInstance().getGUILog().log("IOException " + ex.getMessage());
//        }
//
//        //Application.getInstance().getMainFrame().getBrowser().getDocPanel().repaint();
//        panel.getbSave().setEnabled(false);
//        return null;
//
//    }

}
