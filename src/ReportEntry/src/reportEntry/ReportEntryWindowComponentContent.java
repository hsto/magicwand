/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package reportEntry;

import java.awt.Component;

import reportEntry.view.JPanelMainEdit;

import com.nomagic.magicdraw.ui.browser.Tree;
import com.nomagic.magicdraw.ui.browser.WindowComponentContent;

/**
 *
 * @author rb328
 */
public class ReportEntryWindowComponentContent implements WindowComponentContent {

    private final Tree tree;
    //private JTree jtree;
    //private JPanelSection panel;
    private final JPanelMainEdit panel;
    //private JScrollPaneMainEdit panel;

    //public editWindowComponentContent2(Tree tree, JScrollPaneMainEdit panel) {
    public ReportEntryWindowComponentContent(Tree tree, JPanelMainEdit panel) {
        this.tree = tree;
        this.panel = panel;
    }

    @Override
    public Component getWindowComponent() {
        //Application.getInstance().getGUILog().log("enter getWindowComponent2");
        return panel;
    }

    @Override
    public Component getDefaultFocusComponent() {
        //Application.getInstance().getGUILog().log("enter getDefaultFocusComponent");
        return panel;
    }

}
