/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package reportEntry;

import com.nomagic.magicdraw.openapi.uml.SessionManager;
import com.nomagic.magicdraw.ui.browser.Node;
import com.nomagic.magicdraw.ui.browser.Tree;
import com.nomagic.magicdraw.ui.browser.actions.DefaultBrowserAction;
import com.nomagic.magicdraw.uml.BaseElement;
import com.nomagic.uml2.ext.jmi.helpers.ModelHelper;
import com.nomagic.uml2.ext.magicdraw.classes.mdkernel.Element;
import java.awt.event.ActionEvent;

/**
 *
 * @author rb328
 */
public class BrowserActionClearDocumentation extends DefaultBrowserAction {

    public BrowserActionClearDocumentation() {
        super("", "Clear Documentation", null, null);
    }

    @Override
    public void actionPerformed(ActionEvent evt) {
        SessionManager sessionManager = SessionManager.getInstance();
        sessionManager.createSession("clear documentation");
        if (sessionManager.isSessionCreated()) {
            Tree tree = getTree();
            for (int i = 0; i < tree.getSelectedNodes().length; i++) {
                Node node = tree.getSelectedNodes()[i];
                Object userObject = node.getUserObject();
                if (userObject instanceof BaseElement) {
                    BaseElement element = (BaseElement) userObject;
                    String documentation = ModelHelper.getComment((Element) element);
                    if (TextHelper.checkAlreadyExists(documentation)) {
                        ModelHelper.setComment((Element) element, TextHelper.removeDoc(documentation));
                    }
                }
            }
        } else {
            javax.swing.JOptionPane.showMessageDialog(null, "Couldn't create a session");
        }
        sessionManager.closeSession();
    }

    /**
     * Updates menu item.
     */
    @Override
    public void updateState() {
        setEnabled(getSelectedObject() instanceof BaseElement);
    }
}
