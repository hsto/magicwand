/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package reportEntry;

import com.nomagic.magicdraw.tests.MagicDrawTestCase;
import junit.framework.Test;
import junit.framework.TestSuite;
// import org.junit.Test;

/**
 *
 * @author rb328
 */
public class MDReportPluginTest extends MagicDrawTestCase {

    public MDReportPluginTest() {
    }

    public MDReportPluginTest(String method) {
        super(method);
    }

    @Override
    protected void tearDownTest() throws Exception {
        super.tearDownTest(); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected void setUpTest() throws Exception {
        System.out.println("set-up" );
        super.setMemoryTestReady(false);
        super.setUpTest(); //To change body of generated methods, choose Tools | Templates.
        
    }

    public MDReportPluginTest(String method, String testName) {
        super(method, testName);
    }

    public void testSomething() {
        System.out.println("the test" );
        //implement unit test here
    }

    public static Test suite() throws Exception {
//you may create test suite with several instances of test.
        TestSuite suite = new TestSuite();
        
        suite.addTest(new MDReportPluginTest("testSomething", "MagicDraw Test Sample"));
        suite.addTest(new MDReportPluginTest("testSomething", "Another MagicDraw TestSample"));
        return suite;
    }
    
}

