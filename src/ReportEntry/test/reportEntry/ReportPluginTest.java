/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package reportEntry;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import com.nomagic.magicdraw.core.Application;
import com.nomagic.magicdraw.core.Project;
import com.nomagic.magicdraw.core.project.ProjectDescriptor;
import com.nomagic.magicdraw.core.project.ProjectDescriptorsFactory;
import com.nomagic.magicdraw.core.project.ProjectsManager;
import com.nomagic.runtime.ApplicationExitedException;
import com.nomagic.task.ProgressStatus;
import com.nomagic.task.SimpleProgressStatus;

import com.nomagic.uml2.ext.jmi.helpers.ModelHelper;
import com.nomagic.uml2.ext.magicdraw.auxiliaryconstructs.mdmodels.Model;

import com.nomagic.magicdraw.tests.MagicDrawTestCase;
import java.io.File;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
/**
 *
 * @author Maggi
 */
public class ReportPluginTest {

    public ReportPluginTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of init method, of class ReportPlugin.
     */

    /**
     * Test of changeHide method, of class ReportPlugin.
     */
    @Test
    public void testChangeHide() {
        System.out.println("changeHide");
        String documentation = "<doc hide=\"true\"></doc>";
        //ReportPlugin instance = new ReportPlugin();
        String expResult = "<doc hide=\"true\" ></doc>";
        String result = TextHelper.changeHide(documentation, "false");
        System.out.println("result:\n " + result);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }    @Test
    public void testCleanDoc() {
        System.out.println("cleanDoc");
        String documentation = "sdfa aasdf asf a<doc hide=\"true\"></doc>";
        ReportPlugin instance = new ReportPlugin();
        String expResult = "<doc hide=\"true\"></doc>";
        String result = "";//instance.cleanDoc(documentation);
        System.out.println("result:\n " + result);
        System.out.println("expected:\n " + expResult);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }
       /**
     * Test checkModel method, of class ReportPlugin.
     */
    @Test
    public void testCheck() {
        System.out.println("check");
        // String documentation = "<doc hide=\"true\"></doc>";
        //ReportPlugin instance = new ReportPlugin();
        //String expResult = "<doc hide=\"true\" ></doc>";
        //String result = instance.changeHide(documentation, "false");
        //Application app = new Application();
        //ModelHelper mh = new ModelHelper();
       ProjectsManager projectsManager = null;
        try {
            System.out.println("inside try 1 " );
            String[] s = new String[0];
            Application.getInstance().start(true, true, false, s, null);
            System.out.println("inside try 2 " );
            System.out.println("can create: " + Application.getInstance().isCanCreateNewProject());
            //Application.getInstance().
            System.out.println("inside try 3 " );
            // 1 projectsManager = Application.getInstance().getProjectsManager();
         } catch (ApplicationExitedException ex) {
            System.out.println("app fail: " + ex.getMessage());
            //Logger.getLogger(ReportPluginTest.class.getName()).log(Level.SEVERE, null, ex);
        }

        // 2 Project project = null;
        //Project project = projectsManager.createProject();

        //ProjectDescriptor descriptor = ProjectDescriptorsFactory.createProjectDescriptor(new File("<C:\\mdpro").toURI());
        // 3 ProjectDescriptor descriptor = ProjectDescriptorsFactory.createProjectDescriptor(new File("C:\\mdpro\\s3 analysis model sample [lms_2011] + annotations.mdxml").toURI());
        //ProjectDescriptor descriptor = ProjectDescriptorsFactory.createProjectDescriptor(new File("<project file path>").toURI());
        
        //System.out.println(descriptor.toString());
        // 4 System.out.println("name: " + descriptor.getRepresentationString());
        
        SimpleProgressStatus ps = new SimpleProgressStatus() {};
        
        
        //ProgressStatus ps = new ProgressStatus();
        try {
            Boolean l;
            //projectsManager.loadProject(descriptor,ps); 
            //projectsManager.importModule(project, descriptor);
            // 5 l = projectsManager.importProject(descriptor);
            //System.out.println("l: " + l);
        } catch (Exception e) {
            System.out.println("fail: " + e.toString());
        }
        
        //project = projectsManager.getActiveProject();
        
        Iterator itr = projectsManager.getProjects().iterator();
  
        
            while (itr.hasNext()) {
                //Object element = itr.next();
                Project p = (Project) itr.next();
                System.out.println("project name: " + p.getHumanName());      
            }
        
 
        
          
        System.out.println("result:\n " );
        //assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

}